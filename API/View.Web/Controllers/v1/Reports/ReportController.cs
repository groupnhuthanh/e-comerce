﻿using BusinessLogicLayer.BusinessLogic.Wrapper;
using Cores.SupportCustom.Body;
using Entities.ExtendModels.Reports;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using View.App.Controllers.Base;

namespace View.App.Controllers.v1.Reports;

[Route("api/v1/[controller]")]
[ApiController]
public class ReportController : BaseController
{

    readonly BusinessLogicWrapper _businessLogicWrapper;
    public ReportController(BusinessLogicWrapper businessLogicWrapper)
    {
        _businessLogicWrapper = businessLogicWrapper;
    }
    [HttpPost]
    [AllowAnonymous]

    public async Task<IActionResult> ReportPage()
    {
        
        var result = new ReportResponseModel();

        //{"TypeReport":"PaymentMethods","StartDate":"2023-03-25","EndDate":"2023-03-25", "Locations": [], "Devices": [],"CashDrawers": [],"UserGuid":"Customer/9"}
        var doc = await Request.GetRawBodyAsync<Dictionary<string, object>>();
        if (doc != null)
        {
            result = await _businessLogicWrapper.reportBLL.ReportBase(doc);
            return Ok(result);
        }
        return Ok(result);
    }
}
