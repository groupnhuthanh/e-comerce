﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace View.Web.Controllers.v1.PetStore.Auth
{
    [Route("api/[controller]")]
    [ApiController]
    public class SignInController : ControllerBase
    {
        [HttpPost]
        [AllowAnonymous]
        [Route("Answer")]
        public async Task<IActionResult> GetResult(string prompt)
        {
            return Ok(prompt);
        }
    }
}
