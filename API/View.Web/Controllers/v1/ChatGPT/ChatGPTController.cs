﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenAI_API;
using OpenAI_API.Completions;
using View.App.Controllers.Base;

namespace View.App.Controllers.v1.ChatGPT;

[Route("api/v1/[controller]")]
[ApiController]
public class ChatGPTController : BaseController
{

    [HttpPost]
    [AllowAnonymous]
    [Route("Answer")]
    public async Task<IActionResult> GetResult(string prompt)
    {
        //your OpenAI API key
        string apiKey = "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";
        string answer = string.Empty;
        var openai = new OpenAIAPI(apiKey);
        CompletionRequest completion = new CompletionRequest();
        completion.Prompt = prompt;
        completion.Model = OpenAI_API.Models.Model.DavinciText;
        completion.MaxTokens = 4000;
        var result = await openai.Completions.CreateCompletionAsync(completion);
        if (result != null)
        {
            foreach (var item in result.Completions)
            {
                answer = item.Text;
                //await Response.WriteAsync(answer);
            }
            return Ok(answer);
        }
        else
        {
            return BadRequest("Not found");
        }
    }
}
