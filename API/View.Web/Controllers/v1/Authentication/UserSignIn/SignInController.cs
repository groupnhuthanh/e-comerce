﻿

using BusinessLogicLayer.BusinessLogic.Wrapper;
using Cores.SupportCustom.Body;
using Entities.ExtendModels.Authentication.Users;
using Entities.ExtendModels.NullDySession;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using View.App.Controllers.Base;

namespace View.App.Controllers.v1.Authentication.UserSignIn
{
    [Route("api/v1/authentication")]
    [ApiController]
    public class SignInController : BaseController
    {

        readonly BusinessLogicWrapper _businessLogicWrapper;
        public SignInController(BusinessLogicWrapper businessLogicWrapper)
        {
            _businessLogicWrapper = businessLogicWrapper;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("SignIn")]
        public async Task<IActionResult> SignIn()
        {
            var result = new SignIn();
            var authentication = await _businessLogicWrapper.TokenSecurityManager.TokenHMACValidate(HttpContext, true);
            if (!authentication.errorCode.Equals("200"))
            {
                //dicMessage.Add("message", authentication.message?? "Unauthorized");
                if (authentication.errorCode.Equals("2140"))
                {
                    return BadRequest(authentication);
                }
                return Unauthorized(authentication);
            }
            var dicToken = NullDySession.TokenUser;
            if (dicToken != null)
            {
                var doc = await Request.GetRawBodyAsync<Dictionary<string, object>>();
                if (doc != null)
                {
                    result = await _businessLogicWrapper.authenticationBLL.SignIn(dicToken,doc);
                }


            }


            return Ok(result);
        }


    }
}
