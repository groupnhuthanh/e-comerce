﻿using BusinessLogicLayer.BusinessLogic.Wrapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using View.App.Controllers.Base;

namespace View.App.Controllers.v1.Authentication.Captcha;

[Route("api/v1/authentication/[controller]")]
[ApiController]
public class CaptchaController : BaseController
{
    readonly BusinessLogicWrapper _businessLogicWrapper;
    public CaptchaController(BusinessLogicWrapper businessLogicWrapper)
    {
        _businessLogicWrapper = businessLogicWrapper;
    }
    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> Get(string nftToken_id)
    {
        //var authentication = await _businessWrapper.TokenSecurityBal.TokenHMACValidate(HttpContext, true);
        //if (!authentication.errorCode.Equals("200"))
        //{
        //    //dicMessage.Add("message", authentication.message?? "Unauthorized");
        //    if (authentication.errorCode.Equals("2140"))
        //    {
        //        return BadRequest(authentication);
        //    }
        //    return Unauthorized(authentication);
        //}
        var result = await _businessLogicWrapper.captchaCoreBLL.Get(nftToken_id, HttpContext);
        if (result != null && result.Status.Equals(true))
        {
            return Ok(result.Model);
        }
        var message = result?.Message;
        var dic = new Dictionary<string, string> {
                {"message",message }
            };
        return BadRequest(dic);
    }
}
