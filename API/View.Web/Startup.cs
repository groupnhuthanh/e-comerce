using Cores.Register;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace View.App;

public class Startup
{
    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
  
    public IConfiguration Configuration { get; }
    public IWebHostEnvironment Env { get; }
    /// <summary>
    /// Initializes a new instance of the <see cref="Startup"/> class.
    /// </summary>
    /// <param name="configuration">The configuration.</param>
    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
        Configuration = configuration;
        Env = env;
    }
    public void ConfigureServices(IServiceCollection services)
    {
        services.RegisterConfiguration(Configuration, Env);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IAntiforgery antiforgery, IApiVersionDescriptionProvider apiVersion)
    {
        app.RegisterApplication(env, antiforgery, apiVersion, Configuration);
    }
}