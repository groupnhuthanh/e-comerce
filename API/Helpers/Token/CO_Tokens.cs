using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace Helpers.Token;

public static class CO_Tokens
{
    static HMACSHA256 hmac = new HMACSHA256();
    // private static string Secret = "XCAP05H6LoKvbRRa/QkqLNMI7cOHguaRyHzyg7n5qEkGjQmtBhz4SzYh4Fqwjyi3KJHlSXKPwVu2+bXr6CtpgQ==";//lived
    private static string Secret = "J1f66U54BWikTPIIx3NOsqcHfP31QEkgFgybVQFgfH0QN+JlXX72eVN5ngb+MmNWHErCEi3yRQTvq9n5sca33Q==";//staging
    static string _key = System.Convert.ToBase64String(hmac.Key);
    public static string GenerateToken(string UserGuid, string EmployeeGuid, string LocationGuid, string DeviceGuid = "", int DepartmentId = 0, int EmployeeType = 1)
    {
        byte[] key = System.Convert.FromBase64String(Secret);
        var securityKey = new SymmetricSecurityKey(key);
        var ExpiredDate = DateTime.UtcNow.AddDays(1);
        switch (EmployeeType)
        {
            case 2:
                ExpiredDate = DateTime.UtcNow.AddMonths(6);
                break;
        }
        var descriptor = new SecurityTokenDescriptor
        {
            //Subject = new ClaimsIdentity(new[] {
            //    new Claim(ClaimTypes.Name, EmployeeGuid)
            //    ,new Claim(ClaimTypes.Locality, LocationGuid)
            //    ,new Claim(ClaimTypes.Expired, ExpiredDate.ToString())
            //}),
            //Subject = new ClaimsIdentity(new[]
            //{
            //    //new Claim("UserGuid", UserGuid),
            //    new Claim("EmployeeGuid", EmployeeGuid),
            //    new Claim("LocationGuid", LocationGuid),
            //    //new Claim("DepartmentId", DepartmentId.ToString()),
            //    new Claim("BeforeExpired",DateTime.UtcNow.AddMinutes(-20).ToString()),
            //    new Claim("Expired", DateTime.UtcNow.AddDays(1).ToString())
            //}),

            Subject = new ClaimsIdentity(new[]
            {
                new Claim("EmployeeGuid", EmployeeGuid),
                new Claim("LocationGuid", LocationGuid),
                new Claim("BeforeExpired", ExpiredDate.AddMinutes(-20).ToString()),
                new Claim("Expired", ExpiredDate.ToString()),
                //start v2
                new Claim("UserGuid", UserGuid),
                new Claim("DeviceGuid", DeviceGuid),
                new Claim("DepartmentId", DepartmentId.ToString()),
                new Claim("EmployeeType", EmployeeType.ToString())
                // end v2
            }),
            Expires = ExpiredDate,
            SigningCredentials = new SigningCredentials(securityKey,
                SecurityAlgorithms.HmacSha256Signature)
        };

        var handler = new JwtSecurityTokenHandler();
        var token = handler.CreateJwtSecurityToken(descriptor);
        return handler.WriteToken(token);
    }

    public static Dictionary<string, object> ValidateToken(string token)
    {
        var dic = new Dictionary<string, object>();
        var principal = GetPrincipal(token);
        if (principal == null)
            return null;
        ClaimsIdentity identity = null;
        try
        {
            identity = (ClaimsIdentity)principal.Identity;
        }
        catch (NullReferenceException)
        {
            return null;
        }

        var EmployeeGuid = identity.FindFirst("EmployeeGuid").Value;
        var LocationGuid = identity.FindFirst("LocationGuid").Value;
        var BeforeExpired = identity.FindFirst("BeforeExpired").Value;
        var Expired = identity.FindFirst("Expired").Value;
        //v2
        var UserGuid = identity.FindFirst("UserGuid").Value;
        var DeviceGuid = identity.FindFirst("DeviceGuid").Value;
        var DepartmentId = identity.FindFirst("DepartmentId").Value;
        var EmployeeType = identity.FindFirst("EmployeeType").Value;
        dic = new Dictionary<string, object>
        {
            {"EmployeeGuid",EmployeeGuid}
            ,{"LocationGuid",LocationGuid}
            ,{"BeforeExpired",BeforeExpired}
            ,{"Expired",Expired}
            //start v2
            ,{"UserGuid",UserGuid}
            ,{"DeviceGuid",DeviceGuid}
            ,{"DepartmentId",DepartmentId}
            ,{"EmployeeType",EmployeeType}
            //  end v2
        };
        return dic;
    }

    public static ClaimsPrincipal GetPrincipal(string token)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
            if (jwtToken == null)
                return null;
            byte[] key = System.Convert.FromBase64String(Secret);
            var parameters = new TokenValidationParameters()
            {
                RequireExpirationTime = true,
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = new SymmetricSecurityKey(key)
            };
            var principal = tokenHandler.ValidateToken(token,
                parameters, out _);
            return principal;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}