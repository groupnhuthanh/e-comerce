using Entities.ExtendModels.Authentication.Tokens;
using Microsoft.IdentityModel.Tokens;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
namespace Helpers.Token;

public static class LibsToken
{
    static HMACSHA256 hmac = new HMACSHA256();
    //private static string Secret = "XCAP05H6LoKvbRRa/QkqLNMI7cOHguaRyHzyg7n5qEkGjQmtBhz4SzYh4Fqwjyi3KJHlSXKPwVu2+bXr6CtpgQ==";
    private static string Secret = "J1f66U54BWikTPIIx3NOsqcHfP31QEkgFgybVQFgfH0QN+JlXX72eVN5ngb+MmNWHErCEi3yRQTvq9n5sca33Q==";
    static string _key = System.Convert.ToBase64String(hmac.Key);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="domain">domain audit</param>
    /// <param name="UserGuid">User account</param>
    /// <param name="DeviceGuid">Mã thiết bị</param>
    /// <param name="LocationGuid">Location của thiết bị</param>
    /// <param name="EmployeeGuid">Mã nhân viên</param>
    /// <param name="CustomerGuid">Mã khách hàng</param>
    /// <param name="keyDevice">code thiết bị</param>
    /// <param name="expiredDate">Ngày hết hạn</param>
    /// <param name="DeviceType">Loại thiết bị</param>
    /// <param name="expired">Estimate time</param>
    /// <returns></returns>
    public static async Task<string> GenerateToken(string domain, string UserGuid, string DeviceGuid, string LocationGuid
        , string EmployeeGuid, string CustomerGuid = ""
        , string keyDevice = ""
        , string expiredDate = ""
        , int DeviceType = 1
        , double expired = 0, string token_id = "")
    {
        CultureInfo iv = CultureInfo.InvariantCulture;
        byte[] key = System.Convert.FromBase64String(Secret);
        // byte[] key = Encoding.ASCII.GetBytes(Secret);
        var securityKey = new SymmetricSecurityKey(key);
        //var expiredDateSession = !string.IsNullOrEmpty(expiredDate) ? DateTime.Parse(expiredDate) : DateTime.UtcNow.AddHours(7);
        var expiredDateSession = DateTime.UtcNow;
        var ExpiredDate = expiredDateSession.AddMilliseconds(expired);
        var descriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("UserGuid", UserGuid),
                new Claim("DeviceGuid", DeviceGuid),
                new Claim("LocationGuid", LocationGuid),
                new Claim("KeyDevice", keyDevice),
                new Claim("DeviceType", DeviceType.ToString()),
                new Claim("EmployeeGuid", EmployeeGuid.ToString()),
                new Claim("CustomerGuid", CustomerGuid.ToString()),
                new Claim("Token_id", token_id)//Mã token
            }),
            Expires = ExpiredDate,
            // Expires = DateTime.Now.AddMinutes(1),
            Issuer = domain,
            Audience = domain,
            NotBefore = expiredDateSession,
            SigningCredentials = new SigningCredentials(securityKey,
                SecurityAlgorithms.HmacSha256Signature)
        };

        var handler = new JwtSecurityTokenHandler();
        var token = handler.CreateJwtSecurityToken(descriptor);
        return handler.WriteToken(token);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="domain">domain audit</param>
    /// <param name="UserGuid">User account</param>
    /// <param name="DeviceGuid">Mã thiết bị</param>
    /// <param name="LocationGuid">Location của thiết bị</param>
    /// <param name="EmployeeGuid">Mã nhân viên</param>
    /// <param name="CustomerGuid">Mã khách hàng</param>
    /// <param name="keyDevice">code thiết bị</param>
    /// <param name="expiredDate">Ngày hết hạn</param>
    /// <param name="DeviceType">Loại thiết bị</param>
    /// <param name="expired">Estimate time</param>
    /// <returns></returns>
    public static async Task<string> RefreshToken(string domain, double expired = 0, string token_id = "")
    {
        CultureInfo iv = CultureInfo.InvariantCulture;
        byte[] key = System.Convert.FromBase64String(Secret);
        var securityKey = new SymmetricSecurityKey(key);
        var expiredDateSession = DateTime.UtcNow;
        //var expiredDateSession = DateTime.UtcNow;
        var ExpiredDate = expiredDateSession.AddMilliseconds(expired);
        // var ExpiredDate = expiredDateSession.AddMinutes(2);
        var descriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("Token_id", token_id)
            }),
            Expires = ExpiredDate,
            Issuer = domain,
            Audience = domain,
            NotBefore = expiredDateSession,
            SigningCredentials = new SigningCredentials(securityKey,
                SecurityAlgorithms.HmacSha256Signature)
        };

        var handler = new JwtSecurityTokenHandler();
        var token = handler.CreateJwtSecurityToken(descriptor);
        return handler.WriteToken(token);
    }

    /// <summary>
    /// Validated Token
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    public static async Task<AccessTokenModel> ValidateToken(string token, string domain_api = "")
    {
        var accessTokenModel = new AccessTokenModel();
        var principal = await GetPrincipal(token, domain_api);
        if (principal == null)
            return null;
        ClaimsIdentity identity = null;
        try
        {
            identity = (ClaimsIdentity)principal.Identity;
        }
        catch (NullReferenceException)
        {
            return null;
        }

        if (identity != null)
        {
            var UserGuid = identity.FindFirst("UserGuid")?.Value;
            var DeviceGuid = identity.FindFirst("DeviceGuid")?.Value;
            var LocationGuid = identity.FindFirst("LocationGuid")?.Value;
            var EmployeeGuid = identity.FindFirst("EmployeeGuid")?.Value;
            var CustomerGuid = identity.FindFirst("CustomerGuid")?.Value;
            var KeyDevice = identity.FindFirst("KeyDevice")?.Value;
            var DeviceType = identity.FindFirst("DeviceType")?.Value;
            var Expired = identity.FindFirst("Expired")?.Value;
            var token_id = identity.FindFirst("Token_id")?.Value;
            // dic = new Dictionary<string, object>
            // {
            //     {"UserGuid", UserGuid},
            //     {"DeviceGuid", DeviceGuid},
            //     {"LocationGuid", LocationGuid},
            //     {"KeyDevice", KeyDevice},
            //     {"DeviceType", DeviceType},
            //     {"CustomerGuid", CustomerGuid},
            //     {"EmployeeGuid", EmployeeGuid},
            //     {"Expired", Expired},
            //     {"Token_id", token_id}
            // };

            accessTokenModel = new AccessTokenModel()
            {
                UserGuid = UserGuid,
                DeviceGuid = DeviceGuid,
                LocationGuid = LocationGuid,
                // KeyDevice = KeyDevice,
                // DeviceType = DeviceType,
                CustomerGuid = CustomerGuid,
                // EmployeeGuid = EmployeeGuid,
                // ExpiredDate = (DateTime)Expired,
                // token_id = token_id,
            };
        }
        return accessTokenModel;
    }

    public static async Task<ClaimsPrincipal> GetPrincipal(string token, string domain_api = "")
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
            if (jwtToken == null)
                return null;
            byte[] key = System.Convert.FromBase64String(Secret);
            var parameters = new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = domain_api,
                ValidateAudience = true,
                ValidAudience = domain_api,
                ValidateLifetime = true,
                RequireExpirationTime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key)
            };
            var principal = tokenHandler.ValidateToken(token, parameters, out _);
            return principal;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static async Task<JwtPayload> Verifyfirebase(string token)
    {
        var jwtPayload = new JwtPayload();
        if (token == "") return jwtPayload;
        var tokenHandler = new JwtSecurityTokenHandler();
        tokenHandler.InboundClaimTypeMap.Clear();
        SecurityToken securityToken = null;

        try
        {
            securityToken = tokenHandler.ReadJwtToken(token) ?? null;
        }
        catch (Exception)
        {
        }
        var jwtToken = (JwtSecurityToken)securityToken;
        if (jwtToken != null)
        {
            jwtPayload = jwtToken?.Payload ?? null;

        }
        return jwtPayload;
    }
}