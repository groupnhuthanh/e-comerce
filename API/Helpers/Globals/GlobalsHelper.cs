﻿namespace Helpers.Globals;

public static class GlobalsHelper
{
    public const int TokenExpireIn = 6;//Minutes;
    public const int ExpireType = 2;//1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours
    public const int TokenRefreshExpireIn = 7;
    public const int RefreshTokenExpireType = 1;//1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours
    public const int SystemTokenExpireIn = 3;//Minutes;//test
    public const int SystemExpireType = 1;//1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours//test
    public const int SystemTokenRefreshExpireIn = 15;
    public const int SystemRefreshTokenExpireType = 1;//1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours

    //test
    //public const int TokenExpireIn = 10;//Minutes;
    //public const int ExpireType = 4;//1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours//test
    //public const int TokenRefreshExpireIn = 30;
    //public const int RefreshTokenExpireType = 4;//1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours
    //public const int SystemTokenExpireIn = 10;//Minutes;//test
    //public const int SystemExpireType = 4;//1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours//test
    //public const int SystemTokenRefreshExpireIn = 30;
    //public const int SystemRefreshTokenExpireType = 4;//1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours
}
