using System.Globalization;

namespace Helpers.DelimiterFormatter;

public static class Lib_CurrencyDelimiterFormatter
{
    private static readonly CultureInfo UsCultureInfo = CultureInfo.CreateSpecificCulture("en-US");

    public static string ToStringUsDigitsFormatting(
       this double number,
        DigitsFormattingSettings digitsFormattingSettings = DigitsFormattingSettings.None,
        int precesion = 2, string symbol = "$")
    {
        string result = ToStringUsDigitsFormattingInternal(number, digitsFormattingSettings, precesion, symbol);

        return result;
    }

    public static string ToStringUsDigitsFormatting(this decimal number, DigitsFormattingSettings digitsFormattingSettings = DigitsFormattingSettings.None, int precesion = 2, string symbol = "$")
    {
        string result = ToStringUsDigitsFormattingInternal(number, digitsFormattingSettings, precesion, symbol);

        return result;
    }

    private static string ToStringUsDigitsFormattingInternal(dynamic number, DigitsFormattingSettings digitsFormattingSettings = DigitsFormattingSettings.None, int precesion = 2, string symbol = "$")
    {
        string formattedDigits = string.Empty;
        string currentNoComaFormatSpecifier = string.Concat("#.", new string('0', precesion));
        string currentComaFormatSpecifier = string.Concat("##,#.", new string('0', precesion));
        formattedDigits =
                            digitsFormattingSettings.HasFlag(DigitsFormattingSettings.NoComma) ? number.ToString(currentNoComaFormatSpecifier, UsCultureInfo) :
                            number.ToString(currentComaFormatSpecifier, UsCultureInfo);
        if (digitsFormattingSettings.HasFlag(DigitsFormattingSettings.PrefixDollar))
        {
            formattedDigits = string.Concat(symbol, formattedDigits);
        }
        if (digitsFormattingSettings.HasFlag(DigitsFormattingSettings.PrefixMinus))
        {
            formattedDigits = string.Concat("-", formattedDigits);
        }
        if (digitsFormattingSettings.HasFlag(DigitsFormattingSettings.SufixDollar))
        {
            formattedDigits = string.Concat(formattedDigits, symbol);
        }
        return formattedDigits;
    }

    private static string ToStringUsDigitsFormattingInternal<T>(
        T number, DigitsFormattingSettings digitsFormattingSettings = DigitsFormattingSettings.None, int precesion = 2, string symbol = "$")
        where T : struct,
        IComparable,
        IComparable<T>,
        IConvertible,
        IEquatable<T>,
        IFormattable
    {
        string formattedDigits = string.Empty;
        string currentNoComaFormatSpecifier = string.Concat("#.", new string('0', precesion));
        string currentComaFormatSpecifier = string.Concat("##,#.", new string('0', precesion));
        formattedDigits =
            digitsFormattingSettings.HasFlag(DigitsFormattingSettings.NoComma) ? number.ToString(currentNoComaFormatSpecifier, UsCultureInfo) :
                number.ToString(currentComaFormatSpecifier, UsCultureInfo);
        if (digitsFormattingSettings.HasFlag(DigitsFormattingSettings.PrefixDollar))
        {
            formattedDigits = string.Concat(symbol, formattedDigits);
        }
        if (digitsFormattingSettings.HasFlag(DigitsFormattingSettings.PrefixMinus))
        {
            formattedDigits = string.Concat("-", formattedDigits);
        }
        if (digitsFormattingSettings.HasFlag(DigitsFormattingSettings.SufixDollar))
        {
            formattedDigits = string.Concat(formattedDigits, symbol);
        }
        formattedDigits = formattedDigits != "" ? formattedDigits : number.ToString();
        return formattedDigits;
    }
}

[Flags]
public enum DigitsFormattingSettings
{
    None = 0,
    NoComma = 1,
    PrefixDollar = 2,
    PrefixMinus = 4,
    SufixDollar = 8
}


[Flags]
public enum DigitsFormattingSettingsBitShifting
{
    None = 0,
    NoComma = 1 << 0,
    PrefixDollar = 1 << 1,
    PrefixMinus = 1 << 2,
    SufixDollar = 1 << 3
}