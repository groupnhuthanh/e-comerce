﻿using Entities.ExtendModels.Globals.Path;
using Helpers.Convert;

namespace Helpers.Files;

/// <summary>
/// The file helper.
/// </summary>
public static class FileHelper
{
    private static readonly string environmentName = GOEnvironment.TWO_Nonce;
    private static readonly string folderPath = Environment.CurrentDirectory;
    public static string GetEnvironmentVariable(string name, string defaultValue)
                => Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process) ?? defaultValue;
    /// <summary>
    /// Writes the to file.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <param name="content">The content.</param>
    /// <param name="isAppend">If true, is append.</param>
    /// <returns>A Task.</returns>
    public static async Task<bool> WriteToFile(string path, string content, bool isAppend = false)
    {
        var result = false;
        var pathFile = GetEnvironmentVariable(environmentName, folderPath) + path;
        if (!Directory.Exists(pathFile)) Directory.CreateDirectory(pathFile.Substring(0, pathFile.LastIndexOf("/")));
        using StreamWriter file = new(pathFile, append: isAppend);
        await file.WriteLineAsync(content);
        result = true;
        return result;
    }

    /// <summary>
    /// Writes the to file.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <param name="content">The content.</param>
    /// <param name="isAppend">If true, is append.</param>
    /// <returns>A Task.</returns>
    public static async Task<string> WriteToFilePath(string path, string content, bool isAppend = false)
    {
        var pathFile = GetEnvironmentVariable(environmentName, folderPath) + path;
        if (!Directory.Exists(pathFile)) Directory.CreateDirectory(pathFile.Substring(0, pathFile.LastIndexOf("/")));
        using StreamWriter file = new(pathFile, append: isAppend);
        await file.WriteLineAsync(content);
        return pathFile;
    }

    /// <summary>
    /// Reads the file.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <returns>A Task.</returns>
    public static async Task<T> ReadFile<T>(string path) where T : new()
    {
        var content = "";
        var pathFile = GetEnvironmentVariable(environmentName, folderPath) + path;
        if (File.Exists(pathFile))
        {
            using StreamReader file = new(pathFile);
            content = await file.ReadToEndAsync();
        }
        var result = content == null && content == "" ? ConvertHelper.Deserialize<T>(content) : new T();
        return result;
    }

    /// <summary>
    /// Reads the file.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <returns>A Task.</returns>
    public static async Task<List<string>> ReadFile(string path)
    {
        var pathFile = GetEnvironmentVariable(environmentName, folderPath) + path;
        var content = "";
        if (File.Exists(pathFile))
        {
            using StreamReader file = new(pathFile);
            content = await file.ReadToEndAsync();
        }
        var result = content is not null and not "" ? content.Contains('\n') ? content.Replace("\r", "").Split("\n").ToList() : new[] { content }.ToList() : new List<string>();
        return result;
    }
}
