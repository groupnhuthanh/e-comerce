using Helpers.Algorithm;
using Newtonsoft.Json;

namespace Helpers.Variants;

public static class VariantsMatrix
{
    private static List<Dictionary<string, object>> LstDicVariantGroup { get; set; }
    private static int PricingMethodType { get; set; }
    private static decimal PriceMethodDiscount { get; set; }
    public static Dictionary<string, object> RenderMatrix(string jsonOptionList, string jsonGroupList, int pricingMethodType, decimal pricingMethodDiscount)
    {
        var dic = new Dictionary<string, object>();
        if (jsonGroupList != "")
        {
            LstDicVariantGroup = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(jsonGroupList);
        }
        var variantToListClass = ConvertToVaiantListClass(jsonOptionList);
        if (variantToListClass.Any())
        {

            PricingMethodType = pricingMethodType;
            PriceMethodDiscount = pricingMethodDiscount;

            var lstToTree = ConvertListToTree(variantToListClass);
            if (lstToTree.Any())
            {
                var strLstToTree = JsonConvert.SerializeObject(lstToTree);
                var lstResult = JsonConvert.DeserializeObject<List<MyUserOptionValueListClassModel>>(strLstToTree);
                var strResult2 = JsonConvert.SerializeObject(lstResult);
                var jobject = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(strResult2);
                var lstTree = new List<string>();
                var lstGroup = new List<string>();
                var lstdic = RecursiveParseJson(ref jobject, lstTree, lstGroup);
                if (lstdic.Any())
                {
                    dic = (Dictionary<string, object>)lstdic[0]["Variant"];
                }
            }
        }
        return dic;
    }
    private static List<OptionValueListClass> ConvertListToTree(List<OptionValueListClass> items)
    {
        //if (!items.Any())
        //{
        //items = new List<OptionValueListClass>()
        //{
        //    new MyClass(1, 0, "", 1, "", "CHOOSE YOUR SIZE", 1, true),
        //    new MyClass(2, 1, "S", 1, "Size", "CHOOSE YOUR SIZE", 1, true),
        //    new MyClass(2, 1, "M", 1, "Size", "CHOOSE YOUR SIZE", 1, true),
        //    new MyClass(2, 1, "L", 1, "Size", "CHOOSE YOUR SIZE", 1, true),
        //    new MyClass(3, 2, "Sandwich", 1, "Which kind of toast you would like?",
        //        "WHICH KIND OF TOAST YOU WOULD LIKE?", 2, true),
        //    new MyClass(3, 2, "Baguette", 1, "Which kind of toast you would like?",
        //        "WHICH KIND OF TOAST YOU WOULD LIKE?", 2, true),
        //    new MyClass(4, 3, "French Fries", 1, "Choice of side dish", "CHOICE OF SIDE DISH", 1, true),
        //    new MyClass(4, 3, "Salad", 1, "Choice of side dish", "CHOICE OF SIDE DISH", 1, true),
        //    new MyClass(5, 4, "Mayonanaise Sause", 1, "Choice of sauce for your toast", "", 1, true),
        //    new MyClass(5, 4, "No mayonnaise", 1, "Choice of sauce for your toast", "", 1, true),
        //    new MyClass(6, 5, "Rare", 1, "How do you want to cook?", "", 1, true),
        //    new MyClass(6, 5, "Medium", 1, "How do you want to cook?", "", 1, true),
        //    new MyClass(6, 5, "Well done", 1, "How do you want to cook?", "", 1, true),
        //    new MyClass(7, 6, "Mushroom sauce", 1, "Which kind of sauce you would like to?", "", 1, true),
        //    new MyClass(7, 6, "Pepper sauce", 1, "Which kind of sauce you would like to?", "", 1, true),
        //    new MyClass(7, 6, "Red wine sauce", 1, "Which kind of sauce you would like to?", "", 1, true)
        //};
        //}

        Action<OptionValueListClass> setChildren = null;
        setChildren = parent =>
        {
            var firstItem = items.FirstOrDefault(childItem => childItem.ParentId == parent.Level);
            if (firstItem?.OptionName != null)
            {
                parent.Variant.OptionName = firstItem.OptionName ?? "";
                parent.Variant.DisplayName = firstItem.DisplayName ?? "";
                parent.Variant.IsDisplayName = firstItem.IsDisplayName ?? false;
                parent.Variant.VariantOptionType = firstItem.VariantOptionType ?? 1;
            }
            var lstItem = items
                .Where(childItem => childItem.ParentId == parent.Level & childItem.OptionName != null & childItem.Value != "" & childItem.OptionName != "").ToList();
            parent.Variant.OptionValueList = lstItem;
            parent.Variant.OptionValueList.ForEach(setChildren);
        };
        //Initialize the hierarchical list to root level items
        var hierarchicalItems = items
            .Where(rootItem => rootItem.ParentId == -1)
            .ToList();
        //Call the SetChildren method to set the children on each root level item.
        hierarchicalItems.ForEach(setChildren);
        return hierarchicalItems;
    }
    private static List<OptionValueListClass> ConvertToVaiantListClass(string jsonOptionList)
    {
        var items = new List<OptionValueListClass>
        {
            new OptionValueListClass(0,0, -1, "", true, "", "CHOOSE YOUR SIZE", 1, true)
        };
        if (jsonOptionList == "") return items;
        var lstOption = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(jsonOptionList);
        var iz = 0;
        var id = 0;
        for (int i = 0; i < lstOption.Count; i++)
        {
            iz++;
            var k = i;
            var ilstOption = lstOption[i];
            var OptionName = ilstOption.ContainsKey("OptionName") ? ilstOption["OptionName"].ToString() : "";
            var IsDisplayName =
                ilstOption.ContainsKey("IsDisplayName") &&
                int.Parse(ilstOption["IsDisplayName"].ToString()!).Equals(1);
            var DisplayName = ilstOption.ContainsKey("DisplayName") ? ilstOption["DisplayName"].ToString() : "";
            var VariantOptionType = ilstOption.ContainsKey("VariantOptionType")
                ? int.Parse(ilstOption["VariantOptionType"].ToString()!)
                : 1;
            var OptionValue = ilstOption.ContainsKey("OptionValue") ? ilstOption["OptionValue"].ToString() : "";
            var variantVisible = ilstOption.ContainsKey("Visible") ? int.Parse(ilstOption["Visible"]?.ToString() ?? "1") : 1;
            var visible = variantVisible > 0;
            if (OptionValue == "") continue;
            var spOptionValue = OptionValue.Contains(",")
                ? OptionValue.Split(',').ToList()
                : new List<string> { OptionValue };
            for (int j = 0; j < spOptionValue.Count; j++)
            {
                var value = spOptionValue[j];
                id++;
                if (LstDicVariantGroup != null && LstDicVariantGroup.Any())
                {
                    var lstDicVariantGroupExist = LstDicVariantGroup.Where(x => x.ContainsKey("GroupName") && x["GroupName"].ToString().Contains(value + "•") && int.Parse(x["Visible"]?.ToString() ?? "1").Equals(1)).ToList();
                    visible = lstDicVariantGroupExist != null && lstDicVariantGroupExist.Any();

                }
                var item = new OptionValueListClass(id, iz, k, value, visible, OptionName, DisplayName, VariantOptionType, IsDisplayName);
                //items.AddRange(spOptionValue.Select(value => new OptionValueListClass(iz, iz, k, value, 1, OptionName, DisplayName, VariantOptionType, IsDisplayName)));
                items.Add(item);
            }

        }
        return items;
    }

    private static List<Dictionary<string, object>> RecursiveParseJson(ref List<Dictionary<string, object>> lstDic, List<string> lstTree, List<string> lstGroup)
    {
        var zindex = 0;
        for (var index = 0; index < lstDic.Count; zindex = index++)
        {
            var dic = lstDic[index];
            var value = dic.ContainsKey("Value") ? dic["Value"]?.ToString() ?? "" : "";
            if (value != "")
            {
                lstTree.Add(value);
            }

            var VariantList = dic.ContainsKey("Variant") ? dic["Variant"]?.ToString() ?? "" : "";
            if (VariantList != "")
            {
                var jsonVariantList = JsonConvert.DeserializeObject<Dictionary<string, object>>(VariantList);
                if (jsonVariantList.Any())
                {
                    var OptionValueList = jsonVariantList.ContainsKey("OptionValueList")
                        ? jsonVariantList["OptionValueList"]?.ToString() ?? ""
                        : "";
                    if (OptionValueList != "")
                    {
                        var jsonOptionValueList =
                            JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(OptionValueList);
                        if (jsonOptionValueList.Any())
                        {
                            RecursiveParseJson(ref jsonOptionValueList, lstTree, lstGroup);
                            jsonVariantList["OptionValueList"] = jsonOptionValueList;
                            dic["Variant"] = jsonVariantList;
                            lstDic[index] = dic;
                        }
                        else
                        {
                            var strGroupValue = string.Join("•", lstTree);
                            lstGroup.Add(strGroupValue);
                            dic.Remove("Variant");
                            dic.Add("GroupValue", strGroupValue);
                            var groupId = 0;
                            decimal price = 0;
                            decimal comparePrice = 0;
                            decimal costPerItem = 0;
                            decimal discount = 0;
                            var sku = "";
                            var barcode = "";
                            var inventory = 0;
                            var visible = true;
                            if (LstDicVariantGroup.Any())
                            {
                                var Group = LstDicVariantGroup.Where(x => x["GroupName"].ToString().Equals(strGroupValue))
                                    .ToList();
                                if (Group.Any())
                                {
                                    foreach (var iGroup in Group)
                                    {
                                        groupId = iGroup.ContainsKey("GroupId") ? int.Parse(iGroup["GroupId"].ToString()) : 0;
                                        price = iGroup.ContainsKey("Price") ? decimal.Parse(iGroup["Price"].ToString()) : 0;
                                        if (PricingMethodType.Equals(2))
                                        {
                                            if (PriceMethodDiscount > 0)
                                            {
                                                price = Alg_Math.PriceAfterDiscount(1, price, PriceMethodDiscount);
                                            }
                                        }
                                        else if (PricingMethodType.Equals(1))
                                        {
                                            price = 0;
                                        }
                                        comparePrice = iGroup.ContainsKey("ComparePrice") ? decimal.Parse(iGroup["ComparePrice"].ToString()) : 0;
                                        costPerItem = iGroup.ContainsKey("CostPerItem") ? decimal.Parse(iGroup["CostPerItem"].ToString()) : 0;
                                        discount = iGroup.ContainsKey("DiscountModel") ? decimal.Parse(iGroup["DiscountModel"].ToString()) : 0;
                                        sku = iGroup.ContainsKey("Sku") ? iGroup["Sku"].ToString() : "";
                                        barcode = iGroup.ContainsKey("Barcode") ? iGroup["Barcode"].ToString() : "";
                                        inventory = iGroup.ContainsKey("Inventory") && !string.IsNullOrEmpty(iGroup["Inventory"]?.ToString()) ? int.Parse(iGroup["Inventory"]?.ToString() ?? "0") : 0;
                                        var variantVisible = iGroup.ContainsKey("Visible") ? int.Parse(iGroup["Visible"].ToString()) : 0;
                                        visible = variantVisible > 0;
                                    }
                                }
                            }
                            dic["Id"] = groupId;
                            dic.Add("Price", price);
                            dic.Add("ComparePrice", comparePrice);
                            dic.Add("CostPerItem", costPerItem);
                            dic.Add("DiscountModel", discount);
                            dic.Add("Sku", sku);
                            dic.Add("Barcode", barcode);
                            dic.Add("Inventory", inventory);
                            dic["Visible"] = visible;

                            if (lstTree.Count > 0)
                                lstTree.RemoveAt(lstTree.Count - 1);
                            if (lstDic.Count > 1)
                            {
                                //lstDic.Remove(dic);
                                //index--;
                            }
                            else
                            {
                                if (lstTree.Count > 0)
                                    lstTree.RemoveAt(lstTree.Count - 1);
                            }
                        }
                    }
                    else
                    {
                        if (lstTree.Count > 1)
                            lstTree.RemoveAt(lstTree.Count - 1);
                        if (lstDic.Count > 0)
                        {
                            //lstDic.Remove(dic);
                            //index--;
                        }
                        else
                        {
                            if (lstTree.Count > 0)
                                lstTree.RemoveAt(lstTree.Count - 1);
                        }
                    }
                }
                else
                {
                    if (lstTree.Count > 1)
                        lstTree.RemoveAt(lstTree.Count - 1);
                    if (lstDic.Count > 0)
                    {
                        //lstDic.Remove(dic);
                        //index--;
                    }
                    else
                    {
                        if (lstTree.Count > 0)
                            lstTree.RemoveAt(lstTree.Count - 1);
                    }
                }
            }
            else
            {
                if (lstTree.Count > 0)
                    lstTree.RemoveAt(lstTree.Count - 1);
                if (lstDic.Count > 0)
                {
                    //lstDic.Remove(dic);
                    //index--;
                }
            }

            lstDic[index] = dic;
        }

        if (zindex <= 0 || zindex != lstDic.Count - 1) return lstDic;
        if (lstTree.Count > 0)
            lstTree.RemoveAt(lstTree.Count - 1);
        return lstDic;
    }
}


public class OptionValueListClass
{
    public int Id;
    public int Level;
    public int ParentId;
    public string Value;
    public bool? Visible;
    public int? VariantOptionType;
    public string OptionName;
    public string DisplayName;
    public bool? IsDisplayName;
    public VariantListClass Variant = new VariantListClass();
    /// <summary>
    /// Initializes a new instance of the <see cref="OptionValueListClass"/> class.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <param name="level">The level.</param>
    /// <param name="parentId">The parent id.</param>
    /// <param name="value">The value.</param>
    /// <param name="visible">The visible.</param>
    /// <param name="optionName">The option name.</param>
    /// <param name="displayName">The display name.</param>
    /// <param name="variantOptionType">The variant option type.</param>
    /// <param name="isDisplayName">If true, is display name.</param>
    public OptionValueListClass(int id, int level, int parentId, string value, bool visible, string optionName, string displayName, int variantOptionType, bool isDisplayName)
    {
        Id = id;
        Level = level;
        ParentId = parentId;
        Value = value ?? "";
        Visible = visible;
        OptionName = optionName ?? "";
        DisplayName = displayName ?? "";
        VariantOptionType = variantOptionType;
        IsDisplayName = isDisplayName;
    }
}

public class VariantListClass
{
    public string DisplayName;
    public string OptionName;
    public int VariantOptionType;
    public bool IsDisplayName;
    public List<OptionValueListClass> OptionValueList;
}


public class MyUserOptionValueListClassModel
{
    public int Id;
    public int Level;
    public string Value;
    public bool Visible;
    public MyUserVariantListClassModel Variant = new MyUserVariantListClassModel();
}

public class MyUserVariantListClassModel
{
    public string DisplayName;
    public string OptionName;
    public int VariantOptionType;
    public bool IsDisplayName;
    public List<MyUserOptionValueListClassModel> OptionValueList;
}