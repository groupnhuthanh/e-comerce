﻿namespace Helpers.Queue;

public interface ISerialQueue
{
    Task Enqueue(Action action);
    Task<T> Enqueue<T>(Func<T> function);
}
public interface ITaskQueue
{
    Task<T> Enqueue<T>(Func<Task<T>> taskGenerator);
    Task Enqueue(Func<Task> taskGenerator);
}
/// <summary>
/// The serial queue.
/// </summary>
public class SerialQueue : ISerialQueue
{
    readonly object _locker = new();
    WeakReference<Task> _lastTask;

    /// <summary>
    /// Enqueues the.
    /// </summary>
    /// <param name="action">The action.</param>
    /// <returns>A Task.</returns>
    public Task Enqueue(Action action)
    {
        return Enqueue<object>(() =>
        {
            action();
            return null;
        });
    }

    /// <summary>
    /// Enqueues the.
    /// </summary>
    /// <param name="function">The function.</param>
    /// <returns>A Task.</returns>
    public Task<T> Enqueue<T>(Func<T> function)
    {
        lock (_locker)
        {
            Task<T> resultTask = _lastTask != null && _lastTask.TryGetTarget(out Task lastTask) ? lastTask.ContinueWith(_ => function()) : Task.Run(function);
            _lastTask = new WeakReference<Task>(resultTask);
            return resultTask;
        }
    }
}

/// <summary>
/// The task queue.
/// </summary>
public class TaskQueue : IDisposable, ITaskQueue
{
    private readonly SemaphoreSlim _semaphoreSlim;

    /// <summary>
    /// Initializes a new instance of the <see cref="TaskQueue"/> class.
    /// </summary>
    public TaskQueue()
    {
        _semaphoreSlim = new SemaphoreSlim(1);
    }

    /// <summary>
    /// Enqueues the.
    /// </summary>
    /// <param name="taskGenerator">The task generator.</param>
    /// <returns>A Task.</returns>
    public async Task<T> Enqueue<T>(Func<Task<T>> taskGenerator)
    {
        await _semaphoreSlim.WaitAsync();

        try
        {
            return await taskGenerator();
        }
        finally
        {
            _semaphoreSlim.Release();
        }
    }

    /// <summary>
    /// Enqueues the.
    /// </summary>
    /// <param name="taskGenerator">The task generator.</param>
    /// <returns>A Task.</returns>
    public async Task Enqueue(Func<Task> taskGenerator)
    {
        await _semaphoreSlim.WaitAsync();
        try
        {
            await taskGenerator();
        }
        finally
        {
            _semaphoreSlim.Release();
        }
    }

    /// <summary>
    /// Disposes the.
    /// </summary>
    public void Dispose()
    {
        GC.KeepAlive(_semaphoreSlim);
        GC.SuppressFinalize(this);
    }
}
