﻿using System.Security.Cryptography;
using System.Text;

namespace Helpers.Encryption;

public static class HMAC_SHA256Helper
{
    //https://stackoverflow.com/questions/12185122/calculating-hmacsha256-using-c-sharp-to-match-payment-provider-example
    private static string HexDecode(string hex)
    {
        var sb = new StringBuilder();
        for (int i = 0; i <= hex.Length - 2; i += 2)
        {
            sb.Append(System.Convert.ToString(System.Convert.ToChar(int.Parse(hex.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
        }
        return sb.ToString();
    }

    public static string CalcHMACSHA256Hash(string plaintext, string salt)
    {
        string result = "";
        var enc = Encoding.Default;
        byte[]
        baText2BeHashed = enc.GetBytes(plaintext),
        baSalt = enc.GetBytes(salt);
        HMACSHA256 hasher = new(baSalt);
        byte[] baHashedText = hasher.ComputeHash(baText2BeHashed);
        result = string.Join("", baHashedText.ToList().Select(b => b.ToString("x2")).ToList());
        return result;
    }

    public static string GenerateHMACSignature(string input, string secret)
    {
        var enc = Encoding.Default;
        var baSalt = enc.GetBytes(secret);
        byte[] signature = Encoding.UTF8.GetBytes(input);

        using (HMACSHA256 hmac = new HMACSHA256(baSalt))
        {
            byte[] signatureBytes = hmac.ComputeHash(signature);

            return string.Join("", signatureBytes.ToList().Select(b => b.ToString("x2")).ToList());
        }
    }


    public static string CalcSha256Hash(string input)
    {
        SHA256 sha256 = new SHA256Managed();
        byte[] sha256Bytes = Encoding.UTF8.GetBytes(input);
        byte[] cryString = sha256.ComputeHash(sha256Bytes);
        string sha256Str = string.Empty;
        for (int i = 0; i < cryString.Length; i++)
        {
            sha256Str += cryString[i].ToString("x2");
        }
        return sha256Str;
    }
}
