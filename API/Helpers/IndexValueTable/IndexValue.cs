namespace Helpers.IndexValueTable;

public class IndexKey
{
    public const string Customer_Id = "customer_id";
    public const string Customer_Guests_Id = "customer_guests_id";
    public const string Customer_Guest_Group_Id = "customer_guest_group_id";
    public const string Customer_Guests_Payments_Billings_Id = "customer_guests_payments_billings_id";
    public const string Customer_Guests_Payments_Shippings_Id = "customer_guests_payments_shippings_id";


}
public class IndexValue
{
    public const string Customer_Value = "customer_value";
    public const string Customer_Guests_Value = "customer_guests_value";
    public const string Customer_Guest_Group_Value = "customer_guest_group_value";
    public const string Customer_Guests_payments_Billings_Value = "customer_guests_payments_billings_value";
    public const string Customer_Guests_payments_Shippings_Value = "customer_guests_payments_shippings_value";

}