using Entities.ExtendModels.Settings.Date;
using Entities.ExtendModels.Times;
using Helpers.Encrypt;
using Helpers.Thirdparty;
using Newtonsoft.Json;
using PhoneNumbers;
using System.Data;
using System.Globalization;
using System.Text;
using System.Text.Json.Nodes;
using System.Text.RegularExpressions;

namespace Helpers.Convert;

public static class LIB_convert
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Converts a str to an english. </summary>
    ///
    /// <remarks>   Vuong Tran, 11/12/2018. </remarks>
    ///
    /// <param name="str">  The string. </param>
    ///
    /// <returns>   The given data converted to an english. </returns>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static string _db_SinglePoint_en = "SinglePoint_en";
    public static string _db_SinglePoint_System = "SinglePoint_System";
    public static string _db_SinglePoint_Orders_vi = "SinglePoint_Orders_vi";
    public static string _db_SinglePoint_Payment = "SinglePoint_Payment";

    private static NameParser _nameParser = new NameParser();


    public static string _db_SinglePoint_Pos = "SinglePoint_Pos";


    // public static ResponseResult CreateResponeResult(bool isSuccess = true, int code = 200, string dev_message = "", string cus_message = "", dynamic data = null, int TotalRecord = 0, string request = "")
    // {
    //     var reval = new ResponseResult();
    //     reval.isSuccess = isSuccess;
    //     reval.code = code; //Mã thông báo
    //     reval.cus_message = cus_message; //Nội dung thông báo hiển thị lên ứng dụng cho người dùng
    //     reval.dev_message = dev_message; // Nội dung thông báo lỗi phục vụ cho dev (ko hiển thị lên cho người dùng)
    //     reval.TotalRecord = TotalRecord; //số lượng dữ liệu trả về
    //     reval.request = request; //request ajax
    //     reval.data = data ?? new ExpandoObject(); //Dữ liệu trả về nếu có, ngược lại thì gán new {}
    //     return reval;
    // }
    public static int convertKeyDicToInt(Dictionary<string, object> dic, string _key)
    {
        int result = 0;
        try
        {
            var str = dic.ContainsKey(_key) && dic[_key] != null ? dic[_key].ToString() : "";
            if (checkNumber(str))
            {
                result = int.Parse(str);
            }
        }
        catch (Exception)
        {

        }
        return result;
    }
    public static List<Dictionary<string, object>> ArrangeTime(string TimeOn = "08:00",
        string TimeOff = "22:00", int duration = 15, bool IsNow = false, List<TimeExclude> arangeExludeList = null, int onlyToday = 0, TimesModel timesList = null)
    {
        var lst = new List<Dictionary<string, object>>();
        try
        {

            var DateNow = LibConvert.Get_ByDateTimeNow();
            var now = TimeSpan.Parse(DateTime.Parse(DateNow).ToString("HH:mm"));
            var timeOn = TimeSpan.Parse(TimeOn);
            var timeOff = TimeSpan.Parse(TimeOff);
            var dicDateNow = new Dictionary<string, object>();

            var time1 = "Hôm nay";
            var time2 = "Thời gian sớm nhất";
            var time3 = "Hẹn sau";
            if (timesList != null && !string.IsNullOrEmpty(timesList.Time1) && !string.IsNullOrEmpty(timesList.Time2) && !string.IsNullOrEmpty(timesList.Time3))
            {
                time1 = timesList.Time1 ?? time1;
                time2 = timesList.Time2 ?? time2;
                time3 = timesList.Time3 ?? time3;
            }

            if (IsNow)
            {
                var text = onlyToday.Equals(1) ? time2 : time1;
                if (now >= timeOn && now < timeOff)
                {
                    #region Exlude Time Now in TimeArangeExlude
                    bool isContinue = true;
                    if (arangeExludeList != null && arangeExludeList.Any())
                    {
                        var arangeExludeExist =
                            arangeExludeList.Where(x => x.TimeOn < now && now < x.TimeOff).ToList();
                        if (arangeExludeExist.Any())
                        {
                            isContinue = false;
                        }
                    }
                    #endregion

                    if (isContinue)
                    {
                        dicDateNow = new Dictionary<string, object>
                        {
                            {"Id", 0},
                            {"IsNow", true},
                            {"Value", DateTime.Parse(DateNow).ToString("HH:mm")}
                            ,{"Label", time1}
                            ,{"Text", text}
                        };
                        lst.Add(dicDateNow);
                    }
                }
            }
            if (onlyToday.Equals(0))
            {
                var k = 0;
                int v = 60 / duration;
                var minusEnd = Math.Ceiling(double.Parse(v.ToString()));
                var hoursOn = timeOn.Hours;
                var hoursOff = timeOff.Hours + 1;
                var end = hoursOff - hoursOn;
                for (int i = 0; i < end; i++)
                {
                    var hour = hoursOn + i;
                    for (int j = 0; j < minusEnd; j++)
                    {
                        var minusStep = duration * j;
                        var currentStep = TimeSpan.Parse(hour + ":" + minusStep);

                        #region Exlude Time Arange List
                        bool isContinue = true;
                        if (arangeExludeList != null && arangeExludeList.Any())
                        {
                            if ((from arangeExlude in arangeExludeList select arangeExlude.TimeArangeList ?? new ArangoList<TimeSpan>() into lstTimeSpan where lstTimeSpan.Any() select lstTimeSpan.Where(x => x.Equals(currentStep)).ToList()).Any(timeSpanExludeExist => timeSpanExludeExist.Any()))
                            {
                                isContinue = false;
                            }
                        }
                        #endregion

                        if (isContinue)
                        {
                            if ((IsNow && now < currentStep && currentStep.Subtract(now).TotalMinutes >= duration &&
                                  currentStep >= timeOn || !IsNow && currentStep >= timeOn) &&
                                currentStep <= timeOff)
                            {
                                k++;
                                var dicDate = new Dictionary<string, object>
                            {
                                {"Id", k},
                                {"IsNow", false},
                                {"Value", currentStep.ToString(@"hh\:mm")}
                                ,{"Label", time3},
                                {"Text", currentStep.ToString(@"hh\:mm")}
                            };
                                lst.Add(dicDate);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
        }
        return lst;
    }
    public static double ConvertStringToDouble(string strInput, int digits = 0)
    {
        double myDec = checkNumber(strInput) ? Math.Round(double.Parse(strInput), digits) : 0;
        return myDec;
    }
    public class ArangoList<T> : List<T>
    {
        public long? FullCount { get; set; }
    }
    public static string convermoney_double(string source, int Midpoint = 0)
    {
        var strbdMoney = new StringBuilder();
        var money = "0";
        if (checkNumber(source))
        {
            if (source.Contains("."))
            {
                var arr = source.Split('.');
                double amount1 = arr[0] != "" ? double.Parse(arr[0]) : 0;
                var str_amount1 = amount1.ToString("#,##0");
                double amount2 = arr[1] != "" ? double.Parse(0 + "." + arr[1]) : 0;
                var str_amount2 = Math.Round(amount2, Midpoint, MidpointRounding.ToEven).ToString();

                strbdMoney.Append(str_amount1);
                var arr_amount2 = str_amount2.Split('.');
                if (arr_amount2.Length > 1)
                {
                    strbdMoney.Append(".");
                    strbdMoney.Append(arr_amount2[1]);
                }
            }
            else
            {
                double amount = source != "" ? double.Parse(source) : 0;
                var str_amount1 = amount.ToString("#,##0");
                if (Midpoint > 0)
                {
                    str_amount1 = str_amount1 + ".00";
                }
                strbdMoney.Append(str_amount1);
            }
        }
        else
        {
            money = "0";
            strbdMoney.Append(money);
        }
        return strbdMoney.ToString();
    }
    public static string ConvertToEnglish(string str)
    {
        str = str.Replace("À", "A");
        str = str.Replace("Á", "A");
        str = str.Replace("Ả", "A");
        str = str.Replace("Ã", "A");
        str = str.Replace("Ạ", "A");

        str = str.Replace("Ă", "A");
        str = str.Replace("Ắ", "A");
        str = str.Replace("Ẳ", "A");
        str = str.Replace("Ẵ", "A");
        str = str.Replace("Ặ", "A");
        str = str.Replace("Ằ", "A");

        str = str.Replace("Â", "A");
        str = str.Replace("Ấ", "A");
        str = str.Replace("Ầ", "A");
        str = str.Replace("Ẩ", "A");
        str = str.Replace("Ẫ", "A");
        str = str.Replace("Ậ", "A");

        str = str.Replace("Đ", "D");
        str = str.Replace("È", "E");
        str = str.Replace("É", "E");
        str = str.Replace("Ẻ", "E");
        str = str.Replace("Ẽ", "E");
        str = str.Replace("Ẹ", "E");

        str = str.Replace("Ê", "E");
        str = str.Replace("Ề", "E");
        str = str.Replace("Ế", "E");
        str = str.Replace("Ể", "E");
        str = str.Replace("Ễ", "E");
        str = str.Replace("Ệ", "E");

        str = str.Replace("Ì", "I");
        str = str.Replace("Í", "I");
        str = str.Replace("Ỉ", "I");
        str = str.Replace("Ĩ", "I");
        str = str.Replace("Ị", "I");

        str = str.Replace("Ò", "O");
        str = str.Replace("Ó", "O");
        str = str.Replace("Ỏ", "O");
        str = str.Replace("Õ", "O");
        str = str.Replace("Ọ", "O");

        str = str.Replace("Ô", "O");
        str = str.Replace("Ồ", "O");
        str = str.Replace("Ố", "O");
        str = str.Replace("Ổ", "O");
        str = str.Replace("Ỗ", "O");
        str = str.Replace("Ộ", "O");

        str = str.Replace("Ơ", "O");
        str = str.Replace("Ờ", "O");
        str = str.Replace("Ớ", "O");
        str = str.Replace("Ở", "O");
        str = str.Replace("Ỡ", "O");
        str = str.Replace("Ợ", "O");

        str = str.Replace("Ù", "U");
        str = str.Replace("Ú", "U");
        str = str.Replace("Ủ", "U");
        str = str.Replace("Ũ", "U");
        str = str.Replace("Ụ", "U");

        str = str.Replace("Ư", "U");
        str = str.Replace("Ừ", "U");
        str = str.Replace("Ứ", "U");
        str = str.Replace("Ử", "U");
        str = str.Replace("Ữ", "U");
        str = str.Replace("Ự", "U");

        str = str.Replace("Ỳ", "Y");
        str = str.Replace("Ý", "Y");
        str = str.Replace("Ỷ", "Y");
        str = str.Replace("Ỹ", "Y");
        str = str.Replace("Ỵ", "Y");

        // LOWER CASE
        str = str.Replace("à", "a");
        str = str.Replace("á", "a");
        str = str.Replace("ả", "a");
        str = str.Replace("ã", "a");
        str = str.Replace("ạ", "a");

        str = str.Replace("â", "a");
        str = str.Replace("ấ", "a");
        str = str.Replace("ầ", "a");
        str = str.Replace("ẩ", "a");
        str = str.Replace("ẫ", "a");
        str = str.Replace("ậ", "a");

        str = str.Replace("ă", "a");
        str = str.Replace("ắ", "a");
        str = str.Replace("ằ", "a");
        str = str.Replace("ẳ", "a");
        str = str.Replace("ẵ", "a");
        str = str.Replace("ặ", "a");

        str = str.Replace("đ", "d");

        str = str.Replace("è", "e");
        str = str.Replace("é", "e");
        str = str.Replace("ẻ", "e");
        str = str.Replace("ẽ", "e");
        str = str.Replace("ẹ", "e");

        str = str.Replace("ê", "e");
        str = str.Replace("ề", "e");
        str = str.Replace("ế", "e");
        str = str.Replace("ể", "e");
        str = str.Replace("ễ", "e");
        str = str.Replace("ệ", "e");
        str = str.Replace("ẽ", "e");

        str = str.Replace("ì", "i");
        str = str.Replace("í", "i");
        str = str.Replace("ỉ", "i");
        str = str.Replace("ĩ", "i");
        str = str.Replace("ị", "i");
        str = str.Replace("ị", "i");

        str = str.Replace("ò", "o");
        str = str.Replace("ó", "o");
        str = str.Replace("ỏ", "o");
        str = str.Replace("õ", "o");
        str = str.Replace("ọ", "o");

        str = str.Replace("ô", "o");
        str = str.Replace("ồ", "o");
        str = str.Replace("ố", "o");
        str = str.Replace("ổ", "o");
        str = str.Replace("ỗ", "o");
        str = str.Replace("ộ", "o");

        str = str.Replace("ơ", "o");
        str = str.Replace("ờ", "o");
        str = str.Replace("ớ", "o");
        str = str.Replace("ở", "o");
        str = str.Replace("ỡ", "o");
        str = str.Replace("ợ", "o");

        str = str.Replace("ù", "u");
        str = str.Replace("ú", "u");
        str = str.Replace("ủ", "u");
        str = str.Replace("ũ", "u");
        str = str.Replace("ụ", "u");

        str = str.Replace("ư", "u");
        str = str.Replace("ừ", "u");
        str = str.Replace("ứ", "u");
        str = str.Replace("ử", "u");
        str = str.Replace("ữ", "u");
        str = str.Replace("ự", "u");

        str = str.Replace("ỳ", "y");
        str = str.Replace("ý", "y");
        str = str.Replace("ỷ", "y");
        str = str.Replace("ỹ", "y");
        str = str.Replace("ỵ", "y");
        return str;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Converts a str to an alias. </summary>
    ///
    /// <remarks>   Vuong Tran, 11/12/2018. </remarks>
    ///
    /// <param name="str">  The string. </param>
    ///
    /// <returns>   The given data converted to an alias. </returns>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    private static string ConvertToAlias(string str)
    {
        str = ConvertToEnglish(str);
        str = str.Replace(" ", "");
        str = str.Replace("-", "");
        str = str.Replace("+", "");
        str = str.Replace("/", "");
        str = str.Replace("\\", "");
        str = str.Replace("+", "");
        str = str.Replace("&", "");
        str = str.Replace("$", "");
        str = str.Replace("!", "");
        str = str.Replace("#", "");
        str = str.Replace("@", "");
        str = str.Replace("%", "");
        str = str.Replace("^", "");
        str = str.Replace("*", "");
        str = str.Replace(":", "");
        str = str.Replace(",", "");
        str = str.Replace("?", "");
        str = str.Replace("<", "");
        str = str.Replace(">", "");
        str = str.Replace("_", "");
        str = str.Replace("®", " ");
        str = str.Replace("™", " ");
        return str;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Converts this object to a search google. </summary>
    ///
    /// <remarks>   Vuong Tran, 11/12/2018. </remarks>
    ///
    /// <param name="str">          The string. </param>
    /// <param name="replace">      The replace. </param>
    /// <param name="isEnglish">    True if is english - Vietnamese to English+ Specials, false if not Only Specials Removed.</param>
    ///
    /// <returns>   The given data converted to a search google. </returns>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public static string ConvertToSearchGoogle(string str, string replace, bool isEnglish)
    {
        str = str ?? "";
        str = str.Normalize();
        if (isEnglish)
            str = ConvertToEnglish(str);
        str = ReplaceWordChars(str);
        str = str.Replace("-", " ");
        str = str.Replace("+", " ");
        str = str.Replace("/", " ");
        str = str.Replace("\\", " ");
        str = str.Replace("+", " ");
        str = str.Replace("&", " ");
        str = str.Replace("$", " ");
        str = str.Replace("!", " ");
        str = str.Replace("#", " ");
        str = str.Replace("@", " ");
        str = str.Replace("%", " ");
        str = str.Replace("^", " ");
        str = str.Replace("*", " ");
        str = str.Replace(":", " ");
        str = str.Replace(",", " ");
        str = str.Replace("?", " ");
        str = str.Replace("<", " ");
        str = str.Replace(">", " ");
        str = str.Replace(".", " ");
        str = str.Replace("\'", " ");
        str = str.Replace(";", " ");
        str = str.Replace("®", " ");
        str = str.Replace("™", " ");
        str = str.Replace("�", " ");
        str = str.Replace(((char)34).ToString(), " ");
        str = str.Replace(((char)39).ToString(), " ");
        string fn = Regex.Replace(str, @"[(\n)(\s+)(\b)(\t)'!@#$%^&*()<>?/|{}~`:;-]", " ");
        str = Regex.Replace(fn, @"\s+", " ").Trim();
        str = Regex.Replace(str, @"[^0-9a-zA-Z_-]", " ");
        str = str.Replace(" ", replace);
        return str.ToLower();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   Replace word characters. </summary>
    ///
    /// <remarks>   Vuong Tran, 11/12/2018. </remarks>
    ///
    /// <param name="text"> The text. </param>
    ///
    /// <returns>   A string. </returns>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    private static string ReplaceWordChars(string text)
    {
        var s = text;
        // smart single quotes and apostrophe
        s = Regex.Replace(s, "[\u2018|\u2019|\u201A]", " ");
        // smart double quotes
        s = Regex.Replace(s, "[\u201C|\u201D|\u201E]", " ");
        // ellipsis
        s = Regex.Replace(s, "\u2026", " ");
        // dashes
        s = Regex.Replace(s, "[\u2013|\u2014]", "");
        // circumflex
        s = Regex.Replace(s, "\u02C6", " ");
        // open angle bracket
        s = Regex.Replace(s, "\u2039", " ");
        // close angle bracket
        s = Regex.Replace(s, "\u203A", " ");
        // spaces
        s = Regex.Replace(s, "[\u02DC|\u00A0]", " ");
        return s;
    }

    public static string FirstCharacter(string str, int onlyLetter = 1)
    {
        try
        {
            if (str != "")
            {
                var character = ConvertToSearchGoogle(str, " ", true);
                var name = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(character.ToLower());
                var capture_value = Regex.Match(name, "(?:([A-Z0-9]+)(?:[^A-Z0-9]*))*").Groups[1].Captures
                    .Cast<Capture>()
                    .Select(p => p.Value).ToList();
                var lst = new List<string>();
                if (capture_value.Any())
                {
                    capture_value.RemoveAll(string.IsNullOrWhiteSpace);

                    if (onlyLetter.Equals(2))
                    {
                        if (capture_value.Count >= onlyLetter && capture_value.Any())
                        {
                            //lst.Add(capture_value[0].Length > 1
                            //    ? capture_value[0].Substring(0, 1)
                            //    : capture_value[0]);
                            for (int i = 0; i < onlyLetter; i++)
                            {
                                var capture_value1 = capture_value[i];
                                lst.Add(capture_value1);
                            }
                        }
                        else
                        {
                            if (capture_value[0].Length > 1)
                            {
                                lst.Add(capture_value[0].Substring(0, 1));
                            }
                            else
                            {
                                lst = capture_value;
                            }
                        }
                    }
                    else
                    {
                        //if (capture_value[0].Length > 1)
                        //{
                        //    lst.Add(capture_value[0].Substring(0, 1));
                        //}
                        //else
                        //{
                        //    lst = capture_value;
                        //}
                        lst.Add(capture_value[0]);
                    }
                }
                return string.Join(string.Empty, lst);
            }
        }
        catch (Exception)
        {
            //Console.Write(e);
        }

        return str;
    }

    public static string ToTitleCase(string s) =>
        CultureInfo.InvariantCulture.TextInfo.ToTitleCase(s.ToLower());


    public static string generateCode(int length = 6)
    {
        var str = "";
        try
        {
            length = length > 0 ? length : 6;
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
            str = new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        catch (Exception)
        {
        }

        return str;
    }

    public static string ConvertMoneyUK(string source)
    {
        CultureInfo usCulture = new CultureInfo("en-US");
        string money;
        if (string.IsNullOrEmpty(source.Trim()))
            money = "0";
        else
        {
            if (source.Contains("."))
            {
                decimal num = System.Convert.ToDecimal(source.Replace(",", ".").Trim(), usCulture);
                money = Math.Round(num, 2, MidpointRounding.ToEven).ToString();
            }
            else
            {
                decimal amount = source != "" ? decimal.Parse(source) : 0;
                money = amount.ToString("#,##0");
                return money;
            }
        }

        return money;
    }

    public static int IsValidJson(string strInput)
    {
        var flag = 0;
        if (string.IsNullOrWhiteSpace(strInput))
        {
            return flag;
        }

        strInput = strInput.Trim();
        if (strInput.StartsWith("{") && strInput.EndsWith("}"))

        {
            try
            {
                //var obj = JToken.Parse(strInput);
                flag = 1;
            }
            catch (JsonReaderException)
            {

            }
            catch (Exception) //some other exception
            {
            }
        }
        else if (strInput.StartsWith("[{") && strInput.EndsWith("}]")) //For array
        {
            try
            {
                //var obj = JToken.Parse(strInput);
                flag = 2;
            }
            catch (JsonReaderException)
            {

            }
            catch (Exception) //some other exception
            {
            }
        }
        else if (strInput.StartsWith("[") && strInput.EndsWith("]")) //For array
        {
            try
            {
                //var obj = JToken.Parse(strInput);
                flag = 3;
            }
            catch (JsonReaderException)
            {

            }
            catch (Exception) //some other exception
            {
            }
        }

        return flag;
    }

    public static string license_formatter(List<string> lst, string format = "xxxx-xxxx-xxxx")
    {
        var str = "";
        try
        {
            var spFormat = format.Split('-');
            var k = 0;
            var lstJoinValue = new List<string>();
            foreach (var t in spFormat)
            {
                var ispFormat = t.Length;
                var strBValue = new StringBuilder();
                for (int j = 0; j < ispFormat; j++)
                {
                    var value = lst[k];
                    strBValue.Append(value);
                    k++;
                }

                lstJoinValue.Add(strBValue.ToString());
            }

            str = string.Join("-", lstJoinValue);
        }
        catch (Exception)
        {
        }

        return str;
    }

    public static int extractNumberFromString(string str)
    {
        var Number = 1;
        try
        {
            var ExtractNumber = int.TryParse(new string(str
                .SkipWhile(x => !char.IsDigit(x))
                .TakeWhile(char.IsDigit)
                .ToArray()), out Number);
            Number = ExtractNumber ? Number : 1;
        }
        catch (Exception e)
        {
            Console.Write(e);
        }

        return Number;
    }
    public static string getCurrentDate()
    {
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB", false);
        return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    }

    #region List<int> GetDayBetweenTwoDates

    public static List<int> GetDayBetweenTwoDates(DateTime from, DateTime to)
    {
        try
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB", false);
            var dates = Enumerable
                .Range(0, (int)(to - from).TotalDays)
                .Select(d => from.AddDays(d).Date).ToList();
            return dates.Select(weekGroup => weekGroup.Day).ToList();
        }
        catch (Exception)
        {
        }

        return null;
    }

    #endregion

    #region DataTable GetDayBetweenTwoMonth

    public static List<Dictionary<string, object>> GetDayBetweenTwoMonth(DateTime from, DateTime to)
    {
        var list = new List<Dictionary<string, object>>();
        try
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB", false);
            var dates = Enumerable
                .Range(0, (int)(to - from).TotalDays + 1)
                .Select(d => from.AddDays(d).Date).Select(x => new { x.Year, x.Month }).Distinct()
                .ToList().Distinct().ToList();
            if (dates.Any())
            {
                for (int i = 0; i < dates.Count; i++)
                {
                    var item = dates[i];

                    var dic = new Dictionary<string, object>
                    {
                        {"Year",item.Year}
                        ,{"Month",item.Month}
                    };
                    list.Add(dic);
                }
            }
        }
        catch (Exception)
        {
        }

        return list;
    }

    #endregion


    public static List<Tuple<int, int>> year_month_Between(DateTime d0, DateTime d1)
    {
        List<DateTime> datemonth = Enumerable.Range(0, (d1.Year - d0.Year) * 12 + d1.Month - d0.Month + 1)
            .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m)).ToList();
        List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();

        foreach (DateTime x in datemonth)
        {
            yearmonth.Add(new Tuple<int, int>(x.Year, x.Month));
        }

        return yearmonth;
    }

    public static List<Tuple<int, int>> lsDateTime(DateTime start, DateTime end, int Frequency = 3)
    {
        List<Tuple<int, int>> yearmonth = new List<Tuple<int, int>>();
        while (start <= end)
        {
            var month = start.Month;
            var year = start.Year;
            yearmonth.Add(new Tuple<int, int>(year, month));
            start = start.AddMonths(Frequency);
        }

        return yearmonth;
    }
    public static bool checkdate(string strInput)
    {
        DateTime outDate;
        var Result = !string.IsNullOrEmpty(strInput) && DateTime.TryParse(strInput, out outDate);
        return Result;
    }
    public static bool checkNumber(string strInput)
    {
        decimal myDec;
        var Result = strInput != null && strInput != "" ? decimal.TryParse(strInput, out myDec) : false;
        return Result;
    }
    public static double convertKeyDicToDouble(Dictionary<string, object> dic, string _key)
    {
        double result = 0;
        try
        {
            var str = dic.ContainsKey(_key) && dic[_key] != null ? dic[_key].ToString() : "";
            if (checkNumber(str))
            {
                result = double.Parse(str);
            }
        }
        catch (Exception)
        {

        }
        return result;
    }
    public static string checkKeyDictionary(Dictionary<string, object> dic, string _key)
    {
        var result = "";
        if (dic != null && dic.Count > 0)
        {
            result = dic.ContainsKey(_key) && dic[_key] != null ? dic[_key].ToString() : "";
        }
        return result;
    }
    public static DateTime getDateddMMyyyy(string strDateTime)
    {
        DateTime dt = DateTime.Now;
        if (strDateTime != null && strDateTime != "")
        {
            dt = DateTime.ParseExact(strDateTime, "d/M/yyyy", CultureInfo.InvariantCulture);
        }
        return dt;
    }
    public static List<Dictionary<string, object>> convertStringToListDic(string str)
    {
        var lsDic = new List<Dictionary<string, object>>();
        try
        {
            lsDic = str != "" ? JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(str) : null;
        }
        catch (Exception)
        {

        }
        return lsDic;
    }
    public static Dictionary<string, object> convertStringToDictionary(string str)
    {
        var lsDic = new Dictionary<string, object>();
        try
        {
            if (str != "")
            {
                lsDic = JsonConvert.DeserializeObject<Dictionary<string, object>>(str);
            }
        }
        catch (Exception)
        {
            lsDic = new Dictionary<string, object>();
        }
        return lsDic;
    }

    public static Dictionary<string, object> convertKeyToDic(Dictionary<string, object> dic, string _key)
    {
        var lsDic = new Dictionary<string, object>();
        try
        {
            var str = dic.ContainsKey(_key) && dic[_key] != null ? dic[_key].ToString() : "";
            if (str != "")
            {
                lsDic = JsonConvert.DeserializeObject<Dictionary<string, object>>(str);
            }
        }
        catch (Exception)
        {
            lsDic = new Dictionary<string, object>();
        }
        return lsDic;
    }
    public static List<Dictionary<string, object>> convertKeyToListDic(Dictionary<string, object> dic, string _key)
    {
        var lsDic = new List<Dictionary<string, object>>();
        try
        {
            var str = dic.ContainsKey(_key) && dic[_key] != null ? dic[_key].ToString() : "";
            if (str != "")
            {
                lsDic = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(str);
            }
        }
        catch (Exception)
        {
            lsDic = new List<Dictionary<string, object>>();
        }
        return lsDic;
    }
    public static List<Dictionary<string, int>> get24Hour()
    {
        var lsDic = new List<Dictionary<string, int>>();
        try
        {
            for (int i = 0; i < 24; i++)
            {
                var dic = new Dictionary<string, int> {
                    { "From", i},{ "To", i+1},
                };
                lsDic.Add(dic);
            }
        }
        catch (Exception)
        {

        }
        return lsDic;
    }

    #region DayOfWeek DATECASE

    public static DayOfWeek DateCase(int day)
    {
        var dateOf = DayOfWeek.Monday;
        switch (day)
        {
            case 1:
                dateOf = DayOfWeek.Sunday;
                break;
            case 2:
                dateOf = DayOfWeek.Monday;
                break;
            case 3:
                dateOf = DayOfWeek.Tuesday;
                break;
            case 4:
                dateOf = DayOfWeek.Wednesday;
                break;
            case 5:
                dateOf = DayOfWeek.Thursday;
                break;
            case 6:
                dateOf = DayOfWeek.Friday;
                break;
            case 7:
                dateOf = DayOfWeek.Saturday;
                break;
        }

        return dateOf;
    }

    #endregion       

    #region Convert Durations
    public static string FormatAsDuration(int duration)
    {
        if (duration < 0) throw new ArgumentOutOfRangeException("duration");
        return string.Join(", ", GetDurationParts(duration));
    }

    private static IEnumerable<string> GetDurationParts(int duration)
    {
        var parts = new[]
        {
            //new { Name="Weekday" , Length = 7*24*60*60*1 , } ,
            new { Name="days"  , Length =   24*60*60*1 , } ,
            new { Name="hrs"  , Length =      60*60*1 , } ,
            new { Name="mins"  , Length =         60*1 , } 
            //,new { Name="seconds"  , Length =            1 , }
        };

        foreach (var part in parts)
        {
            int n = Math.DivRem(duration, part.Length, out duration);
            if (n > 0) yield return string.Format("{0} {1}", n, part.Name);
        }
    }

    //https://rosettacode.org/wiki/Convert_seconds_to_compound_duration
    //private static string FormatAsDuration(int duration)
    //{
    //    if (duration < 0) throw new ArgumentOutOfRangeException("duration");
    //    return string.Join(", ", GetDurationParts(duration));
    //}

    //private static IEnumerable<string> GetDurationParts(int duration)
    //{
    //    var parts = new[]
    //    {
    //        new { Name="wk" , Length = 7*24*60*60*1 , } ,
    //        new { Name="d"  , Length =   24*60*60*1 , } ,
    //        new { Name="h"  , Length =      60*60*1 , } ,
    //        new { Name="m"  , Length =         60*1 , } ,
    //        new { Name="s"  , Length =            1 , } ,
    //    };

    //    foreach (var part in parts)
    //    {
    //        int n = Math.DivRem(duration, part.Length, out duration);
    //        if (n > 0) yield return string.Format("{0} {1}", n, part.Name);
    //    }

    //}

    //Output:
    //7,259 seconds ==> 2 h, 59 s

    //86,400 seconds ==> 1 d

    //6,000,000 seconds ==> 9 wk, 6 d, 10 h, 40 m
    #endregion

    public static string NxtKeyCode(string KeyCode)
    {
        byte[] ASCIIValues = Encoding.ASCII.GetBytes(KeyCode);
        int StringLength = ASCIIValues.Length;
        bool isAllZed = false;
        bool isAllNine = true;
        //Check if all has ZZZ.... then do nothing just return empty string.

        for (int i = 0; i < StringLength - 1; i++)
        {
            if (ASCIIValues[i] != 90)
            {
                isAllZed = false;
                break;
            }
        }
        if (isAllZed && ASCIIValues[StringLength - 1] == 57)
        {
            ASCIIValues[StringLength - 1] = 64;
        }

        // Check if all has 999... then make it A0
        for (int i = 0; i < StringLength; i++)
        {
            if (ASCIIValues[i] != 57)
            {
                isAllNine = false;
                break;
            }
        }
        if (isAllNine)
        {
            ASCIIValues[StringLength - 1] = 47;
            ASCIIValues[0] = 65;
            for (int i = 1; i < StringLength - 1; i++)
            {
                ASCIIValues[i] = 48;
            }
        }


        for (int i = StringLength; i > 0; i--)
        {
            if (i - StringLength == 0)
            {
                ASCIIValues[i - 1] += 1;
            }
            if (ASCIIValues[i - 1] == 58)
            {
                ASCIIValues[i - 1] = 48;
                if (i - 2 == -1)
                {
                    break;
                }
                ASCIIValues[i - 2] += 1;
            }
            else if (ASCIIValues[i - 1] == 91)
            {
                ASCIIValues[i - 1] = 65;
                if (i - 2 == -1)
                {
                    break;
                }
                ASCIIValues[i - 2] += 1;

            }
            else
            {
                break;
            }

        }
        KeyCode = Encoding.ASCII.GetString(ASCIIValues);
        return KeyCode;
    }
    //Co the se tach 2 phan nay ra de pick thogian dung voi ngay dc chon.
    /// <summary>
    /// Arranges the time_v1.
    /// </summary>
    /// <param name="TimeOn">The time on.</param>
    /// <param name="TimeOff">The time off.</param>
    /// <param name="duration">The duration.</param>
    /// <returns>A Task.</returns>
    public static Task<List<Dictionary<string, object>>> ArrangeTime_v1(string TimeOn = "08:00", string TimeOff = "22:00", int duration = 15)
    {
        var lst = new List<Dictionary<string, object>>();
        try
        {
            var DateNow = getCurrentDate();

            var timeOn = TimeSpan.Parse(TimeOn);
            var timeOff = TimeSpan.Parse(TimeOff);
            //var duration = 15;
            var arrangeTime = new List<TimeSpan>();
            var now = TimeSpan.Parse(DateTime.Parse(DateNow).ToString("HH:mm"));
            var startOn = now > timeOn ? now : timeOn;
            var hourOn = startOn.Hours;
            var hourEnd = timeOff.Hours;
            var hourDuration = hourEnd - hourOn;
            int v = 60 / duration;
            var minusEnd = Math.Ceiling(double.Parse(v.ToString()));
            var lstTime = new List<Dictionary<string, object>>();

            var dicDateNow = new Dictionary<string, object>();
            if (now <= timeOn || now < timeOff)
            {
                dicDateNow = new Dictionary<string, object>
                {
                    {"Id", 0},
                    {"Value", DateTime.Parse(DateNow).ToString("HH:mm")}
                    ,{"Text", "Now"}
                };
                lstTime.Add(dicDateNow);
            }
            var k = 0;
            if (hourDuration > 0)
            {
                var timeBefore = TimeSpan.Parse(hourOn + ":" + timeOn.Minutes);
                if (now.Hours == timeBefore.Hours - 1)
                {
                    timeBefore = now.Minutes >= duration ? TimeSpan.Parse(hourOn + ":" + (60 - duration)) : timeOn;
                }
                else
                {
                    timeBefore = TimeSpan.Parse(hourOn + 1 + ":" + timeOn.Minutes);
                }
                var timeEnd = TimeSpan.Parse(hourEnd + ":" + (60 - duration));
                for (int i = 0; i < hourDuration; i++)//cho phep tinh tang them 1h hay khong?
                {
                    var hour = hourOn + i;
                    for (int j = 0; j < minusEnd; j++)
                    {
                        var minusStep = duration * j;
                        var currentStep = TimeSpan.Parse(hour + ":" + minusStep);
                        if (timeBefore < currentStep && currentStep <= timeEnd)
                        {
                            k++;
                            var stepNext = TimeSpan.Parse(hour + ":" + minusStep);
                            timeBefore = stepNext;
                            var dicDate = new Dictionary<string, object>
                            {
                                {"Id",k},
                                {"Value",stepNext.ToString(@"hh\:mm")}
                                ,{"Text","Later"}
                            };
                            lstTime.Add(dicDate);
                        }
                    }
                }
            }


            var arrangeDate = new List<int> { 1, 2 };
            var lstArrangeDate = new List<Dictionary<string, object>>();

            var dicTimeNow = new Dictionary<string, object>
            {
                {"Id",0},
                {"Value",DateTime.Parse(DateNow).ToString("dd/MM/yyyy")}
                ,{"Text","Today"}
            };
            lstArrangeDate.Add(dicTimeNow);
            for (int i = 0; i < arrangeDate.Count; i++)
            {
                var arangeDate1 = arrangeDate[i];
                var date = DateTime.Parse(DateNow).AddDays(arangeDate1);

                var dicTime = new Dictionary<string, object>
                {
                    {"Id",arangeDate1},
                    {"Value",date.ToString("dd/MM/yyyy")}
                    ,{"Text","Later"}
                };

                lstArrangeDate.Add(dicTime);
            }
            var dic = new Dictionary<string, object>
        {
            {"Dates",lstArrangeDate}
            ,{"Times",lstTime}
        };
            lst.Add(dic);
        }
        catch (Exception)
        {
        }

        return Task.FromResult(lst);
    }

    //Co the se tach 2 phan nay ra de pick thogian dung voi ngay dc chon.
    //IsNow: true- lấy thời gian hiện tại để tính realtime cho arrangetime cần lấy.
    // public static async Task<List<Dictionary<string, object>>> ArrangeTime(string TimeOn = "08:00",
    //     string TimeOff = "22:00", int duration = 15, bool IsNow = false, List<TimeExclude> arangeExludeList = null)
    // {
    //     var lst = new List<Dictionary<string, object>>();
    //     try
    //     {

    //         var DateNow = LibConvert.Get_ByDateTimeNow();
    //         var now = TimeSpan.Parse(DateTime.Parse(DateNow).ToString("HH:mm"));
    //         var timeOn = TimeSpan.Parse(TimeOn);
    //         var timeOff = TimeSpan.Parse(TimeOff);
    //         var dicDateNow = new Dictionary<string, object>();
    //         if (IsNow)
    //         {
    //             if (now >= timeOn && now < timeOff)
    //             {
    //                 #region Exlude Time Now in TimeArangeExlude
    //                 bool isContinue = true;
    //                 if (arangeExludeList != null && arangeExludeList.Any())
    //                 {
    //                     var arangeExludeExist =
    //                         arangeExludeList.Where(x => x.TimeOn < now && now < x.TimeOff).ToList();
    //                     if (arangeExludeExist.Any())
    //                     {
    //                         isContinue = false;
    //                     }
    //                 }
    //                 #endregion

    //                 if (isContinue)
    //                 {
    //                     dicDateNow = new Dictionary<string, object>
    //                     {
    //                         {"Id", 0},
    //                         {"IsNow", true},
    //                         {"Value", DateTime.Parse(DateNow).ToString("HH:mm")}
    //                         ,{"Label", "Now"}
    //                         ,{"Text", "Now"}
    //                     };
    //                     lst.Add(dicDateNow);
    //                 }
    //             }
    //         }

    //         var k = 0;
    //         int v = 60 / duration;
    //         var minusEnd = Math.Ceiling(double.Parse(v.ToString()));
    //         var hoursOn = timeOn.Hours;
    //         var hoursOff = timeOff.Hours + 1;
    //         var end = hoursOff - hoursOn;
    //         for (int i = 0; i < end; i++)
    //         {
    //             var hour = hoursOn + i;
    //             for (int j = 0; j < minusEnd; j++)
    //             {
    //                 var minusStep = duration * j;
    //                 var currentStep = TimeSpan.Parse(hour + ":" + minusStep);

    //                 #region Exlude Time Arange List
    //                 bool isContinue = true;
    //                 if (arangeExludeList != null && arangeExludeList.Any())
    //                 {
    //                     if ((from arangeExlude in arangeExludeList select arangeExlude.TimeArangeList ?? new ArangoList<TimeSpan>() into lstTimeSpan where lstTimeSpan.Any() select lstTimeSpan.Where(x => x.Equals(currentStep)).ToList()).Any(timeSpanExludeExist => timeSpanExludeExist.Any()))
    //                     {
    //                         isContinue = false;
    //                     }
    //                 }
    //                 #endregion

    //                 if (isContinue)
    //                 {
    //                     if (((IsNow && now < currentStep && currentStep.Subtract(now).TotalMinutes >= duration &&
    //                           currentStep >= timeOn) || (!IsNow && currentStep >= timeOn)) &&
    //                         currentStep <= timeOff)
    //                     {
    //                         k++;
    //                         var dicDate = new Dictionary<string, object>
    //                         {
    //                             {"Id", k},
    //                             {"IsNow", false},
    //                             {"Value", currentStep.ToString(@"hh\:mm")}
    //                             ,{"Label", "Later"},
    //                             {"Text", currentStep.ToString(@"hh\:mm")}
    //                         };
    //                         lst.Add(dicDate);
    //                     }
    //                 }
    //             }
    //         }

    //     }
    //     catch (Exception e)
    //     {
    //     }
    //     return lst;
    // }

    public static Dictionary<string, object> ArrangeDate(int day = 0, DateModel dateSetting = null)
    {
        var DateNow = LibConvert.Get_ByDateTimeNow();
        var date = DateTime.Parse(DateNow).AddDays(day);
        //var Label = day.Equals(0) ? "Today" : day.Equals(1) ? "Tomorrow" : "Next Day";
        //var Text = day.Equals(0) ? "Today" : day.Equals(1) ? "Tomorrow" : "Next Day";
        var Label = day.Equals(0) ? "Today" : day.Equals(1) ? "Tomorrow" : "Next Day";
        var Text = day.Equals(0) ? "Hôm nay" : day.Equals(1) ? "Ngày mai" : "Ngày tiếp theo";
        if (dateSetting != null && !string.IsNullOrEmpty(dateSetting.Date1) && !string.IsNullOrEmpty(dateSetting.Date2) && !string.IsNullOrEmpty(dateSetting.Date3))
        {
            Label = day.Equals(0) ? dateSetting.Date1 : day.Equals(1) ? dateSetting.Date2 : dateSetting.Date3;
            Text = day.Equals(0) ? dateSetting.Date1 : day.Equals(1) ? dateSetting.Date2 : dateSetting.Date3;
        }
        var id = day;
        var dicTime = new Dictionary<string, object>
        {
            {"Id", id},
            {"Value", date.ToString("dd/MM/yyyy")}, {"Label", Label}, {"Text", Text}
        };
        return dicTime;
    }

    /// <summary>
    /// Parses a prefix number to double. Unit is ignored.
    /// Prefixes greater than 1 ([K]ilo, [G]ega etc) etc are case insensitive.
    /// (However, m is milli, M is Mega)
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static decimal ParsePrefix(string value)
    {
        string[] superSuffix = new string[] { "K", "M", "G", "T", "P", "A", };
        string[] subSuffix = new string[] { "m", "u", "n", "p", "f", "a" };
        char sufChar = '\0';
        foreach (char c in value)
        {
            foreach (string s in subSuffix)
                if (c.ToString() == s)
                {
                    sufChar = c;
                    string num = value.Substring(0, value.IndexOf(sufChar));
                    return decimal.Parse(num) / (decimal)Math.Pow(1000, subSuffix.ToList().IndexOf(sufChar.ToString()) + 1);
                }
            foreach (string s in superSuffix)
                if (c.ToString().ToLower() == s.ToLower())
                {
                    sufChar = s[0];
                    string num = value.Substring(0, value.IndexOf(c));
                    decimal mult = (decimal)Math.Pow(1000, superSuffix.ToList().IndexOf
                        (sufChar.ToString()) + 1);
                    return decimal.Parse(num) * mult;
                }
        }
        return decimal.Parse(value);
    }

    /// <summary>
    /// Selects a suitable prefix for the value to keep 1-3 significant figures
    /// on the left of the decimal point.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="unit">Optional unit to be put after the prefix</param>
    /// <returns></returns>
    public static string AddPrefix(decimal value, string unit = "")
    {
        string[] superSuffix = new string[] { "K", "M", "G", "T", "P", "A", };
        string[] subSuffix = new string[] { "m", "u", "n", "p", "f", "a" };
        decimal v = value;
        int exp = 0;
        while (v - Math.Floor(v) > 0)
        {
            if (exp >= 18)
                break;
            exp += 3;
            v *= 1000;
            v = Math.Round(v, 12);
        }

        while (Math.Floor(v).ToString().Length > 3)
        {
            if (exp <= -18)
                break;
            exp -= 3;
            v /= 1000;
            v = Math.Round(v, 12);
        }
        if (exp > 0)
            return v.ToString() + subSuffix[exp / 3 - 1] + unit;
        else if (exp < 0)
            return v.ToString() + superSuffix[-exp / 3 - 1] + unit;
        return v.ToString() + unit;
    }

    //pick color random
    public static string GetRandColor()
    {
        Random rnd = new Random();
        string hexOutput = string.Format("{0:X}", rnd.Next(0, 0xFFFFFF));
        while (hexOutput.Length < 6)
            hexOutput = "0" + hexOutput;
        return "#" + hexOutput;
    }

    //public static string QrCode(string text)
    //{
    //    QRCodeGenerator qrGenerator = new QRCodeGenerator();
    //    QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.M);
    //    QRCode qrCode = new QRCode(qrCodeData);
    //    //Base64QRCode qrCode = new Base64QRCode(qrCodeData);
    //    //string base64 = qrCode.GetGraphic(20, Color.Black, Color.White, true);
    //    //var qrCodeImage = "data:image/" + ImageFormat.Jpeg + ";base64," + base64;
    //    var qrCodeImage = qrCode.GetGraphic(20, Color.Black, Color.White, true);
    //    return qrCodeImage;
    //}

    public static string parseDate(string value, string format = "dd/MM/yyyy HH:mm")
    {
        string[] formats =
        {
            "dd/MM/yyyy"
            , "dd/M/yyyy"
            , "d/M/yyyy"
            , "d/MM/yyyy"
            , "dd/MM/yy"
            , "dd/M/yy"
            , "d/M/yy"
            , "d/MM/yy"
            , "MM/dd/yyyy"
            , "dd-MM-yyyy"
            , "MM-dd-yyyy"
            , "yyyy-MM-dd"
            , "yyyy-dd-MM"
            , "yyyy/MM/dd"
            , "yyyy/dd/MM"
            , "dd/MM/yyyy HH:mm"
            , "dd/M/yyyy HH:mm"
            , "d/M/yyyy HH:mm"
            , "d/MM/yyyy HH:mm"
            , "dd/MM/yy HH:mm"
            , "dd/M/yy HH:mm"
            , "d/M/yy HH:mm"
            , "d/MM/yy HH:mm"

            , "MM/dd/yyyy HH:mm"
            , "dd-MM-yyyy HH:mm"
            , "MM-dd-yyyy HH:mm"
            , "yyyy-MM-dd HH:mm"
            , "yyyy-dd-MM HH:mm"
            , "yyyy/MM/dd HH:mm"
            , "yyyy/dd/MM HH:mm"

            , "dd/MM/yyyy HH:mm:ss"
            , "dd/M/yyyy HH:mm:ss"
            , "d/M/yyyy HH:mm:ss"
            , "d/MM/yyyy HH:mm:ss"
            , "dd/MM/yy HH:mm:ss"
            , "dd/M/yy HH:mm:ss"
            , "d/M/yy HH:mm:ss"
            , "d/MM/yy HH:mm:ss"
            , "MM/dd/yyyy HH:mm:ss"
            , "dd-MM-yyyy HH:mm:ss"
            , "MM-dd-yyyy HH:mm:ss"
            , "yyyy-MM-dd HH:mm:ss"
            , "yyyy-dd-MM HH:mm:ss"
            , "yyyy/MM/dd HH:mm:ss"
            , "yyyy/dd/MM HH:mm:ss"
            , "yyyy-MM-dd'T'HH:mm:ss.fffffff'Z'"
            , "yyyy-MM-dd'T'HH:mm:ss.fffffff"
            , "yyyy-MM-dd HH:mm:ss.fffffff"
            , "yyyy-MM-dd'T'HH:mm:ss'Z'"
        };
        DateTime.TryParseExact(value, formats,
            CultureInfo.InvariantCulture,
            DateTimeStyles.None, out var dd);

        var date = dd.ToString(format);
        return date;
    }

    public static string ConvertToSizeImage(string url, int sizeType = 1)
    {
        var strUrl = url;
        var sizeDefault = "342_342";
        switch (sizeType)
        {
            case 1:
                strUrl = url.Replace(sizeDefault, "116_116");
                break;
            case 2:
                strUrl = url.Replace(sizeDefault, "120_120");
                break;
            case 3:
                strUrl = url.Replace(sizeDefault, "127_127");
                break;
            case 4:
                strUrl = url.Replace(sizeDefault, "200_200");
                break;
            case 5:
                strUrl = url.Replace(sizeDefault, "232_232");
                break;
            case 6:
                strUrl = url.Replace(sizeDefault, "252_252");
                break;
            case 7:
                strUrl = url.Replace(sizeDefault, "342_342");
                break;
            case 8:
                strUrl = url.Replace(sizeDefault, "400_400");
                break;
            case 9:
                strUrl = url.Replace(sizeDefault, "600_600");
                break;
            default:
                break;
        }
        return strUrl;
    }

    public static DateTime parseStringToDate(string value, string format = "yyyy-MM-dd HH:mm:ss")
    {
        return DateTime.Parse(parseDate(value, format));
    }

    public static Name parseName(string fullName)
    {
        var nameParser = new Name();
        if (!string.IsNullOrEmpty(fullName))
        {
            try
            {
                nameParser = _nameParser.Parse(fullName);
            }
            catch (Exception e)
            {
                nameParser.LastName = fullName;
            }
        }
        return nameParser;
    }

    public static PhoneNumber parsePhone(string phone, string regionCode = "VN")
    {
        var number = new PhoneNumber();
        try
        {
            if (phone != "")
            {
                var phoneNumberUtil = PhoneNumberUtil.GetInstance();

                number = phoneNumberUtil.Parse(phone, regionCode);
            }
        }
        catch (Exception e)
        {
        }
        return number;
    }

    public static string ReplaceTokens(Dictionary<string, string> tokens, string content)
    {
        foreach (var token in tokens.Keys)
        {
            content = content.Replace(token, tokens[token]);
        }

        return content;
    }

    public static bool HtmlValidate(string str)
    {
        Regex regex = new Regex(@"<[^>]+>");
        return regex.IsMatch(str);
    }
    public static bool EmailValidate(string email)
    {
        var regex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        bool isValid = Regex.IsMatch(email, regex, RegexOptions.IgnoreCase);
        return isValid;
    }
    public static bool PhoneValidate(string phone)
    {
        var isValid = false;
        var phoneParse = phone != "" ? parsePhone(phone) : new PhoneNumber();
        if (phoneParse.NationalNumber > 0)
        {
            phone = phoneParse.NationalNumber.ToString();
            phone = phone.StartsWith("0") ? phone : "0" + phone;
            var regex = @"^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$";
            isValid = Regex.IsMatch(phone, regex, RegexOptions.IgnoreCase);
        }
        return isValid;
    }

    public static string GetMd5Hash(string input)
    {
        byte[] bs = Encoding.UTF8.GetBytes(input);
        Md5Managed md5 = new Md5Managed();
        byte[] hash = md5.ComputeHash(bs);

        var sb = new StringBuilder();
        foreach (byte b in hash)
        {
            sb.Append(b.ToString("x2").ToLower());
        }

        return sb.ToString();
    }

    public static readonly string _PATTERN = @"(?<=[\w]{3})[\w-\._\+%\\]*(?=@)";
    /// <summary>
    /// Masks the email.
    /// </summary>
    /// <param name="s">The s.</param>
    /// <returns>A string.</returns>
    public static string MaskEmail(this string s)
    {
        if (s != "")
        {
            if (s.Length > 5)
            {
                if (!s.Contains("@"))
                    return new string('*', s.Length);
                if (s.Split('@')[0].Length < 4)
                {
                    var s0 = s.Split('@')[0];
                    var s1 = s.Split('@')[1];
                    if (s0.Length > 0)
                        return string.Concat(s0, "******", "@", s1);
                    else
                    {
                        return @"*@*.*";
                    }
                }
            }
            return Regex.Replace(s, _PATTERN, m => new string('*', 3));
        }
        return s;
    }

    public static string MaskerPhone(string phone)
    {
        var res = "";
        if (phone != "" && phone.Length > 4)
        {
            var phoneValue = phone.Substring(phone.Length - 3, phone.Length - (phone.Length - 3));
            var phoneReformat = cleanSpecial(phone);
            var phoneReplace = Regex.Replace(phoneReformat, @"[0-9]", "*");
            var phoneMaskValue = phoneReplace.Substring(0, phoneReplace.Length - 3);
            res = phoneMaskValue + phoneValue;
        }
        return res;
    }

    public static string cleanSpecial(string phone)
    {
        Regex reg = new Regex("[*'\",_&#^@+]");
        return reg.Replace(phone, string.Empty);
    }



    /// <summary>
    /// Sorts the.
    /// </summary>
    /// <param name="jsonString">The json string.</param>
    /// <returns>A string.</returns>
    public static string Sort(string jsonString)
    {
        if (jsonString != null)
        {
            var jObj = (JsonObject)JsonNode.Parse(jsonString);
            Sort(jObj);
            var jsonObj = jObj.ToString();
            return jsonObj;
        }

        return null;
    }

    #region Private

    /// <summary>
    /// Sorts the.
    /// </summary>
    /// <param name="jObj">The j obj.</param>
    static void Sort(JsonObject jObj)
    {
        var props = jObj.ToList();
        foreach (var prop in props)
        {
            jObj.Remove(prop.Key);
        }

        foreach (var prop in props.OrderByDescending(p => p.Key))
        {
            jObj.Add(prop);
            if (prop.Value is JsonObject)
                Sort((JsonObject)prop.Value);
        }
    }

    #endregion

    /// <summary>
    /// Checks the sum.
    /// </summary>
    /// <param name="doc">The doc.</param>
    /// <returns>A string.</returns>
    public static string CheckSum(object doc, List<string> excludeField = null)
    {
        try
        {
            var jsonJtoken = JsonConvert.SerializeObject(doc);
            var sortJsonOrder = Sort(jsonJtoken);
            if (sortJsonOrder != "")
            {
                if (sortJsonOrder != null && sortJsonOrder.ToString() != "")
                {
                    var dssDic = JsonConvert.DeserializeObject<Dictionary<string, object>>(sortJsonOrder.ToString());
                    if (dssDic != null && dssDic.Any())
                    {
                        if (excludeField != null && excludeField.Any())
                        {
                            foreach (string field in from field in excludeField
                                                     where dssDic.ContainsKey(field)
                                                     select field)
                            {
                                dssDic.Remove(field);
                            }
                        }
                        //dssDic["ChecksumDate"] = DateTime.Now.ToString("yyyyMMddHHmm");
                        sortJsonOrder = JsonConvert.SerializeObject(dssDic, Formatting.Indented)
                            .Replace("\r", string.Empty);//Sử dụng để json export đến checksum không bị lỗi.và số tiền không có trailing zeros.
                    }
                }
                return Md5.Md5Func(sortJsonOrder);
            }
        }
        catch (Exception)
        {
        }
        return "";
    }

    public static List<TimeSpanObj> Arrange24h()
    {
        var timeSpanList = new List<TimeSpanObj>();
        for (int i = 0; i < 24; i++)
        {
            var from = TimeSpan.FromHours(i);
            var endMinutes = i >= 23 ? 59 : 0;
            var endHour = i >= 23 ? i : i + 1;
            var endText = i >= 24 ? i : i + 1;
            var to = TimeSpan.FromHours(endHour).Add(TimeSpan.FromMinutes(endMinutes)).Add(TimeSpan.FromSeconds(endMinutes));
            //var to = TimeSpan.FromHours(endHour);
            var timeSpanObj = new TimeSpanObj { From = from, To = to, FromText = i.ToString(), ToText = endText.ToString() };
            timeSpanList.Add(timeSpanObj);
        }
        return timeSpanList;
    }
}



public static class CloningService
{
    public static T Clone<T>(T source)
    {
        // Don't serialize a null object, simply return the default for that object
        if (ReferenceEquals(source, null))
        {
            return default;
        }

        var deserializeSettings = new JsonSerializerSettings { ObjectCreationHandling = ObjectCreationHandling.Replace };
        var serializeSettings = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
        return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source, serializeSettings), deserializeSettings);
    }
}

public static class VersionNumber
{
    //v1: version mới, v2: version cũ
    public static int versionCompare(string v1, string v2)
    {
        var useNewVersion = 0;
        // vnum stores each numeric
        // part of version
        v1 = LIB_convert.ConvertToSearchGoogle(v1, ".", true);
        v2 = LIB_convert.ConvertToSearchGoogle(v2, ".", true);
        v1 = Regex.Replace(v1, "[A-Za-z]", "");
        v2 = Regex.Replace(v2, "[A-Za-z]", "");

        string[] v1parts = v1.Split('.');
        string[] v2parts = v2.Split('.');
        if (v1parts.Length < 5 && v2parts.Length < 5)
        {
            var version1 = new Version(v1);
            var version2 = new Version(v2);

            var result = version1.CompareTo(version2);
            if (result > 0)
                useNewVersion = 1;
            else if (result < 0)
                useNewVersion = 0;
            else
                useNewVersion = 0;
        }
        else
        {
            useNewVersion = VersionCompare2(v1, v2);
        }
        return useNewVersion;
    }

    public static int VersionCompare2(string v1, string v2)
    {
        int rc = -1000;
        v1 = v1.ToLower();
        v2 = v2.ToLower();
        if (v1 == v2)
            return 0;

        string[] v1parts = v1.Split('.');
        string[] v2parts = v2.Split('.');
        for (int i = 0; i < v1parts.Length; i++)
        {
            if (v2parts.Length < i + 1)
                break; // we're done here

            rc = string.Compare(v1parts[i], v2parts[i], StringComparison.Ordinal);
            if (rc != 0)
                break;
        }

        if (rc == 0)
        {
            // catch this scenario: v1="1.0.1" v2="1.0"
            if (v1parts.Length > v2parts.Length)
                rc = 1; // v1 is higher version than v2
                        // catch this scenario: v1="1.0" v2="1.0.1"
            else if (v2parts.Length > v1parts.Length)
                rc = -1; // v1 is lower version than v2
        }

        if (rc == 0 || rc == -1000)
            return 0;
        else
            return rc < 0 ? 0 : 1;
    }
}


//public class SemiNumericComparer : IComparer<string>
//{
//    public int Compare(string s1, string s2)
//    {
//        int res = Compare(s1, s2, true);
//        return res;
//    }
//    public int Compare(string s1, string s2, bool toplevel)
//    {
//        int localRes = 0;
//        //Purely Numeric Comparisons
//        if (IsNumeric(s1) && IsNumeric(s2))
//        {
//            if (System.Convert.ToInt32(s1) > System.Convert.ToInt32(s2)) return 1;
//            if (System.Convert.ToInt32(s1) < System.Convert.ToInt32(s2)) return -1;
//            if (System.Convert.ToInt32(s1) == System.Convert.ToInt32(s2)) return 0;
//        }

//        string[] split1 = s1.Split(' ');
//        string[] split2 = s2.Split(' ');
//        //If we're dealing with words...might need to recursively call
//        if (split1.Length > 1 || split2.Length > 1)
//        {
//            if (split1.Length == 1) //only one of these has words, and it must be split2
//            {
//                localRes = Compare(s1, split2[0], false);//check s1 against the first word in s2
//                if (localRes == 0) { return -1; }//If the single word == the first word in the other..then the single wins
//                else return localRes;

//            }
//            if (split2.Length == 1) //only one of these has words, and it must be split1 
//            {
//                localRes = Compare(split1[0], s2, false);//check s2 against the first word in s1
//                if (localRes == 0) { return 1; }
//                else return localRes;
//            }

//            //If the first word is the same..lets compare the rest
//            if (split1[0] == split2[0])
//            {
//                List<string> l1 = new List<string>(split1);
//                l1.RemoveAt(0);
//                List<string> l2 = new List<string>(split2);
//                l2.RemoveAt(0);

//                return Compare(String.Join(" ", l1), string.Join(" ", l2), false);
//            }

//        }


//        if (IsNumeric(s1) && !IsNumeric(s2))
//            return -1;

//        if (!IsNumeric(s1) && IsNumeric(s2))
//            return 1;

//        return String.Compare(s1, s2, StringComparison.OrdinalIgnoreCase);
//    }

//    public static bool IsNumeric(object value)
//    {
//        try
//        {
//            int i = System.Convert.ToInt32(value.ToString());
//            return true;
//        }
//        catch (FormatException)
//        {
//            return false;
//        }
//    }
//}

//Sort Object 
public class SemiNumericComparer : IComparer<string>
{
    public int Compare(string s1, string s2)
    {
        int s1r, s2r;
        var s1n = IsNumeric(s1, out s1r);
        var s2n = IsNumeric(s2, out s2r);

        if (s1n && s2n) return s1r - s2r;
        else if (s1n) return -1;
        else if (s2n) return 1;

        var num1 = Regex.Match(s1, @"\d+$");
        var num2 = Regex.Match(s2, @"\d+$");

        var onlyString1 = s1.Remove(num1.Index, num1.Length);
        var onlyString2 = s2.Remove(num2.Index, num2.Length);

        if (onlyString1 != onlyString2) return string.Compare(s1, s2, true);
        if (num1.Success && num2.Success) return System.Convert.ToInt32(num1.Value) - System.Convert.ToInt32(num2.Value);
        else if (num1.Success) return 1;
        else if (num2.Success) return -1;

        return string.Compare(s1, s2, true);
    }

    public bool IsNumeric(string value, out int result)
    {
        return int.TryParse(value, out result);
    }
}

public class ItemEqualityComparer : IEqualityComparer<Item>
{
    public bool Equals(Item x, Item y)
    {
        // Two items are equal if their keys are equal.
        return x.Key == y.Key;
    }

    public int GetHashCode(Item obj)
    {
        return obj.Key.GetHashCode();
    }
}

public class Item
{
    public int Key;
    public string Value;
}

/// <summary>
/// The time span obj.
/// </summary>
public partial class TimeSpanObj
{
    public TimeSpan From { get; set; }
    public TimeSpan To { get; set; }
    public string FromText { get; set; }
    public string ToText { get; set; }
}