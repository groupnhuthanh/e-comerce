using Microsoft.IdentityModel.Tokens;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace Helpers.Convert;

public static class libs_Tokens
{
    static HMACSHA256 hmac = new HMACSHA256();
    //private static string Secret = "XCAP05H6LoKvbRRa/QkqLNMI7cOHguaRyHzyg7n5qEkGjQmtBhz4SzYh4Fqwjyi3KJHlSXKPwVu2+bXr6CtpgQ==";
    private static string Secret = "J1f66U54BWikTPIIx3NOsqcHfP31QEkgFgybVQFgfH0QN+JlXX72eVN5ngb+MmNWHErCEi3yRQTvq9n5sca33Q==";
    static string _key = System.Convert.ToBase64String(hmac.Key);
    public static string GenerateToken(string domain, string UserGuid, string DeviceGuid, string LocationGuid, string keyDevice, string expiredDate, int DeviceType = 1, double expired = 0)
    {
        CultureInfo iv = CultureInfo.InvariantCulture;
        byte[] key = System.Convert.FromBase64String(Secret);
        var securityKey = new SymmetricSecurityKey(key);
        //var expiredDateSession = !string.IsNullOrEmpty(expiredDate)
        //    ? DateTime.Parse(expiredDate) : DateTime.UtcNow.AddHours(7);
        var expiredDateSession = DateTime.UtcNow;
        var ExpiredDate = expiredDateSession.AddMilliseconds(expired);
        var descriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("UserGuid", UserGuid),
                new Claim("DeviceGuid", DeviceGuid),
                new Claim("LocationGuid", LocationGuid),
                new Claim("KeyDevice", keyDevice),
                new Claim("DeviceType", DeviceType.ToString()),
            }),
            Expires = ExpiredDate,
            Issuer = domain,
            Audience = domain,
            NotBefore = new DateTimeOffset(expiredDateSession).DateTime,
            SigningCredentials = new SigningCredentials(securityKey,
                SecurityAlgorithms.HmacSha256Signature)
        };

        var handler = new JwtSecurityTokenHandler();
        var token = handler.CreateJwtSecurityToken(descriptor);
        return handler.WriteToken(token);
    }

    public static string RefreshToken(string domain, string expiredDate, double expired = 0, string _id = "")
    {
        CultureInfo iv = CultureInfo.InvariantCulture;
        byte[] key = System.Convert.FromBase64String(Secret);
        var securityKey = new SymmetricSecurityKey(key);
        var expiredDateSession = !string.IsNullOrEmpty(expiredDate)
            ? DateTime.Parse(expiredDate) : DateTime.UtcNow.AddHours(7);
        var ExpiredDate = expiredDateSession.AddMilliseconds(expired);
        var descriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("_id", _id)
            }),
            Expires = ExpiredDate,
            Issuer = domain,
            Audience = domain,
            NotBefore = new DateTimeOffset(DateTime.Now).DateTime,
            SigningCredentials = new SigningCredentials(securityKey,
                SecurityAlgorithms.HmacSha256Signature)
        };

        var handler = new JwtSecurityTokenHandler();
        var token = handler.CreateJwtSecurityToken(descriptor);
        return handler.WriteToken(token);
    }

    /// <summary>
    /// Validated Token
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    public static Dictionary<string, object> ValidateToken(string token)
    {
        var dic = new Dictionary<string, object>();
        var principal = GetPrincipal(token);
        if (principal == null)
            return null;
        ClaimsIdentity identity = null;
        try
        {
            identity = (ClaimsIdentity)principal.Identity;
        }
        catch (NullReferenceException)
        {
            return null;
        }

        var token_id = identity?.FindFirst("Token_id")?.Value;
        var UserGuid = identity?.FindFirst("UserGuid")?.Value;
        var DeviceGuid = identity?.FindFirst("DeviceGuid")?.Value;
        var LocationGuid = identity?.FindFirst("LocationGuid")?.Value;
        var CustomerGuid = identity?.FindFirst("CustomerGuid")?.Value;
        var KeyDevice = identity?.FindFirst("KeyDevice")?.Value;
        var DeviceType = identity?.FindFirst("DeviceType")?.Value;
        var Expired = identity?.FindFirst("Expired")?.Value;
        dic = new Dictionary<string, object>
        {
            {"Token_id",token_id},
            {"UserGuid",UserGuid},
            {"DeviceGuid",DeviceGuid},
            {"LocationGuid",LocationGuid},
            {"CustomerGuid",CustomerGuid},
            {"KeyDevice",KeyDevice},
            {"DeviceType",DeviceType}
            ,{"Expired",Expired}
        };
        return dic;
    }

    public static ClaimsPrincipal GetPrincipal(string token)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
            if (jwtToken == null)
                return null;
            byte[] key = System.Convert.FromBase64String(Secret);
            var parameters = new TokenValidationParameters()
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false,
                ValidateIssuerSigningKey = false,
                RequireExpirationTime = false,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ClockSkew = TimeSpan.Zero
            };
            var principal = tokenHandler.ValidateToken(token, parameters, out _);
            return principal;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}