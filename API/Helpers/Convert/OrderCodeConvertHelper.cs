﻿namespace Helpers.Convert;

/// <summary>
/// The order code convert helper.
/// </summary>
public static class OrderCodeConvertHelper
{
    /// <summary>
    /// Tos the encoder.
    /// </summary>
    /// <param name="orderCode">The order code.</param>
    /// <param name="length">The length.</param>
    /// <returns>A string.</returns>
    public static string ToEncoder(int length = 8)
    {
        var random = new Random();
        var rd = random.Next(999999);
        var Now = DateTime.Now.AddHours(7);
        var dto = new DateTimeOffset(Now.ToUniversalTime());
        var plainText = rd + dto.ToUnixTimeMilliseconds();
        var encodeNumber = EncodeHelper.EncodeNumber(plainText.ToString(), 0, length);
        return encodeNumber;
    }
}

/// <summary>
/// The encode helper.
/// </summary>
public class EncodeHelper
{
    private static readonly string alphanums = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
    private static readonly string number = "0123456789";
    private static readonly string alphaToUpper = "ABCDEFGHIJKLMN0PQRSTUVWXYZ";
    private static readonly string alphaToLower = "abcdefghijklmnopqrstuvwxyz";
    private static readonly string alphaToSpecial = ",.;:?!/@#$%^&()=+*-_{}[]<>|~";
    private static readonly string alphaFull = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN0PQRSTUVWXYZ,.;:?!/@#$%^&()=+*-_{}[]<>|~";
    private static readonly int _codeLen = 8;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="num">plain text</param>
    /// <param name="characterType">1: number, 2: Alpha to Upper, 3: Alpha to Lower, 4: Alpha to Special, 5: Full</param>
    /// <returns></returns>
    public static string EncodeNumber(string num, int characterType = 0, int codelen = 0)
    {
        if (num == "") //or throw an exception
            return "";
        //var maxLength = num.Length - num.Length;
        var maxLength = 0;
        //switch (num.Length)
        //{
        //    case 11:
        //        codeLen = _codeLen + 1;
        //        break;
        //}
        string alphaNums;
        switch (characterType)
        {
            case 1:
                alphaNums = number;
                break;
            case 2:
                alphaNums = alphaToUpper;
                break;
            case 3:
                alphaNums = alphaToLower;
                break;
            case 4:
                alphaNums = alphaToSpecial;
                break;
            case 5:
                alphaNums = alphaFull;
                break;
            default:
                alphaNums = alphanums;
                break;
        }
        var codeLen = codelen > 0 ? codelen : maxLength > 0 ? _codeLen + maxLength : _codeLen;
        var nums = new long[codeLen];
        var pos = 0;
        var N = long.Parse(num);
        while (N != 0)
        {
            nums[pos] = N % alphaNums.Length;
            N /= alphaNums.Length;
            pos += 1;
        }
        return nums.Cast<long>().Aggregate("", (current, numIndex) => alphaNums[(int)numIndex].ToString() + current);
    }

    public static long DecodeNumber(string str, int characterType = 1, int codelen = 0)
    {
        //Check for invalid string
        //var codeLen = _codeLen;
        var maxLength = 0;
        string alphaNums;
        switch (characterType)
        {
            case 1:
                alphaNums = number;
                break;
            case 2:
                alphaNums = alphaToUpper;
                break;
            case 3:
                alphaNums = alphaToLower;
                break;
            case 4:
                alphaNums = alphaToSpecial;
                break;
            case 5:
                alphaNums = alphaFull;
                break;
            default:
                alphaNums = alphanums;
                break;
        }
        var codeLen = codelen > 0 ? codelen : _codeLen + maxLength;

        if (str.Length != codeLen) //Or throw an exception
            return -1;
        long num = 0;

        foreach (char ch in str)
        {
            num *= alphaNums.Length;
            num += alphaNums.IndexOf(ch);
        }

        //Check for invalid number
        if (num < 0) //or throw exception
            return -1;
        return num;
    }
}
