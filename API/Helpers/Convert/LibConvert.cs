using Helpers.Globals;
using System.Globalization;
using TimeZoneConverter;
namespace Helpers.Convert;

public static class LibConvert
{

    public static string Get_ByDateNow(string TimeZoneId = "SE Asia Standard Time")
    {
        var Now = DateTime.Now.AddHours(7).ToString("yyyy-MM-dd");
        CultureInfo iv = CultureInfo.InvariantCulture;
        var dateNow = DateTime.Now;
        //var windowsTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId);
        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(TimeZoneId);
        //string tz = TZConvert.WindowsToIana(TimeZoneId);//Window to Linux
        var dataTimeByZoneId = TimeZoneInfo.ConvertTime(dateNow, TimeZoneInfo.Local, tzi);
        Now = dataTimeByZoneId.ToString("yyyy-MM-dd") ?? Now;
        return Now;
    }


    public static string Get_ByDateTimeNow(string TimeZoneId = "SE Asia Standard Time")
    {
        var Now = DateTime.Now.AddHours(7).ToString("yyyy-MM-dd HH:mm:ss.fffffff");
        CultureInfo iv = CultureInfo.InvariantCulture;
        var dateNow = DateTime.Now;
        //var windowsTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId);
        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(TimeZoneId);
        //string tz = TZConvert.WindowsToIana(TimeZoneId);//Window to Linux
        var dataTimeByZoneId = TimeZoneInfo.ConvertTime(dateNow, TimeZoneInfo.Local, tzi);
        Now = dataTimeByZoneId.ToString("yyyy-MM-dd HH:mm:ss.fffffff") ?? Now;
        return Now;
    }

    public static long Get_ByDateTimeNowLong(string TimeZoneId = "SE Asia Standard Time")
    {
        var Now = long.Parse(DateTime.Now.AddHours(7).ToString("yyMMddHHmmssff"));
        if (Now <= 0)
        {
            CultureInfo iv = CultureInfo.InvariantCulture;
            var dateNow = DateTime.Now;
            //var windowsTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId);
            TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(TimeZoneId);
            //string tz = TZConvert.WindowsToIana(TimeZoneId);//Window to Linux
            var dataTimeByZoneId = TimeZoneInfo.ConvertTime(dateNow, TimeZoneInfo.Local, tzi);
            Now = Now > 0 ? Now : long.Parse(dataTimeByZoneId.ToString("yyMMddHHmmssff"));
        }
        return Now;
    }

    public static DateTime Get(string TimeZoneId = "SE Asia Standard Time")
    {
        CultureInfo iv = CultureInfo.InvariantCulture;
        var dateNow = DateTime.Now;
        //var windowsTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId);
        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(TimeZoneId);
        //string tz = TZConvert.WindowsToIana(TimeZoneId);//Window to Linux
        var dataTimeByZoneId = TimeZoneInfo.ConvertTime(dateNow, TimeZoneInfo.Local, tzi);
        var Now = dataTimeByZoneId;
        return Now;
    }

    public static string ConvertToUtc(DateTime dateTime, string TimeZoneId = "UTC", string TimeZoneIdTo = "SE Asia Standard Time")
    {
        var tziAsia = TZConvert.GetTimeZoneInfo(TimeZoneIdTo);
        var baseUtcToOffset = tziAsia.BaseUtcOffset.TotalMilliseconds;
        var tzi = TZConvert.GetTimeZoneInfo(TimeZoneId);
        var baseUtcFromOffset = tzi.BaseUtcOffset.TotalMilliseconds;
        return TimeZoneInfo.ConvertTime(dateTime.AddMilliseconds(baseUtcToOffset).ToUniversalTime(), TimeZoneInfo.Utc, tzi).ToString("yyyy-MM-ddTHH:mm:ss.szzz");
    }

    public static DateTimeOffset ConvertToUtcNow(string TimeZoneId = "UTC", string TimeZoneIdTo = "SE Asia Standard Time")
    {
        var dateTime = DateTime.Now;
        var tzi = TZConvert.GetTimeZoneInfo(TimeZoneId);
        var tziAsia = TZConvert.GetTimeZoneInfo(TimeZoneIdTo);
        var baseUtcToOffset = -tziAsia.BaseUtcOffset.TotalMilliseconds;
        return TimeZoneInfo.ConvertTime(dateTime.AddMilliseconds(baseUtcToOffset).ToUniversalTime(), TimeZoneInfo.Utc, tzi);
    }

    public static async Task<string> DateTimeFromUtc(DateTime dateTime, string TimeZoneIdTo = "SE Asia Standard Time")
    {
        var tziAsia = TZConvert.GetTimeZoneInfo(TimeZoneIdTo);
        var baseUtcToOffset = -tziAsia.BaseUtcOffset.TotalMilliseconds;
        var utcOffset = new DateTimeOffset(dateTime.AddMilliseconds(baseUtcToOffset), TimeSpan.Zero);
        var now = utcOffset.ToOffset(tziAsia.GetUtcOffset(utcOffset)).ToString("yyyy-MM-ddTHH:mm:ss.szzz");
        return now;
    }

    public static async Task<DateTime> DateTimeParseFromUtc(DateTime dateTime, string TimeZoneIdTo = "SE Asia Standard Time")
    {
        var tziAsia = TZConvert.GetTimeZoneInfo(TimeZoneIdTo);
        var baseUtcToOffset = -tziAsia.BaseUtcOffset.TotalMilliseconds;
        var utcOffset = new DateTimeOffset(dateTime.AddMilliseconds(baseUtcToOffset), TimeSpan.Zero);
        var now = utcOffset.ToOffset(tziAsia.GetUtcOffset(utcOffset)).DateTime;
        return now;
    }

    public static DateTimeOffset ConvertToOffset(DateTime dateTime, string TimeZoneId = "UTC", string TimeZoneIdTo = "SE Asia Standard Time")
    {
        var utc = DateTimeOffset.UtcNow;
        try
        {
            var tziAsia = TZConvert.GetTimeZoneInfo(TimeZoneIdTo);
            var baseUtcToOffset = tziAsia.BaseUtcOffset.TotalMilliseconds;
            var tzi = TZConvert.GetTimeZoneInfo(TimeZoneId);
            var baseUtcFromOffset = tzi.BaseUtcOffset.TotalMilliseconds;
            dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);

            var utcOffset = new DateTimeOffset(dateTime.AddMilliseconds(baseUtcFromOffset), TimeSpan.Zero);
            utc = utcOffset.ToOffset(tziAsia.GetUtcOffset(utcOffset));
        }
        catch (Exception)
        {
        }

        return utc;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <param name="type">1: day, 2: hours, 3: minutes, 4: seconds, 5 miliseconds, default: hours</param>
    /// <returns></returns>
    public static async Task<double> ConvertToMilliseconds(int value = 0, int type = 0)
    {
        value = value > 0 ? value : GlobalsHelper.TokenExpireIn;
        type = type > 0 ? type : GlobalsHelper.ExpireType;
        double millisecond = 0;
        switch (type)
        {
            case 1://day
                millisecond = TimeSpan.FromDays(value).TotalMilliseconds;
                break;
            case 2://hours
                millisecond = TimeSpan.FromHours(value).TotalMilliseconds;
                break;
            case 3://minutes
                millisecond = TimeSpan.FromMinutes(value).TotalMilliseconds;
                break;
            case 4://seconds
                millisecond = TimeSpan.FromSeconds(value).TotalMilliseconds;
                break;
            case 5://miliseconds
                millisecond = TimeSpan.FromMilliseconds(value).TotalMilliseconds;
                break;
            default:
                millisecond = TimeSpan.FromHours(value).TotalMilliseconds;
                break;
        }

        return millisecond;
    }
}

/// <summary>
/// The date extension.
/// </summary>
//public static class DateExtension
//{
//    /// <summary>
//    /// Tos the unix time.
//    /// </summary>
//    /// <param name="dateTime">The date time.</param>
//    /// <returns>A long.</returns>
//    public static long ToUnixTime(this DateTime dateTime)
//    {
//        var Now = dateTime.AddHours(7);
//        DateTimeOffset dto = new DateTimeOffset(Now.ToUniversalTime());
//        return dto.ToUnixTimeSeconds();
//    }

//    // Convert datetime to UNIX time including miliseconds
//    /// <summary>
//    /// Tos the unix time milli seconds.
//    /// </summary>
//    /// <param name="dateTime">The date time.</param>
//    /// <returns>A long.</returns>
//    public static long ToUnixTimeMilliSeconds(this DateTime dateTime)
//    {
//        var Now = dateTime.AddHours(7);
//        DateTimeOffset dto = new DateTimeOffset(Now.ToUniversalTime());
//        return dto.ToUnixTimeMilliseconds();
//    }
//}