﻿using Entities.ExtendModels.Authentication.Password;
using Entities.ExtendModels.DateToText;
using HashidsNet;
using Newtonsoft.Json;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Helpers.Convert;

public static class ConvertHelper
{
    
    public static string Now() => DateTime.Now.ToString("o");

    public static string Serialize(object obj) => JsonConvert.SerializeObject(obj);
    public static T Deserialize<T>(string str) => JsonConvert.DeserializeObject<T>(str);
    public static string ToLower(this string str) => string.Join("", str.Split(' ')).Trim().ToLower();
    public static string ToCapitalize(this string str) => CultureInfo.InvariantCulture.TextInfo.ToTitleCase(str.ToLower());
    public static string DecimalToCurrency(this decimal money, string format = "#,###") => money.ToString(format) ?? "0";
    public static string IntToCurrency(this int value, string format = "#,###") => value.ToString(format) ?? "0";
    public static string ToCode(this string value, int numberFormat, string prefix = "", string hyphen = "")
    {
        if (value != null && value != "")
        {
            var maxCharacter = value.Length > numberFormat ? numberFormat + (value.Length - numberFormat) : numberFormat;
            var valueFormat = value.PadLeft(maxCharacter, '0');
            return string.Concat(prefix, hyphen, valueFormat);
        }
        return default;
    }
    public static string ToAcronymn(this string str)
    {
        var name = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(str.ToLower());
        return string.Join(string.Empty, Regex.Match(name, "(?:([A-Z]+)(?:[^A-Z]*))*").Groups[1].Captures.Cast<Capture>().Select(p => p.Value));
    }

    public static bool ToRegrexMessage(this string value)
    {
        var regex = new Regex("[a-zA-Z0-9-_.~%]+");
        return regex.IsMatch(value);
    }

    public static PasswordModel PasswordEncrypt(string password)
    {
        //https://github.com/trichards57/zxcvbn-cs
        var generate = Zxcvbn.Core.EvaluatePassword(password);
        double calc = generate.CrackTime.OfflineFastHashing1e10PerSecond / double.MaxValue * 100.000d;
        var result = new PasswordModel
        {
            Password = calc.ToString(CultureInfo.InvariantCulture),
            Calc = calc,
            Score = generate.Score,
        };
        return result;
    }

    public static string ToHashId(this long key)
    {
        if (key > 0)
        {
            var hashids = new Hashids("", 0, "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789");
            return hashids.EncodeLong(key);
        }
        return "";
    }

    public static DateToTextModel DateToText(string startDate, string endDate)
    {
        var result = new DateToTextModel();
        if (startDate != "" || endDate != "")
        {
            startDate = startDate.Trim();
            endDate = endDate.Trim();
            var currentDate = DateTime.Now;
            var fromStartDate = startDate != "" ? DateTime.Parse(startDate) : currentDate;
            var fromEndDate = endDate != "" ? DateTime.Parse(endDate) : currentDate;
            double startTimeTotal = 0;
            double endTimeTotal = 0;
            var dateOfWeekStart = 0;
            var dateOfWeekEnd = 0;

            if (startDate != "")
            {
                startTimeTotal = fromStartDate.Subtract(currentDate).TotalHours;
                dateOfWeekStart = (int)fromStartDate.DayOfWeek;
            }
            if (endDate != "")
            {
                endTimeTotal = fromEndDate.Subtract(currentDate).TotalHours;
                dateOfWeekEnd = (int)fromEndDate.DayOfWeek;
            }

            if (endTimeTotal < 0)
            {
                result.DateText = "Kết thúc sự kiện";
                result.HasEnded = true;
                return result;
            }

            var hour = 24;
            var weekTotal = 7 * hour;
            var dateOfWeekStartText = WeekOfDay(dateOfWeekStart);
            var dateOfWeekEndText = WeekOfDay(dateOfWeekEnd);
            var dateStartText = "";
            var dateEndText = "";

            if (startTimeTotal < hour)
            {
                dateOfWeekStartText = "Hôm Nay";
            }
            else if (startTimeTotal >= hour && startTimeTotal < hour * 2)
            {
                dateOfWeekStartText = "Ngày Mai";
            }
            else if (startTimeTotal >= weekTotal)
            {
                dateOfWeekStartText = "";
            }
            if (endTimeTotal < hour)
            {
                dateStartText = fromStartDate.ToString("HH:mm");
            }
            else if (endTimeTotal >= hour && endTimeTotal < hour * 2)
            {
                //dateStartText = dateOfWeekStartText+" "+ fromStartDate.ToString("HH:mm");
                dateStartText = fromStartDate.ToString("HH:mm");
            }
            else if (endTimeTotal >= hour * 2 && endTimeTotal < weekTotal)
            {
                //if (dateOfWeekStartText == "Hôm Nay")
                //{
                //    dateStartText = (!String.IsNullOrEmpty(dateOfWeekStartText) ? dateOfWeekStartText + ", " : "") +  currentDate .ToString("dd/MM HH:mm");
                //}
                //else
                //{
                //    dateStartText =(!String.IsNullOrEmpty(dateOfWeekStartText)? dateOfWeekStartText + ", ":"") + fromStartDate.ToString("dd/MM HH:mm");
                //}   
                if (dateOfWeekStartText == "Hôm Nay")
                {
                    dateStartText = currentDate.ToString("dd/MM HH:mm");
                }
                else
                {
                    dateStartText = fromStartDate.ToString("dd/MM HH:mm");
                }
            }
            else if (endTimeTotal >= weekTotal)
            {
                //if (dateOfWeekStartText == "Hôm Nay")
                //{
                //    dateStartText = (!String.IsNullOrEmpty(dateOfWeekStartText) ? dateOfWeekStartText + ", " : "") + currentDate.ToString("dd/MM HH:mm");
                //}
                //else
                //{
                //    dateStartText = (!String.IsNullOrEmpty(dateOfWeekStartText) ? dateOfWeekStartText + ", " : "") + fromStartDate.ToString("dd/MM HH:mm");
                //}                       
                if (dateOfWeekStartText == "Hôm Nay")
                {
                    dateStartText = currentDate.ToString("dd/MM HH:mm");
                }
                else
                {
                    dateStartText = fromStartDate.ToString("dd/MM HH:mm");
                }
            }

            if (endTimeTotal < hour)
            {
                dateEndText = fromEndDate.ToString("HH:mm");
            }
            else if (endTimeTotal >= hour && endTimeTotal < hour * 2)
            {
                dateEndText = fromEndDate.ToString("HH:mm");
            }
            else if (endTimeTotal >= hour * 2 && endTimeTotal < weekTotal)
            {
                dateEndText = fromEndDate.ToString("dd/MM HH:mm");
            }
            else if (endTimeTotal >= weekTotal)
            {

                //dateEndText = dateOfWeekEndText + ", " + fromEndDate.ToString("dd/MM HH:mm");
                dateEndText = fromEndDate.ToString("dd/MM HH:mm");
            }

            var dateList = new List<string>
            {
                dateStartText,dateEndText
            };
            dateList.RemoveAll(string.IsNullOrEmpty);
            result.DateStartText = dateStartText;
            result.DateEndText = dateEndText;
            result.DateOfWeek = dateOfWeekStartText;
            result.DateText = string.Join(" Tới ", dateList);
        }
        return result;
    }

    public static string WeekOfDay(int dateOfWeek)
    {
        var dateOfWeekText = "";
        switch (dateOfWeek)
        {
            case 0:
                dateOfWeekText = "Chủ Nhật";
                break;
            case 1:
                dateOfWeekText = "Thứ Hai";
                break;
            case 2:
                dateOfWeekText = "Thứ Ba";
                break;
            case 3:
                dateOfWeekText = "Thứ Tư";
                break;
            case 4:
                dateOfWeekText = "Thứ Năm";
                break;
            case 5:
                dateOfWeekText = "Thứ Sáu";
                break;
            case 6:
                dateOfWeekText = "Thứ Bảy";
                break;
        }
        return dateOfWeekText;
    }

    private static string alphanums = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
    private static int _codeLen = 6;
    public static string EncodeNumber(this string num)
    {
        if (num == "") //or throw an exception
            return "";
        var maxLength = num.Length - 9;
        //switch (num.Length)
        //{
        //    case 11:
        //        codeLen = _codeLen + 1;
        //        break;
        //}

        var codeLen = maxLength > 0 ? _codeLen + maxLength : _codeLen;
        var nums = new long[codeLen];
        var pos = 0;
        var N = long.Parse(num);
        while (N != 0)
        {
            nums[pos] = N % alphanums.Length;
            N /= alphanums.Length;
            pos += 1;
        }
        return nums.Cast<long>().Aggregate("", (current, numIndex) => alphanums[(int)numIndex].ToString() + current);
    }

    public static T DeepCopy<T>(this T self)
    {
        var serialized = JsonConvert.SerializeObject(self);
        return JsonConvert.DeserializeObject<T>(serialized);
    }
}

public static partial class Extensions
{
    /// <summary>
    ///     Converts a time to the time in a particular time zone.
    /// </summary>
    /// <param name="dateTimeOffset">The date and time to convert.</param>
    /// <param name="destinationTimeZone">The time zone to convert  to.</param>
    /// <returns>The date and time in the destination time zone.</returns>
    public static DateTimeOffset ConvertTime(this DateTimeOffset dateTimeOffset)
    {
        //2022.01.22
        //https://csharp-extension.com/en/method/1002275/datetimeoffset-converttime
        //https://dotnetfiddle.net/zcc9CF
        TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
        return TimeZoneInfo.ConvertTime(dateTimeOffset, tst);
    }
}

public static class DateTimeExtensions
{
    //// Get the offset from current time in UTC time
    //DateTimeOffset dto = new DateTimeOffset(DateTime.UtcNow);
    //// Get the unix timestamp in seconds
    //string unixTime = dto.ToUnixTimeSeconds().ToString();
    //// Get the unix timestamp in seconds, and add the milliseconds
    //string unixTimeMilliSeconds = dto.ToUnixTimeMilliseconds().ToString();

    // Convert datetime to UNIX time
    public static string ToUnixTime(this DateTime dateTime)
    {
        var Now = dateTime.AddHours(7);
        DateTimeOffset dto = new DateTimeOffset(Now.ToUniversalTime());
        return dto.ToUnixTimeSeconds().ToString();
    }

    // Convert datetime to UNIX time including miliseconds
    public static string ToUnixTimeMilliSeconds(this DateTime dateTime)
    {
        var Now = dateTime.AddHours(7);
        DateTimeOffset dto = new DateTimeOffset(Now.ToUniversalTime());
        return dto.ToUnixTimeMilliseconds().ToString();
    }
}

//public static class QueueGFG
//{
//    static void removeQueue(string t, ref Queue q)
//    {

//        // Helper queue to store the elements
//        // temporarily.
//        Queue reff = new Queue();
//        int s = q.Count;
//        int cnt = 0;

//        // Finding the value to be removed
//        while (q.Count != 0 && q.Peek() != t)
//        {

//            reff.Enqueue(q.Peek());
//            q.Dequeue();
//            cnt++;
//        }

//        // If element is not found
//        if (q.Count == 0)
//        {
//            Console.WriteLine("element not found!!");

//            while (reff.Count != 0)
//            {

//                // Pushing all the elements back into q
//                q.Enqueue(reff.Peek());
//                reff.Dequeue();
//            }
//        }

//        // If element is found
//        else
//        {
//            q.Dequeue();
//            while (reff.Count != 0)
//            {

//                // Pushing all the elements back into q
//                q.Enqueue(reff.Peek());
//                reff.Dequeue();
//            }
//            int k = s - cnt - 1;
//            while (k-- > 0)
//            {

//                // Pushing elements from front of q to its back
//                var p = q.Peek();
//                q.Dequeue();
//                q.Enqueue(p);
//            }
//        }
//    }
//}
