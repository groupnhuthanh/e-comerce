using Helpers.Encrypt.Payoo.Model;

namespace Helpers.Convert;

public class PaymentXMLFactory
{
    private static string _XML = @"<shops>
                                <shop>
                                    <session>{0}</session>
                                    <username>{1}</username>
                                    <shop_id>{2}</shop_id>
                                    <shop_title>{3}</shop_title>
                                    <shop_domain>{4}</shop_domain>
                                    <shop_back_url>{5}</shop_back_url>
                                    <order_no>{6}</order_no>
                                    <order_cash_amount>{7}</order_cash_amount>
                                    <order_ship_date>{8}</order_ship_date>
                                    <order_ship_days>{9}</order_ship_days>
                                    <order_description>{10}</order_description>
                                    <notify_url>{11}</notify_url>
                                    <validity_time>{12}</validity_time>
                                    <JsonResponse>true</JsonResponse>
                                    <customer>
                                       <name>{13}</name>
                                       <phone>{14}</phone>
                                       <address>{15}</address>
                                       <city>{16}</city>
                                       <email>{17}</email>
                                    </customer>
                                    <region_code>{18}</region_code>
                                    <store_code>{19}</store_code>
                                </shop>
                            </shops>";
    //13: customer full name here
    //14: customer phone number here
    //15: customer address here
    //16: city
    //17: customer email here
    //18: partner's Region_Code
    //19: Partner's Store_Code
    public static string GetPaymentXML(PayooOrder Info)
    {
        try
        {
            if (Info == null)
                throw new Exception("Parameter is not set.");
            return string.Format(_XML, Info.Session, Info.BusinessUsername, Info.ShopID, Info.ShopTitle,
                          Info.ShopDomain, Info.ShopBackUrl, Info.OrderNo, Info.OrderCashAmount, Info.StartShippingDate,
                          Info.ShippingDays, Info.OrderDescription, Info.NotifyUrl, Info.ValidityTime,
                          Info.CustomerName, Info.CustomerPhone, Info.CustomerAddress, Info.CustomerCity, Info.CustomerEmail, Info.order_region_code, Info.order_store_code);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}