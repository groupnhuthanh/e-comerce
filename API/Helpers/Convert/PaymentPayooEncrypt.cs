using System.Security.Cryptography;
using System.Text;

namespace Helpers.Convert;

public class PaymentPayooEncrypt
{
    public static string EncryptSHA512(string input)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(input);
        using (SHA512 hash = SHA512.Create())
        {
            byte[] hashedInputBytes = hash.ComputeHash(bytes);

            // Convert to text
            // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
            StringBuilder hashedInputStringBuilder = new StringBuilder(128);
            foreach (byte b in hashedInputBytes)
            {
                hashedInputStringBuilder.Append(b.ToString("X2"));
            }
            return hashedInputStringBuilder.ToString();
        }
    }

    public static bool VerifyCheckSum(string Data, string CheckSum)
    {
        try
        {
            if (string.IsNullOrEmpty(Data) ||
                string.IsNullOrEmpty(CheckSum))
            {
                return false;
            }
            return CheckSum.Equals(EncryptSHA512(Data), StringComparison.OrdinalIgnoreCase);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}