namespace Helpers.Convert;

public class Libs_RequestExtension
{
    public static string Url(string domain)
    {
        var url = domain;
        if (domain == "") return url;
        var uri = new Uri(domain);
        var port = uri.Port > 0 && uri.Port != 443 && uri.Port != 80 ? ":" + uri.Port : "";
        url = $"{uri.Scheme}://{uri.Host}{port}";
        return url;
    }
}