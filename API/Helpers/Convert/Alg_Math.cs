namespace Helpers.Convert;

public class Alg_Math
{
    /// <summary>
    /// DiscountModel calculation
    /// </summary>
    /// <param name="id">Id: 1:Percentage, 2: Free, 3: Amount, 4: No DiscountModel value, 5: Specific Price</param>
    /// <param name="value">Original Price</param>
    /// <param name="discount">DiscountModel value</param>
    /// <returns></returns>
    public static decimal PriceAfterDiscount(int id, decimal value, decimal discount)
    {
        var result = value;
        if (id > 0 && value > 0)
        {
            switch (id)
            {
                case 1:
                    //x:value,y:DiscountModel
                    //1: Percent= 1-y/100.
                    //sample: (1-30/100)*100=70.

                    //2: result= x-(x*y/100).
                    //sample= 100-(100*30/100)= 70.
                    result = (1 - discount / 100) * value;
                    break;
                case 2:
                    result = value - discount;
                    result = result > 0 ? result : 0;
                    break;
                case 3:
                    result = value * 0;
                    break;
                case 4:
                    result = value;
                    break;
                case 5:
                    result = discount;
                    break;
            }
        }
        return result;
    }


    public static decimal Discounts(int id, decimal value, decimal discount)
    {
        var result = value;
        if (id > 0 && value > 0)
        {
            switch (id)
            {
                case 1:
                    //x:value,y:DiscountModel
                    //1: Percent= 1-y/100.
                    //sample: (1-30/100)*100=70.

                    //2: result= x-(x*y/100).
                    //sample= 100-(100*30/100)= 70.
                    result = discount / 100 * value;
                    break;
                case 2://giá để giảm                    
                    result = discount;
                    break;
                case 3:
                    result = value - discount;
                    result = result > 0 ? result : 0;
                    break;
                case 4:
                    result = value;
                    break;
                case 5:
                case 10:
                    result = value - discount;
                    result = result > 0 ? result : 0;
                    break;
            }
        }
        return result;
    }

    public static decimal Loyalty(int id, decimal value, decimal amountSpent, int points)
    {
        decimal result = 0;
        if (value > 0 && amountSpent > 0)
        {
            var excess = Math.Floor(value / amountSpent);
            result = excess * points;
        }
        return result;
    }
}