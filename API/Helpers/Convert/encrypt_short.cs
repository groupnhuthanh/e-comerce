using System.Security.Cryptography;
using System.Text;

namespace Helpers.Convert;

public class encrypt_short
{
    public const string strPermutation = "ouiveyxaqtd";
    public const int bytePermutation1 = 0x19;
    public const int bytePermutation2 = 0x59;
    public const int bytePermutation3 = 0x17;
    public const int bytePermutation4 = 0x41;
    // encoding
    public string Encrypt_bk_01(string strData)
    {

        return System.Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(strData)));
        // reference https://msdn.microsoft.com/en-us/library/ds4kkd55(v=vs.110).aspx

    }


    // decoding
    public string Decrypt_bk_01(string strData)
    {
        return Encoding.UTF8.GetString(Decrypt(System.Convert.FromBase64String(strData)));
        // reference https://msdn.microsoft.com/en-us/library/system.convert.frombase64string(v=vs.110).aspx

    }

    // encrypt
    public static byte[] Encrypt(byte[] strData)
    {
        PasswordDeriveBytes passbytes =
        new PasswordDeriveBytes(strPermutation,
        new byte[] { bytePermutation1,
                     bytePermutation2,
                     bytePermutation3,
                     bytePermutation4
        });

        MemoryStream memstream = new MemoryStream();
        Aes aes = new AesManaged();
        aes.Key = passbytes.GetBytes(aes.KeySize / 8);
        aes.IV = passbytes.GetBytes(aes.BlockSize / 8);

        CryptoStream cryptostream = new CryptoStream(memstream,
        aes.CreateEncryptor(), CryptoStreamMode.Write);
        cryptostream.Write(strData, 0, strData.Length);
        cryptostream.Close();
        return memstream.ToArray();
    }

    // decrypt
    public static byte[] Decrypt(byte[] strData)
    {
        PasswordDeriveBytes passbytes =
        new PasswordDeriveBytes(strPermutation,
        new byte[] { bytePermutation1,
                     bytePermutation2,
                     bytePermutation3,
                     bytePermutation4
        });

        MemoryStream memstream = new MemoryStream();
        Aes aes = new AesManaged();
        aes.Key = passbytes.GetBytes(aes.KeySize / 8);
        aes.IV = passbytes.GetBytes(aes.BlockSize / 8);

        CryptoStream cryptostream = new CryptoStream(memstream,
        aes.CreateDecryptor(), CryptoStreamMode.Write);
        cryptostream.Write(strData, 0, strData.Length);
        cryptostream.Close();
        return memstream.ToArray();
    }
    // reference
    // https://msdn.microsoft.com/en-us/library/system.security.cryptography(v=vs.110).aspx
    // https://msdn.microsoft.com/en-us/library/system.security.cryptography.cryptostream%28v=vs.110%29.aspx?f=255&MSPPError=-2147217396
    // https://msdn.microsoft.com/en-us/library/system.security.cryptography.rfc2898derivebytes(v=vs.110).aspx
    // https://msdn.microsoft.com/en-us/library/system.security.cryptography.aesmanaged%28v=vs.110%29.aspx?f=255&MSPPError=-2147217396



    public string Encrypt_okie_01(string stringToEncrypt)
    {
        byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
        byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
        byte[] key = { };
        try
        {
            key = Encoding.UTF8.GetBytes("A0D1nX0Q");
            using var des = new DESCryptoServiceProvider();
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, rgbIV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return System.Convert.ToBase64String(ms.ToArray());
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    public string Decrypt_okie_01(string EncryptedText)
    {
        byte[] inputByteArray = new byte[EncryptedText.Length + 1];
        byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
        byte[] key = { };

        try
        {
            key = Encoding.UTF8.GetBytes("A0D1nX0Q");
            using var des = new DESCryptoServiceProvider();
            inputByteArray = System.Convert.FromBase64String(EncryptedText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, rgbIV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            Encoding encoding = Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }


    static readonly char[] padding = { '=' };
    public string Encrypt(string clearText)
    {
        try
        {
            string EncryptionKey = "JHK@!$RTYEWQRGH";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = System.Convert.ToBase64String(ms.ToArray()).TrimEnd(padding).Replace('+', '-').Replace('/', '_');
                }
            }
            return clearText;
        }
        catch
        {

        }
        return "";
    }

    public string Decrypt(string cipherText)
    {
        if (cipherText != null)
        {
            string incoming = cipherText.Replace('_', '/').Replace('-', '+');
            switch (cipherText.Length % 4)
            {
                case 2:
                    incoming += "==";
                    break;
                case 3:
                    incoming += "=";
                    break;
            }

            try
            {
                string EncryptionKey = "JHK@!$RTYEWQRGH";
                byte[] cipherBytes = System.Convert.FromBase64String(incoming);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey,
                        new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(),
                            CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }

                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }

                return cipherText;

            }
            catch (Exception)
            {

            }
        }

        return "";
    }
}