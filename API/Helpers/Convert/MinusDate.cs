﻿namespace Helpers.Convert;

public class MinusDate
{
    public static string GetMinusDate(DateTime LastLoginDate)
    {
        var status = "";

        DateTime DateNow = DateTime.Parse(LibConvert.Get_ByDateTimeNow());


        TimeSpan diff = DateNow - LastLoginDate;

        var day = diff.Days;
        if (day > 0)
        {
            status = day.ToString() + "Ngày ".PadRight(3);
        }

        var hours = diff.Hours;

        if (hours > 0)
        {
            status = status + hours.ToString() + "Giờ ".PadRight(3);
        }
        var minutes = diff.Minutes;
        if (minutes > 5)
        {
            status = status + minutes.ToString() + "Phút ".PadRight(3);
        }

        return status;
    }
}
