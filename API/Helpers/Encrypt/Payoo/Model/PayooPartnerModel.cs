using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Helpers.Encrypt.Payoo.Model;

class PayooPartnerModel
{
}

public class APIRequest
{
    public string? RequestData;
    public string? Signature;
}
public class APIResponse
{
    public string? ResponseData;
    public string? Signature;
}
public class GetOrderInfoRequest
{
    public string? OrderId;
    public string? ShopId;
    public string? PurchaseDate;
}

public class OrderInformation
{
    public string? PaymentMethod;
    public string? PaymentSource;
    public string? QRPaymentSystemTrace;
    public string? PurchaseDate;
    public string? MerchantUsername;
    public string? ShopId;
    public long MasterShopId;
    public string? OrderNo;
    [BsonRepresentation(BsonType.Decimal128)]
    public decimal OrderCash;
    public string? BankName;
    public string? CardNumber;
    public string? BillingCode;
    public int CardIssuanceType;
    public int PaymentStatus;
    [BsonRepresentation(BsonType.Decimal128)]
    public decimal? TransactionFee;
}
public class CreateBillingCodeRequest
{
    public string? OrderNo;
    public string? ShopID;
    public string? FromShipDate;
    public string? ShipNumDay;
    public string? Description;
    public string? CyberCash;
    public string? PaymentExpireDate;
    public string? NotifyUrl;
    public string? InfoEx;
    public string? BillingCode;
}
public class CreateBillingCodeResponse
{
    public string? BillingCode;
    public string? ResponseCode;
}
public class CancelOrder
{
    public string? ShopID;
    public string? OrderID;
    public string? NewStatus;
    public string? UpdateLog;
}


public class PayooOrder
{
    private string? _Session;
    private string? _BusinessUsername;
    private long _ShopID;
    private string? _ShopTitle;
    private string? _ShopDomain;
    private string? _ShopBackUrl;
    private string? _OrderNo;
    private long _OrderCashAmount;
    private string? _StartShippingDate; //Format: dd/mm/yyyy
    private short _ShippingDays;
    private string? _OrderDescription;
    private string? _NotifyUrl = "";
    private string? _validityTime; //yyyyMMddhhmmss
    private string? _customerName;
    private string? _customerPhone;
    private string? _customerAddress;
    private string? _customerCity;
    private string? _customerEmail;
    private string? _BillingCode;
    private string? _PaymentExpireDate;
    private string? _order_ship_days = "0";//This is total shipping days. It may equal 0. In that case the order status will change to "Shipping" immediately after it has been paid.
    private string? _order_ship_date;//this is the date that shopping website intends to deliver goods/services to the buyer. Formatting: dd/MM/yyyy Noted: The shipping date must be larger than payment date (future)
    private string? _region_code;
    private string? _store_code;


    public string? Session
    {
        set { _Session = value; }
        get { return _Session; }
    }
    public string? BusinessUsername
    {
        set { _BusinessUsername = value; }
        get { return _BusinessUsername; }
    }
    public long ShopID
    {
        set { _ShopID = value; }
        get { return _ShopID; }
    }
    public string? ShopTitle
    {
        set { _ShopTitle = value; }
        get { return _ShopTitle; }
    }
    public string? ShopDomain
    {
        set { _ShopDomain = value; }
        get { return _ShopDomain; }
    }
    public string? ShopBackUrl
    {
        set { _ShopBackUrl = value; }
        get { return _ShopBackUrl; }
    }
    public string? OrderNo
    {
        set { _OrderNo = value; }
        get { return _OrderNo; }
    }
    public long OrderCashAmount
    {
        set { _OrderCashAmount = value; }
        get { return _OrderCashAmount; }
    }
    public string? StartShippingDate
    {
        set { _StartShippingDate = value; }
        get { return _StartShippingDate; }
    }
    public short ShippingDays
    {
        set { _ShippingDays = value; }
        get { return _ShippingDays; }
    }
    public string? OrderDescription
    {
        set { _OrderDescription = value; }
        get { return _OrderDescription; }
    }
    public string? NotifyUrl
    {
        set { _NotifyUrl = value; }
        get { return _NotifyUrl; }
    }
    public string? ValidityTime
    {
        get { return _validityTime; }
        set { _validityTime = value; }
    }
    public string? CustomerName
    {
        get { return _customerName; }
        set { _customerName = value; }
    }

    public string? CustomerPhone
    {
        get { return _customerPhone; }
        set { _customerPhone = value; }
    }

    public string? CustomerAddress
    {
        get { return _customerAddress; }
        set { _customerAddress = value; }
    }
    public string? CustomerEmail
    {
        get { return _customerEmail; }
        set { _customerEmail = value; }
    }

    public string? CustomerCity
    {
        get { return _customerCity; }
        set { _customerCity = value; }
    }
    public string? BillingCode
    {
        get { return _BillingCode; }
        set { _BillingCode = value; }
    }

    public string? PaymentExpireDate
    {
        get { return _PaymentExpireDate; }
        set { _PaymentExpireDate = value; }
    }
    public string? order_shipping_days
    {
        get { return _order_ship_days; }
        set { _order_ship_days = value; }
    }
    public string? order_shipping_date
    {
        get { return _order_ship_date; }
        set { _order_ship_date = value; }
    }

    public string? order_region_code
    {
        get { return _region_code; }
        set { _region_code = value; }
    }

    public string? order_store_code
    {
        get { return _store_code; }
        set { _store_code = value; }
    }
}

public class CreateOrderResponse
{
    public string? result;
    public string? checksum;
    public object order;
}
public class PreOrderInfo
{
    public string? payment_url;
}

public class GetQRCodeRequest
{
    private long _ShopID; // NOT NULL

    public long ShopID
    {
        get { return _ShopID; }
        set { _ShopID = value; }
    }
    private string? _OrderNo; //Ma don hang doanh nghiep NOT NULL

    public string? OrderNo
    {
        get { return _OrderNo; }
        set { _OrderNo = value; }
    }
    private string? _FromShipDate; // NOT NULL

    public string? FromShipDate
    {
        get { return _FromShipDate; }
        set { _FromShipDate = value; }
    }
    [BsonRepresentation(BsonType.Decimal128)]
    private decimal _ShipNumDay; // So ngay chuyen hang NOT NULL

    public decimal ShipNumDay
    {
        get { return _ShipNumDay; }
        set { _ShipNumDay = value; }
    }
    private string? _Description; // NOT NULL

    public string? Description
    {
        get { return _Description; }
        set { _Description = value; }
    }
    private decimal _CyberCash; // NOT NULL

    public decimal CyberCash
    {
        get { return _CyberCash; }
        set { _CyberCash = value; }
    }

    private string? _PaymentExpireDate = null; // Ngay het han NOT NULL

    public string? PaymentExpireDate
    {
        get { return _PaymentExpireDate; }
        set { _PaymentExpireDate = value; }
    }
    private string? _UserName; // NULL

    public string? UserName
    {
        get { return _UserName; }
        set { _UserName = value; }
    }

    private string? _NotifyUrl; // NOT NULL

    public string? NotifyUrl
    {
        get { return _NotifyUrl; }
        set { _NotifyUrl = value; }
    }
}

public class GetQRCodeResponse
{
    private string? _QRCode;

    public string? QRCode
    {
        get { return _QRCode; }
        set { _QRCode = value; }
    }
    private string? _QRCodeLink;

    public string? QRCodeLink
    {
        get { return _QRCodeLink; }
        set { _QRCodeLink = value; }
    }

    private string? _ResponseCode;

    public string? ResponseCode
    {
        get { return _ResponseCode; }
        set { _ResponseCode = value; }
    }
}


public partial class ResponseData
{
    [JsonProperty("OrderStatus", NullValueHandling = NullValueHandling.Ignore)]
    public int? OrderStatus { get; set; }

    [JsonProperty("OrderCash", NullValueHandling = NullValueHandling.Ignore)]
    public decimal? OrderCash { get; set; }

    [JsonProperty("PaymentDate", NullValueHandling = NullValueHandling.Ignore)]
    public string? PaymentDate { get; set; }

    [JsonProperty("ShippingDate", NullValueHandling = NullValueHandling.Ignore)]
    public string? ShippingDate { get; set; }

    [JsonProperty("DeliveryDate", NullValueHandling = NullValueHandling.Ignore)]
    public string? DeliveryDate { get; set; }

    [JsonProperty("PaymentMethod", NullValueHandling = NullValueHandling.Ignore)]
    public int? PaymentMethod { get; set; }

    [JsonProperty("BankName", NullValueHandling = NullValueHandling.Ignore)]
    public string? BankName { get; set; }

    [JsonProperty("CardNumber", NullValueHandling = NullValueHandling.Ignore)]
    public string? CardNumber { get; set; }

    [JsonProperty("CardHolderName", NullValueHandling = NullValueHandling.Ignore)]
    public string? CardHolderName { get; set; }

    [JsonProperty("InstallmentPeriod")]
    public object InstallmentPeriod { get; set; }

    [JsonProperty("InstallmentMoneyTotal", NullValueHandling = NullValueHandling.Ignore)]
    public decimal? InstallmentMoneyTotal { get; set; }

    [JsonProperty("BillingCode", NullValueHandling = NullValueHandling.Ignore)]
    public string? BillingCode { get; set; }

    [JsonProperty("BuyerIPAddress", NullValueHandling = NullValueHandling.Ignore)]
    public string? BuyerIpAddress { get; set; }

    [JsonProperty("RequestValueAmount")]
    public object RequestValueAmount { get; set; }

    [JsonProperty("PaymentSource")]
    public object PaymentSource { get; set; }

    [JsonProperty("QRPaymentSystemTrace")]
    public object QrPaymentSystemTrace { get; set; }

    [JsonProperty("ResponseCode", NullValueHandling = NullValueHandling.Ignore)]
    public int? ResponseCode { get; set; }
}