using Helpers.Thirdparty;
using System.Security.Cryptography;
using System.Text;

namespace Helpers.Encrypt;

public static class Md5
{
    public static string GetMd5Hash(string input)
    {
        byte[] bs = Encoding.UTF8.GetBytes(input);
        Md5Managed md5 = new Md5Managed();
        byte[] hash = md5.ComputeHash(bs);

        var sb = new StringBuilder();
        foreach (byte b in hash)
        {
            sb.Append(b.ToString("x2").ToLower());
        }

        return sb.ToString();
    }

    public static string Md5Func(string source)
    {
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] data = Encoding.UTF8.GetBytes(source);
        byte[] md5Data = md5.ComputeHash(data, 0, data.Length);
        md5.Clear();

        string destString = string.Empty;
        for (int i = 0; i < md5Data.Length; i++)
        {
            //Return a new string, which is filled with the specified Unicode characters on the left side of the characters in this instance to reach the specified total length, so that these characters are right-aligned.
            // string num=12; num.PadLeft(4, '0'); The result is '0012' to see if the length of the string meets 4 digits, if not, the left side of the string is filled with "0"
            //Call Convert.ToString (integer, base number) to convert to the desired base number
            destString += System.Convert.ToString(md5Data[i], 16).PadLeft(2, '0');
        }
        //Use PadLeft and PadRight to easily fill in
        destString = destString.PadLeft(32, '0');
        return destString;
    }
}