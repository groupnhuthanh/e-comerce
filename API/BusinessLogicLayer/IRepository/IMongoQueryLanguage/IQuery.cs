﻿using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.IMongoQueryLanguage;

public interface IQuery
{
    Task<List<TEntity>> GetList<TEntity>(string database, string table, FilterDefinition<TEntity> filter = null, SortDefinition<TEntity> sort = null, int limit = 0);
    Task<TEntity> GetSingle<TEntity>(string database, string table, FilterDefinition<TEntity> filter = null, SortDefinition<TEntity> sort = null, int limit = 0) where TEntity : new();
    Task<bool> Delete<TEntity>(string database, string table, FilterDefinition<TEntity> filter = null);
    Task<bool> Put<TEntity>(string database, string table, FilterDefinition<TEntity> filter, UpdateDefinition<TEntity> updateDefinition);
    Task<TEntity> Post<TEntity>(string database, string table, TEntity doc);
}
