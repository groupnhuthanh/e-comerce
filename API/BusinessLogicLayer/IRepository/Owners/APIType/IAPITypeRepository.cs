﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.APIType;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.APIType;

public interface IAPITypeRepository : IRepositoryBase<APITypeModel>
{
    Task<APITypeModel> Post(APITypeModel doc);
    Task<APITypeModel> GetSingle(FilterDefinition<APITypeModel> filter = null, SortDefinition<APITypeModel> sort = null, int limit = 0);
    Task<List<APITypeModel>> GetList(FilterDefinition<APITypeModel> filter, SortDefinition<APITypeModel> sort, int limit = 0);
    Task<bool> Put<APITypeModel>(FilterDefinition<APITypeModel> filter, UpdateDefinition<APITypeModel> updateDefinition);
    Task<bool> Delete<APITypeModel>(FilterDefinition<APITypeModel> filter);


}
