﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.notification.EmailSetting;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.notification.EmailSetting;

public interface IEmailSettingRepository : IRepositoryBase<EmailSettingModel>
{
    Task<EmailSettingModel> Post(EmailSettingModel doc);
    Task<EmailSettingModel> GetSingle(FilterDefinition<EmailSettingModel> filter = null, SortDefinition<EmailSettingModel> sort = null, int limit = 0);
    Task<List<EmailSettingModel>> GetList(FilterDefinition<EmailSettingModel> filter, SortDefinition<EmailSettingModel> sort, int limit = 0);
    Task<bool> Put<EmailSettingModel>(FilterDefinition<EmailSettingModel> filter, UpdateDefinition<EmailSettingModel> updateDefinition);
    Task<bool> Delete<EmailSettingModel>(FilterDefinition<EmailSettingModel> filter);
}
