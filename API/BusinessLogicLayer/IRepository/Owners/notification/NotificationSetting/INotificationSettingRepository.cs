﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.notification.NotificationSetting;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.notification.NotificationSetting;

public interface INotificationSettingRepository : IRepositoryBase<NotificationSettingModel>
{
    Task<NotificationSettingModel> Post(NotificationSettingModel doc);
    Task<NotificationSettingModel> GetSingle(FilterDefinition<NotificationSettingModel> filter = null, SortDefinition<NotificationSettingModel> sort = null, int limit = 0);
    Task<List<NotificationSettingModel>> GetList(FilterDefinition<NotificationSettingModel> filter, SortDefinition<NotificationSettingModel> sort, int limit = 0);
    Task<bool> Put<NotificationSettingModel>(FilterDefinition<NotificationSettingModel> filter, UpdateDefinition<NotificationSettingModel> updateDefinition);
    Task<bool> Delete<NotificationSettingModel>(FilterDefinition<NotificationSettingModel> filter);
}
