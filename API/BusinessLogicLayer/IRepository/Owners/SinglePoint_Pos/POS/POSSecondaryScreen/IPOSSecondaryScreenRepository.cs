﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.POS.POSSecondaryScreen;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSSecondaryScreen;

public interface IPOSSecondaryScreenRepository : IRepositoryBase<POSSecondaryScreenModel>
{
    Task<POSSecondaryScreenModel> Post(POSSecondaryScreenModel doc);
    Task<POSSecondaryScreenModel> GetSingle(FilterDefinition<POSSecondaryScreenModel> filter = null, SortDefinition<POSSecondaryScreenModel> sort = null, int limit = 0);
    Task<List<POSSecondaryScreenModel>> GetList(FilterDefinition<POSSecondaryScreenModel> filter, SortDefinition<POSSecondaryScreenModel> sort, int limit = 0);
    Task<bool> Put<POSSecondaryScreenModel>(FilterDefinition<POSSecondaryScreenModel> filter, UpdateDefinition<POSSecondaryScreenModel> updateDefinition);
    Task<bool> Delete<POSSecondaryScreenModel>(FilterDefinition<POSSecondaryScreenModel> filter);
}
