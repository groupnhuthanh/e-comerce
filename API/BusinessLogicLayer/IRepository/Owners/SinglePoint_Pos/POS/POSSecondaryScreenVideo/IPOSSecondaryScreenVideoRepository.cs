﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.POS.POSSecondaryScreenVideo;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSSecondaryScreenVideo;

public interface IPOSSecondaryScreenVideoRepository : IRepositoryBase<POSSecondaryScreenVideoModel>
{
    Task<POSSecondaryScreenVideoModel> Post(POSSecondaryScreenVideoModel doc);
    Task<POSSecondaryScreenVideoModel> GetSingle(FilterDefinition<POSSecondaryScreenVideoModel> filter = null, SortDefinition<POSSecondaryScreenVideoModel> sort = null, int limit = 0);
    Task<List<POSSecondaryScreenVideoModel>> GetList(FilterDefinition<POSSecondaryScreenVideoModel> filter, SortDefinition<POSSecondaryScreenVideoModel> sort, int limit = 0);
    Task<bool> Put<POSSecondaryScreenVideoModel>(FilterDefinition<POSSecondaryScreenVideoModel> filter, UpdateDefinition<POSSecondaryScreenVideoModel> updateDefinition);
    Task<bool> Delete<POSSecondaryScreenVideoModel>(FilterDefinition<POSSecondaryScreenVideoModel> filter);
}
