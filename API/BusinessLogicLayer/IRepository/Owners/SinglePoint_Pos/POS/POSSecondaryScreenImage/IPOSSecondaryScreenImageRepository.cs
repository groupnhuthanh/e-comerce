﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.POS.POSSecondaryScreenImage;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSSecondaryScreenImage;

public interface IPOSSecondaryScreenImageRepository : IRepositoryBase<POSSecondaryScreenImageModel>
{
    Task<POSSecondaryScreenImageModel> Post(POSSecondaryScreenImageModel doc);
    Task<POSSecondaryScreenImageModel> GetSingle(FilterDefinition<POSSecondaryScreenImageModel> filter = null, SortDefinition<POSSecondaryScreenImageModel> sort = null, int limit = 0);
    Task<List<POSSecondaryScreenImageModel>> GetList(FilterDefinition<POSSecondaryScreenImageModel> filter, SortDefinition<POSSecondaryScreenImageModel> sort, int limit = 0);
    Task<bool> Put<POSSecondaryScreenImageModel>(FilterDefinition<POSSecondaryScreenImageModel> filter, UpdateDefinition<POSSecondaryScreenImageModel> updateDefinition);
    Task<bool> Delete<POSSecondaryScreenImageModel>(FilterDefinition<POSSecondaryScreenImageModel> filter);
}
