﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.POS.POSVideos;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSVideos;

public interface IPOSVideosRepository : IRepositoryBase<POSVideosModel>
{
    Task<POSVideosModel> Post(POSVideosModel doc);
    Task<POSVideosModel> GetSingle(FilterDefinition<POSVideosModel> filter = null, SortDefinition<POSVideosModel> sort = null, int limit = 0);
    Task<List<POSVideosModel>> GetList(FilterDefinition<POSVideosModel> filter, SortDefinition<POSVideosModel> sort, int limit = 0);
    Task<bool> Put<POSVideosModel>(FilterDefinition<POSVideosModel> filter, UpdateDefinition<POSVideosModel> updateDefinition);
    Task<bool> Delete<POSVideosModel>(FilterDefinition<POSVideosModel> filter);
}
