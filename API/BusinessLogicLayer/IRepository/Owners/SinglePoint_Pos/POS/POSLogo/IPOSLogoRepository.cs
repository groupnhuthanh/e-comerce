﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.POS.POSLogo;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSLogo;

public interface IPOSLogoRepository : IRepositoryBase<POSLogoModel>
{
    Task<POSLogoModel> Post(POSLogoModel doc);
    Task<POSLogoModel> GetSingle(FilterDefinition<POSLogoModel> filter = null, SortDefinition<POSLogoModel> sort = null, int limit = 0);
    Task<List<POSLogoModel>> GetList(FilterDefinition<POSLogoModel> filter, SortDefinition<POSLogoModel> sort, int limit = 0);
    Task<bool> Put<POSLogoModel>(FilterDefinition<POSLogoModel> filter, UpdateDefinition<POSLogoModel> updateDefinition);
    Task<bool> Delete<POSLogoModel>(FilterDefinition<POSLogoModel> filter);
}
