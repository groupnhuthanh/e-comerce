﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.TableType;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.TableType;

public interface ITableTypeRepository : IRepositoryBase<TableTypeModel>
{
    Task<TableTypeModel> Post(TableTypeModel doc);
    Task<TableTypeModel> GetSingle(FilterDefinition<TableTypeModel> filter = null, SortDefinition<TableTypeModel> sort = null, int limit = 0);
    Task<List<TableTypeModel>> GetList(FilterDefinition<TableTypeModel> filter, SortDefinition<TableTypeModel> sort, int limit = 0);
    Task<bool> Put<TableTypeModel>(FilterDefinition<TableTypeModel> filter, UpdateDefinition<TableTypeModel> updateDefinition);
    Task<bool> Delete<TableTypeModel>(FilterDefinition<TableTypeModel> filter);
}
