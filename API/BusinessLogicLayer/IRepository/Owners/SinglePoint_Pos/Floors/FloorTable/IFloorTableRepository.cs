﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.Floors.FloorTable;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.Floors.FloorTable;

public interface IFloorTableRepository : IRepositoryBase<FloorTableModel>
{
    Task<FloorTableModel> Post(FloorTableModel doc);
    Task<FloorTableModel> GetSingle(FilterDefinition<FloorTableModel> filter = null, SortDefinition<FloorTableModel> sort = null, int limit = 0);
    Task<List<FloorTableModel>> GetList(FilterDefinition<FloorTableModel> filter, SortDefinition<FloorTableModel> sort, int limit = 0);
    Task<bool> Put<FloorTableModel>(FilterDefinition<FloorTableModel> filter, UpdateDefinition<FloorTableModel> updateDefinition);
    Task<bool> Delete<FloorTableModel>(FilterDefinition<FloorTableModel> filter);
}
