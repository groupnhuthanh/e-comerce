﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.Floors.Floor;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.Floors.Floor;

public interface IFloorRepository : IRepositoryBase<FloorModel>
{
    Task<FloorModel> Post(FloorModel doc);
    Task<FloorModel> GetSingle(FilterDefinition<FloorModel> filter = null, SortDefinition<FloorModel> sort = null, int limit = 0);
    Task<List<FloorModel>> GetList(FilterDefinition<FloorModel> filter, SortDefinition<FloorModel> sort, int limit = 0);
    Task<bool> Put<FloorModel>(FilterDefinition<FloorModel> filter, UpdateDefinition<FloorModel> updateDefinition);
    Task<bool> Delete<FloorModel>(FilterDefinition<FloorModel> filter);
}
