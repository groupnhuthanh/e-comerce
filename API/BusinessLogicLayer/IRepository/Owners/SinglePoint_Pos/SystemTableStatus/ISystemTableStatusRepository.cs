﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.SystemTableStatus;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.SystemTableStatus;

public interface ISystemTableStatusRepository : IRepositoryBase<SystemTableStatusModel>
{
    Task<SystemTableStatusModel> Post(SystemTableStatusModel doc);
    Task<SystemTableStatusModel> GetSingle(FilterDefinition<SystemTableStatusModel> filter = null, SortDefinition<SystemTableStatusModel> sort = null, int limit = 0);
    Task<List<SystemTableStatusModel>> GetList(FilterDefinition<SystemTableStatusModel> filter, SortDefinition<SystemTableStatusModel> sort, int limit = 0);
    Task<bool> Put<SystemTableStatusModel>(FilterDefinition<SystemTableStatusModel> filter, UpdateDefinition<SystemTableStatusModel> updateDefinition);
    Task<bool> Delete<SystemTableStatusModel>(FilterDefinition<SystemTableStatusModel> filter);
}
