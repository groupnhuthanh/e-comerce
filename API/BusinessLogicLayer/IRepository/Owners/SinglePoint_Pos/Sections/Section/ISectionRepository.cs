﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.Sections.Section;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.Sections.Section;

public interface ISectionRepository : IRepositoryBase<SectionModel>

{
    Task<SectionModel> Post(SectionModel doc);
    Task<SectionModel> GetSingle(FilterDefinition<SectionModel> filter = null, SortDefinition<SectionModel> sort = null, int limit = 0);
    Task<List<SectionModel>> GetList(FilterDefinition<SectionModel> filter, SortDefinition<SectionModel> sort, int limit = 0);
    Task<bool> Put<SectionModel>(FilterDefinition<SectionModel> filter, UpdateDefinition<SectionModel> updateDefinition);
    Task<bool> Delete<SectionModel>(FilterDefinition<SectionModel> filter);
}
