﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.Sections.SectionTable;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.Sections.SectionTable;

public interface ISectionTableRepository : IRepositoryBase<SectionTableModel>
{
    Task<SectionTableModel> Post(SectionTableModel doc);
    Task<SectionTableModel> GetSingle(FilterDefinition<SectionTableModel> filter = null, SortDefinition<SectionTableModel> sort = null, int limit = 0);
    Task<List<SectionTableModel>> GetList(FilterDefinition<SectionTableModel> filter, SortDefinition<SectionTableModel> sort, int limit = 0);
    Task<bool> Put<SectionTableModel>(FilterDefinition<SectionTableModel> filter, UpdateDefinition<SectionTableModel> updateDefinition);
    Task<bool> Delete<SectionTableModel>(FilterDefinition<SectionTableModel> filter);
}
