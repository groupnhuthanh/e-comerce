﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Pos.NamingType;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.NamingType;

public interface INamingTypeRepository : IRepositoryBase<NamingTypeModel>
{
    Task<NamingTypeModel> Post(NamingTypeModel doc);
    Task<NamingTypeModel> GetSingle(FilterDefinition<NamingTypeModel> filter = null, SortDefinition<NamingTypeModel> sort = null, int limit = 0);
    Task<List<NamingTypeModel>> GetList(FilterDefinition<NamingTypeModel> filter, SortDefinition<NamingTypeModel> sort, int limit = 0);
    Task<bool> Put<NamingTypeModel>(FilterDefinition<NamingTypeModel> filter, UpdateDefinition<NamingTypeModel> updateDefinition);
    Task<bool> Delete<NamingTypeModel>(FilterDefinition<NamingTypeModel> filter);
}
