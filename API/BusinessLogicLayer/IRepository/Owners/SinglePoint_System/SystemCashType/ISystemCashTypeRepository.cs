﻿using Entities.Owners.SinglePoint_System.SystemCashType;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_System.SystemCashType;

public interface ISystemCashTypeRepository
{
    Task<SystemCashTypeModel> Post(SystemCashTypeModel doc);
    Task<SystemCashTypeModel> GetSingle(FilterDefinition<SystemCashTypeModel> filter = null, SortDefinition<SystemCashTypeModel> sort = null, int limit = 0);
    Task<List<SystemCashTypeModel>> GetList(FilterDefinition<SystemCashTypeModel> filter, SortDefinition<SystemCashTypeModel> sort, int limit = 0);
    Task<bool> Put<SystemCashTypeModel>(FilterDefinition<SystemCashTypeModel> filter, UpdateDefinition<SystemCashTypeModel> updateDefinition);
    Task<bool> Delete<SystemCashTypeModel>(FilterDefinition<SystemCashTypeModel> filter);
}
