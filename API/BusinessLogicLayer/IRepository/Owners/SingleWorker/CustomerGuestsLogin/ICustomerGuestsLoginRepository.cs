﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SingleWorker.CustomerGuestsLogin;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SingleWorker.CustomerGuestsLogin;

public interface ICustomerGuestsLoginRepository : IRepositoryBase<CustomerGuestsLoginModel>
{
    Task<CustomerGuestsLoginModel> Post(CustomerGuestsLoginModel doc);
    Task<CustomerGuestsLoginModel> GetSingle(FilterDefinition<CustomerGuestsLoginModel> filter = null, SortDefinition<CustomerGuestsLoginModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsLoginModel>> GetList(FilterDefinition<CustomerGuestsLoginModel> filter, SortDefinition<CustomerGuestsLoginModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsLoginModel>(FilterDefinition<CustomerGuestsLoginModel> filter, UpdateDefinition<CustomerGuestsLoginModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsLoginModel>(FilterDefinition<CustomerGuestsLoginModel> filter);
}
