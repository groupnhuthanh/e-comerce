﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SingleWorker.Settings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SingleWorker.Settings;

public interface ISettingsRepository : IRepositoryBase<SettingsModel>
{
    Task<SettingsModel> Post(SettingsModel doc);
    Task<SettingsModel> GetSingle(FilterDefinition<SettingsModel> filter = null, SortDefinition<SettingsModel> sort = null, int limit = 0);
    Task<List<SettingsModel>> GetList(FilterDefinition<SettingsModel> filter, SortDefinition<SettingsModel> sort, int limit = 0);
    Task<bool> Put<SettingsModel>(FilterDefinition<SettingsModel> filter, UpdateDefinition<SettingsModel> updateDefinition);
    Task<bool> Delete<SettingsModel>(FilterDefinition<SettingsModel> filter);
}
