﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SingleWorker.WorkerLogs;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SingleWorker.WorkerLogs;

public interface IWorkerLogsRepository : IRepositoryBase<WorkerLogsModel>
{
    Task<WorkerLogsModel> Post(WorkerLogsModel doc);
    Task<WorkerLogsModel> GetSingle(FilterDefinition<WorkerLogsModel> filter = null, SortDefinition<WorkerLogsModel> sort = null, int limit = 0);
    Task<List<WorkerLogsModel>> GetList(FilterDefinition<WorkerLogsModel> filter, SortDefinition<WorkerLogsModel> sort, int limit = 0);
    Task<bool> Put<WorkerLogsModel>(FilterDefinition<WorkerLogsModel> filter, UpdateDefinition<WorkerLogsModel> updateDefinition);
    Task<bool> Delete<WorkerLogsModel>(FilterDefinition<WorkerLogsModel> filter);
}
