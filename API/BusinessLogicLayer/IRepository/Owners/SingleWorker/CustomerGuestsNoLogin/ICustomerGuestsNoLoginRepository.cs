﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SingleWorker.CustomerGuestsNoLogin;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SingleWorker.CustomerGuestsNoLogin;

public interface ICustomerGuestsNoLoginRepository : IRepositoryBase<CustomerGuestsNoLoginModel>
{
    Task<CustomerGuestsNoLoginModel> Post(CustomerGuestsNoLoginModel doc);
    Task<CustomerGuestsNoLoginModel> GetSingle(FilterDefinition<CustomerGuestsNoLoginModel> filter = null, SortDefinition<CustomerGuestsNoLoginModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsNoLoginModel>> GetList(FilterDefinition<CustomerGuestsNoLoginModel> filter, SortDefinition<CustomerGuestsNoLoginModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsNoLoginModel>(FilterDefinition<CustomerGuestsNoLoginModel> filter, UpdateDefinition<CustomerGuestsNoLoginModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsNoLoginModel>(FilterDefinition<CustomerGuestsNoLoginModel> filter);
}
