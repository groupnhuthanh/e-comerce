﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.EnvironmentGroup;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.EnvironmentGroup;

public interface IEnvironmentGroupRepository : IRepositoryBase<EnvironmentGroupModel>
{
    Task<EnvironmentGroupModel> Post(EnvironmentGroupModel doc);
    Task<EnvironmentGroupModel> GetSingle(FilterDefinition<EnvironmentGroupModel> filter = null, SortDefinition<EnvironmentGroupModel> sort = null, int limit = 0);
    Task<List<EnvironmentGroupModel>> GetList(FilterDefinition<EnvironmentGroupModel> filter, SortDefinition<EnvironmentGroupModel> sort, int limit = 0);
    Task<bool> Put<EnvironmentGroupModel>(FilterDefinition<EnvironmentGroupModel> filter, UpdateDefinition<EnvironmentGroupModel> updateDefinition);
    Task<bool> Delete<EnvironmentGroupModel>(FilterDefinition<EnvironmentGroupModel> filter);
}
