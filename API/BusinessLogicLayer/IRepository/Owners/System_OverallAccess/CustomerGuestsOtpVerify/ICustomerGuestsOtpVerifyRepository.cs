﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.CustomerGuestsOtpVerify;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.CustomerGuestsOtpVerify;

public interface ICustomerGuestsOtpVerifyRepository : IRepositoryBase<CustomerGuestsOtpVerifyModel>
{
    Task<CustomerGuestsOtpVerifyModel> Post(CustomerGuestsOtpVerifyModel doc);
    Task<CustomerGuestsOtpVerifyModel> GetSingle(FilterDefinition<CustomerGuestsOtpVerifyModel> filter = null, SortDefinition<CustomerGuestsOtpVerifyModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsOtpVerifyModel>> GetList(FilterDefinition<CustomerGuestsOtpVerifyModel> filter, SortDefinition<CustomerGuestsOtpVerifyModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsOtpVerifyModel>(FilterDefinition<CustomerGuestsOtpVerifyModel> filter, UpdateDefinition<CustomerGuestsOtpVerifyModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsOtpVerifyModel>(FilterDefinition<CustomerGuestsOtpVerifyModel> filter);
}
