﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.VariableDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.VariableDetail;

public interface IVariableDetailRepository : IRepositoryBase<VariableDetailModel>
{
    Task<VariableDetailModel> Post(VariableDetailModel doc);
    Task<VariableDetailModel> GetSingle(FilterDefinition<VariableDetailModel> filter = null, SortDefinition<VariableDetailModel> sort = null, int limit = 0);
    Task<List<VariableDetailModel>> GetList(FilterDefinition<VariableDetailModel> filter, SortDefinition<VariableDetailModel> sort, int limit = 0);
    Task<bool> Put<VariableDetailModel>(FilterDefinition<VariableDetailModel> filter, UpdateDefinition<VariableDetailModel> updateDefinition);
    Task<bool> Delete<VariableDetailModel>(FilterDefinition<VariableDetailModel> filter);
}
