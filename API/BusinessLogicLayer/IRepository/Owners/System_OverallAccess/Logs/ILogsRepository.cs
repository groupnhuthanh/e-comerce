﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.Logs;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.Logs;

public interface ILogsRepository : IRepositoryBase<LogsModel>
{
    Task<LogsModel> Post(LogsModel doc);
    Task<LogsModel> GetSingle(FilterDefinition<LogsModel> filter = null, SortDefinition<LogsModel> sort = null, int limit = 0);
    Task<List<LogsModel>> GetList(FilterDefinition<LogsModel> filter, SortDefinition<LogsModel> sort, int limit = 0);
    Task<bool> Put<LogsModel>(FilterDefinition<LogsModel> filter, UpdateDefinition<LogsModel> updateDefinition);
    Task<bool> Delete<LogsModel>(FilterDefinition<LogsModel> filter);
}
