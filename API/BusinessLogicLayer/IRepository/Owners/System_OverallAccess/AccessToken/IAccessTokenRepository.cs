﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.AccessToken;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.AccessToken;

public interface IAccessTokenRepository : IRepositoryBase<AccessTokenModel>
{
    Task<AccessTokenModel> Post(AccessTokenModel doc);
    Task<AccessTokenModel> GetSingle(FilterDefinition<AccessTokenModel> filter = null, SortDefinition<AccessTokenModel> sort = null, int limit = 0);
    Task<List<AccessTokenModel>> GetList(FilterDefinition<AccessTokenModel> filter, SortDefinition<AccessTokenModel> sort, int limit = 0);
    Task<bool> Put<AccessTokenModel>(FilterDefinition<AccessTokenModel> filter, UpdateDefinition<AccessTokenModel> updateDefinition);
    Task<bool> Delete<AccessTokenModel>(FilterDefinition<AccessTokenModel> filter);
}
