﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.OtpSettings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.OtpSettings;

public interface IOtpSettingsRepository : IRepositoryBase<OtpSettingsModel>
{
    Task<OtpSettingsModel> Post(OtpSettingsModel doc);
    Task<OtpSettingsModel> GetSingle(FilterDefinition<OtpSettingsModel> filter = null, SortDefinition<OtpSettingsModel> sort = null, int limit = 0);
    Task<List<OtpSettingsModel>> GetList(FilterDefinition<OtpSettingsModel> filter, SortDefinition<OtpSettingsModel> sort, int limit = 0);
    Task<bool> Put<OtpSettingsModel>(FilterDefinition<OtpSettingsModel> filter, UpdateDefinition<OtpSettingsModel> updateDefinition);
    Task<bool> Delete<OtpSettingsModel>(FilterDefinition<OtpSettingsModel> filter);
}
