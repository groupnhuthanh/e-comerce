﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.Variable;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.Variable;

public interface IVariableRepository : IRepositoryBase<VariableModel>
{
    Task<VariableModel> Post(VariableModel doc);
    Task<VariableModel> GetSingle(FilterDefinition<VariableModel> filter = null, SortDefinition<VariableModel> sort = null, int limit = 0);
    Task<List<VariableModel>> GetList(FilterDefinition<VariableModel> filter, SortDefinition<VariableModel> sort, int limit = 0);
    Task<bool> Put<VariableModel>(FilterDefinition<VariableModel> filter, UpdateDefinition<VariableModel> updateDefinition);
    Task<bool> Delete<VariableModel>(FilterDefinition<VariableModel> filter);
}
