﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.Status;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.Status;

public interface IStatusRepository : IRepositoryBase<StatusModel>
{
    Task<StatusModel> Post(StatusModel doc);
    Task<StatusModel> GetSingle(FilterDefinition<StatusModel> filter = null, SortDefinition<StatusModel> sort = null, int limit = 0);
    Task<List<StatusModel>> GetList(FilterDefinition<StatusModel> filter, SortDefinition<StatusModel> sort, int limit = 0);
    Task<bool> Put<StatusModel>(FilterDefinition<StatusModel> filter, UpdateDefinition<StatusModel> updateDefinition);
    Task<bool> Delete<StatusModel>(FilterDefinition<StatusModel> filter);
}
