﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.System_OverallAccess.TypeServer;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.System_OverallAccess.TypeServer;

public interface ITypeServerRepository : IRepositoryBase<TypeServerModel>
{
    Task<TypeServerModel> Post(TypeServerModel doc);
    Task<TypeServerModel> GetSingle(FilterDefinition<TypeServerModel> filter = null, SortDefinition<TypeServerModel> sort = null, int limit = 0);
    Task<List<TypeServerModel>> GetList(FilterDefinition<TypeServerModel> filter, SortDefinition<TypeServerModel> sort, int limit = 0);
    Task<bool> Put<TypeServerModel>(FilterDefinition<TypeServerModel> filter, UpdateDefinition<TypeServerModel> updateDefinition);
    Task<bool> Delete<TypeServerModel>(FilterDefinition<TypeServerModel> filter);
}
