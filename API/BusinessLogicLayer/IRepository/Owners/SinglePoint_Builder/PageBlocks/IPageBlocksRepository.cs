using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Builder.PageBlocks;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Builder.PageBlocks;


public interface IPageBlocksRepository : IRepositoryBase<PageBlocksModel>
{
    Task<PageBlocksModel> Post(PageBlocksModel doc);
    Task<PageBlocksModel> GetSingle(FilterDefinition<PageBlocksModel> filter = null, SortDefinition<PageBlocksModel> sort = null, int limit = 0);
    Task<List<PageBlocksModel>> GetList(FilterDefinition<PageBlocksModel> filter, SortDefinition<PageBlocksModel> sort, int limit = 0);
    Task<bool> Put<PageBlocksModel>(FilterDefinition<PageBlocksModel> filter, UpdateDefinition<PageBlocksModel> updateDefinition);
    Task<bool> Delete<PageBlocksModel>(FilterDefinition<PageBlocksModel> filter);
}