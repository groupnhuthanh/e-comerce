using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Builder.NavigationLink;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Builder.NavigationLink;

public interface INavigationLinkRepository : IRepositoryBase<NavigationLinkModel>
{
    Task<NavigationLinkModel> Post(NavigationLinkModel doc);
    Task<NavigationLinkModel> GetSingle(FilterDefinition<NavigationLinkModel> filter = null, SortDefinition<NavigationLinkModel> sort = null, int limit = 0);
    Task<List<NavigationLinkModel>> GetList(FilterDefinition<NavigationLinkModel> filter, SortDefinition<NavigationLinkModel> sort, int limit = 0);
    Task<bool> Put<NavigationLinkModel>(FilterDefinition<NavigationLinkModel> filter, UpdateDefinition<NavigationLinkModel> updateDefinition);
    Task<bool> Delete<NavigationLinkModel>(FilterDefinition<NavigationLinkModel> filter);
}