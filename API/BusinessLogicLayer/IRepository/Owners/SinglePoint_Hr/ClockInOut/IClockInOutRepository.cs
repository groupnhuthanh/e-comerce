﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Hr.ClockInOut;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Hr.ClockInOut;

public interface IClockInOutRepository : IRepositoryBase<ClockInOutModel>
{
    Task<ClockInOutModel> Post(ClockInOutModel doc);
    Task<ClockInOutModel> GetSingle(FilterDefinition<ClockInOutModel> filter = null, SortDefinition<ClockInOutModel> sort = null, int limit = 0);
    Task<List<ClockInOutModel>> GetList(FilterDefinition<ClockInOutModel> filter, SortDefinition<ClockInOutModel> sort, int limit = 0);
    Task<bool> Put<ClockInOutModel>(FilterDefinition<ClockInOutModel> filter, UpdateDefinition<ClockInOutModel> updateDefinition);
    Task<bool> Delete<ClockInOutModel>(FilterDefinition<ClockInOutModel> filter);
}
