﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisitionDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisitionDetail;

public interface IPurchaseRequisitionDetailRepository : IRepositoryBase<PurchaseRequisitionDetailModel>
{
    Task<PurchaseRequisitionDetailModel> Post(PurchaseRequisitionDetailModel doc);
    Task<PurchaseRequisitionDetailModel> GetSingle(FilterDefinition<PurchaseRequisitionDetailModel> filter = null, SortDefinition<PurchaseRequisitionDetailModel> sort = null, int limit = 0);
    Task<List<PurchaseRequisitionDetailModel>> GetList(FilterDefinition<PurchaseRequisitionDetailModel> filter, SortDefinition<PurchaseRequisitionDetailModel> sort, int limit = 0);
    Task<bool> Put<PurchaseRequisitionDetailModel>(FilterDefinition<PurchaseRequisitionDetailModel> filter, UpdateDefinition<PurchaseRequisitionDetailModel> updateDefinition);
    Task<bool> Delete<PurchaseRequisitionDetailModel>(FilterDefinition<PurchaseRequisitionDetailModel> filter);
}
