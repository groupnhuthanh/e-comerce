﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisition;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisition;

public interface IPurchaseRequisitionRepository : IRepositoryBase<PurchaseRequisitionModel>
{
    Task<PurchaseRequisitionModel> Post(PurchaseRequisitionModel doc);
    Task<PurchaseRequisitionModel> GetSingle(FilterDefinition<PurchaseRequisitionModel> filter = null, SortDefinition<PurchaseRequisitionModel> sort = null, int limit = 0);
    Task<List<PurchaseRequisitionModel>> GetList(FilterDefinition<PurchaseRequisitionModel> filter, SortDefinition<PurchaseRequisitionModel> sort, int limit = 0);
    Task<bool> Put<PurchaseRequisitionModel>(FilterDefinition<PurchaseRequisitionModel> filter, UpdateDefinition<PurchaseRequisitionModel> updateDefinition);
    Task<bool> Delete<PurchaseRequisitionModel>(FilterDefinition<PurchaseRequisitionModel> filter);
}
