﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedger;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedger;

public interface IPurchaseOrderLedgerRepository : IRepositoryBase<PurchaseOrderLedgerModel>
{
    Task<PurchaseOrderLedgerModel> Post(PurchaseOrderLedgerModel doc);
    Task<PurchaseOrderLedgerModel> GetSingle(FilterDefinition<PurchaseOrderLedgerModel> filter = null, SortDefinition<PurchaseOrderLedgerModel> sort = null, int limit = 0);
    Task<List<PurchaseOrderLedgerModel>> GetList(FilterDefinition<PurchaseOrderLedgerModel> filter, SortDefinition<PurchaseOrderLedgerModel> sort, int limit = 0);
    Task<bool> Put<PurchaseOrderLedgerModel>(FilterDefinition<PurchaseOrderLedgerModel> filter, UpdateDefinition<PurchaseOrderLedgerModel> updateDefinition);
    Task<bool> Delete<PurchaseOrderLedgerModel>(FilterDefinition<PurchaseOrderLedgerModel> filter);
}
