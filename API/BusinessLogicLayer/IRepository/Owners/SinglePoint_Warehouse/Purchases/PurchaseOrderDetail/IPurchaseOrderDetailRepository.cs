﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderDetail;

public interface IPurchaseOrderDetailRepository : IRepositoryBase<PurchaseOrderDetailModel>
{
    Task<PurchaseOrderDetailModel> Post(PurchaseOrderDetailModel doc);
    Task<PurchaseOrderDetailModel> GetSingle(FilterDefinition<PurchaseOrderDetailModel> filter = null, SortDefinition<PurchaseOrderDetailModel> sort = null, int limit = 0);
    Task<List<PurchaseOrderDetailModel>> GetList(FilterDefinition<PurchaseOrderDetailModel> filter, SortDefinition<PurchaseOrderDetailModel> sort, int limit = 0);
    Task<bool> Put<PurchaseOrderDetailModel>(FilterDefinition<PurchaseOrderDetailModel> filter, UpdateDefinition<PurchaseOrderDetailModel> updateDefinition);
    Task<bool> Delete<PurchaseOrderDetailModel>(FilterDefinition<PurchaseOrderDetailModel> filter);
}
