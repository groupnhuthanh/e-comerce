﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrder;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrder;

public interface IPurchaseOrderRepository : IRepositoryBase<PurchaseOrderModel>
{
    Task<PurchaseOrderModel> Post(PurchaseOrderModel doc);
    Task<PurchaseOrderModel> GetSingle(FilterDefinition<PurchaseOrderModel> filter = null, SortDefinition<PurchaseOrderModel> sort = null, int limit = 0);
    Task<List<PurchaseOrderModel>> GetList(FilterDefinition<PurchaseOrderModel> filter, SortDefinition<PurchaseOrderModel> sort, int limit = 0);
    Task<bool> Put<PurchaseOrderModel>(FilterDefinition<PurchaseOrderModel> filter, UpdateDefinition<PurchaseOrderModel> updateDefinition);
    Task<bool> Delete<PurchaseOrderModel>(FilterDefinition<PurchaseOrderModel> filter);
}
