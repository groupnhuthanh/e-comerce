﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedgerDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedgerDetail;

public interface IPurchaseOrderLedgerDetailRepository : IRepositoryBase<PurchaseOrderLedgerDetailModel>
{
    Task<PurchaseOrderLedgerDetailModel> Post(PurchaseOrderLedgerDetailModel doc);
    Task<PurchaseOrderLedgerDetailModel> GetSingle(FilterDefinition<PurchaseOrderLedgerDetailModel> filter = null, SortDefinition<PurchaseOrderLedgerDetailModel> sort = null, int limit = 0);
    Task<List<PurchaseOrderLedgerDetailModel>> GetList(FilterDefinition<PurchaseOrderLedgerDetailModel> filter, SortDefinition<PurchaseOrderLedgerDetailModel> sort, int limit = 0);
    Task<bool> Put<PurchaseOrderLedgerDetailModel>(FilterDefinition<PurchaseOrderLedgerDetailModel> filter, UpdateDefinition<PurchaseOrderLedgerDetailModel> updateDefinition);
    Task<bool> Delete<PurchaseOrderLedgerDetailModel>(FilterDefinition<PurchaseOrderLedgerDetailModel> filter);
}
