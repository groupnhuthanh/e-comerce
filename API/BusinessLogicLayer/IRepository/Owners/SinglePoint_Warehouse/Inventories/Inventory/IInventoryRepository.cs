﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Inventories.Inventory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Inventories.Inventory;

public interface IInventoryRepository : IRepositoryBase<InventoryModel>
{
    Task<InventoryModel> Post(InventoryModel doc);
    Task<InventoryModel> GetSingle(FilterDefinition<InventoryModel> filter = null, SortDefinition<InventoryModel> sort = null, int limit = 0);
    Task<List<InventoryModel>> GetList(FilterDefinition<InventoryModel> filter, SortDefinition<InventoryModel> sort, int limit = 0);
    Task<bool> Put<InventoryModel>(FilterDefinition<InventoryModel> filter, UpdateDefinition<InventoryModel> updateDefinition);
    Task<bool> Delete<InventoryModel>(FilterDefinition<InventoryModel> filter);
}
