﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Inventories.InventoryHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Inventories.InventoryHistory;

public interface IInventoryHistoryRepository : IRepositoryBase<InventoryHistoryModel>
{
    Task<InventoryHistoryModel> Post(InventoryHistoryModel doc);
    Task<InventoryHistoryModel> GetSingle(FilterDefinition<InventoryHistoryModel> filter = null, SortDefinition<InventoryHistoryModel> sort = null, int limit = 0);
    Task<List<InventoryHistoryModel>> GetList(FilterDefinition<InventoryHistoryModel> filter, SortDefinition<InventoryHistoryModel> sort, int limit = 0);
    Task<bool> Put<InventoryHistoryModel>(FilterDefinition<InventoryHistoryModel> filter, UpdateDefinition<InventoryHistoryModel> updateDefinition);
    Task<bool> Delete<InventoryHistoryModel>(FilterDefinition<InventoryHistoryModel> filter);
}
