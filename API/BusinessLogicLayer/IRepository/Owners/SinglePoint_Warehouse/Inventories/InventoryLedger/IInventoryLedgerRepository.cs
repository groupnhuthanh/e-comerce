﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Inventories.InventoryLedger;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Inventories.InventoryLedger;

public interface IInventoryLedgerRepository : IRepositoryBase<InventoryLedgerModel>
{
    Task<InventoryLedgerModel> Post(InventoryLedgerModel doc);
    Task<InventoryLedgerModel> GetSingle(FilterDefinition<InventoryLedgerModel> filter = null, SortDefinition<InventoryLedgerModel> sort = null, int limit = 0);
    Task<List<InventoryLedgerModel>> GetList(FilterDefinition<InventoryLedgerModel> filter, SortDefinition<InventoryLedgerModel> sort, int limit = 0);
    Task<bool> Put<InventoryLedgerModel>(FilterDefinition<InventoryLedgerModel> filter, UpdateDefinition<InventoryLedgerModel> updateDefinition);
    Task<bool> Delete<InventoryLedgerModel>(FilterDefinition<InventoryLedgerModel> filter);
}
