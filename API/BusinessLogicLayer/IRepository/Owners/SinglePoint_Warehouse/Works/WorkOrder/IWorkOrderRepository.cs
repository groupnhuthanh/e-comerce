﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Works.WorkOrder;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Works.WorkOrder;

public interface IWorkOrderRepository : IRepositoryBase<WorkOrderModel>
{
    Task<WorkOrderModel> Post(WorkOrderModel doc);
    Task<WorkOrderModel> GetSingle(FilterDefinition<WorkOrderModel> filter = null, SortDefinition<WorkOrderModel> sort = null, int limit = 0);
    Task<List<WorkOrderModel>> GetList(FilterDefinition<WorkOrderModel> filter, SortDefinition<WorkOrderModel> sort, int limit = 0);
    Task<bool> Put<WorkOrderModel>(FilterDefinition<WorkOrderModel> filter, UpdateDefinition<WorkOrderModel> updateDefinition);
    Task<bool> Delete<WorkOrderModel>(FilterDefinition<WorkOrderModel> filter);
}
