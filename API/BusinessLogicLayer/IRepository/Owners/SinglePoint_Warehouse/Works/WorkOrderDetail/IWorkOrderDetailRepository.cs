﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Works.WorkOrderDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Works.WorkOrderDetail;

public interface IWorkOrderDetailRepository : IRepositoryBase<WorkOrderDetailModel>
{
    Task<WorkOrderDetailModel> Post(WorkOrderDetailModel doc);
    Task<WorkOrderDetailModel> GetSingle(FilterDefinition<WorkOrderDetailModel> filter = null, SortDefinition<WorkOrderDetailModel> sort = null, int limit = 0);
    Task<List<WorkOrderDetailModel>> GetList(FilterDefinition<WorkOrderDetailModel> filter, SortDefinition<WorkOrderDetailModel> sort, int limit = 0);
    Task<bool> Put<WorkOrderDetailModel>(FilterDefinition<WorkOrderDetailModel> filter, UpdateDefinition<WorkOrderDetailModel> updateDefinition);
    Task<bool> Delete<WorkOrderDetailModel>(FilterDefinition<WorkOrderDetailModel> filter);
}
