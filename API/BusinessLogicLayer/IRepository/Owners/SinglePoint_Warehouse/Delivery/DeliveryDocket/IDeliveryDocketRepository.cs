﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocket;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocket;

public interface IDeliveryDocketRepository : IRepositoryBase<DeliveryDocketModel>
{
    Task<DeliveryDocketModel> Post(DeliveryDocketModel doc);
    Task<DeliveryDocketModel> GetSingle(FilterDefinition<DeliveryDocketModel> filter = null, SortDefinition<DeliveryDocketModel> sort = null, int limit = 0);
    Task<List<DeliveryDocketModel>> GetList(FilterDefinition<DeliveryDocketModel> filter, SortDefinition<DeliveryDocketModel> sort, int limit = 0);
    Task<bool> Put<DeliveryDocketModel>(FilterDefinition<DeliveryDocketModel> filter, UpdateDefinition<DeliveryDocketModel> updateDefinition);
    Task<bool> Delete<DeliveryDocketModel>(FilterDefinition<DeliveryDocketModel> filter);
}
