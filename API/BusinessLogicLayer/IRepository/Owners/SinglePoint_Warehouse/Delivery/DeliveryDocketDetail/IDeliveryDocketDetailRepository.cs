﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocketDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocketDetail;

public interface IDeliveryDocketDetailRepository : IRepositoryBase<DeliveryDocketDetailModel>
{
    Task<DeliveryDocketDetailModel> Post(DeliveryDocketDetailModel doc);
    Task<DeliveryDocketDetailModel> GetSingle(FilterDefinition<DeliveryDocketDetailModel> filter = null, SortDefinition<DeliveryDocketDetailModel> sort = null, int limit = 0);
    Task<List<DeliveryDocketDetailModel>> GetList(FilterDefinition<DeliveryDocketDetailModel> filter, SortDefinition<DeliveryDocketDetailModel> sort, int limit = 0);
    Task<bool> Put<DeliveryDocketDetailModel>(FilterDefinition<DeliveryDocketDetailModel> filter, UpdateDefinition<DeliveryDocketDetailModel> updateDefinition);
    Task<bool> Delete<DeliveryDocketDetailModel>(FilterDefinition<DeliveryDocketDetailModel> filter);
}
