﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.Heineken.CustomerNFTsWallet;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.Heineken.CustomerNFTsWallet;

public interface ICustomerNFTsWalletRepository : IRepositoryBase<CustomerNFTsWalletModel>
{
    Task<CustomerNFTsWalletModel> Post(CustomerNFTsWalletModel doc);
    Task<CustomerNFTsWalletModel> GetSingle(FilterDefinition<CustomerNFTsWalletModel> filter = null, SortDefinition<CustomerNFTsWalletModel> sort = null, int limit = 0);
    Task<List<CustomerNFTsWalletModel>> GetList(FilterDefinition<CustomerNFTsWalletModel> filter, SortDefinition<CustomerNFTsWalletModel> sort, int limit = 0);
    Task<bool> Put<CustomerNFTsWalletModel>(FilterDefinition<CustomerNFTsWalletModel> filter, UpdateDefinition<CustomerNFTsWalletModel> updateDefinition);
    Task<bool> Delete<CustomerNFTsWalletModel>(FilterDefinition<CustomerNFTsWalletModel> filter);
}
