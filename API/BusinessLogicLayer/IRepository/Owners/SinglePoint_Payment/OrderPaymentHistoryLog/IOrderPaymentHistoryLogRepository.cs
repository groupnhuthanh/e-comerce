﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Payment.OrderPaymentHistoryLog;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.OrderPaymentHistoryLog;

public interface IOrderPaymentHistoryLogRepository : IRepositoryBase<OrderPaymentHistoryLogModel>
{
    Task<OrderPaymentHistoryLogModel> Post(OrderPaymentHistoryLogModel doc);
    Task<OrderPaymentHistoryLogModel> GetSingle(FilterDefinition<OrderPaymentHistoryLogModel> filter = null, SortDefinition<OrderPaymentHistoryLogModel> sort = null, int limit = 0);
    Task<List<OrderPaymentHistoryLogModel>> GetList(FilterDefinition<OrderPaymentHistoryLogModel> filter, SortDefinition<OrderPaymentHistoryLogModel> sort, int limit = 0);
    Task<bool> Put<OrderPaymentHistoryLogModel>(FilterDefinition<OrderPaymentHistoryLogModel> filter, UpdateDefinition<OrderPaymentHistoryLogModel> updateDefinition);
    Task<bool> Delete<OrderPaymentHistoryLogModel>(FilterDefinition<OrderPaymentHistoryLogModel> filter);
}
