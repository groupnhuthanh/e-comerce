﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Payment.OrderGetInfoPayooHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.OrderGetInfoPayooHistory;

public interface IOrderGetInfoPayooHistoryRepository : IRepositoryBase<OrderGetInfoPayooHistoryModel>
{
    Task<OrderGetInfoPayooHistoryModel> Post(OrderGetInfoPayooHistoryModel doc);
    Task<OrderGetInfoPayooHistoryModel> GetSingle(FilterDefinition<OrderGetInfoPayooHistoryModel> filter = null, SortDefinition<OrderGetInfoPayooHistoryModel> sort = null, int limit = 0);
    Task<List<OrderGetInfoPayooHistoryModel>> GetList(FilterDefinition<OrderGetInfoPayooHistoryModel> filter, SortDefinition<OrderGetInfoPayooHistoryModel> sort, int limit = 0);
    Task<bool> Put<OrderGetInfoPayooHistoryModel>(FilterDefinition<OrderGetInfoPayooHistoryModel> filter, UpdateDefinition<OrderGetInfoPayooHistoryModel> updateDefinition);
    Task<bool> Delete<OrderGetInfoPayooHistoryModel>(FilterDefinition<OrderGetInfoPayooHistoryModel> filter);
}
