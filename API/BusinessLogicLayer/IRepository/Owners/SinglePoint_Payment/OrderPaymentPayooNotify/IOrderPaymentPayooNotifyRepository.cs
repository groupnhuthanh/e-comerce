﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Payment.OrderPaymentPayooNotify;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.OrderPaymentPayooNotify;

public interface IOrderPaymentPayooNotifyRepository : IRepositoryBase<OrderPaymentPayooNotifyModel>
{
    Task<OrderPaymentPayooNotifyModel> Post(OrderPaymentPayooNotifyModel doc);
    Task<OrderPaymentPayooNotifyModel> GetSingle(FilterDefinition<OrderPaymentPayooNotifyModel> filter = null, SortDefinition<OrderPaymentPayooNotifyModel> sort = null, int limit = 0);
    Task<List<OrderPaymentPayooNotifyModel>> GetList(FilterDefinition<OrderPaymentPayooNotifyModel> filter, SortDefinition<OrderPaymentPayooNotifyModel> sort, int limit = 0);
    Task<bool> Put<OrderPaymentPayooNotifyModel>(FilterDefinition<OrderPaymentPayooNotifyModel> filter, UpdateDefinition<OrderPaymentPayooNotifyModel> updateDefinition);
    Task<bool> Delete<OrderPaymentPayooNotifyModel>(FilterDefinition<OrderPaymentPayooNotifyModel> filter);

}
