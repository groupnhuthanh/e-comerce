﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Payment.ClSOrderPaymentPrepaid;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.ClSOrderPaymentPrepaid;

public interface IClSOrderPaymentPrepaidRepository : IRepositoryBase<ClSOrderPaymentPrepaidModel>
{
    Task<ClSOrderPaymentPrepaidModel> Post(ClSOrderPaymentPrepaidModel doc);
    Task<ClSOrderPaymentPrepaidModel> GetSingle(FilterDefinition<ClSOrderPaymentPrepaidModel> filter = null, SortDefinition<ClSOrderPaymentPrepaidModel> sort = null, int limit = 0);
    Task<List<ClSOrderPaymentPrepaidModel>> GetList(FilterDefinition<ClSOrderPaymentPrepaidModel> filter, SortDefinition<ClSOrderPaymentPrepaidModel> sort, int limit = 0);
    Task<bool> Put<ClSOrderPaymentPrepaidModel>(FilterDefinition<ClSOrderPaymentPrepaidModel> filter, UpdateDefinition<ClSOrderPaymentPrepaidModel> updateDefinition);
    Task<bool> Delete<ClSOrderPaymentPrepaidModel>(FilterDefinition<ClSOrderPaymentPrepaidModel> filter);
}
