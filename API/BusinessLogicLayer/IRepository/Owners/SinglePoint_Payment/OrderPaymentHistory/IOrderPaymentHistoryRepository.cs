﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Payment.OrderPaymentHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.OrderPaymentHistory;

public interface IOrderPaymentHistoryRepository : IRepositoryBase<OrderPaymentHistoryModel>
{
    Task<OrderPaymentHistoryModel> Post(OrderPaymentHistoryModel doc);
    Task<OrderPaymentHistoryModel> GetSingle(FilterDefinition<OrderPaymentHistoryModel> filter = null, SortDefinition<OrderPaymentHistoryModel> sort = null, int limit = 0);
    Task<List<OrderPaymentHistoryModel>> GetList(FilterDefinition<OrderPaymentHistoryModel> filter, SortDefinition<OrderPaymentHistoryModel> sort, int limit = 0);
    Task<bool> Put<OrderPaymentHistoryModel>(FilterDefinition<OrderPaymentHistoryModel> filter, UpdateDefinition<OrderPaymentHistoryModel> updateDefinition);
    Task<bool> Delete<OrderPaymentHistoryModel>(FilterDefinition<OrderPaymentHistoryModel> filter);
}
