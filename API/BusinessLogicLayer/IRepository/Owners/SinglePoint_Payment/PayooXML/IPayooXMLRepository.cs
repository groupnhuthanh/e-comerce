﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Payment.PayooXML;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.PayooXML;

public interface IPayooXMLRepository : IRepositoryBase<PayooXMLModel>
{
    Task<PayooXMLModel> Post(PayooXMLModel doc);
    Task<PayooXMLModel> GetSingle(FilterDefinition<PayooXMLModel> filter = null, SortDefinition<PayooXMLModel> sort = null, int limit = 0);
    Task<List<PayooXMLModel>> GetList(FilterDefinition<PayooXMLModel> filter, SortDefinition<PayooXMLModel> sort, int limit = 0);
    Task<bool> Put<PayooXMLModel>(FilterDefinition<PayooXMLModel> filter, UpdateDefinition<PayooXMLModel> updateDefinition);
    Task<bool> Delete<PayooXMLModel>(FilterDefinition<PayooXMLModel> filter);
}
