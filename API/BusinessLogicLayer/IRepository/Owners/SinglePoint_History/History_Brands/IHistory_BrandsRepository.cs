﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_History.History_Brands;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_History.History_Brands;

public interface IHistory_BrandsRepository : IRepositoryBase<History_BrandsModel>
{
    Task<History_BrandsModel> Post(History_BrandsModel doc);
    Task<History_BrandsModel> GetSingle(FilterDefinition<History_BrandsModel> filter = null, SortDefinition<History_BrandsModel> sort = null, int limit = 0);
    Task<List<History_BrandsModel>> GetList(FilterDefinition<History_BrandsModel> filter, SortDefinition<History_BrandsModel> sort, int limit = 0);
    Task<bool> Put<History_BrandsModel>(FilterDefinition<History_BrandsModel> filter, UpdateDefinition<History_BrandsModel> updateDefinition);
    Task<bool> Delete<History_BrandsModel>(FilterDefinition<History_BrandsModel> filter);
}
