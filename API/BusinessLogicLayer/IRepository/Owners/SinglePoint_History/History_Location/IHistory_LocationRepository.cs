﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_History.History_Location;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_History.History_Location;

public interface IHistory_LocationRepository : IRepositoryBase<History_LocationModel>
{
    Task<History_LocationModel> Post(History_LocationModel doc);
    Task<History_LocationModel> GetSingle(FilterDefinition<History_LocationModel> filter = null, SortDefinition<History_LocationModel> sort = null, int limit = 0);
    Task<List<History_LocationModel>> GetList(FilterDefinition<History_LocationModel> filter, SortDefinition<History_LocationModel> sort, int limit = 0);
    Task<bool> Put<History_LocationModel>(FilterDefinition<History_LocationModel> filter, UpdateDefinition<History_LocationModel> updateDefinition);
    Task<bool> Delete<History_LocationModel>(FilterDefinition<History_LocationModel> filter);
}
