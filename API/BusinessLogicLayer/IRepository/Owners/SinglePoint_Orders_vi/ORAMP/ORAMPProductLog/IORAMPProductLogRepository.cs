﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPProductLog;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPProductLog;

public interface IORAMPProductLogRepository : IRepositoryBase<ORAMPProductLogModel>
{
    Task<ORAMPProductLogModel> Post(ORAMPProductLogModel doc);
    Task<ORAMPProductLogModel> GetSingle(FilterDefinition<ORAMPProductLogModel> filter = null, SortDefinition<ORAMPProductLogModel> sort = null, int limit = 0);
    Task<List<ORAMPProductLogModel>> GetList(FilterDefinition<ORAMPProductLogModel> filter, SortDefinition<ORAMPProductLogModel> sort, int limit = 0);
    Task<bool> Put<ORAMPProductLogModel>(FilterDefinition<ORAMPProductLogModel> filter, UpdateDefinition<ORAMPProductLogModel> updateDefinition);
    Task<bool> Delete<ORAMPProductLogModel>(FilterDefinition<ORAMPProductLogModel> filter);
}
