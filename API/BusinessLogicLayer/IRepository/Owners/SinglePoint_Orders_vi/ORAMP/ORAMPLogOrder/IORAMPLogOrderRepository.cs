﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPLogOrder;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPLogOrder;

public interface IORAMPLogOrderRepository : IRepositoryBase<ORAMPLogOrderModel>
{
    Task<ORAMPLogOrderModel> Post(ORAMPLogOrderModel doc);
    Task<ORAMPLogOrderModel> GetSingle(FilterDefinition<ORAMPLogOrderModel> filter = null, SortDefinition<ORAMPLogOrderModel> sort = null, int limit = 0);
    Task<List<ORAMPLogOrderModel>> GetList(FilterDefinition<ORAMPLogOrderModel> filter, SortDefinition<ORAMPLogOrderModel> sort, int limit = 0);
    Task<bool> Put<ORAMPLogOrderModel>(FilterDefinition<ORAMPLogOrderModel> filter, UpdateDefinition<ORAMPLogOrderModel> updateDefinition);
    Task<bool> Delete<ORAMPLogOrderModel>(FilterDefinition<ORAMPLogOrderModel> filter);
}
