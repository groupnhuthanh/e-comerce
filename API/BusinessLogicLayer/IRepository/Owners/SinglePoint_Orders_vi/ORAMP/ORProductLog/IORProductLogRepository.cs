﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORProductLog;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.ORAMP.ORProductLog;

public interface IORProductLogRepository : IRepositoryBase<ORProductLogModel>
{
    Task<ORProductLogModel> Post(ORProductLogModel doc);
    Task<ORProductLogModel> GetSingle(FilterDefinition<ORProductLogModel> filter = null, SortDefinition<ORProductLogModel> sort = null, int limit = 0);
    Task<List<ORProductLogModel>> GetList(FilterDefinition<ORProductLogModel> filter, SortDefinition<ORProductLogModel> sort, int limit = 0);
    Task<bool> Put<ORProductLogModel>(FilterDefinition<ORProductLogModel> filter, UpdateDefinition<ORProductLogModel> updateDefinition);
    Task<bool> Delete<ORProductLogModel>(FilterDefinition<ORProductLogModel> filter);
}
