﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Refunds.RefundOrders;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Refunds.RefundOrders;

public interface IRefundOrdersRepository : IRepositoryBase<RefundOrdersModel>
{
    Task<RefundOrdersModel> Post(RefundOrdersModel doc);
    Task<RefundOrdersModel> GetSingle(FilterDefinition<RefundOrdersModel> filter = null, SortDefinition<RefundOrdersModel> sort = null, int limit = 0);
    Task<List<RefundOrdersModel>> GetList(FilterDefinition<RefundOrdersModel> filter, SortDefinition<RefundOrdersModel> sort, int limit = 0);
    Task<bool> Put<RefundOrdersModel>(FilterDefinition<RefundOrdersModel> filter, UpdateDefinition<RefundOrdersModel> updateDefinition);
    Task<bool> Delete<RefundOrdersModel>(FilterDefinition<RefundOrdersModel> filter);
}
