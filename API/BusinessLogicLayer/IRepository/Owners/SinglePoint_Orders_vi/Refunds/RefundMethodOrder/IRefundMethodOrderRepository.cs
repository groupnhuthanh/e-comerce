﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Refunds.RefundMethodOrder;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Refunds.RefundMethodOrder;

public interface IRefundMethodOrderRepository : IRepositoryBase<RefundMethodOrderModel>
{
    Task<RefundMethodOrderModel> Post(RefundMethodOrderModel doc);
    Task<RefundMethodOrderModel> GetSingle(FilterDefinition<RefundMethodOrderModel> filter = null, SortDefinition<RefundMethodOrderModel> sort = null, int limit = 0);
    Task<List<RefundMethodOrderModel>> GetList(FilterDefinition<RefundMethodOrderModel> filter, SortDefinition<RefundMethodOrderModel> sort, int limit = 0);
    Task<bool> Put<RefundMethodOrderModel>(FilterDefinition<RefundMethodOrderModel> filter, UpdateDefinition<RefundMethodOrderModel> updateDefinition);
    Task<bool> Delete<RefundMethodOrderModel>(FilterDefinition<RefundMethodOrderModel> filter);
}
