﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerSaleOrders;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerSaleOrders;

public interface ICashDrawerSaleOrdersRepository : IRepositoryBase<CashDrawerSaleOrdersModel>
{
    Task<CashDrawerSaleOrdersModel> Post(CashDrawerSaleOrdersModel doc);
    Task<CashDrawerSaleOrdersModel> GetSingle(FilterDefinition<CashDrawerSaleOrdersModel> filter = null, SortDefinition<CashDrawerSaleOrdersModel> sort = null, int limit = 0);
    Task<List<CashDrawerSaleOrdersModel>> GetList(FilterDefinition<CashDrawerSaleOrdersModel> filter, SortDefinition<CashDrawerSaleOrdersModel> sort, int limit = 0);
    Task<bool> Put<CashDrawerSaleOrdersModel>(FilterDefinition<CashDrawerSaleOrdersModel> filter, UpdateDefinition<CashDrawerSaleOrdersModel> updateDefinition);
    Task<bool> Delete<CashDrawerSaleOrdersModel>(FilterDefinition<CashDrawerSaleOrdersModel> filter);
}
