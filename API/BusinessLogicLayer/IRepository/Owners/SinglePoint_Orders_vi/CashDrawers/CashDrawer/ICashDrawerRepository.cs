﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;

public interface ICashDrawerRepository : IRepositoryBase<CashDrawerModel>
{
    Task<CashDrawerModel> Post(CashDrawerModel doc);
    Task<CashDrawerModel> GetSingle(FilterDefinition<CashDrawerModel> filter = null, SortDefinition<CashDrawerModel> sort = null, int limit = 0);
    Task<List<CashDrawerModel>> GetList(FilterDefinition<CashDrawerModel> filter, SortDefinition<CashDrawerModel> sort, int limit = 0);
    Task<bool> Put<CashDrawerModel>(FilterDefinition<CashDrawerModel> filter, UpdateDefinition<CashDrawerModel> updateDefinition);
    Task<bool> Delete<CashDrawerModel>(FilterDefinition<CashDrawerModel> filter);
}
