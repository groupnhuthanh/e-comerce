﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerPayInOut;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerPayInOut;

public interface ICashDrawerPayInOutRepository : IRepositoryBase<CashDrawerPayInOutModel>
{
    Task<CashDrawerPayInOutModel> Post(CashDrawerPayInOutModel doc);
    Task<CashDrawerPayInOutModel> GetSingle(FilterDefinition<CashDrawerPayInOutModel> filter = null, SortDefinition<CashDrawerPayInOutModel> sort = null, int limit = 0);
    Task<List<CashDrawerPayInOutModel>> GetList(FilterDefinition<CashDrawerPayInOutModel> filter, SortDefinition<CashDrawerPayInOutModel> sort, int limit = 0);
    Task<bool> Put<CashDrawerPayInOutModel>(FilterDefinition<CashDrawerPayInOutModel> filter, UpdateDefinition<CashDrawerPayInOutModel> updateDefinition);
    Task<bool> Delete<CashDrawerPayInOutModel>(FilterDefinition<CashDrawerPayInOutModel> filter);
}
