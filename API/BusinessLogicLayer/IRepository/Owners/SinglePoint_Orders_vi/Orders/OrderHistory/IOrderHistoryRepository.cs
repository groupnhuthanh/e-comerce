﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderHistory;

public interface IOrderHistoryRepository : IRepositoryBase<OrderHistoryModel>
{
    Task<OrderHistoryModel> Post(OrderHistoryModel doc);
    Task<OrderHistoryModel> GetSingle(FilterDefinition<OrderHistoryModel> filter = null, SortDefinition<OrderHistoryModel> sort = null, int limit = 0);
    Task<List<OrderHistoryModel>> GetList(FilterDefinition<OrderHistoryModel> filter, SortDefinition<OrderHistoryModel> sort, int limit = 0);
    Task<bool> Put<OrderHistoryModel>(FilterDefinition<OrderHistoryModel> filter, UpdateDefinition<OrderHistoryModel> updateDefinition);
    Task<bool> Delete<OrderHistoryModel>(FilterDefinition<OrderHistoryModel> filter);
}
