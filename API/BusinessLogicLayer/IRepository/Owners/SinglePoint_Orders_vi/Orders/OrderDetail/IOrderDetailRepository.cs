﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;

public interface IOrderDetailRepository : IRepositoryBase<OrderDetailModel>
{
    Task<OrderDetailModel> Post(OrderDetailModel doc);
    Task<OrderDetailModel> GetSingle(FilterDefinition<OrderDetailModel> filter = null, SortDefinition<OrderDetailModel> sort = null, int limit = 0);
    Task<List<OrderDetailModel>> GetList(FilterDefinition<OrderDetailModel> filter, SortDefinition<OrderDetailModel> sort, int limit = 0);
    Task<bool> Put<OrderDetailModel>(FilterDefinition<OrderDetailModel> filter, UpdateDefinition<OrderDetailModel> updateDefinition);
    Task<bool> Delete<OrderDetailModel>(FilterDefinition<OrderDetailModel> filter);
}
