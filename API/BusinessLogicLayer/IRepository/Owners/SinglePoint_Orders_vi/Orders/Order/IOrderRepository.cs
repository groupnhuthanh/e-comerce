﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Orders.Order;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.Order;

public interface IOrderRepository : IRepositoryBase<OrderModel>
{
    Task<OrderModel> Post(OrderModel doc);
    Task<OrderModel> GetSingle(FilterDefinition<OrderModel> filter = null, SortDefinition<OrderModel> sort = null, int limit = 0);
    Task<List<OrderModel>> GetList(FilterDefinition<OrderModel> filter, SortDefinition<OrderModel> sort, int limit = 0);
    Task<bool> Put<OrderModel>(FilterDefinition<OrderModel> filter, UpdateDefinition<OrderModel> updateDefinition);
    Task<bool> Delete<OrderModel>(FilterDefinition<OrderModel> filter);
}
