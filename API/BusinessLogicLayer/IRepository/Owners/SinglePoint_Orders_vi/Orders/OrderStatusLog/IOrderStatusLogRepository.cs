﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderStatusLog;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderStatusLog;

public interface IOrderStatusLogRepository : IRepositoryBase<OrderStatusLogModel>
{
    Task<OrderStatusLogModel> Post(OrderStatusLogModel doc);
    Task<OrderStatusLogModel> GetSingle(FilterDefinition<OrderStatusLogModel> filter = null, SortDefinition<OrderStatusLogModel> sort = null, int limit = 0);
    Task<List<OrderStatusLogModel>> GetList(FilterDefinition<OrderStatusLogModel> filter, SortDefinition<OrderStatusLogModel> sort, int limit = 0);
    Task<bool> Put<OrderStatusLogModel>(FilterDefinition<OrderStatusLogModel> filter, UpdateDefinition<OrderStatusLogModel> updateDefinition);
    Task<bool> Delete<OrderStatusLogModel>(FilterDefinition<OrderStatusLogModel> filter);

}
