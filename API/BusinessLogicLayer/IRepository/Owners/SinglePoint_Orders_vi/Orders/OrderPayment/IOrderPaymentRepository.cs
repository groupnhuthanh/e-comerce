﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderPayment;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderPayment;

public interface IOrderPaymentRepository : IRepositoryBase<OrderPaymentModel>
{
    Task<OrderPaymentModel> Post(OrderPaymentModel doc);
    Task<OrderPaymentModel> GetSingle(FilterDefinition<OrderPaymentModel> filter = null, SortDefinition<OrderPaymentModel> sort = null, int limit = 0);
    Task<List<OrderPaymentModel>> GetList(FilterDefinition<OrderPaymentModel> filter, SortDefinition<OrderPaymentModel> sort, int limit = 0);
    Task<bool> Put<OrderPaymentModel>(FilterDefinition<OrderPaymentModel> filter, UpdateDefinition<OrderPaymentModel> updateDefinition);
    Task<bool> Delete<OrderPaymentModel>(FilterDefinition<OrderPaymentModel> filter);

}
