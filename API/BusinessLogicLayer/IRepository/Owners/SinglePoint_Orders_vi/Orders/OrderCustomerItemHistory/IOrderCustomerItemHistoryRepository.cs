﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderCustomerItemHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderCustomerItemHistory;

public interface IOrderCustomerItemHistoryRepository : IRepositoryBase<OrderCustomerItemHistoryModel>
{
    Task<OrderCustomerItemHistoryModel> Post(OrderCustomerItemHistoryModel doc);
    Task<OrderCustomerItemHistoryModel> GetSingle(FilterDefinition<OrderCustomerItemHistoryModel> filter = null, SortDefinition<OrderCustomerItemHistoryModel> sort = null, int limit = 0);
    Task<List<OrderCustomerItemHistoryModel>> GetList(FilterDefinition<OrderCustomerItemHistoryModel> filter, SortDefinition<OrderCustomerItemHistoryModel> sort, int limit = 0);
    Task<bool> Put<OrderCustomerItemHistoryModel>(FilterDefinition<OrderCustomerItemHistoryModel> filter, UpdateDefinition<OrderCustomerItemHistoryModel> updateDefinition);
    Task<bool> Delete<OrderCustomerItemHistoryModel>(FilterDefinition<OrderCustomerItemHistoryModel> filter);
}
