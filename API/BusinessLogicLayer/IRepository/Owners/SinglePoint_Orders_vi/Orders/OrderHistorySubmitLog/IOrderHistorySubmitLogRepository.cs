﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderHistorySubmitLog;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderHistorySubmitLog;

public interface IOrderHistorySubmitLogRepository : IRepositoryBase<OrderHistorySubmitLogModel>
{
    Task<OrderHistorySubmitLogModel> Post(OrderHistorySubmitLogModel doc);
    Task<OrderHistorySubmitLogModel> GetSingle(FilterDefinition<OrderHistorySubmitLogModel> filter = null, SortDefinition<OrderHistorySubmitLogModel> sort = null, int limit = 0);
    Task<List<OrderHistorySubmitLogModel>> GetList(FilterDefinition<OrderHistorySubmitLogModel> filter, SortDefinition<OrderHistorySubmitLogModel> sort, int limit = 0);
    Task<bool> Put<OrderHistorySubmitLogModel>(FilterDefinition<OrderHistorySubmitLogModel> filter, UpdateDefinition<OrderHistorySubmitLogModel> updateDefinition);
    Task<bool> Delete<OrderHistorySubmitLogModel>(FilterDefinition<OrderHistorySubmitLogModel> filter);
}
