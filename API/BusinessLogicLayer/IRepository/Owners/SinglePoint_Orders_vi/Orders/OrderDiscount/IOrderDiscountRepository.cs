﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDiscount;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderDiscount;

public interface IOrderDiscountRepository : IRepositoryBase<OrderDiscountModel>
{
    Task<OrderDiscountModel> Post(OrderDiscountModel doc);
    Task<OrderDiscountModel> GetSingle(FilterDefinition<OrderDiscountModel> filter = null, SortDefinition<OrderDiscountModel> sort = null, int limit = 0);
    Task<List<OrderDiscountModel>> GetList(FilterDefinition<OrderDiscountModel> filter, SortDefinition<OrderDiscountModel> sort, int limit = 0);
    Task<bool> Put<OrderDiscountModel>(FilterDefinition<OrderDiscountModel> filter, UpdateDefinition<OrderDiscountModel> updateDefinition);
    Task<bool> Delete<OrderDiscountModel>(FilterDefinition<OrderDiscountModel> filter);
}
