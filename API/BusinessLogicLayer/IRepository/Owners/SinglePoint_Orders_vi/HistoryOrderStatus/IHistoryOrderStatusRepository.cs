﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_Orders_vi.HistoryOrderStatus;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.HistoryOrderStatus;

public interface IHistoryOrderStatusRepository : IRepositoryBase<HistoryOrderStatusModel>
{
    Task<HistoryOrderStatusModel> Post(HistoryOrderStatusModel doc);
    Task<HistoryOrderStatusModel> GetSingle(FilterDefinition<HistoryOrderStatusModel> filter = null, SortDefinition<HistoryOrderStatusModel> sort = null, int limit = 0);
    Task<List<HistoryOrderStatusModel>> GetList(FilterDefinition<HistoryOrderStatusModel> filter, SortDefinition<HistoryOrderStatusModel> sort, int limit = 0);
    Task<bool> Put<HistoryOrderStatusModel>(FilterDefinition<HistoryOrderStatusModel> filter, UpdateDefinition<HistoryOrderStatusModel> updateDefinition);
    Task<bool> Delete<HistoryOrderStatusModel>(FilterDefinition<HistoryOrderStatusModel> filter);
}
