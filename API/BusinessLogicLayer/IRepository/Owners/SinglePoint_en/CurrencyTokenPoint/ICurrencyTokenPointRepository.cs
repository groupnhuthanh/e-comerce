﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.CurrencyTokenPoint;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.CurrencyTokenPoint;

public interface ICurrencyTokenPointRepository : IRepositoryBase<CurrencyTokenPointModel>
{
    Task<CurrencyTokenPointModel> Post(CurrencyTokenPointModel doc);
    Task<CurrencyTokenPointModel> GetSingle(FilterDefinition<CurrencyTokenPointModel> filter = null, SortDefinition<CurrencyTokenPointModel> sort = null, int limit = 0);
    Task<List<CurrencyTokenPointModel>> GetList(FilterDefinition<CurrencyTokenPointModel> filter, SortDefinition<CurrencyTokenPointModel> sort, int limit = 0);
    Task<bool> Put<CurrencyTokenPointModel>(FilterDefinition<CurrencyTokenPointModel> filter, UpdateDefinition<CurrencyTokenPointModel> updateDefinition);
    Task<bool> Delete<CurrencyTokenPointModel>(FilterDefinition<CurrencyTokenPointModel> filter);
}
