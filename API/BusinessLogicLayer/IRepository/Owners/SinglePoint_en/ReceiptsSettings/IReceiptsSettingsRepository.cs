﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.ReceiptsSettings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.ReceiptsSettings;

public interface IReceiptsSettingsRepository : IRepositoryBase<ReceiptsSettingsModel>
{
    Task<ReceiptsSettingsModel> Post(ReceiptsSettingsModel doc);
    Task<ReceiptsSettingsModel> GetSingle(FilterDefinition<ReceiptsSettingsModel> filter = null, SortDefinition<ReceiptsSettingsModel> sort = null, int limit = 0);
    Task<List<ReceiptsSettingsModel>> GetList(FilterDefinition<ReceiptsSettingsModel> filter, SortDefinition<ReceiptsSettingsModel> sort, int limit = 0);
    Task<bool> Put<ReceiptsSettingsModel>(FilterDefinition<ReceiptsSettingsModel> filter, UpdateDefinition<ReceiptsSettingsModel> updateDefinition);
    Task<bool> Delete<ReceiptsSettingsModel>(FilterDefinition<ReceiptsSettingsModel> filter);
}
