﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.MembershipLevel;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.MembershipLevel;

public interface IMembershipLevelRepository : IRepositoryBase<MembershipLevelModel>
{
    Task<MembershipLevelModel> Post(MembershipLevelModel doc);
    Task<MembershipLevelModel> GetSingle(FilterDefinition<MembershipLevelModel> filter = null, SortDefinition<MembershipLevelModel> sort = null, int limit = 0);
    Task<List<MembershipLevelModel>> GetList(FilterDefinition<MembershipLevelModel> filter, SortDefinition<MembershipLevelModel> sort, int limit = 0);
    Task<bool> Put<MembershipLevelModel>(FilterDefinition<MembershipLevelModel> filter, UpdateDefinition<MembershipLevelModel> updateDefinition);
    Task<bool> Delete<MembershipLevelModel>(FilterDefinition<MembershipLevelModel> filter);
}
