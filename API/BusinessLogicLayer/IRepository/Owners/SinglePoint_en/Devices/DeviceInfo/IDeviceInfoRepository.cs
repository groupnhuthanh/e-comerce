﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.DeviceInfo;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceInfo;

public interface IDeviceInfoRepository : IRepositoryBase<DeviceInfoModel>
{
    Task<DeviceInfoModel> Post(DeviceInfoModel doc);
    Task<DeviceInfoModel> GetSingle(FilterDefinition<DeviceInfoModel> filter = null, SortDefinition<DeviceInfoModel> sort = null, int limit = 0);
    Task<List<DeviceInfoModel>> GetList(FilterDefinition<DeviceInfoModel> filter, SortDefinition<DeviceInfoModel> sort, int limit = 0);
    Task<bool> Put<DeviceInfoModel>(FilterDefinition<DeviceInfoModel> filter, UpdateDefinition<DeviceInfoModel> updateDefinition);
    Task<bool> Delete<DeviceInfoModel>(FilterDefinition<DeviceInfoModel> filter);
}
