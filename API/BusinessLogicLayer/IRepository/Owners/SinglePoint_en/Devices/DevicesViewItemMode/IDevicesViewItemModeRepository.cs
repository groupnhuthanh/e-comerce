﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.DevicesViewItemMode;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DevicesViewItemMode;

public interface IDevicesViewItemModeRepository : IRepositoryBase<DevicesViewItemModeModel>
{
    Task<DevicesViewItemModeModel> Post(DevicesViewItemModeModel doc);
    Task<DevicesViewItemModeModel> GetSingle(FilterDefinition<DevicesViewItemModeModel> filter = null, SortDefinition<DevicesViewItemModeModel> sort = null, int limit = 0);
    Task<List<DevicesViewItemModeModel>> GetList(FilterDefinition<DevicesViewItemModeModel> filter, SortDefinition<DevicesViewItemModeModel> sort, int limit = 0);
    Task<bool> Put<DevicesViewItemModeModel>(FilterDefinition<DevicesViewItemModeModel> filter, UpdateDefinition<DevicesViewItemModeModel> updateDefinition);
    Task<bool> Delete<DevicesViewItemModeModel>(FilterDefinition<DevicesViewItemModeModel> filter);
}
