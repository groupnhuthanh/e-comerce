﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.DeviceRequestHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceRequestHistory;

public interface IDeviceRequestHistoryRepository : IRepositoryBase<DeviceRequestHistoryModel>
{
    Task<DeviceRequestHistoryModel> Post(DeviceRequestHistoryModel doc);
    Task<DeviceRequestHistoryModel> GetSingle(FilterDefinition<DeviceRequestHistoryModel> filter = null, SortDefinition<DeviceRequestHistoryModel> sort = null, int limit = 0);
    Task<List<DeviceRequestHistoryModel>> GetList(FilterDefinition<DeviceRequestHistoryModel> filter, SortDefinition<DeviceRequestHistoryModel> sort, int limit = 0);
    Task<bool> Put<DeviceRequestHistoryModel>(FilterDefinition<DeviceRequestHistoryModel> filter, UpdateDefinition<DeviceRequestHistoryModel> updateDefinition);
    Task<bool> Delete<DeviceRequestHistoryModel>(FilterDefinition<DeviceRequestHistoryModel> filter);
}
