﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.DeviceSettingIdsMultiple;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceSettingIdsMultiple;

public interface IDeviceSettingIdsMultipleRepository : IRepositoryBase<DeviceSettingIdsMultipleModel>
{
    Task<DeviceSettingIdsMultipleModel> Post(DeviceSettingIdsMultipleModel doc);
    Task<DeviceSettingIdsMultipleModel> GetSingle(FilterDefinition<DeviceSettingIdsMultipleModel> filter = null, SortDefinition<DeviceSettingIdsMultipleModel> sort = null, int limit = 0);
    Task<List<DeviceSettingIdsMultipleModel>> GetList(FilterDefinition<DeviceSettingIdsMultipleModel> filter, SortDefinition<DeviceSettingIdsMultipleModel> sort, int limit = 0);
    Task<bool> Put<DeviceSettingIdsMultipleModel>(FilterDefinition<DeviceSettingIdsMultipleModel> filter, UpdateDefinition<DeviceSettingIdsMultipleModel> updateDefinition);
    Task<bool> Delete<DeviceSettingIdsMultipleModel>(FilterDefinition<DeviceSettingIdsMultipleModel> filter);
}
