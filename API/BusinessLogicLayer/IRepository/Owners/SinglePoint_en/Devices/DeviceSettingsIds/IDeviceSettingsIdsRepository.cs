﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.DeviceSettingsIds;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceSettingsIds;

public interface IDeviceSettingsIdsRepository : IRepositoryBase<DeviceSettingsIdsModel>
{
    Task<DeviceSettingsIdsModel> Post(DeviceSettingsIdsModel doc);
    Task<DeviceSettingsIdsModel> GetSingle(FilterDefinition<DeviceSettingsIdsModel> filter = null, SortDefinition<DeviceSettingsIdsModel> sort = null, int limit = 0);
    Task<List<DeviceSettingsIdsModel>> GetList(FilterDefinition<DeviceSettingsIdsModel> filter, SortDefinition<DeviceSettingsIdsModel> sort, int limit = 0);
    Task<bool> Put<DeviceSettingsIdsModel>(FilterDefinition<DeviceSettingsIdsModel> filter, UpdateDefinition<DeviceSettingsIdsModel> updateDefinition);
    Task<bool> Delete<DeviceSettingsIdsModel>(FilterDefinition<DeviceSettingsIdsModel> filter);
}
