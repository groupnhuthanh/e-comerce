﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.DeviceVideo;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceVideo;

public interface IDeviceVideoRepository : IRepositoryBase<DeviceVideoModel>
{
    Task<DeviceVideoModel> Post(DeviceVideoModel doc);
    Task<DeviceVideoModel> GetSingle(FilterDefinition<DeviceVideoModel> filter = null, SortDefinition<DeviceVideoModel> sort = null, int limit = 0);
    Task<List<DeviceVideoModel>> GetList(FilterDefinition<DeviceVideoModel> filter, SortDefinition<DeviceVideoModel> sort, int limit = 0);
    Task<bool> Put<DeviceVideoModel>(FilterDefinition<DeviceVideoModel> filter, UpdateDefinition<DeviceVideoModel> updateDefinition);
    Task<bool> Delete<DeviceVideoModel>(FilterDefinition<DeviceVideoModel> filter);
}
