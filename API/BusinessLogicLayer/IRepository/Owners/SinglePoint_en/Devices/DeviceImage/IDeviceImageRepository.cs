﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.DeviceImage;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceImage;

public interface IDeviceImageRepository : IRepositoryBase<DeviceImageModel>
{
    Task<DeviceImageModel> Post(DeviceImageModel doc);
    Task<DeviceImageModel> GetSingle(FilterDefinition<DeviceImageModel> filter = null, SortDefinition<DeviceImageModel> sort = null, int limit = 0);
    Task<List<DeviceImageModel>> GetList(FilterDefinition<DeviceImageModel> filter, SortDefinition<DeviceImageModel> sort, int limit = 0);
    Task<bool> Put<DeviceImageModel>(FilterDefinition<DeviceImageModel> filter, UpdateDefinition<DeviceImageModel> updateDefinition);
    Task<bool> Delete<DeviceImageModel>(FilterDefinition<DeviceImageModel> filter);
}
