﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.DevicesSettings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DevicesSettings;

public interface IDevicesSettingsRepository : IRepositoryBase<DevicesSettingsModel>
{
    Task<DevicesSettingsModel> Post(DevicesSettingsModel doc);
    Task<DevicesSettingsModel> GetSingle(FilterDefinition<DevicesSettingsModel> filter = null, SortDefinition<DevicesSettingsModel> sort = null, int limit = 0);
    Task<List<DevicesSettingsModel>> GetList(FilterDefinition<DevicesSettingsModel> filter, SortDefinition<DevicesSettingsModel> sort, int limit = 0);
    Task<bool> Put<DevicesSettingsModel>(FilterDefinition<DevicesSettingsModel> filter, UpdateDefinition<DevicesSettingsModel> updateDefinition);
    Task<bool> Delete<DevicesSettingsModel>(FilterDefinition<DevicesSettingsModel> filter);
}
