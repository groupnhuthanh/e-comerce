﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Devices.Device;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.Device;

public interface IDeviceRepository : IRepositoryBase<DeviceModel>
{
    Task<DeviceModel> Post(DeviceModel doc);
    Task<DeviceModel> GetSingle(FilterDefinition<DeviceModel> filter = null, SortDefinition<DeviceModel> sort = null, int limit = 0);
    Task<List<DeviceModel>> GetList(FilterDefinition<DeviceModel> filter, SortDefinition<DeviceModel> sort, int limit = 0);
    Task<bool> Put<DeviceModel>(FilterDefinition<DeviceModel> filter, UpdateDefinition<DeviceModel> updateDefinition);
    Task<bool> Delete<DeviceModel>(FilterDefinition<DeviceModel> filter);
}
