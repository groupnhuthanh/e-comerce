﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.AppVersion;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.AppVersion;

public interface IAppVersionRepository : IRepositoryBase<AppVersionModel>
{
    Task<AppVersionModel> Post(AppVersionModel doc);
    Task<AppVersionModel> GetSingle(FilterDefinition<AppVersionModel> filter = null, SortDefinition<AppVersionModel> sort = null, int limit = 0);
    Task<List<AppVersionModel>> GetList(FilterDefinition<AppVersionModel> filter, SortDefinition<AppVersionModel> sort, int limit = 0);
    Task<bool> Put<AppVersionModel>(FilterDefinition<AppVersionModel> filter, UpdateDefinition<AppVersionModel> updateDefinition);
    Task<bool> Delete<AppVersionModel>(FilterDefinition<AppVersionModel> filter);

}
