﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.PaymentType;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.PaymentType;

public interface IPaymentTypeRepository : IRepositoryBase<PaymentTypeModel>
{
    Task<PaymentTypeModel> Post(PaymentTypeModel doc);
    Task<PaymentTypeModel> GetSingle(FilterDefinition<PaymentTypeModel> filter = null, SortDefinition<PaymentTypeModel> sort = null, int limit = 0);
    Task<List<PaymentTypeModel>> GetList(FilterDefinition<PaymentTypeModel> filter, SortDefinition<PaymentTypeModel> sort, int limit = 0);
    Task<bool> Put<PaymentTypeModel>(FilterDefinition<PaymentTypeModel> filter, UpdateDefinition<PaymentTypeModel> updateDefinition);
    Task<bool> Delete<PaymentTypeModel>(FilterDefinition<PaymentTypeModel> filter);
}
