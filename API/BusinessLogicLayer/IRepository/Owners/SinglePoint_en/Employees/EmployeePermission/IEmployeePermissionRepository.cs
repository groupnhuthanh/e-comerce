﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Employees.EmployeePermission;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Employees.EmployeePermission;

public interface IEmployeePermissionRepository : IRepositoryBase<EmployeePermissionModel>
{
    Task<EmployeePermissionModel> Post(EmployeePermissionModel doc);
    Task<EmployeePermissionModel> GetSingle(FilterDefinition<EmployeePermissionModel> filter = null, SortDefinition<EmployeePermissionModel> sort = null, int limit = 0);
    Task<List<EmployeePermissionModel>> GetList(FilterDefinition<EmployeePermissionModel> filter, SortDefinition<EmployeePermissionModel> sort, int limit = 0);
    Task<bool> Put<EmployeePermissionModel>(FilterDefinition<EmployeePermissionModel> filter, UpdateDefinition<EmployeePermissionModel> updateDefinition);
    Task<bool> Delete<EmployeePermissionModel>(FilterDefinition<EmployeePermissionModel> filter);
}
