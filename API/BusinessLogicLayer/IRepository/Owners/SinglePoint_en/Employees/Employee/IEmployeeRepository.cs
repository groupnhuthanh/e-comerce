﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Employees.Employee;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Employees.Employee;

public interface IEmployeeRepository : IRepositoryBase<EmployeeModel>
{
    Task<EmployeeModel> Post(EmployeeModel doc);
    Task<EmployeeModel> GetSingle(FilterDefinition<EmployeeModel> filter = null, SortDefinition<EmployeeModel> sort = null, int limit = 0);
    Task<List<EmployeeModel>> GetList(FilterDefinition<EmployeeModel> filter, SortDefinition<EmployeeModel> sort, int limit = 0);
    Task<bool> Put<EmployeeModel>(FilterDefinition<EmployeeModel> filter, UpdateDefinition<EmployeeModel> updateDefinition);
    Task<bool> Delete<EmployeeModel>(FilterDefinition<EmployeeModel> filter);
}
