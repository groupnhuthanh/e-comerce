﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Notifications.NotificationType;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Notifications.NotificationType;

public interface INotificationTypeRepository : IRepositoryBase<NotificationTypeModel>
{
    Task<NotificationTypeModel> Post(NotificationTypeModel doc);
    Task<NotificationTypeModel> GetSingle(FilterDefinition<NotificationTypeModel> filter = null, SortDefinition<NotificationTypeModel> sort = null, int limit = 0);
    Task<List<NotificationTypeModel>> GetList(FilterDefinition<NotificationTypeModel> filter, SortDefinition<NotificationTypeModel> sort, int limit = 0);
    Task<bool> Put<NotificationTypeModel>(FilterDefinition<NotificationTypeModel> filter, UpdateDefinition<NotificationTypeModel> updateDefinition);
    Task<bool> Delete<NotificationTypeModel>(FilterDefinition<NotificationTypeModel> filter);
}
