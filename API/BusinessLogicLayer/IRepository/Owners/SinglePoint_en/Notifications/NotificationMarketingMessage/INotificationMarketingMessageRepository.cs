﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Notifications.NotificationMarketingMessage;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Notifications.NotificationMarketingMessage;

public interface INotificationMarketingMessageRepository : IRepositoryBase<NotificationMarketingMessageModel>
{
    Task<NotificationMarketingMessageModel> Post(NotificationMarketingMessageModel doc);
    Task<NotificationMarketingMessageModel> GetSingle(FilterDefinition<NotificationMarketingMessageModel> filter = null, SortDefinition<NotificationMarketingMessageModel> sort = null, int limit = 0);
    Task<List<NotificationMarketingMessageModel>> GetList(FilterDefinition<NotificationMarketingMessageModel> filter, SortDefinition<NotificationMarketingMessageModel> sort, int limit = 0);
    Task<bool> Put<NotificationMarketingMessageModel>(FilterDefinition<NotificationMarketingMessageModel> filter, UpdateDefinition<NotificationMarketingMessageModel> updateDefinition);
    Task<bool> Delete<NotificationMarketingMessageModel>(FilterDefinition<NotificationMarketingMessageModel> filter);
}
