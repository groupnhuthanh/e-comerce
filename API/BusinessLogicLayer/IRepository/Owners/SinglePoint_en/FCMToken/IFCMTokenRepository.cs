﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.FCMToken;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FCMToken;

public interface IFCMTokenRepository : IRepositoryBase<FCMTokenModel>
{
    Task<FCMTokenModel> Post(FCMTokenModel doc);
    Task<FCMTokenModel> GetSingle(FilterDefinition<FCMTokenModel> filter = null, SortDefinition<FCMTokenModel> sort = null, int limit = 0);
    Task<List<FCMTokenModel>> GetList(FilterDefinition<FCMTokenModel> filter, SortDefinition<FCMTokenModel> sort, int limit = 0);
    Task<bool> Put<FCMTokenModel>(FilterDefinition<FCMTokenModel> filter, UpdateDefinition<FCMTokenModel> updateDefinition);
    Task<bool> Delete<FCMTokenModel>(FilterDefinition<FCMTokenModel> filter);
}
