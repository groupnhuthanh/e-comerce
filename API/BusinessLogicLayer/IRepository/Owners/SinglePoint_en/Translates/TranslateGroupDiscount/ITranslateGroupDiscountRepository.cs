﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateGroupDiscount;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateGroupDiscount;

public interface ITranslateGroupDiscountRepository : IRepositoryBase<TranslateGroupDiscountModel>
{
    Task<TranslateGroupDiscountModel> Post(TranslateGroupDiscountModel doc);
    Task<TranslateGroupDiscountModel> GetSingle(FilterDefinition<TranslateGroupDiscountModel> filter = null, SortDefinition<TranslateGroupDiscountModel> sort = null, int limit = 0);
    Task<List<TranslateGroupDiscountModel>> GetList(FilterDefinition<TranslateGroupDiscountModel> filter, SortDefinition<TranslateGroupDiscountModel> sort, int limit = 0);
    Task<bool> Put<TranslateGroupDiscountModel>(FilterDefinition<TranslateGroupDiscountModel> filter, UpdateDefinition<TranslateGroupDiscountModel> updateDefinition);
    Task<bool> Delete<TranslateGroupDiscountModel>(FilterDefinition<TranslateGroupDiscountModel> filter);
}
