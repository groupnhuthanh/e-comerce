﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateModifierItem;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateModifierItem;

public interface ITranslateModifierItemRepository : IRepositoryBase<TranslateModifierItemModel>
{
    Task<TranslateModifierItemModel> Post(TranslateModifierItemModel doc);
    Task<TranslateModifierItemModel> GetSingle(FilterDefinition<TranslateModifierItemModel> filter = null, SortDefinition<TranslateModifierItemModel> sort = null, int limit = 0);
    Task<List<TranslateModifierItemModel>> GetList(FilterDefinition<TranslateModifierItemModel> filter, SortDefinition<TranslateModifierItemModel> sort, int limit = 0);
    Task<bool> Put<TranslateModifierItemModel>(FilterDefinition<TranslateModifierItemModel> filter, UpdateDefinition<TranslateModifierItemModel> updateDefinition);
    Task<bool> Delete<TranslateModifierItemModel>(FilterDefinition<TranslateModifierItemModel> filter);
}
