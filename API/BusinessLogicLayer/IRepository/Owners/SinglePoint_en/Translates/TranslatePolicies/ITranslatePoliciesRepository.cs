﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslatePolicies;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslatePolicies;

public interface ITranslatePoliciesRepository : IRepositoryBase<TranslatePoliciesModel>
{
    Task<TranslatePoliciesModel> Post(TranslatePoliciesModel doc);
    Task<TranslatePoliciesModel> GetSingle(FilterDefinition<TranslatePoliciesModel> filter = null, SortDefinition<TranslatePoliciesModel> sort = null, int limit = 0);
    Task<List<TranslatePoliciesModel>> GetList(FilterDefinition<TranslatePoliciesModel> filter, SortDefinition<TranslatePoliciesModel> sort, int limit = 0);
    Task<bool> Put<TranslatePoliciesModel>(FilterDefinition<TranslatePoliciesModel> filter, UpdateDefinition<TranslatePoliciesModel> updateDefinition);
    Task<bool> Delete<TranslatePoliciesModel>(FilterDefinition<TranslatePoliciesModel> filter);
}
