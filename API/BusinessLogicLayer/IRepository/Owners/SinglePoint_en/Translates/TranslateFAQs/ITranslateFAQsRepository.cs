﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateFAQs;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateFAQs;

public interface ITranslateFAQsRepository : IRepositoryBase<TranslateFAQsModel>
{
    Task<TranslateFAQsModel> Post(TranslateFAQsModel doc);
    Task<TranslateFAQsModel> GetSingle(FilterDefinition<TranslateFAQsModel> filter = null, SortDefinition<TranslateFAQsModel> sort = null, int limit = 0);
    Task<List<TranslateFAQsModel>> GetList(FilterDefinition<TranslateFAQsModel> filter, SortDefinition<TranslateFAQsModel> sort, int limit = 0);
    Task<bool> Put<TranslateFAQsModel>(FilterDefinition<TranslateFAQsModel> filter, UpdateDefinition<TranslateFAQsModel> updateDefinition);
    Task<bool> Delete<TranslateFAQsModel>(FilterDefinition<TranslateFAQsModel> filter);
}
