﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateModifier;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateModifier;

public interface ITranslateModifierRepository : IRepositoryBase<TranslateModifierModel>
{
    Task<TranslateModifierModel> Post(TranslateModifierModel doc);
    Task<TranslateModifierModel> GetSingle(FilterDefinition<TranslateModifierModel> filter = null, SortDefinition<TranslateModifierModel> sort = null, int limit = 0);
    Task<List<TranslateModifierModel>> GetList(FilterDefinition<TranslateModifierModel> filter, SortDefinition<TranslateModifierModel> sort, int limit = 0);
    Task<bool> Put<TranslateModifierModel>(FilterDefinition<TranslateModifierModel> filter, UpdateDefinition<TranslateModifierModel> updateDefinition);
    Task<bool> Delete<TranslateModifierModel>(FilterDefinition<TranslateModifierModel> filter);
}
