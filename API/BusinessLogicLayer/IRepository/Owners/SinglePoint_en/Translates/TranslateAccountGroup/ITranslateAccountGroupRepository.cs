﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateAccountGroup;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateAccountGroup;

public interface ITranslateAccountGroupRepository : IRepositoryBase<TranslateAccountGroupModel>
{
    Task<TranslateAccountGroupModel> Post(TranslateAccountGroupModel doc);
    Task<TranslateAccountGroupModel> GetSingle(FilterDefinition<TranslateAccountGroupModel> filter = null, SortDefinition<TranslateAccountGroupModel> sort = null, int limit = 0);
    Task<List<TranslateAccountGroupModel>> GetList(FilterDefinition<TranslateAccountGroupModel> filter, SortDefinition<TranslateAccountGroupModel> sort, int limit = 0);
    Task<bool> Put<TranslateAccountGroupModel>(FilterDefinition<TranslateAccountGroupModel> filter, UpdateDefinition<TranslateAccountGroupModel> updateDefinition);
    Task<bool> Delete<TranslateAccountGroupModel>(FilterDefinition<TranslateAccountGroupModel> filter);
}
