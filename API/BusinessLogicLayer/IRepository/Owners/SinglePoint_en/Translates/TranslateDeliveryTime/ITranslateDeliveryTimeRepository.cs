﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateDeliveryTime;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateDeliveryTime;

public interface ITranslateDeliveryTimeRepository : IRepositoryBase<TranslateDeliveryTimeModel>
{
    Task<TranslateDeliveryTimeModel> Post(TranslateDeliveryTimeModel doc);
    Task<TranslateDeliveryTimeModel> GetSingle(FilterDefinition<TranslateDeliveryTimeModel> filter = null, SortDefinition<TranslateDeliveryTimeModel> sort = null, int limit = 0);
    Task<List<TranslateDeliveryTimeModel>> GetList(FilterDefinition<TranslateDeliveryTimeModel> filter, SortDefinition<TranslateDeliveryTimeModel> sort, int limit = 0);
    Task<bool> Put<TranslateDeliveryTimeModel>(FilterDefinition<TranslateDeliveryTimeModel> filter, UpdateDefinition<TranslateDeliveryTimeModel> updateDefinition);
    Task<bool> Delete<TranslateDeliveryTimeModel>(FilterDefinition<TranslateDeliveryTimeModel> filter);
}
