﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateCategory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateCategory;

public interface ITranslateCategoryRepository : IRepositoryBase<TranslateCategoryModel>
{
    Task<TranslateCategoryModel> Post(TranslateCategoryModel doc);
    Task<TranslateCategoryModel> GetSingle(FilterDefinition<TranslateCategoryModel> filter = null, SortDefinition<TranslateCategoryModel> sort = null, int limit = 0);
    Task<List<TranslateCategoryModel>> GetList(FilterDefinition<TranslateCategoryModel> filter, SortDefinition<TranslateCategoryModel> sort, int limit = 0);
    Task<bool> Put<TranslateCategoryModel>(FilterDefinition<TranslateCategoryModel> filter, UpdateDefinition<TranslateCategoryModel> updateDefinition);
    Task<bool> Delete<TranslateCategoryModel>(FilterDefinition<TranslateCategoryModel> filter);
}
