﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateFAQsGroup;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateFAQsGroup;

public interface ITranslateFAQsGroupRepository : IRepositoryBase<TranslateFAQsGroupModel>
{
    Task<TranslateFAQsGroupModel> Post(TranslateFAQsGroupModel doc);
    Task<TranslateFAQsGroupModel> GetSingle(FilterDefinition<TranslateFAQsGroupModel> filter = null, SortDefinition<TranslateFAQsGroupModel> sort = null, int limit = 0);
    Task<List<TranslateFAQsGroupModel>> GetList(FilterDefinition<TranslateFAQsGroupModel> filter, SortDefinition<TranslateFAQsGroupModel> sort, int limit = 0);
    Task<bool> Put<TranslateFAQsGroupModel>(FilterDefinition<TranslateFAQsGroupModel> filter, UpdateDefinition<TranslateFAQsGroupModel> updateDefinition);
    Task<bool> Delete<TranslateFAQsGroupModel>(FilterDefinition<TranslateFAQsGroupModel> filter);
}
