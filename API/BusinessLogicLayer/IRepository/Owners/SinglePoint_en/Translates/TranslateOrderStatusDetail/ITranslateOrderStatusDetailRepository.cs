﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Translates.TranslateOrderStatusDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateOrderStatusDetail;

public interface ITranslateOrderStatusDetailRepository : IRepositoryBase<TranslateOrderStatusDetailModel>
{
    Task<TranslateOrderStatusDetailModel> Post(TranslateOrderStatusDetailModel doc);
    Task<TranslateOrderStatusDetailModel> GetSingle(FilterDefinition<TranslateOrderStatusDetailModel> filter = null, SortDefinition<TranslateOrderStatusDetailModel> sort = null, int limit = 0);
    Task<List<TranslateOrderStatusDetailModel>> GetList(FilterDefinition<TranslateOrderStatusDetailModel> filter, SortDefinition<TranslateOrderStatusDetailModel> sort, int limit = 0);
    Task<bool> Put<TranslateOrderStatusDetailModel>(FilterDefinition<TranslateOrderStatusDetailModel> filter, UpdateDefinition<TranslateOrderStatusDetailModel> updateDefinition);
    Task<bool> Delete<TranslateOrderStatusDetailModel>(FilterDefinition<TranslateOrderStatusDetailModel> filter);
}
