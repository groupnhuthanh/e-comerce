﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.ShippingZone;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.ShippingZone;

public interface IShippingZoneRepository : IRepositoryBase<ShippingZoneModel>
{
    Task<ShippingZoneModel> Post(ShippingZoneModel doc);
    Task<ShippingZoneModel> GetSingle(FilterDefinition<ShippingZoneModel> filter = null, SortDefinition<ShippingZoneModel> sort = null, int limit = 0);
    Task<List<ShippingZoneModel>> GetList(FilterDefinition<ShippingZoneModel> filter, SortDefinition<ShippingZoneModel> sort, int limit = 0);
    Task<bool> Put<ShippingZoneModel>(FilterDefinition<ShippingZoneModel> filter, UpdateDefinition<ShippingZoneModel> updateDefinition);
    Task<bool> Delete<ShippingZoneModel>(FilterDefinition<ShippingZoneModel> filter);
}
