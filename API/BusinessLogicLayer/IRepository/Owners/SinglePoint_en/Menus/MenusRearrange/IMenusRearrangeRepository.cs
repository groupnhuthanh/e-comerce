﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Menus.MenusRearrange;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.MenusRearrange;

public interface IMenusRearrangeRepository : IRepositoryBase<MenusRearrangeModel>
{
    Task<MenusRearrangeModel> Post(MenusRearrangeModel doc);
    Task<MenusRearrangeModel> GetSingle(FilterDefinition<MenusRearrangeModel> filter = null, SortDefinition<MenusRearrangeModel> sort = null, int limit = 0);
    Task<List<MenusRearrangeModel>> GetList(FilterDefinition<MenusRearrangeModel> filter, SortDefinition<MenusRearrangeModel> sort, int limit = 0);
    Task<bool> Put<MenusRearrangeModel>(FilterDefinition<MenusRearrangeModel> filter, UpdateDefinition<MenusRearrangeModel> updateDefinition);
    Task<bool> Delete<MenusRearrangeModel>(FilterDefinition<MenusRearrangeModel> filter);
}
