﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Menus.MenusGroupItem;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.MenusGroupItem;

public interface IMenusGroupItemRepository : IRepositoryBase<MenusGroupItemModel>
{
    Task<MenusGroupItemModel> Post(MenusGroupItemModel doc);
    Task<MenusGroupItemModel> GetSingle(FilterDefinition<MenusGroupItemModel> filter = null, SortDefinition<MenusGroupItemModel> sort = null, int limit = 0);
    Task<List<MenusGroupItemModel>> GetList(FilterDefinition<MenusGroupItemModel> filter, SortDefinition<MenusGroupItemModel> sort, int limit = 0);
    Task<bool> Put<MenusGroupItemModel>(FilterDefinition<MenusGroupItemModel> filter, UpdateDefinition<MenusGroupItemModel> updateDefinition);
    Task<bool> Delete<MenusGroupItemModel>(FilterDefinition<MenusGroupItemModel> filter);
}
