﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Menus.MenusLocation;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.MenusLocation;

public interface IMenusLocationRepository : IRepositoryBase<MenusLocationModel>
{
    Task<MenusLocationModel> Post(MenusLocationModel doc);
    Task<MenusLocationModel> GetSingle(FilterDefinition<MenusLocationModel> filter = null, SortDefinition<MenusLocationModel> sort = null, int limit = 0);
    Task<List<MenusLocationModel>> GetList(FilterDefinition<MenusLocationModel> filter, SortDefinition<MenusLocationModel> sort, int limit = 0);
    Task<bool> Put<MenusLocationModel>(FilterDefinition<MenusLocationModel> filter, UpdateDefinition<MenusLocationModel> updateDefinition);
    Task<bool> Delete<MenusLocationModel>(FilterDefinition<MenusLocationModel> filter);
}
