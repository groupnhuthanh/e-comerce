﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Menus.Menu;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.Menu;

public interface IMenuRepository : IRepositoryBase<MenuModel>
{
    Task<MenuModel> Post(MenuModel doc);
    Task<MenuModel> GetSingle(FilterDefinition<MenuModel> filter = null, SortDefinition<MenuModel> sort = null, int limit = 0);
    Task<List<MenuModel>> GetList(FilterDefinition<MenuModel> filter, SortDefinition<MenuModel> sort, int limit = 0);
    Task<bool> Put<MenuModel>(FilterDefinition<MenuModel> filter, UpdateDefinition<MenuModel> updateDefinition);
    Task<bool> Delete<MenuModel>(FilterDefinition<MenuModel> filter);
}
