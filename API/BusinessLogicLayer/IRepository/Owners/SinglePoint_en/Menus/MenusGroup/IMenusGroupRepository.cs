﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Menus.MenusGroup;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.MenusGroup;

public interface IMenusGroupRepository : IRepositoryBase<MenusGroupModel>
{
    Task<MenusGroupModel> Post(MenusGroupModel doc);
    Task<MenusGroupModel> GetSingle(FilterDefinition<MenusGroupModel> filter = null, SortDefinition<MenusGroupModel> sort = null, int limit = 0);
    Task<List<MenusGroupModel>> GetList(FilterDefinition<MenusGroupModel> filter, SortDefinition<MenusGroupModel> sort, int limit = 0);
    Task<bool> Put<MenusGroupModel>(FilterDefinition<MenusGroupModel> filter, UpdateDefinition<MenusGroupModel> updateDefinition);
    Task<bool> Delete<MenusGroupModel>(FilterDefinition<MenusGroupModel> filter);
}
