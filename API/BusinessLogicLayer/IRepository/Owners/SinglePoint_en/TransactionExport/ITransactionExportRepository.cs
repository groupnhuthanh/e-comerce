﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.TransactionExport;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.TransactionExport;

public interface ITransactionExportRepository : IRepositoryBase<TransactionExportModel>
{
    Task<TransactionExportModel> Post(TransactionExportModel doc);
    Task<TransactionExportModel> GetSingle(FilterDefinition<TransactionExportModel> filter = null, SortDefinition<TransactionExportModel> sort = null, int limit = 0);
    Task<List<TransactionExportModel>> GetList(FilterDefinition<TransactionExportModel> filter, SortDefinition<TransactionExportModel> sort, int limit = 0);
    Task<bool> Put<TransactionExportModel>(FilterDefinition<TransactionExportModel> filter, UpdateDefinition<TransactionExportModel> updateDefinition);
    Task<bool> Delete<TransactionExportModel>(FilterDefinition<TransactionExportModel> filter);
}
