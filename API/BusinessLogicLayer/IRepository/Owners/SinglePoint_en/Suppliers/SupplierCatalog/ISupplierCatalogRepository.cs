﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Suppliers.SupplierCatalog;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Suppliers.SupplierCatalog;

public interface ISupplierCatalogRepository : IRepositoryBase<SupplierCatalogModel>
{
    Task<SupplierCatalogModel> Post(SupplierCatalogModel doc);
    Task<SupplierCatalogModel> GetSingle(FilterDefinition<SupplierCatalogModel> filter = null, SortDefinition<SupplierCatalogModel> sort = null, int limit = 0);
    Task<List<SupplierCatalogModel>> GetList(FilterDefinition<SupplierCatalogModel> filter, SortDefinition<SupplierCatalogModel> sort, int limit = 0);
    Task<bool> Put<SupplierCatalogModel>(FilterDefinition<SupplierCatalogModel> filter, UpdateDefinition<SupplierCatalogModel> updateDefinition);
    Task<bool> Delete<SupplierCatalogModel>(FilterDefinition<SupplierCatalogModel> filter);
}
