﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Suppliers.Supplier;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Suppliers.Supplier;

public interface ISupplierRepository : IRepositoryBase<SupplierModel>
{
    Task<SupplierModel> Post(SupplierModel doc);
    Task<SupplierModel> GetSingle(FilterDefinition<SupplierModel> filter = null, SortDefinition<SupplierModel> sort = null, int limit = 0);
    Task<List<SupplierModel>> GetList(FilterDefinition<SupplierModel> filter, SortDefinition<SupplierModel> sort, int limit = 0);
    Task<bool> Put<SupplierModel>(FilterDefinition<SupplierModel> filter, UpdateDefinition<SupplierModel> updateDefinition);
    Task<bool> Delete<SupplierModel>(FilterDefinition<SupplierModel> filter);
}
