﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.HistoryLocation;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.HistoryLocation;

public interface IHistoryLocationRepository : IRepositoryBase<HistoryLocationModel>
{
    Task<HistoryLocationModel> Post(HistoryLocationModel doc);
    Task<HistoryLocationModel> GetSingle(FilterDefinition<HistoryLocationModel> filter = null, SortDefinition<HistoryLocationModel> sort = null, int limit = 0);
    Task<List<HistoryLocationModel>> GetList(FilterDefinition<HistoryLocationModel> filter, SortDefinition<HistoryLocationModel> sort, int limit = 0);
    Task<bool> Put<HistoryLocationModel>(FilterDefinition<HistoryLocationModel> filter, UpdateDefinition<HistoryLocationModel> updateDefinition);
    Task<bool> Delete<HistoryLocationModel>(FilterDefinition<HistoryLocationModel> filter);
}
