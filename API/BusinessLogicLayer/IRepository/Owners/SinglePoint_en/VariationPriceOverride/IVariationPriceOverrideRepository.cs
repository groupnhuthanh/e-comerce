﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.VariationPriceOverride;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.VariationPriceOverride;

public interface IVariationPriceOverrideRepository : IRepositoryBase<VariationPriceOverrideModel>
{
    Task<VariationPriceOverrideModel> Post(VariationPriceOverrideModel doc);
    Task<VariationPriceOverrideModel> GetSingle(FilterDefinition<VariationPriceOverrideModel> filter = null, SortDefinition<VariationPriceOverrideModel> sort = null, int limit = 0);
    Task<List<VariationPriceOverrideModel>> GetList(FilterDefinition<VariationPriceOverrideModel> filter, SortDefinition<VariationPriceOverrideModel> sort, int limit = 0);
    Task<bool> Put<VariationPriceOverrideModel>(FilterDefinition<VariationPriceOverrideModel> filter, UpdateDefinition<VariationPriceOverrideModel> updateDefinition);
    Task<bool> Delete<VariationPriceOverrideModel>(FilterDefinition<VariationPriceOverrideModel> filter);
}
