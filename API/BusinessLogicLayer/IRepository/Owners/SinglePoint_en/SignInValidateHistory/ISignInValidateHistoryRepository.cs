﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.SignInValidateHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.SignInValidateHistory;

public interface ISignInValidateHistoryRepository : IRepositoryBase<SignInValidateHistoryModel>
{
    Task<SignInValidateHistoryModel> Post(SignInValidateHistoryModel doc);
    Task<SignInValidateHistoryModel> GetSingle(FilterDefinition<SignInValidateHistoryModel> filter = null, SortDefinition<SignInValidateHistoryModel> sort = null, int limit = 0);
    Task<List<SignInValidateHistoryModel>> GetList(FilterDefinition<SignInValidateHistoryModel> filter, SortDefinition<SignInValidateHistoryModel> sort, int limit = 0);
    Task<bool> Put<SignInValidateHistoryModel>(FilterDefinition<SignInValidateHistoryModel> filter, UpdateDefinition<SignInValidateHistoryModel> updateDefinition);
    Task<bool> Delete<SignInValidateHistoryModel>(FilterDefinition<SignInValidateHistoryModel> filter);
}
