﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Building;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Building;

public interface IBuildingRepository : IRepositoryBase<BuildingModel>
{
    Task<BuildingModel> Post(BuildingModel doc);
    Task<BuildingModel> GetSingle(FilterDefinition<BuildingModel> filter = null, SortDefinition<BuildingModel> sort = null, int limit = 0);
    Task<List<BuildingModel>> GetList(FilterDefinition<BuildingModel> filter, SortDefinition<BuildingModel> sort, int limit = 0);
    Task<bool> Put<BuildingModel>(FilterDefinition<BuildingModel> filter, UpdateDefinition<BuildingModel> updateDefinition);
    Task<bool> Delete<BuildingModel>(FilterDefinition<BuildingModel> filter);
}
