﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Hardware;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Hardware;

public interface IHardwareRepository : IRepositoryBase<HardwareModel>
{
    Task<HardwareModel> Post(HardwareModel doc);
    Task<HardwareModel> GetSingle(FilterDefinition<HardwareModel> filter = null, SortDefinition<HardwareModel> sort = null, int limit = 0);
    Task<List<HardwareModel>> GetList(FilterDefinition<HardwareModel> filter, SortDefinition<HardwareModel> sort, int limit = 0);
    Task<bool> Put<HardwareModel>(FilterDefinition<HardwareModel> filter, UpdateDefinition<HardwareModel> updateDefinition);
    Task<bool> Delete<HardwareModel>(FilterDefinition<HardwareModel> filter);
}
