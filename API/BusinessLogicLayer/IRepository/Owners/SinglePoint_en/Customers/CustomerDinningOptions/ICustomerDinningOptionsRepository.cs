﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerDinningOptions;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerDinningOptions;

public interface ICustomerDinningOptionsRepository : IRepositoryBase<CustomerDinningOptionsModel>
{
    Task<CustomerDinningOptionsModel> Post(CustomerDinningOptionsModel doc);
    Task<CustomerDinningOptionsModel> GetSingle(FilterDefinition<CustomerDinningOptionsModel> filter = null, SortDefinition<CustomerDinningOptionsModel> sort = null, int limit = 0);
    Task<List<CustomerDinningOptionsModel>> GetList(FilterDefinition<CustomerDinningOptionsModel> filter, SortDefinition<CustomerDinningOptionsModel> sort, int limit = 0);
    Task<bool> Put<CustomerDinningOptionsModel>(FilterDefinition<CustomerDinningOptionsModel> filter, UpdateDefinition<CustomerDinningOptionsModel> updateDefinition);
    Task<bool> Delete<CustomerDinningOptionsModel>(FilterDefinition<CustomerDinningOptionsModel> filter);

}
