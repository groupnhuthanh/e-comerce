﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyalty;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestLoyalty;

public interface ICustomerGuestLoyaltyRepository : IRepositoryBase<CustomerGuestLoyaltyModel>
{
    Task<CustomerGuestLoyaltyModel> Post(CustomerGuestLoyaltyModel doc);
    Task<CustomerGuestLoyaltyModel> GetSingle(FilterDefinition<CustomerGuestLoyaltyModel> filter = null, SortDefinition<CustomerGuestLoyaltyModel> sort = null, int limit = 0);
    Task<List<CustomerGuestLoyaltyModel>> GetList(FilterDefinition<CustomerGuestLoyaltyModel> filter, SortDefinition<CustomerGuestLoyaltyModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestLoyaltyModel>(FilterDefinition<CustomerGuestLoyaltyModel> filter, UpdateDefinition<CustomerGuestLoyaltyModel> updateDefinition);
    Task<bool> Delete<CustomerGuestLoyaltyModel>(FilterDefinition<CustomerGuestLoyaltyModel> filter);
}
