﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerDinningOptionsLocation;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerDinningOptionsLocation;

public interface ICustomerDinningOptionsLocationRepository : IRepositoryBase<CustomerDinningOptionsLocationModel>
{
    Task<CustomerDinningOptionsLocationModel> Post(CustomerDinningOptionsLocationModel doc);
    Task<CustomerDinningOptionsLocationModel> GetSingle(FilterDefinition<CustomerDinningOptionsLocationModel> filter = null, SortDefinition<CustomerDinningOptionsLocationModel> sort = null, int limit = 0);
    Task<List<CustomerDinningOptionsLocationModel>> GetList(FilterDefinition<CustomerDinningOptionsLocationModel> filter, SortDefinition<CustomerDinningOptionsLocationModel> sort, int limit = 0);
    Task<bool> Put<CustomerDinningOptionsLocationModel>(FilterDefinition<CustomerDinningOptionsLocationModel> filter, UpdateDefinition<CustomerDinningOptionsLocationModel> updateDefinition);
    Task<bool> Delete<CustomerDinningOptionsLocationModel>(FilterDefinition<CustomerDinningOptionsLocationModel> filter);
}
