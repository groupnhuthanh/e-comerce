﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsDiningOption;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsDiningOption;

public interface ICustomerGuestsDiningOptionRepository : IRepositoryBase<CustomerGuestsDiningOptionModel>
{
    Task<CustomerGuestsDiningOptionModel> Post(CustomerGuestsDiningOptionModel doc);
    Task<CustomerGuestsDiningOptionModel> GetSingle(FilterDefinition<CustomerGuestsDiningOptionModel> filter = null, SortDefinition<CustomerGuestsDiningOptionModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsDiningOptionModel>> GetList(FilterDefinition<CustomerGuestsDiningOptionModel> filter, SortDefinition<CustomerGuestsDiningOptionModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsDiningOptionModel>(FilterDefinition<CustomerGuestsDiningOptionModel> filter, UpdateDefinition<CustomerGuestsDiningOptionModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsDiningOptionModel>(FilterDefinition<CustomerGuestsDiningOptionModel> filter);
}
