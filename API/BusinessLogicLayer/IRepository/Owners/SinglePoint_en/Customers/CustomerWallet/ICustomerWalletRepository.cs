﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerWallet;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerWallet;

public interface ICustomerWalletRepository : IRepositoryBase<CustomerWalletModel>
{
    Task<CustomerWalletModel> Post(CustomerWalletModel doc);
    Task<CustomerWalletModel> GetSingle(FilterDefinition<CustomerWalletModel> filter = null, SortDefinition<CustomerWalletModel> sort = null, int limit = 0);
    Task<List<CustomerWalletModel>> GetList(FilterDefinition<CustomerWalletModel> filter, SortDefinition<CustomerWalletModel> sort, int limit = 0);
    Task<bool> Put<CustomerWalletModel>(FilterDefinition<CustomerWalletModel> filter, UpdateDefinition<CustomerWalletModel> updateDefinition);
    Task<bool> Delete<CustomerWalletModel>(FilterDefinition<CustomerWalletModel> filter);
}
