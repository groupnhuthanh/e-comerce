﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsLoginHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsLoginHistory;

public interface ICustomerGuestsLoginHistoryRepository : IRepositoryBase<CustomerGuestsLoginHistoryModel>
{
    Task<CustomerGuestsLoginHistoryModel> Post(CustomerGuestsLoginHistoryModel doc);
    Task<CustomerGuestsLoginHistoryModel> GetSingle(FilterDefinition<CustomerGuestsLoginHistoryModel> filter = null, SortDefinition<CustomerGuestsLoginHistoryModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsLoginHistoryModel>> GetList(FilterDefinition<CustomerGuestsLoginHistoryModel> filter, SortDefinition<CustomerGuestsLoginHistoryModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsLoginHistoryModel>(FilterDefinition<CustomerGuestsLoginHistoryModel> filter, UpdateDefinition<CustomerGuestsLoginHistoryModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsLoginHistoryModel>(FilterDefinition<CustomerGuestsLoginHistoryModel> filter);
}
