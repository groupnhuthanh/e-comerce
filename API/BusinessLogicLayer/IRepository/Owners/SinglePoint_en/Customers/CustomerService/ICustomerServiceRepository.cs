﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerService;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerService;

public interface ICustomerServiceRepository : IRepositoryBase<CustomerServiceModel>
{
    Task<CustomerServiceModel> Post(CustomerServiceModel doc);
    Task<CustomerServiceModel> GetSingle(FilterDefinition<CustomerServiceModel> filter = null, SortDefinition<CustomerServiceModel> sort = null, int limit = 0);
    Task<List<CustomerServiceModel>> GetList(FilterDefinition<CustomerServiceModel> filter, SortDefinition<CustomerServiceModel> sort, int limit = 0);
    Task<bool> Put<CustomerServiceModel>(FilterDefinition<CustomerServiceModel> filter, UpdateDefinition<CustomerServiceModel> updateDefinition);
    Task<bool> Delete<CustomerServiceModel>(FilterDefinition<CustomerServiceModel> filter);
}
