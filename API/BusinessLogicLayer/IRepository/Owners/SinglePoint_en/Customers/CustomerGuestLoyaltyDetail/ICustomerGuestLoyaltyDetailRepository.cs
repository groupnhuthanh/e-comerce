﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyDetail;

public interface ICustomerGuestLoyaltyDetailRepository : IRepositoryBase<CustomerGuestLoyaltyDetailModel>
{
    Task<CustomerGuestLoyaltyDetailModel> Post(CustomerGuestLoyaltyDetailModel doc);
    Task<CustomerGuestLoyaltyDetailModel> GetSingle(FilterDefinition<CustomerGuestLoyaltyDetailModel> filter = null, SortDefinition<CustomerGuestLoyaltyDetailModel> sort = null, int limit = 0);
    Task<List<CustomerGuestLoyaltyDetailModel>> GetList(FilterDefinition<CustomerGuestLoyaltyDetailModel> filter, SortDefinition<CustomerGuestLoyaltyDetailModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestLoyaltyDetailModel>(FilterDefinition<CustomerGuestLoyaltyDetailModel> filter, UpdateDefinition<CustomerGuestLoyaltyDetailModel> updateDefinition);
    Task<bool> Delete<CustomerGuestLoyaltyDetailModel>(FilterDefinition<CustomerGuestLoyaltyDetailModel> filter);
}
