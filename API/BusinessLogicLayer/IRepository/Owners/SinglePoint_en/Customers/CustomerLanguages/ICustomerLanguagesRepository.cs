﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerLanguages;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerLanguages;

public interface ICustomerLanguagesRepository : IRepositoryBase<CustomerLanguagesModel>
{
    Task<CustomerLanguagesModel> Post(CustomerLanguagesModel doc);
    Task<CustomerLanguagesModel> GetSingle(FilterDefinition<CustomerLanguagesModel> filter = null, SortDefinition<CustomerLanguagesModel> sort = null, int limit = 0);
    Task<List<CustomerLanguagesModel>> GetList(FilterDefinition<CustomerLanguagesModel> filter, SortDefinition<CustomerLanguagesModel> sort, int limit = 0);
    Task<bool> Put<CustomerLanguagesModel>(FilterDefinition<CustomerLanguagesModel> filter, UpdateDefinition<CustomerLanguagesModel> updateDefinition);
    Task<bool> Delete<CustomerLanguagesModel>(FilterDefinition<CustomerLanguagesModel> filter);
}
