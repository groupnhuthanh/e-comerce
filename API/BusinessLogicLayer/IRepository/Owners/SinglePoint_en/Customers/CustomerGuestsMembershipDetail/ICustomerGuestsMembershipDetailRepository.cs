﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsMembershipDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsMembershipDetail;

public interface ICustomerGuestsMembershipDetailRepository : IRepositoryBase<CustomerGuestsMembershipDetailModel>
{
    Task<CustomerGuestsMembershipDetailModel> Post(CustomerGuestsMembershipDetailModel doc);
    Task<CustomerGuestsMembershipDetailModel> GetSingle(FilterDefinition<CustomerGuestsMembershipDetailModel> filter = null, SortDefinition<CustomerGuestsMembershipDetailModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsMembershipDetailModel>> GetList(FilterDefinition<CustomerGuestsMembershipDetailModel> filter, SortDefinition<CustomerGuestsMembershipDetailModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsMembershipDetailModel>(FilterDefinition<CustomerGuestsMembershipDetailModel> filter, UpdateDefinition<CustomerGuestsMembershipDetailModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsMembershipDetailModel>(FilterDefinition<CustomerGuestsMembershipDetailModel> filter);
}
