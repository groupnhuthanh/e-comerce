﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsDeleteHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsDeleteHistory;

public interface ICustomerGuestsDeleteHistoryRepository : IRepositoryBase<CustomerGuestsDeleteHistoryModel>
{
    Task<CustomerGuestsDeleteHistoryModel> Post(CustomerGuestsDeleteHistoryModel doc);
    Task<CustomerGuestsDeleteHistoryModel> GetSingle(FilterDefinition<CustomerGuestsDeleteHistoryModel> filter = null, SortDefinition<CustomerGuestsDeleteHistoryModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsDeleteHistoryModel>> GetList(FilterDefinition<CustomerGuestsDeleteHistoryModel> filter, SortDefinition<CustomerGuestsDeleteHistoryModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsDeleteHistoryModel>(FilterDefinition<CustomerGuestsDeleteHistoryModel> filter, UpdateDefinition<CustomerGuestsDeleteHistoryModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsDeleteHistoryModel>(FilterDefinition<CustomerGuestsDeleteHistoryModel> filter);

}
