﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerLedger;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerLedger;

public interface ICustomerLedgerRepository : IRepositoryBase<CustomerLedgerModel>
{
    Task<CustomerLedgerModel> Post(CustomerLedgerModel doc);
    Task<CustomerLedgerModel> GetSingle(FilterDefinition<CustomerLedgerModel> filter = null, SortDefinition<CustomerLedgerModel> sort = null, int limit = 0);
    Task<List<CustomerLedgerModel>> GetList(FilterDefinition<CustomerLedgerModel> filter, SortDefinition<CustomerLedgerModel> sort, int limit = 0);
    Task<bool> Put<CustomerLedgerModel>(FilterDefinition<CustomerLedgerModel> filter, UpdateDefinition<CustomerLedgerModel> updateDefinition);
    Task<bool> Delete<CustomerLedgerModel>(FilterDefinition<CustomerLedgerModel> filter);
}
