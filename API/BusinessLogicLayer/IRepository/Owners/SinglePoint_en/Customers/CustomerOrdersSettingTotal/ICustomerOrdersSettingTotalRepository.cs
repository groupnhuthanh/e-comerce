﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerOrdersSettingTotal;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerOrdersSettingTotal;

public interface ICustomerOrdersSettingTotalRepository : IRepositoryBase<CustomerOrdersSettingTotalModel>
{
    Task<CustomerOrdersSettingTotalModel> Post(CustomerOrdersSettingTotalModel doc);
    Task<CustomerOrdersSettingTotalModel> GetSingle(FilterDefinition<CustomerOrdersSettingTotalModel> filter = null, SortDefinition<CustomerOrdersSettingTotalModel> sort = null, int limit = 0);
    Task<List<CustomerOrdersSettingTotalModel>> GetList(FilterDefinition<CustomerOrdersSettingTotalModel> filter, SortDefinition<CustomerOrdersSettingTotalModel> sort, int limit = 0);
    Task<bool> Put<CustomerOrdersSettingTotalModel>(FilterDefinition<CustomerOrdersSettingTotalModel> filter, UpdateDefinition<CustomerOrdersSettingTotalModel> updateDefinition);
    Task<bool> Delete<CustomerOrdersSettingTotalModel>(FilterDefinition<CustomerOrdersSettingTotalModel> filter);
}
