﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsShippings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsShippings;

public interface ICustomerGuestsPaymentsShippingsRepository : IRepositoryBase<CustomerGuestsPaymentsShippingsModel>
{
    Task<CustomerGuestsPaymentsShippingsModel> Post(CustomerGuestsPaymentsShippingsModel doc);
    Task<CustomerGuestsPaymentsShippingsModel> GetSingle(FilterDefinition<CustomerGuestsPaymentsShippingsModel> filter = null, SortDefinition<CustomerGuestsPaymentsShippingsModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsPaymentsShippingsModel>> GetList(FilterDefinition<CustomerGuestsPaymentsShippingsModel> filter, SortDefinition<CustomerGuestsPaymentsShippingsModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsPaymentsShippingsModel>(FilterDefinition<CustomerGuestsPaymentsShippingsModel> filter, UpdateDefinition<CustomerGuestsPaymentsShippingsModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsPaymentsShippingsModel>(FilterDefinition<CustomerGuestsPaymentsShippingsModel> filter);
}
