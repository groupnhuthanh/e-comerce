﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyPending;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyPending;

public interface ICustomerGuestLoyaltyPendingRepository : IRepositoryBase<CustomerGuestLoyaltyPendingModel>
{
    Task<CustomerGuestLoyaltyPendingModel> Post(CustomerGuestLoyaltyPendingModel doc);
    Task<CustomerGuestLoyaltyPendingModel> GetSingle(FilterDefinition<CustomerGuestLoyaltyPendingModel> filter = null, SortDefinition<CustomerGuestLoyaltyPendingModel> sort = null, int limit = 0);
    Task<List<CustomerGuestLoyaltyPendingModel>> GetList(FilterDefinition<CustomerGuestLoyaltyPendingModel> filter, SortDefinition<CustomerGuestLoyaltyPendingModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestLoyaltyPendingModel>(FilterDefinition<CustomerGuestLoyaltyPendingModel> filter, UpdateDefinition<CustomerGuestLoyaltyPendingModel> updateDefinition);
    Task<bool> Delete<CustomerGuestLoyaltyPendingModel>(FilterDefinition<CustomerGuestLoyaltyPendingModel> filter);
}
