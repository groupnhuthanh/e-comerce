﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuests;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuests;

public interface ICustomerGuestsRepository : IRepositoryBase<CustomerGuestsModel>
{
    Task<CustomerGuestsModel> Post(CustomerGuestsModel doc);
    Task<CustomerGuestsModel> GetSingle(FilterDefinition<CustomerGuestsModel> filter = null, SortDefinition<CustomerGuestsModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsModel>> GetList(FilterDefinition<CustomerGuestsModel> filter, SortDefinition<CustomerGuestsModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsModel>(FilterDefinition<CustomerGuestsModel> filter, UpdateDefinition<CustomerGuestsModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsModel>(FilterDefinition<CustomerGuestsModel> filter);

}