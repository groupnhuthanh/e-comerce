﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerSurcharge;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerSurcharge;

public interface ICustomerSurchargeRepository : IRepositoryBase<CustomerSurchargeModel>
{
    Task<CustomerSurchargeModel> Post(CustomerSurchargeModel doc);
    Task<CustomerSurchargeModel> GetSingle(FilterDefinition<CustomerSurchargeModel> filter = null, SortDefinition<CustomerSurchargeModel> sort = null, int limit = 0);
    Task<List<CustomerSurchargeModel>> GetList(FilterDefinition<CustomerSurchargeModel> filter, SortDefinition<CustomerSurchargeModel> sort, int limit = 0);
    Task<bool> Put<CustomerSurchargeModel>(FilterDefinition<CustomerSurchargeModel> filter, UpdateDefinition<CustomerSurchargeModel> updateDefinition);
    Task<bool> Delete<CustomerSurchargeModel>(FilterDefinition<CustomerSurchargeModel> filter);
}
