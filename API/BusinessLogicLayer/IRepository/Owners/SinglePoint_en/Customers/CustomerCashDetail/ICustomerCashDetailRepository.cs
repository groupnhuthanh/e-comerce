﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerCashDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerCashDetail;

public interface ICustomerCashDetailRepository : IRepositoryBase<CustomerCashDetailModel>
{
    Task<CustomerCashDetailModel> Post(CustomerCashDetailModel doc);
    Task<CustomerCashDetailModel> GetSingle(FilterDefinition<CustomerCashDetailModel> filter = null, SortDefinition<CustomerCashDetailModel> sort = null, int limit = 0);
    Task<List<CustomerCashDetailModel>> GetList(FilterDefinition<CustomerCashDetailModel> filter, SortDefinition<CustomerCashDetailModel> sort, int limit = 0);
    Task<bool> Put<CustomerCashDetailModel>(FilterDefinition<CustomerCashDetailModel> filter, UpdateDefinition<CustomerCashDetailModel> updateDefinition);
    Task<bool> Delete<CustomerCashDetailModel>(FilterDefinition<CustomerCashDetailModel> filter);
}
