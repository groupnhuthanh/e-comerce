﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerSurchargeProduct;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerSurchargeProduct;

public interface ICustomerSurchargeProductRepository : IRepositoryBase<CustomerSurchargeProductModel>
{
    Task<CustomerSurchargeProductModel> Post(CustomerSurchargeProductModel doc);
    Task<CustomerSurchargeProductModel> GetSingle(FilterDefinition<CustomerSurchargeProductModel> filter = null, SortDefinition<CustomerSurchargeProductModel> sort = null, int limit = 0);
    Task<List<CustomerSurchargeProductModel>> GetList(FilterDefinition<CustomerSurchargeProductModel> filter, SortDefinition<CustomerSurchargeProductModel> sort, int limit = 0);
    Task<bool> Put<CustomerSurchargeProductModel>(FilterDefinition<CustomerSurchargeProductModel> filter, UpdateDefinition<CustomerSurchargeProductModel> updateDefinition);
    Task<bool> Delete<CustomerSurchargeProductModel>(FilterDefinition<CustomerSurchargeProductModel> filter);
}
