﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerOthersPayments;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerOthersPayments;

public interface ICustomerOthersPaymentsRepository : IRepositoryBase<CustomerOthersPaymentsModel>
{
    Task<CustomerOthersPaymentsModel> Post(CustomerOthersPaymentsModel doc);
    Task<CustomerOthersPaymentsModel> GetSingle(FilterDefinition<CustomerOthersPaymentsModel> filter = null, SortDefinition<CustomerOthersPaymentsModel> sort = null, int limit = 0);
    Task<List<CustomerOthersPaymentsModel>> GetList(FilterDefinition<CustomerOthersPaymentsModel> filter, SortDefinition<CustomerOthersPaymentsModel> sort, int limit = 0);
    Task<bool> Put<CustomerOthersPaymentsModel>(FilterDefinition<CustomerOthersPaymentsModel> filter, UpdateDefinition<CustomerOthersPaymentsModel> updateDefinition);
    Task<bool> Delete<CustomerOthersPaymentsModel>(FilterDefinition<CustomerOthersPaymentsModel> filter);
}
