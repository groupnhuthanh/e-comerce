﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerSettings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerSettings;

public interface ICustomerSettingsRepository : IRepositoryBase<CustomerSettingsModel>
{
    Task<CustomerSettingsModel> Post(CustomerSettingsModel doc);
    Task<CustomerSettingsModel> GetSingle(FilterDefinition<CustomerSettingsModel> filter = null, SortDefinition<CustomerSettingsModel> sort = null, int limit = 0);
    Task<List<CustomerSettingsModel>> GetList(FilterDefinition<CustomerSettingsModel> filter, SortDefinition<CustomerSettingsModel> sort, int limit = 0);
    Task<bool> Put<CustomerSettingsModel>(FilterDefinition<CustomerSettingsModel> filter, UpdateDefinition<CustomerSettingsModel> updateDefinition);
    Task<bool> Delete<CustomerSettingsModel>(FilterDefinition<CustomerSettingsModel> filter);
}
