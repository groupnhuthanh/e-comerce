﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerLedgerDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerLedgerDetail;

public interface ICustomerLedgerDetailRepository : IRepositoryBase<CustomerLedgerDetailModel>
{
    Task<CustomerLedgerDetailModel> Post(CustomerLedgerDetailModel doc);
    Task<CustomerLedgerDetailModel> GetSingle(FilterDefinition<CustomerLedgerDetailModel> filter = null, SortDefinition<CustomerLedgerDetailModel> sort = null, int limit = 0);
    Task<List<CustomerLedgerDetailModel>> GetList(FilterDefinition<CustomerLedgerDetailModel> filter, SortDefinition<CustomerLedgerDetailModel> sort, int limit = 0);
    Task<bool> Put<CustomerLedgerDetailModel>(FilterDefinition<CustomerLedgerDetailModel> filter, UpdateDefinition<CustomerLedgerDetailModel> updateDefinition);
    Task<bool> Delete<CustomerLedgerDetailModel>(FilterDefinition<CustomerLedgerDetailModel> filter);
}
