﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerCompVoid;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerCompVoid;

public interface ICustomerCompVoidRepository : IRepositoryBase<CustomerCompVoidModel>
{
    Task<CustomerCompVoidModel> Post(CustomerCompVoidModel doc);
    Task<CustomerCompVoidModel> GetSingle(FilterDefinition<CustomerCompVoidModel> filter = null, SortDefinition<CustomerCompVoidModel> sort = null, int limit = 0);
    Task<List<CustomerCompVoidModel>> GetList(FilterDefinition<CustomerCompVoidModel> filter, SortDefinition<CustomerCompVoidModel> sort, int limit = 0);
    Task<bool> Put<CustomerCompVoidModel>(FilterDefinition<CustomerCompVoidModel> filter, UpdateDefinition<CustomerCompVoidModel> updateDefinition);
    Task<bool> Delete<CustomerCompVoidModel>(FilterDefinition<CustomerCompVoidModel> filter);
}
