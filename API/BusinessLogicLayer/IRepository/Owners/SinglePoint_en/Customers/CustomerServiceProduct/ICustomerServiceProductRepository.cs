﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerServiceProduct;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerServiceProduct;

public interface ICustomerServiceProductRepository : IRepositoryBase<CustomerServiceProductModel>
{
    Task<CustomerServiceProductModel> Post(CustomerServiceProductModel doc);
    Task<CustomerServiceProductModel> GetSingle(FilterDefinition<CustomerServiceProductModel> filter = null, SortDefinition<CustomerServiceProductModel> sort = null, int limit = 0);
    Task<List<CustomerServiceProductModel>> GetList(FilterDefinition<CustomerServiceProductModel> filter, SortDefinition<CustomerServiceProductModel> sort, int limit = 0);
    Task<bool> Put<CustomerServiceProductModel>(FilterDefinition<CustomerServiceProductModel> filter, UpdateDefinition<CustomerServiceProductModel> updateDefinition);
    Task<bool> Delete<CustomerServiceProductModel>(FilterDefinition<CustomerServiceProductModel> filter);
}
