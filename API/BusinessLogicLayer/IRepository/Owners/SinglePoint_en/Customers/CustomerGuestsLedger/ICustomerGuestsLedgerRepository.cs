﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsLedger;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsLedger;

public interface ICustomerGuestsLedgerRepository : IRepositoryBase<CustomerGuestsLedgerModel>
{
    Task<CustomerGuestsLedgerModel> Post(CustomerGuestsLedgerModel doc);
    Task<CustomerGuestsLedgerModel> GetSingle(FilterDefinition<CustomerGuestsLedgerModel> filter = null, SortDefinition<CustomerGuestsLedgerModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsLedgerModel>> GetList(FilterDefinition<CustomerGuestsLedgerModel> filter, SortDefinition<CustomerGuestsLedgerModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsLedgerModel>(FilterDefinition<CustomerGuestsLedgerModel> filter, UpdateDefinition<CustomerGuestsLedgerModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsLedgerModel>(FilterDefinition<CustomerGuestsLedgerModel> filter);
}
