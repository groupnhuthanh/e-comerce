﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerCash;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerCash;

public interface ICustomerCashRepository : IRepositoryBase<CustomerCashModel>
{
    Task<CustomerCashModel> Post(CustomerCashModel doc);
    Task<CustomerCashModel> GetSingle(FilterDefinition<CustomerCashModel> filter = null, SortDefinition<CustomerCashModel> sort = null, int limit = 0);
    Task<List<CustomerCashModel>> GetList(FilterDefinition<CustomerCashModel> filter, SortDefinition<CustomerCashModel> sort, int limit = 0);
    Task<bool> Put<CustomerCashModel>(FilterDefinition<CustomerCashModel> filter, UpdateDefinition<CustomerCashModel> updateDefinition);
    Task<bool> Delete<CustomerCashModel>(FilterDefinition<CustomerCashModel> filter);
}
