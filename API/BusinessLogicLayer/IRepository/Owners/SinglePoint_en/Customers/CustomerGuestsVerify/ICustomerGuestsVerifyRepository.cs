﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsVerify;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsVerify;

public interface ICustomerGuestsVerifyRepository : IRepositoryBase<CustomerGuestsVerifyModel>
{
    Task<CustomerGuestsVerifyModel> Post(CustomerGuestsVerifyModel doc);
    Task<CustomerGuestsVerifyModel> GetSingle(FilterDefinition<CustomerGuestsVerifyModel> filter = null, SortDefinition<CustomerGuestsVerifyModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsVerifyModel>> GetList(FilterDefinition<CustomerGuestsVerifyModel> filter, SortDefinition<CustomerGuestsVerifyModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsVerifyModel>(FilterDefinition<CustomerGuestsVerifyModel> filter, UpdateDefinition<CustomerGuestsVerifyModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsVerifyModel>(FilterDefinition<CustomerGuestsVerifyModel> filter);
}
