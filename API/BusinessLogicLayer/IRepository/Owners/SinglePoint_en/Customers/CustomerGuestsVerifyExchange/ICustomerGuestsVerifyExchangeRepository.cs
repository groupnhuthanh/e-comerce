﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsVerifyExchange;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsVerifyExchange;

public interface ICustomerGuestsVerifyExchangeRepository : IRepositoryBase<CustomerGuestsVerifyExchangeModel>
{
    Task<CustomerGuestsVerifyExchangeModel> Post(CustomerGuestsVerifyExchangeModel doc);
    Task<CustomerGuestsVerifyExchangeModel> GetSingle(FilterDefinition<CustomerGuestsVerifyExchangeModel> filter = null, SortDefinition<CustomerGuestsVerifyExchangeModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsVerifyExchangeModel>> GetList(FilterDefinition<CustomerGuestsVerifyExchangeModel> filter, SortDefinition<CustomerGuestsVerifyExchangeModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsVerifyExchangeModel>(FilterDefinition<CustomerGuestsVerifyExchangeModel> filter, UpdateDefinition<CustomerGuestsVerifyExchangeModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsVerifyExchangeModel>(FilterDefinition<CustomerGuestsVerifyExchangeModel> filter);
}
