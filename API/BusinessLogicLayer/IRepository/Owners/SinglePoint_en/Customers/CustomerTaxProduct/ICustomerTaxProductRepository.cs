﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerTaxProduct;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerTaxProduct;

public interface ICustomerTaxProductRepository : IRepositoryBase<CustomerTaxProductModel>
{
    Task<CustomerTaxProductModel> Post(CustomerTaxProductModel doc);
    Task<CustomerTaxProductModel> GetSingle(FilterDefinition<CustomerTaxProductModel> filter = null, SortDefinition<CustomerTaxProductModel> sort = null, int limit = 0);
    Task<List<CustomerTaxProductModel>> GetList(FilterDefinition<CustomerTaxProductModel> filter, SortDefinition<CustomerTaxProductModel> sort, int limit = 0);
    Task<bool> Put<CustomerTaxProductModel>(FilterDefinition<CustomerTaxProductModel> filter, UpdateDefinition<CustomerTaxProductModel> updateDefinition);
    Task<bool> Delete<CustomerTaxProductModel>(FilterDefinition<CustomerTaxProductModel> filter);
}
