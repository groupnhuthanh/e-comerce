﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerWalletDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerWalletDetail;

public interface ICustomerWalletDetailRepository : IRepositoryBase<CustomerWalletDetailModel>
{
    Task<CustomerWalletDetailModel> Post(CustomerWalletDetailModel doc);
    Task<CustomerWalletDetailModel> GetSingle(FilterDefinition<CustomerWalletDetailModel> filter = null, SortDefinition<CustomerWalletDetailModel> sort = null, int limit = 0);
    Task<List<CustomerWalletDetailModel>> GetList(FilterDefinition<CustomerWalletDetailModel> filter, SortDefinition<CustomerWalletDetailModel> sort, int limit = 0);
    Task<bool> Put<CustomerWalletDetailModel>(FilterDefinition<CustomerWalletDetailModel> filter, UpdateDefinition<CustomerWalletDetailModel> updateDefinition);
    Task<bool> Delete<CustomerWalletDetailModel>(FilterDefinition<CustomerWalletDetailModel> filter);
}
