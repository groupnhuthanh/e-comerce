﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerTax;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerTax;

public interface ICustomerTaxRepository : IRepositoryBase<CustomerTaxModel>
{
    Task<CustomerTaxModel> Post(CustomerTaxModel doc);
    Task<CustomerTaxModel> GetSingle(FilterDefinition<CustomerTaxModel> filter = null, SortDefinition<CustomerTaxModel> sort = null, int limit = 0);
    Task<List<CustomerTaxModel>> GetList(FilterDefinition<CustomerTaxModel> filter, SortDefinition<CustomerTaxModel> sort, int limit = 0);
    Task<bool> Put<CustomerTaxModel>(FilterDefinition<CustomerTaxModel> filter, UpdateDefinition<CustomerTaxModel> updateDefinition);
    Task<bool> Delete<CustomerTaxModel>(FilterDefinition<CustomerTaxModel> filter);
}
