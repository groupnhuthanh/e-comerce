﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGroupType;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGroupType;

public interface ICustomerGroupTypeRepository : IRepositoryBase<CustomerGroupTypeModel>
{
    Task<CustomerGroupTypeModel> Post(CustomerGroupTypeModel doc);
    Task<CustomerGroupTypeModel> GetSingle(FilterDefinition<CustomerGroupTypeModel> filter = null, SortDefinition<CustomerGroupTypeModel> sort = null, int limit = 0);
    Task<List<CustomerGroupTypeModel>> GetList(FilterDefinition<CustomerGroupTypeModel> filter, SortDefinition<CustomerGroupTypeModel> sort, int limit = 0);
    Task<bool> Put<CustomerGroupTypeModel>(FilterDefinition<CustomerGroupTypeModel> filter, UpdateDefinition<CustomerGroupTypeModel> updateDefinition);
    Task<bool> Delete<CustomerGroupTypeModel>(FilterDefinition<CustomerGroupTypeModel> filter);
}
