﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerDiscounts;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerDiscounts;

public interface ICustomerDiscountsRepository : IRepositoryBase<CustomerDiscountsModel>
{
    Task<CustomerDiscountsModel> Post(CustomerDiscountsModel doc);
    Task<CustomerDiscountsModel> GetSingle(FilterDefinition<CustomerDiscountsModel> filter = null, SortDefinition<CustomerDiscountsModel> sort = null, int limit = 0);
    Task<List<CustomerDiscountsModel>> GetList(FilterDefinition<CustomerDiscountsModel> filter, SortDefinition<CustomerDiscountsModel> sort, int limit = 0);
    Task<bool> Put<CustomerDiscountsModel>(FilterDefinition<CustomerDiscountsModel> filter, UpdateDefinition<CustomerDiscountsModel> updateDefinition);
    Task<bool> Delete<CustomerDiscountsModel>(FilterDefinition<CustomerDiscountsModel> filter);

}
