﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsBillings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsBillings;

public interface ICustomerGuestsPaymentsBillingsRepository : IRepositoryBase<CustomerGuestsPaymentsBillingsModel>
{
    Task<CustomerGuestsPaymentsBillingsModel> Post(CustomerGuestsPaymentsBillingsModel doc);
    Task<CustomerGuestsPaymentsBillingsModel> GetSingle(FilterDefinition<CustomerGuestsPaymentsBillingsModel> filter = null, SortDefinition<CustomerGuestsPaymentsBillingsModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsPaymentsBillingsModel>> GetList(FilterDefinition<CustomerGuestsPaymentsBillingsModel> filter, SortDefinition<CustomerGuestsPaymentsBillingsModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsPaymentsBillingsModel>(FilterDefinition<CustomerGuestsPaymentsBillingsModel> filter, UpdateDefinition<CustomerGuestsPaymentsBillingsModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsPaymentsBillingsModel>(FilterDefinition<CustomerGuestsPaymentsBillingsModel> filter);
}
