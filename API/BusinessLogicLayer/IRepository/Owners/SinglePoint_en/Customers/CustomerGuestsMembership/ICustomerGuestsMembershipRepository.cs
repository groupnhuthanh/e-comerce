﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsMembership;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsMembership;

public interface ICustomerGuestsMembershipRepository : IRepositoryBase<CustomerGuestsMembershipModel>
{
    Task<CustomerGuestsMembershipModel> Post(CustomerGuestsMembershipModel doc);
    Task<CustomerGuestsMembershipModel> GetSingle(FilterDefinition<CustomerGuestsMembershipModel> filter = null, SortDefinition<CustomerGuestsMembershipModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsMembershipModel>> GetList(FilterDefinition<CustomerGuestsMembershipModel> filter, SortDefinition<CustomerGuestsMembershipModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsMembershipModel>(FilterDefinition<CustomerGuestsMembershipModel> filter, UpdateDefinition<CustomerGuestsMembershipModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsMembershipModel>(FilterDefinition<CustomerGuestsMembershipModel> filter);
}
