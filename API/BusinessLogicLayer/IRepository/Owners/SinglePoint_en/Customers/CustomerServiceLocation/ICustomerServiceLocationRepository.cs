﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerServiceLocation;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerServiceLocation;

public interface ICustomerServiceLocationRepository : IRepositoryBase<CustomerServiceLocationModel>
{
    Task<CustomerServiceLocationModel> Post(CustomerServiceLocationModel doc);
    Task<CustomerServiceLocationModel> GetSingle(FilterDefinition<CustomerServiceLocationModel> filter = null, SortDefinition<CustomerServiceLocationModel> sort = null, int limit = 0);
    Task<List<CustomerServiceLocationModel>> GetList(FilterDefinition<CustomerServiceLocationModel> filter, SortDefinition<CustomerServiceLocationModel> sort, int limit = 0);
    Task<bool> Put<CustomerServiceLocationModel>(FilterDefinition<CustomerServiceLocationModel> filter, UpdateDefinition<CustomerServiceLocationModel> updateDefinition);
    Task<bool> Delete<CustomerServiceLocationModel>(FilterDefinition<CustomerServiceLocationModel> filter);
}
