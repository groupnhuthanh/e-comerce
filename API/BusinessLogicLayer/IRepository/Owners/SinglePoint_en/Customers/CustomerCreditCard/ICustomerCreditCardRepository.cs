﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerCreditCard;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerCreditCard;

public interface ICustomerCreditCardRepository : IRepositoryBase<CustomerCreditCardModel>
{
    Task<CustomerCreditCardModel> Post(CustomerCreditCardModel doc);
    Task<CustomerCreditCardModel> GetSingle(FilterDefinition<CustomerCreditCardModel> filter = null, SortDefinition<CustomerCreditCardModel> sort = null, int limit = 0);
    Task<List<CustomerCreditCardModel>> GetList(FilterDefinition<CustomerCreditCardModel> filter, SortDefinition<CustomerCreditCardModel> sort, int limit = 0);
    Task<bool> Put<CustomerCreditCardModel>(FilterDefinition<CustomerCreditCardModel> filter, UpdateDefinition<CustomerCreditCardModel> updateDefinition);
    Task<bool> Delete<CustomerCreditCardModel>(FilterDefinition<CustomerCreditCardModel> filter);
}
