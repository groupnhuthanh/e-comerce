﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerSurchargeLocation;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerSurchargeLocation;

public interface ICustomerSurchargeLocationRepository : IRepositoryBase<CustomerSurchargeLocationModel>
{
    Task<CustomerSurchargeLocationModel> Post(CustomerSurchargeLocationModel doc);
    Task<CustomerSurchargeLocationModel> GetSingle(FilterDefinition<CustomerSurchargeLocationModel> filter = null, SortDefinition<CustomerSurchargeLocationModel> sort = null, int limit = 0);
    Task<List<CustomerSurchargeLocationModel>> GetList(FilterDefinition<CustomerSurchargeLocationModel> filter, SortDefinition<CustomerSurchargeLocationModel> sort, int limit = 0);
    Task<bool> Put<CustomerSurchargeLocationModel>(FilterDefinition<CustomerSurchargeLocationModel> filter, UpdateDefinition<CustomerSurchargeLocationModel> updateDefinition);
    Task<bool> Delete<CustomerSurchargeLocationModel>(FilterDefinition<CustomerSurchargeLocationModel> filter);
}
