﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.Customer;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.Customer;

public interface ICustomerRepository : IRepositoryBase<CustomerModel>
{
    Task<CustomerModel> Post(CustomerModel doc);
    Task<CustomerModel> GetSingle(FilterDefinition<CustomerModel> filter = null, SortDefinition<CustomerModel> sort = null, int limit = 0);
    Task<List<CustomerModel>> GetList(FilterDefinition<CustomerModel> filter, SortDefinition<CustomerModel> sort, int limit = 0);
    Task<bool> Put<CustomerModel>(FilterDefinition<CustomerModel> filter, UpdateDefinition<CustomerModel> updateDefinition);
    Task<bool> Delete<CustomerModel>(FilterDefinition<CustomerModel> filter);
}
