﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsGroup;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsGroup;

public interface ICustomerGuestsGroupRepository : IRepositoryBase<CustomerGuestsGroupModel>

{
    Task<CustomerGuestsGroupModel> Post(CustomerGuestsGroupModel doc);
    Task<CustomerGuestsGroupModel> GetSingle(FilterDefinition<CustomerGuestsGroupModel> filter = null, SortDefinition<CustomerGuestsGroupModel> sort = null, int limit = 0);
    Task<List<CustomerGuestsGroupModel>> GetList(FilterDefinition<CustomerGuestsGroupModel> filter, SortDefinition<CustomerGuestsGroupModel> sort, int limit = 0);
    Task<bool> Put<CustomerGuestsGroupModel>(FilterDefinition<CustomerGuestsGroupModel> filter, UpdateDefinition<CustomerGuestsGroupModel> updateDefinition);
    Task<bool> Delete<CustomerGuestsGroupModel>(FilterDefinition<CustomerGuestsGroupModel> filter);

}
