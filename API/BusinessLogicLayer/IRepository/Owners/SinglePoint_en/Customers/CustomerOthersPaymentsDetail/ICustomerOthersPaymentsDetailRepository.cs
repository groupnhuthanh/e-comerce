﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerOthersPaymentsDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerOthersPaymentsDetail;

public interface ICustomerOthersPaymentsDetailRepository : IRepositoryBase<CustomerOthersPaymentsDetailModel>
{
    Task<CustomerOthersPaymentsDetailModel> Post(CustomerOthersPaymentsDetailModel doc);
    Task<CustomerOthersPaymentsDetailModel> GetSingle(FilterDefinition<CustomerOthersPaymentsDetailModel> filter = null, SortDefinition<CustomerOthersPaymentsDetailModel> sort = null, int limit = 0);
    Task<List<CustomerOthersPaymentsDetailModel>> GetList(FilterDefinition<CustomerOthersPaymentsDetailModel> filter, SortDefinition<CustomerOthersPaymentsDetailModel> sort, int limit = 0);
    Task<bool> Put<CustomerOthersPaymentsDetailModel>(FilterDefinition<CustomerOthersPaymentsDetailModel> filter, UpdateDefinition<CustomerOthersPaymentsDetailModel> updateDefinition);
    Task<bool> Delete<CustomerOthersPaymentsDetailModel>(FilterDefinition<CustomerOthersPaymentsDetailModel> filter);
}
