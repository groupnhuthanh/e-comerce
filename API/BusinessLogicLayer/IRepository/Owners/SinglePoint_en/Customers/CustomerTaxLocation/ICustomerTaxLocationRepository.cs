﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Customers.CustomerTaxLocation;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerTaxLocation;

public interface ICustomerTaxLocationRepository : IRepositoryBase<CustomerTaxLocationModel>
{
    Task<CustomerTaxLocationModel> Post(CustomerTaxLocationModel doc);
    Task<CustomerTaxLocationModel> GetSingle(FilterDefinition<CustomerTaxLocationModel> filter = null, SortDefinition<CustomerTaxLocationModel> sort = null, int limit = 0);
    Task<List<CustomerTaxLocationModel>> GetList(FilterDefinition<CustomerTaxLocationModel> filter, SortDefinition<CustomerTaxLocationModel> sort, int limit = 0);
    Task<bool> Put<CustomerTaxLocationModel>(FilterDefinition<CustomerTaxLocationModel> filter, UpdateDefinition<CustomerTaxLocationModel> updateDefinition);
    Task<bool> Delete<CustomerTaxLocationModel>(FilterDefinition<CustomerTaxLocationModel> filter);
}
