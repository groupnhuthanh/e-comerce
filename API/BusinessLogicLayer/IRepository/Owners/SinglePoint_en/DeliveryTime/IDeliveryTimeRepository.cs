﻿using BusinessLogicLayer.IRepository.Base;
using Entities.ExtendModels.Times;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.DeliveryTime;

public interface IDeliveryTimeRepository : IRepositoryBase<DeliveryTimeModel>
{
    Task<DeliveryTimeModel> Post(DeliveryTimeModel doc);
    Task<DeliveryTimeModel> GetSingle(FilterDefinition<DeliveryTimeModel> filter = null, SortDefinition<DeliveryTimeModel> sort = null, int limit = 0);
    Task<List<DeliveryTimeModel>> GetList(FilterDefinition<DeliveryTimeModel> filter, SortDefinition<DeliveryTimeModel> sort, int limit = 0);
    Task<bool> Put<DeliveryTimeModel>(FilterDefinition<DeliveryTimeModel> filter, UpdateDefinition<DeliveryTimeModel> updateDefinition);
    Task<bool> Delete<DeliveryTimeModel>(FilterDefinition<DeliveryTimeModel> filter);
}
