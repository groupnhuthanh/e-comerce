﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Locations.Location;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Locations.Location;

public interface ILocationRepository : IRepositoryBase<LocationModel>
{
    Task<LocationModel> Post(LocationModel doc);
    Task<LocationModel> GetSingle(FilterDefinition<LocationModel> filter = null, SortDefinition<LocationModel> sort = null, int limit = 0);
    Task<List<LocationModel>> GetList(FilterDefinition<LocationModel> filter, SortDefinition<LocationModel> sort, int limit = 0);
    Task<bool> Put<LocationModel>(FilterDefinition<LocationModel> filter, UpdateDefinition<LocationModel> updateDefinition);
    Task<bool> Delete<LocationModel>(FilterDefinition<LocationModel> filter);
}
