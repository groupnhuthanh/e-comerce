﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Locations.LocationSettingsIds;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Locations.LocationSettingsIds;

public interface ILocationSettingsIdsRepository : IRepositoryBase<LocationSettingsIdsModel>
{
    Task<LocationSettingsIdsModel> Post(LocationSettingsIdsModel doc);
    Task<LocationSettingsIdsModel> GetSingle(FilterDefinition<LocationSettingsIdsModel> filter = null, SortDefinition<LocationSettingsIdsModel> sort = null, int limit = 0);
    Task<List<LocationSettingsIdsModel>> GetList(FilterDefinition<LocationSettingsIdsModel> filter, SortDefinition<LocationSettingsIdsModel> sort, int limit = 0);
    Task<bool> Put<LocationSettingsIdsModel>(FilterDefinition<LocationSettingsIdsModel> filter, UpdateDefinition<LocationSettingsIdsModel> updateDefinition);
    Task<bool> Delete<LocationSettingsIdsModel>(FilterDefinition<LocationSettingsIdsModel> filter);
}
