﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Locations.LocationShipping;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Locations.LocationShipping;

public interface ILocationShippingRepository : IRepositoryBase<LocationShippingModel>
{
    Task<LocationShippingModel> Post(LocationShippingModel doc);
    Task<LocationShippingModel> GetSingle(FilterDefinition<LocationShippingModel> filter = null, SortDefinition<LocationShippingModel> sort = null, int limit = 0);
    Task<List<LocationShippingModel>> GetList(FilterDefinition<LocationShippingModel> filter, SortDefinition<LocationShippingModel> sort, int limit = 0);
    Task<bool> Put<LocationShippingModel>(FilterDefinition<LocationShippingModel> filter, UpdateDefinition<LocationShippingModel> updateDefinition);
    Task<bool> Delete<LocationShippingModel>(FilterDefinition<LocationShippingModel> filter);
}
