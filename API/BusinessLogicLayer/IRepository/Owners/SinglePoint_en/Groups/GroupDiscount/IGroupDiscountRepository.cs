﻿

using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Groups.GroupDiscount;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupDiscount;

public interface IGroupDiscountRepository : IRepositoryBase<GroupDiscountModel>
{
    Task<GroupDiscountModel> Post(GroupDiscountModel doc);
    Task<GroupDiscountModel> GetSingle(FilterDefinition<GroupDiscountModel> filter = null, SortDefinition<GroupDiscountModel> sort = null, int limit = 0);
    Task<List<GroupDiscountModel>> GetList(FilterDefinition<GroupDiscountModel> filter, SortDefinition<GroupDiscountModel> sort, int limit = 0);
    Task<bool> Put<GroupDiscountModel>(FilterDefinition<GroupDiscountModel> filter, UpdateDefinition<GroupDiscountModel> updateDefinition);
    Task<bool> Delete<GroupDiscountModel>(FilterDefinition<GroupDiscountModel> filter);
}
