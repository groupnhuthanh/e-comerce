﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Groups.GroupDiscountItem;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupDiscountItem;

public interface IGroupDiscountItemRepository : IRepositoryBase<GroupDiscountItemModel>
{
    Task<GroupDiscountItemModel> Post(GroupDiscountItemModel doc);
    Task<GroupDiscountItemModel> GetSingle(FilterDefinition<GroupDiscountItemModel> filter = null, SortDefinition<GroupDiscountItemModel> sort = null, int limit = 0);
    Task<List<GroupDiscountItemModel>> GetList(FilterDefinition<GroupDiscountItemModel> filter, SortDefinition<GroupDiscountItemModel> sort, int limit = 0);
    Task<bool> Put<GroupDiscountItemModel>(FilterDefinition<GroupDiscountItemModel> filter, UpdateDefinition<GroupDiscountItemModel> updateDefinition);
    Task<bool> Delete<GroupDiscountItemModel>(FilterDefinition<GroupDiscountItemModel> filter);
}
