﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Groups.GroupEvent;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupEvent;

public interface IGroupEventRepository : IRepositoryBase<GroupEventModel>
{
    Task<GroupEventModel> Post(GroupEventModel doc);
    Task<GroupEventModel> GetSingle(FilterDefinition<GroupEventModel> filter = null, SortDefinition<GroupEventModel> sort = null, int limit = 0);
    Task<List<GroupEventModel>> GetList(FilterDefinition<GroupEventModel> filter, SortDefinition<GroupEventModel> sort, int limit = 0);
    Task<bool> Put<GroupEventModel>(FilterDefinition<GroupEventModel> filter, UpdateDefinition<GroupEventModel> updateDefinition);
    Task<bool> Delete<GroupEventModel>(FilterDefinition<GroupEventModel> filter);
}
