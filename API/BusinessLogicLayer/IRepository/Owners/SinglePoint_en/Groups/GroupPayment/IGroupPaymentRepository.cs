﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Groups.GroupPayment;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupPayment;

public interface IGroupPaymentRepository : IRepositoryBase<GroupPaymentModel>
{
    Task<GroupPaymentModel> Post(GroupPaymentModel doc);
    Task<GroupPaymentModel> GetSingle(FilterDefinition<GroupPaymentModel> filter = null, SortDefinition<GroupPaymentModel> sort = null, int limit = 0);
    Task<List<GroupPaymentModel>> GetList(FilterDefinition<GroupPaymentModel> filter, SortDefinition<GroupPaymentModel> sort, int limit = 0);
    Task<bool> Put<GroupPaymentModel>(FilterDefinition<GroupPaymentModel> filter, UpdateDefinition<GroupPaymentModel> updateDefinition);
    Task<bool> Delete<GroupPaymentModel>(FilterDefinition<GroupPaymentModel> filter);
}
