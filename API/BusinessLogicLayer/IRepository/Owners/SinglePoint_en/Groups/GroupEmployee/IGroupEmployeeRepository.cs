﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Groups.GroupEmployee;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupEmployee;

public interface IGroupEmployeeRepository : IRepositoryBase<GroupEmployeeModel>
{
    Task<GroupEmployeeModel> Post(GroupEmployeeModel doc);
    Task<GroupEmployeeModel> GetSingle(FilterDefinition<GroupEmployeeModel> filter = null, SortDefinition<GroupEmployeeModel> sort = null, int limit = 0);
    Task<List<GroupEmployeeModel>> GetList(FilterDefinition<GroupEmployeeModel> filter, SortDefinition<GroupEmployeeModel> sort, int limit = 0);
    Task<bool> Put<GroupEmployeeModel>(FilterDefinition<GroupEmployeeModel> filter, UpdateDefinition<GroupEmployeeModel> updateDefinition);
    Task<bool> Delete<GroupEmployeeModel>(FilterDefinition<GroupEmployeeModel> filter);
}

