﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Groups.GroupLocation;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupLocation;

public interface IGroupLocationRepository : IRepositoryBase<GroupLocationModel>
{

    Task<GroupLocationModel> Post(GroupLocationModel doc);
    Task<GroupLocationModel> GetSingle(FilterDefinition<GroupLocationModel> filter = null, SortDefinition<GroupLocationModel> sort = null, int limit = 0);
    Task<List<GroupLocationModel>> GetList(FilterDefinition<GroupLocationModel> filter, SortDefinition<GroupLocationModel> sort, int limit = 0);
    Task<bool> Put<GroupLocationModel>(FilterDefinition<GroupLocationModel> filter, UpdateDefinition<GroupLocationModel> updateDefinition);
    Task<bool> Delete<GroupLocationModel>(FilterDefinition<GroupLocationModel> filter);
}
