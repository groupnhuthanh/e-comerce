﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.CompressFile;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.CompressFile;

public interface ICompressFileRepository : IRepositoryBase<CompressFileModel>
{
    Task<CompressFileModel> Post(CompressFileModel doc);
    Task<CompressFileModel> GetSingle(FilterDefinition<CompressFileModel> filter = null, SortDefinition<CompressFileModel> sort = null, int limit = 0);
    Task<List<CompressFileModel>> GetList(FilterDefinition<CompressFileModel> filter, SortDefinition<CompressFileModel> sort, int limit = 0);
    Task<bool> Put<CompressFileModel>(FilterDefinition<CompressFileModel> filter, UpdateDefinition<CompressFileModel> updateDefinition);
    Task<bool> Delete<CompressFileModel>(FilterDefinition<CompressFileModel> filter);
}
