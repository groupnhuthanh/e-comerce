﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.FriendRequests;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FriendRequests;

public interface IFriendRequestsRepository : IRepositoryBase<FriendRequestsModel>
{
    Task<FriendRequestsModel> Post(FriendRequestsModel doc);
    Task<FriendRequestsModel> GetSingle(FilterDefinition<FriendRequestsModel> filter = null, SortDefinition<FriendRequestsModel> sort = null, int limit = 0);
    Task<List<FriendRequestsModel>> GetList(FilterDefinition<FriendRequestsModel> filter, SortDefinition<FriendRequestsModel> sort, int limit = 0);
    Task<bool> Put<FriendRequestsModel>(FilterDefinition<FriendRequestsModel> filter, UpdateDefinition<FriendRequestsModel> updateDefinition);
    Task<bool> Delete<FriendRequestsModel>(FilterDefinition<FriendRequestsModel> filter);
}
