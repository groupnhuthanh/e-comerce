﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.UserRewards.UserRewardsLedger;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.UserRewards.UserRewardsLedger;

public interface IUserRewardsLedgerRepository : IRepositoryBase<UserRewardsLedgerModel>
{
    Task<UserRewardsLedgerModel> Post(UserRewardsLedgerModel doc);
    Task<UserRewardsLedgerModel> GetSingle(FilterDefinition<UserRewardsLedgerModel> filter = null, SortDefinition<UserRewardsLedgerModel> sort = null, int limit = 0);
    Task<List<UserRewardsLedgerModel>> GetList(FilterDefinition<UserRewardsLedgerModel> filter, SortDefinition<UserRewardsLedgerModel> sort, int limit = 0);
    Task<bool> Put<UserRewardsLedgerModel>(FilterDefinition<UserRewardsLedgerModel> filter, UpdateDefinition<UserRewardsLedgerModel> updateDefinition);
    Task<bool> Delete<UserRewardsLedgerModel>(FilterDefinition<UserRewardsLedgerModel> filter);
}
