﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.UserRewards.UserReward;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.UserRewards.UserReward;

public interface IUserRewardRepository : IRepositoryBase<UserRewardModel>
{
    Task<UserRewardModel> Post(UserRewardModel doc);
    Task<UserRewardModel> GetSingle(FilterDefinition<UserRewardModel> filter = null, SortDefinition<UserRewardModel> sort = null, int limit = 0);
    Task<List<UserRewardModel>> GetList(FilterDefinition<UserRewardModel> filter, SortDefinition<UserRewardModel> sort, int limit = 0);
    Task<bool> Put<UserRewardModel>(FilterDefinition<UserRewardModel> filter, UpdateDefinition<UserRewardModel> updateDefinition);
    Task<bool> Delete<UserRewardModel>(FilterDefinition<UserRewardModel> filter);
}
