﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.UserRewards.UserRewardsList;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.UserRewards.UserRewardsList;

public interface IUserRewardsListRepository : IRepositoryBase<UserRewardsListModel>
{
    Task<UserRewardsListModel> Post(UserRewardsListModel doc);
    Task<UserRewardsListModel> GetSingle(FilterDefinition<UserRewardsListModel> filter = null, SortDefinition<UserRewardsListModel> sort = null, int limit = 0);
    Task<List<UserRewardsListModel>> GetList(FilterDefinition<UserRewardsListModel> filter, SortDefinition<UserRewardsListModel> sort, int limit = 0);
    Task<bool> Put<UserRewardsListModel>(FilterDefinition<UserRewardsListModel> filter, UpdateDefinition<UserRewardsListModel> updateDefinition);
    Task<bool> Delete<UserRewardsListModel>(FilterDefinition<UserRewardsListModel> filter);
}
