﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.ReportsOrdersHistory;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.ReportsOrdersHistory;

public interface IReportsOrdersHistoryRepository : IRepositoryBase<ReportsOrdersHistoryModel>
{
    Task<ReportsOrdersHistoryModel> Post(ReportsOrdersHistoryModel doc);
    Task<ReportsOrdersHistoryModel> GetSingle(FilterDefinition<ReportsOrdersHistoryModel> filter = null, SortDefinition<ReportsOrdersHistoryModel> sort = null, int limit = 0);
    Task<List<ReportsOrdersHistoryModel>> GetList(FilterDefinition<ReportsOrdersHistoryModel> filter, SortDefinition<ReportsOrdersHistoryModel> sort, int limit = 0);
    Task<bool> Put<ReportsOrdersHistoryModel>(FilterDefinition<ReportsOrdersHistoryModel> filter, UpdateDefinition<ReportsOrdersHistoryModel> updateDefinition);
    Task<bool> Delete<ReportsOrdersHistoryModel>(FilterDefinition<ReportsOrdersHistoryModel> filter);
}
