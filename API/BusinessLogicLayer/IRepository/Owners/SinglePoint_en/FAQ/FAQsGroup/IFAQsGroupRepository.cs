﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.FAQ.FAQsGroup;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FAQ.FAQsGroup;

public interface IFAQsGroupRepository : IRepositoryBase<FAQsGroupModel>
{
    Task<FAQsGroupModel> Post(FAQsGroupModel doc);
    Task<FAQsGroupModel> GetSingle(FilterDefinition<FAQsGroupModel> filter = null, SortDefinition<FAQsGroupModel> sort = null, int limit = 0);
    Task<List<FAQsGroupModel>> GetList(FilterDefinition<FAQsGroupModel> filter, SortDefinition<FAQsGroupModel> sort, int limit = 0);
    Task<bool> Put<FAQsGroupModel>(FilterDefinition<FAQsGroupModel> filter, UpdateDefinition<FAQsGroupModel> updateDefinition);
    Task<bool> Delete<FAQsGroupModel>(FilterDefinition<FAQsGroupModel> filter);
}
