﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.FAQ.FAQs;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FAQ.FAQs;

public interface IFAQsRepository : IRepositoryBase<FAQsModel>
{
    Task<FAQsModel> Post(FAQsModel doc);
    Task<FAQsModel> GetSingle(FilterDefinition<FAQsModel> filter = null, SortDefinition<FAQsModel> sort = null, int limit = 0);
    Task<List<FAQsModel>> GetList(FilterDefinition<FAQsModel> filter, SortDefinition<FAQsModel> sort, int limit = 0);
    Task<bool> Put<FAQsModel>(FilterDefinition<FAQsModel> filter, UpdateDefinition<FAQsModel> updateDefinition);
    Task<bool> Delete<FAQsModel>(FilterDefinition<FAQsModel> filter);
}
