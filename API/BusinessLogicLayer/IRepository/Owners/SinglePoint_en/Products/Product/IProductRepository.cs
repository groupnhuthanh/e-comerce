﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Products.Product;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Products.Product;

public interface IProductRepository : IRepositoryBase<ProductModel>
{
    Task<ProductModel> Post(ProductModel doc);
    Task<ProductModel> GetSingle(FilterDefinition<ProductModel> filter = null, SortDefinition<ProductModel> sort = null, int limit = 0);
    Task<List<ProductModel>> GetList(FilterDefinition<ProductModel> filter, SortDefinition<ProductModel> sort, int limit = 0);
    Task<bool> Put<ProductModel>(FilterDefinition<ProductModel> filter, UpdateDefinition<ProductModel> updateDefinition);
    Task<bool> Delete<ProductModel>(FilterDefinition<ProductModel> filter);
}
