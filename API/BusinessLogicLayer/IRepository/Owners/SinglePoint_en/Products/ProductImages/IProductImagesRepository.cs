﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Products.ProductImages;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Products.ProductImages;

public interface IProductImagesRepository : IRepositoryBase<ProductImagesModel>
{
    Task<ProductImagesModel> Post(ProductImagesModel doc);
    Task<ProductImagesModel> GetSingle(FilterDefinition<ProductImagesModel> filter = null, SortDefinition<ProductImagesModel> sort = null, int limit = 0);
    Task<List<ProductImagesModel>> GetList(FilterDefinition<ProductImagesModel> filter, SortDefinition<ProductImagesModel> sort, int limit = 0);
    Task<bool> Put<ProductImagesModel>(FilterDefinition<ProductImagesModel> filter, UpdateDefinition<ProductImagesModel> updateDefinition);
    Task<bool> Delete<ProductImagesModel>(FilterDefinition<ProductImagesModel> filter);
}
