﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Products.ProductModifier;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Products.ProductModifier;

public interface IProductModifierRepository : IRepositoryBase<ProductModifierModel>
{
    Task<ProductModifierModel> Post(ProductModifierModel doc);
    Task<ProductModifierModel> GetSingle(FilterDefinition<ProductModifierModel> filter = null, SortDefinition<ProductModifierModel> sort = null, int limit = 0);
    Task<List<ProductModifierModel>> GetList(FilterDefinition<ProductModifierModel> filter, SortDefinition<ProductModifierModel> sort, int limit = 0);
    Task<bool> Put<ProductModifierModel>(FilterDefinition<ProductModifierModel> filter, UpdateDefinition<ProductModifierModel> updateDefinition);
    Task<bool> Delete<ProductModifierModel>(FilterDefinition<ProductModifierModel> filter);
}
