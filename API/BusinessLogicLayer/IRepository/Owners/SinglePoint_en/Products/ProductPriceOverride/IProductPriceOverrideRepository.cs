﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Products.ProductPriceOverride;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Products.ProductPriceOverride;

public interface IProductPriceOverrideRepository : IRepositoryBase<ProductPriceOverrideModel>
{
    Task<ProductPriceOverrideModel> Post(ProductPriceOverrideModel doc);
    Task<ProductPriceOverrideModel> GetSingle(FilterDefinition<ProductPriceOverrideModel> filter = null, SortDefinition<ProductPriceOverrideModel> sort = null, int limit = 0);
    Task<List<ProductPriceOverrideModel>> GetList(FilterDefinition<ProductPriceOverrideModel> filter, SortDefinition<ProductPriceOverrideModel> sort, int limit = 0);
    Task<bool> Put<ProductPriceOverrideModel>(FilterDefinition<ProductPriceOverrideModel> filter, UpdateDefinition<ProductPriceOverrideModel> updateDefinition);
    Task<bool> Delete<ProductPriceOverrideModel>(FilterDefinition<ProductPriceOverrideModel> filter);
}
