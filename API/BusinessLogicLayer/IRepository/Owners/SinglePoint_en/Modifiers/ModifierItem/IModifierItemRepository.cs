﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Modifiers.ModifierItem;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Modifiers.ModifierItem;

public interface IModifierItemRepository : IRepositoryBase<ModifierItemModel>
{
    Task<ModifierItemModel> Post(ModifierItemModel doc);
    Task<ModifierItemModel> GetSingle(FilterDefinition<ModifierItemModel> filter = null, SortDefinition<ModifierItemModel> sort = null, int limit = 0);
    Task<List<ModifierItemModel>> GetList(FilterDefinition<ModifierItemModel> filter, SortDefinition<ModifierItemModel> sort, int limit = 0);
    Task<bool> Put<ModifierItemModel>(FilterDefinition<ModifierItemModel> filter, UpdateDefinition<ModifierItemModel> updateDefinition);
    Task<bool> Delete<ModifierItemModel>(FilterDefinition<ModifierItemModel> filter);
}
