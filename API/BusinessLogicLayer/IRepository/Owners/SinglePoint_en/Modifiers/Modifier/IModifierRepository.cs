﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Modifiers.Modifier;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Modifiers.Modifier;

public interface IModifierRepository : IRepositoryBase<ModifierModel>
{
    Task<ModifierModel> Post(ModifierModel doc);
    Task<ModifierModel> GetSingle(FilterDefinition<ModifierModel> filter = null, SortDefinition<ModifierModel> sort = null, int limit = 0);
    Task<List<ModifierModel>> GetList(FilterDefinition<ModifierModel> filter, SortDefinition<ModifierModel> sort, int limit = 0);
    Task<bool> Put<ModifierModel>(FilterDefinition<ModifierModel> filter, UpdateDefinition<ModifierModel> updateDefinition);
    Task<bool> Delete<ModifierModel>(FilterDefinition<ModifierModel> filter);
}
