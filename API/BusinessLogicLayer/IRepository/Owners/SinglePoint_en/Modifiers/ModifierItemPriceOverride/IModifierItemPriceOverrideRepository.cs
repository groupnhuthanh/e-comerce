﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Modifiers.ModifierItemPriceOverride;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Modifiers.ModifierItemPriceOverride;

public interface IModifierItemPriceOverrideRepository : IRepositoryBase<ModifierItemPriceOverrideModel>
{
    Task<ModifierItemPriceOverrideModel> Post(ModifierItemPriceOverrideModel doc);
    Task<ModifierItemPriceOverrideModel> GetSingle(FilterDefinition<ModifierItemPriceOverrideModel> filter = null, SortDefinition<ModifierItemPriceOverrideModel> sort = null, int limit = 0);
    Task<List<ModifierItemPriceOverrideModel>> GetList(FilterDefinition<ModifierItemPriceOverrideModel> filter, SortDefinition<ModifierItemPriceOverrideModel> sort, int limit = 0);
    Task<bool> Put<ModifierItemPriceOverrideModel>(FilterDefinition<ModifierItemPriceOverrideModel> filter, UpdateDefinition<ModifierItemPriceOverrideModel> updateDefinition);
    Task<bool> Delete<ModifierItemPriceOverrideModel>(FilterDefinition<ModifierItemPriceOverrideModel> filter);
}
