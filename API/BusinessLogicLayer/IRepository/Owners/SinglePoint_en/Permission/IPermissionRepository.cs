﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Permission;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Permission;

public interface IPermissionRepository : IRepositoryBase<PermissionModel>
{
    Task<PermissionModel> Post(PermissionModel doc);
    Task<PermissionModel> GetSingle(FilterDefinition<PermissionModel> filter = null, SortDefinition<PermissionModel> sort = null, int limit = 0);
    Task<List<PermissionModel>> GetList(FilterDefinition<PermissionModel> filter, SortDefinition<PermissionModel> sort, int limit = 0);
    Task<bool> Put<PermissionModel>(FilterDefinition<PermissionModel> filter, UpdateDefinition<PermissionModel> updateDefinition);
    Task<bool> Delete<PermissionModel>(FilterDefinition<PermissionModel> filter);
}
