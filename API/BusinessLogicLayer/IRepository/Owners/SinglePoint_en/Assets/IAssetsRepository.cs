﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Assets;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Assets;

public interface IAssetsRepository : IRepositoryBase<AssetsModel>
{
    Task<AssetsModel> Post(AssetsModel doc);
    Task<AssetsModel> GetSingle(FilterDefinition<AssetsModel> filter = null, SortDefinition<AssetsModel> sort = null, int limit = 0);
    Task<List<AssetsModel>> GetList(FilterDefinition<AssetsModel> filter, SortDefinition<AssetsModel> sort, int limit = 0);
    Task<bool> Put<AssetsModel>(FilterDefinition<AssetsModel> filter, UpdateDefinition<AssetsModel> updateDefinition);
    Task<bool> Delete<AssetsModel>(FilterDefinition<AssetsModel> filter);
}
