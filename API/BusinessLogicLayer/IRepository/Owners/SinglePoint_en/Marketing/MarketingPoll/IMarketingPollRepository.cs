﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Marketing.MarketingPoll;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Marketing.MarketingPoll;

public interface IMarketingPollRepository : IRepositoryBase<MarketingPollModel>
{
    Task<MarketingPollModel> Post(MarketingPollModel doc);
    Task<MarketingPollModel> GetSingle(FilterDefinition<MarketingPollModel> filter = null, SortDefinition<MarketingPollModel> sort = null, int limit = 0);
    Task<List<MarketingPollModel>> GetList(FilterDefinition<MarketingPollModel> filter, SortDefinition<MarketingPollModel> sort, int limit = 0);
    Task<bool> Put<MarketingPollModel>(FilterDefinition<MarketingPollModel> filter, UpdateDefinition<MarketingPollModel> updateDefinition);
    Task<bool> Delete<MarketingPollModel>(FilterDefinition<MarketingPollModel> filter);
}
