﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Marketing.MarketingMessage;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Marketing.MarketingMessage;

public interface IMarketingMessageRepository : IRepositoryBase<MarketingMessageModel>
{
    Task<MarketingMessageModel> Post(MarketingMessageModel doc);
    Task<MarketingMessageModel> GetSingle(FilterDefinition<MarketingMessageModel> filter = null, SortDefinition<MarketingMessageModel> sort = null, int limit = 0);
    Task<List<MarketingMessageModel>> GetList(FilterDefinition<MarketingMessageModel> filter, SortDefinition<MarketingMessageModel> sort, int limit = 0);
    Task<bool> Put<MarketingMessageModel>(FilterDefinition<MarketingMessageModel> filter, UpdateDefinition<MarketingMessageModel> updateDefinition);
    Task<bool> Delete<MarketingMessageModel>(FilterDefinition<MarketingMessageModel> filter);
}
