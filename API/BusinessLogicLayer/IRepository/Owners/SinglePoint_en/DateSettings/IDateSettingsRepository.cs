﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.DateSettings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.DateSettings;

public interface IDateSettingsRepository : IRepositoryBase<DateSettingsModel>
{
    Task<DateSettingsModel> Post(DateSettingsModel doc);
    Task<DateSettingsModel> GetSingle(FilterDefinition<DateSettingsModel> filter = null, SortDefinition<DateSettingsModel> sort = null, int limit = 0);
    Task<List<DateSettingsModel>> GetList(FilterDefinition<DateSettingsModel> filter, SortDefinition<DateSettingsModel> sort, int limit = 0);
    Task<bool> Put<DateSettingsModel>(FilterDefinition<DateSettingsModel> filter, UpdateDefinition<DateSettingsModel> updateDefinition);
    Task<bool> Delete<DateSettingsModel>(FilterDefinition<DateSettingsModel> filter);
}
