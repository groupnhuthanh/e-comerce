﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Payoo.PayooPrepaid;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Payoo.PayooPrepaid;

public interface IPayooPrepaidRepository : IRepositoryBase<PayooPrepaidModel>
{
    Task<PayooPrepaidModel> Post(PayooPrepaidModel doc);
    Task<PayooPrepaidModel> GetSingle(FilterDefinition<PayooPrepaidModel> filter = null, SortDefinition<PayooPrepaidModel> sort = null, int limit = 0);
    Task<List<PayooPrepaidModel>> GetList(FilterDefinition<PayooPrepaidModel> filter, SortDefinition<PayooPrepaidModel> sort, int limit = 0);
    Task<bool> Put<PayooPrepaidModel>(FilterDefinition<PayooPrepaidModel> filter, UpdateDefinition<PayooPrepaidModel> updateDefinition);
    Task<bool> Delete<PayooPrepaidModel>(FilterDefinition<PayooPrepaidModel> filter);
}
