﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Payoo.PayooPrepaidDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Payoo.PayooPrepaidDetail;

public interface IPayooPrepaidDetailRepository : IRepositoryBase<PayooPrepaidDetailModel>
{
    Task<PayooPrepaidDetailModel> Post(PayooPrepaidDetailModel doc);
    Task<PayooPrepaidDetailModel> GetSingle(FilterDefinition<PayooPrepaidDetailModel> filter = null, SortDefinition<PayooPrepaidDetailModel> sort = null, int limit = 0);
    Task<List<PayooPrepaidDetailModel>> GetList(FilterDefinition<PayooPrepaidDetailModel> filter, SortDefinition<PayooPrepaidDetailModel> sort, int limit = 0);
    Task<bool> Put<PayooPrepaidDetailModel>(FilterDefinition<PayooPrepaidDetailModel> filter, UpdateDefinition<PayooPrepaidDetailModel> updateDefinition);
    Task<bool> Delete<PayooPrepaidDetailModel>(FilterDefinition<PayooPrepaidDetailModel> filter);
}
