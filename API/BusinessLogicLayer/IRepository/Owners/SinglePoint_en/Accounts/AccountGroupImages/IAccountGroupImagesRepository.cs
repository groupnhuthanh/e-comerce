﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Accounts.AccountGroupImages;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Accounts.AccountGroupImages;

public interface IAccountGroupImagesRepository : IRepositoryBase<AccountGroupImagesModel>
{
    Task<AccountGroupImagesModel> Post(AccountGroupImagesModel doc);
    Task<AccountGroupImagesModel> GetSingle(FilterDefinition<AccountGroupImagesModel> filter = null, SortDefinition<AccountGroupImagesModel> sort = null, int limit = 0);
    Task<List<AccountGroupImagesModel>> GetList(FilterDefinition<AccountGroupImagesModel> filter, SortDefinition<AccountGroupImagesModel> sort, int limit = 0);
    Task<bool> Put<AccountGroupImagesModel>(FilterDefinition<AccountGroupImagesModel> filter, UpdateDefinition<AccountGroupImagesModel> updateDefinition);
    Task<bool> Delete<AccountGroupImagesModel>(FilterDefinition<AccountGroupImagesModel> filter);
}
