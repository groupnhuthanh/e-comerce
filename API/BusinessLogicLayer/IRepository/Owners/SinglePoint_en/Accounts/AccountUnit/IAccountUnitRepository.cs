﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Accounts.AccountUnit;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Accounts.AccountUnit;

public interface IAccountUnitRepository : IRepositoryBase<AccountUnitModel>
{

}
