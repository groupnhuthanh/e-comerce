﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Accounts.AccountGroup;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Accounts.AccountGroup;

public interface IAccountGroupRepository : IRepositoryBase<AccountGroupModel>
{
    Task<AccountGroupModel> Post(AccountGroupModel doc);
    Task<AccountGroupModel> GetSingle(FilterDefinition<AccountGroupModel> filter = null, SortDefinition<AccountGroupModel> sort = null, int limit = 0);
    Task<List<AccountGroupModel>> GetList(FilterDefinition<AccountGroupModel> filter, SortDefinition<AccountGroupModel> sort, int limit = 0);
    Task<bool> Put<AccountGroupModel>(FilterDefinition<AccountGroupModel> filter, UpdateDefinition<AccountGroupModel> updateDefinition);
    Task<bool> Delete<AccountGroupModel>(FilterDefinition<AccountGroupModel> filter);
}
