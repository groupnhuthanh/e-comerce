﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Campaigns.CampaignImage;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Campaigns.CampaignImage;

public interface ICampaignImageRepository : IRepositoryBase<CampaignImageModel>
{
    Task<CampaignImageModel> Post(CampaignImageModel doc);
    Task<CampaignImageModel> GetSingle(FilterDefinition<CampaignImageModel> filter = null, SortDefinition<CampaignImageModel> sort = null, int limit = 0);
    Task<List<CampaignImageModel>> GetList(FilterDefinition<CampaignImageModel> filter, SortDefinition<CampaignImageModel> sort, int limit = 0);
    Task<bool> Put<CampaignImageModel>(FilterDefinition<CampaignImageModel> filter, UpdateDefinition<CampaignImageModel> updateDefinition);
    Task<bool> Delete<CampaignImageModel>(FilterDefinition<CampaignImageModel> filter);
}
