﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Campaigns.Campaign;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Campaigns.Campaign;

public interface ICampaignRepository : IRepositoryBase<CampaignModel>
{
    Task<CampaignModel> Post(CampaignModel doc);
    Task<CampaignModel> GetSingle(FilterDefinition<CampaignModel> filter = null, SortDefinition<CampaignModel> sort = null, int limit = 0);
    Task<List<CampaignModel>> GetList(FilterDefinition<CampaignModel> filter, SortDefinition<CampaignModel> sort, int limit = 0);
    Task<bool> Put<CampaignModel>(FilterDefinition<CampaignModel> filter, UpdateDefinition<CampaignModel> updateDefinition);
    Task<bool> Delete<CampaignModel>(FilterDefinition<CampaignModel> filter);

}

