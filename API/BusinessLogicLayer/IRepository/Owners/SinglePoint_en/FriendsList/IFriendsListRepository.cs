﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.FriendsList;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FriendsList;

public interface IFriendsListRepository : IRepositoryBase<FriendsListModel>
{
    Task<FriendsListModel> Post(FriendsListModel doc);
    Task<FriendsListModel> GetSingle(FilterDefinition<FriendsListModel> filter = null, SortDefinition<FriendsListModel> sort = null, int limit = 0);
    Task<List<FriendsListModel>> GetList(FilterDefinition<FriendsListModel> filter, SortDefinition<FriendsListModel> sort, int limit = 0);
    Task<bool> Put<FriendsListModel>(FilterDefinition<FriendsListModel> filter, UpdateDefinition<FriendsListModel> updateDefinition);
    Task<bool> Delete<FriendsListModel>(FilterDefinition<FriendsListModel> filter);
}
