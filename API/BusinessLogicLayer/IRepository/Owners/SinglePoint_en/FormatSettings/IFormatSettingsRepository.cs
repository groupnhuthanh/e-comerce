﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.FormatSettings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FormatSettings;

public interface IFormatSettingsRepository : IRepositoryBase<FormatSettingsModel>
{
    Task<FormatSettingsModel> Post(FormatSettingsModel doc);
    Task<FormatSettingsModel> GetSingle(FilterDefinition<FormatSettingsModel> filter = null, SortDefinition<FormatSettingsModel> sort = null, int limit = 0);
    Task<List<FormatSettingsModel>> GetList(FilterDefinition<FormatSettingsModel> filter, SortDefinition<FormatSettingsModel> sort, int limit = 0);
    Task<bool> Put<FormatSettingsModel>(FilterDefinition<FormatSettingsModel> filter, UpdateDefinition<FormatSettingsModel> updateDefinition);
    Task<bool> Delete<FormatSettingsModel>(FilterDefinition<FormatSettingsModel> filter);
}
