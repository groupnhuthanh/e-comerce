﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Firebases.FirebaseCustomerDiscounts;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Firebases.FirebaseCustomerDiscounts;

public interface IFirebaseCustomerDiscountsRepository : IRepositoryBase<FirebaseCustomerDiscountsModel>
{
    Task<FirebaseCustomerDiscountsModel> Post(FirebaseCustomerDiscountsModel doc);
    Task<FirebaseCustomerDiscountsModel> GetSingle(FilterDefinition<FirebaseCustomerDiscountsModel> filter = null, SortDefinition<FirebaseCustomerDiscountsModel> sort = null, int limit = 0);
    Task<List<FirebaseCustomerDiscountsModel>> GetList(FilterDefinition<FirebaseCustomerDiscountsModel> filter, SortDefinition<FirebaseCustomerDiscountsModel> sort, int limit = 0);
    Task<bool> Put<FirebaseCustomerDiscountsModel>(FilterDefinition<FirebaseCustomerDiscountsModel> filter, UpdateDefinition<FirebaseCustomerDiscountsModel> updateDefinition);
    Task<bool> Delete<FirebaseCustomerDiscountsModel>(FilterDefinition<FirebaseCustomerDiscountsModel> filter);
}
