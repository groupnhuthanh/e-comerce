﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Firebases.FirebaseMenus;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Firebases.FirebaseMenus;

public interface IFirebaseMenusRepository : IRepositoryBase<FirebaseMenusModel>
{
    Task<FirebaseMenusModel> Post(FirebaseMenusModel doc);
    Task<FirebaseMenusModel> GetSingle(FilterDefinition<FirebaseMenusModel> filter = null, SortDefinition<FirebaseMenusModel> sort = null, int limit = 0);
    Task<List<FirebaseMenusModel>> GetList(FilterDefinition<FirebaseMenusModel> filter, SortDefinition<FirebaseMenusModel> sort, int limit = 0);
    Task<bool> Put<FirebaseMenusModel>(FilterDefinition<FirebaseMenusModel> filter, UpdateDefinition<FirebaseMenusModel> updateDefinition);
    Task<bool> Delete<FirebaseMenusModel>(FilterDefinition<FirebaseMenusModel> filter);
}
