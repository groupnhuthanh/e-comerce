﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Firebases.FirebaseSettings;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Firebases.FirebaseSettings;

public interface IFirebaseSettingsRepository : IRepositoryBase<FirebaseSettingsModel>
{
    Task<FirebaseSettingsModel> Post(FirebaseSettingsModel doc);
    Task<FirebaseSettingsModel> GetSingle(FilterDefinition<FirebaseSettingsModel> filter = null, SortDefinition<FirebaseSettingsModel> sort = null, int limit = 0);
    Task<List<FirebaseSettingsModel>> GetList(FilterDefinition<FirebaseSettingsModel> filter, SortDefinition<FirebaseSettingsModel> sort, int limit = 0);
    Task<bool> Put<FirebaseSettingsModel>(FilterDefinition<FirebaseSettingsModel> filter, UpdateDefinition<FirebaseSettingsModel> updateDefinition);
    Task<bool> Delete<FirebaseSettingsModel>(FilterDefinition<FirebaseSettingsModel> filter);
}
