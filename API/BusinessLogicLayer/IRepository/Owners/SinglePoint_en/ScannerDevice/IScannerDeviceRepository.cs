﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.ScannerDevice;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.ScannerDevice;

public interface IScannerDeviceRepository : IRepositoryBase<ScannerDeviceModel>
{
    Task<ScannerDeviceModel> Post(ScannerDeviceModel doc);
    Task<ScannerDeviceModel> GetSingle(FilterDefinition<ScannerDeviceModel> filter = null, SortDefinition<ScannerDeviceModel> sort = null, int limit = 0);
    Task<List<ScannerDeviceModel>> GetList(FilterDefinition<ScannerDeviceModel> filter, SortDefinition<ScannerDeviceModel> sort, int limit = 0);
    Task<bool> Put<ScannerDeviceModel>(FilterDefinition<ScannerDeviceModel> filter, UpdateDefinition<ScannerDeviceModel> updateDefinition);
    Task<bool> Delete<ScannerDeviceModel>(FilterDefinition<ScannerDeviceModel> filter);
}
