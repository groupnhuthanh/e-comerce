﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Brands;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Brands;

public interface IBrandsRepository : IRepositoryBase<BrandsModel>
{
    Task<BrandsModel> Post(BrandsModel doc);
    Task<BrandsModel> GetSingle(FilterDefinition<BrandsModel> filter = null, SortDefinition<BrandsModel> sort = null, int limit = 0);
    Task<List<BrandsModel>> GetList(FilterDefinition<BrandsModel> filter, SortDefinition<BrandsModel> sort, int limit = 0);
    Task<bool> Put<BrandsModel>(FilterDefinition<BrandsModel> filter, UpdateDefinition<BrandsModel> updateDefinition);
    Task<bool> Delete<BrandsModel>(FilterDefinition<BrandsModel> filter);
}
