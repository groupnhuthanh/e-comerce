﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_ApplyToItem;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_ApplyToItem;

public interface IDiscounts_ApplyToItemRepository : IRepositoryBase<Discounts_ApplyToItemModel>
{
    Task<Discounts_ApplyToItemModel> Post(Discounts_ApplyToItemModel doc);
    Task<Discounts_ApplyToItemModel> GetSingle(FilterDefinition<Discounts_ApplyToItemModel> filter = null, SortDefinition<Discounts_ApplyToItemModel> sort = null, int limit = 0);
    Task<List<Discounts_ApplyToItemModel>> GetList(FilterDefinition<Discounts_ApplyToItemModel> filter, SortDefinition<Discounts_ApplyToItemModel> sort, int limit = 0);
    Task<bool> Put<Discounts_ApplyToItemModel>(FilterDefinition<Discounts_ApplyToItemModel> filter, UpdateDefinition<Discounts_ApplyToItemModel> updateDefinition);
    Task<bool> Delete<Discounts_ApplyToItemModel>(FilterDefinition<Discounts_ApplyToItemModel> filter);
}
