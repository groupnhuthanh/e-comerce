﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_Percentage;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_Percentage;

public interface IDiscounts_PercentageRepository : IRepositoryBase<Discounts_PercentageModel>
{
    Task<Discounts_PercentageModel> Post(Discounts_PercentageModel doc);
    Task<Discounts_PercentageModel> GetSingle(FilterDefinition<Discounts_PercentageModel> filter = null, SortDefinition<Discounts_PercentageModel> sort = null, int limit = 0);
    Task<List<Discounts_PercentageModel>> GetList(FilterDefinition<Discounts_PercentageModel> filter, SortDefinition<Discounts_PercentageModel> sort, int limit = 0);
    Task<bool> Put<Discounts_PercentageModel>(FilterDefinition<Discounts_PercentageModel> filter, UpdateDefinition<Discounts_PercentageModel> updateDefinition);
    Task<bool> Delete<Discounts_PercentageModel>(FilterDefinition<Discounts_PercentageModel> filter);
}
