﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_SpecificPrice;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_SpecificPrice;

public interface IDiscounts_SpecificPriceRepository : IRepositoryBase<Discounts_SpecificPriceModel>
{
    Task<Discounts_SpecificPriceModel> Post(Discounts_SpecificPriceModel doc);
    Task<Discounts_SpecificPriceModel> GetSingle(FilterDefinition<Discounts_SpecificPriceModel> filter = null, SortDefinition<Discounts_SpecificPriceModel> sort = null, int limit = 0);
    Task<List<Discounts_SpecificPriceModel>> GetList(FilterDefinition<Discounts_SpecificPriceModel> filter, SortDefinition<Discounts_SpecificPriceModel> sort, int limit = 0);
    Task<bool> Put<Discounts_SpecificPriceModel>(FilterDefinition<Discounts_SpecificPriceModel> filter, UpdateDefinition<Discounts_SpecificPriceModel> updateDefinition);
    Task<bool> Delete<Discounts_SpecificPriceModel>(FilterDefinition<Discounts_SpecificPriceModel> filter);
}
