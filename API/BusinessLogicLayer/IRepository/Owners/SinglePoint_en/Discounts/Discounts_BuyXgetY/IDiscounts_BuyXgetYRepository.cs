﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_BuyXgetY;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_BuyXgetY;

public interface IDiscounts_BuyXgetYRepository : IRepositoryBase<Discounts_BuyXgetYModel>
{
    Task<Discounts_BuyXgetYModel> Post(Discounts_BuyXgetYModel doc);
    Task<Discounts_BuyXgetYModel> GetSingle(FilterDefinition<Discounts_BuyXgetYModel> filter = null, SortDefinition<Discounts_BuyXgetYModel> sort = null, int limit = 0);
    Task<List<Discounts_BuyXgetYModel>> GetList(FilterDefinition<Discounts_BuyXgetYModel> filter, SortDefinition<Discounts_BuyXgetYModel> sort, int limit = 0);
    Task<bool> Put<Discounts_BuyXgetYModel>(FilterDefinition<Discounts_BuyXgetYModel> filter, UpdateDefinition<Discounts_BuyXgetYModel> updateDefinition);
    Task<bool> Delete<Discounts_BuyXgetYModel>(FilterDefinition<Discounts_BuyXgetYModel> filter);
}
