﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Discounts.DiscountCode;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.DiscountCode;

public interface IDiscountCodeRepository : IRepositoryBase<DiscountCodeModel>
{
    Task<DiscountCodeModel> Post(DiscountCodeModel doc);
    Task<DiscountCodeModel> GetSingle(FilterDefinition<DiscountCodeModel> filter = null, SortDefinition<DiscountCodeModel> sort = null, int limit = 0);
    Task<List<DiscountCodeModel>> GetList(FilterDefinition<DiscountCodeModel> filter, SortDefinition<DiscountCodeModel> sort, int limit = 0);
    Task<bool> Put<DiscountCodeModel>(FilterDefinition<DiscountCodeModel> filter, UpdateDefinition<DiscountCodeModel> updateDefinition);
    Task<bool> Delete<DiscountCodeModel>(FilterDefinition<DiscountCodeModel> filter);
}
