﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_Combo;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_Combo;

public interface IDiscounts_ComboRepository : IRepositoryBase<Discounts_ComboModel>
{
    Task<Discounts_ComboModel> Post(Discounts_ComboModel doc);
    Task<Discounts_ComboModel> GetSingle(FilterDefinition<Discounts_ComboModel> filter = null, SortDefinition<Discounts_ComboModel> sort = null, int limit = 0);
    Task<List<Discounts_ComboModel>> GetList(FilterDefinition<Discounts_ComboModel> filter, SortDefinition<Discounts_ComboModel> sort, int limit = 0);
    Task<bool> Put<Discounts_ComboModel>(FilterDefinition<Discounts_ComboModel> filter, UpdateDefinition<Discounts_ComboModel> updateDefinition);
    Task<bool> Delete<Discounts_ComboModel>(FilterDefinition<Discounts_ComboModel> filter);
}
