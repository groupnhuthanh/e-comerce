﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_FixedAmount;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_FixedAmount;

public interface IDiscounts_FixedAmountRepository : IRepositoryBase<Discounts_FixedAmountModel>
{
    Task<Discounts_FixedAmountModel> Post(Discounts_FixedAmountModel doc);
    Task<Discounts_FixedAmountModel> GetSingle(FilterDefinition<Discounts_FixedAmountModel> filter = null, SortDefinition<Discounts_FixedAmountModel> sort = null, int limit = 0);
    Task<List<Discounts_FixedAmountModel>> GetList(FilterDefinition<Discounts_FixedAmountModel> filter, SortDefinition<Discounts_FixedAmountModel> sort, int limit = 0);
    Task<bool> Put<Discounts_FixedAmountModel>(FilterDefinition<Discounts_FixedAmountModel> filter, UpdateDefinition<Discounts_FixedAmountModel> updateDefinition);
    Task<bool> Delete<Discounts_FixedAmountModel>(FilterDefinition<Discounts_FixedAmountModel> filter);
}
