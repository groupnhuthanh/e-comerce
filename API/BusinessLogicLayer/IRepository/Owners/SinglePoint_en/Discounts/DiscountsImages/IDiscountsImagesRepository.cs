﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Discounts.DiscountsImages;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.DiscountsImages;

public interface IDiscountsImagesRepository : IRepositoryBase<DiscountsImagesModel>
{
    Task<DiscountsImagesModel> Post(DiscountsImagesModel doc);
    Task<DiscountsImagesModel> GetSingle(FilterDefinition<DiscountsImagesModel> filter = null, SortDefinition<DiscountsImagesModel> sort = null, int limit = 0);
    Task<List<DiscountsImagesModel>> GetList(FilterDefinition<DiscountsImagesModel> filter, SortDefinition<DiscountsImagesModel> sort, int limit = 0);
    Task<bool> Put<DiscountsImagesModel>(FilterDefinition<DiscountsImagesModel> filter, UpdateDefinition<DiscountsImagesModel> updateDefinition);
    Task<bool> Delete<DiscountsImagesModel>(FilterDefinition<DiscountsImagesModel> filter);
}
