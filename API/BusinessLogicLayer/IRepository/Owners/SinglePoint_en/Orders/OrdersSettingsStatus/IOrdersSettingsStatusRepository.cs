﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Orders.OrdersSettingsStatus;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Orders.OrdersSettingsStatus;

public interface IOrdersSettingsStatusRepository : IRepositoryBase<OrdersSettingsStatusModel>
{
    Task<OrdersSettingsStatusModel> Post(OrdersSettingsStatusModel doc);
    Task<OrdersSettingsStatusModel> GetSingle(FilterDefinition<OrdersSettingsStatusModel> filter = null, SortDefinition<OrdersSettingsStatusModel> sort = null, int limit = 0);
    Task<List<OrdersSettingsStatusModel>> GetList(FilterDefinition<OrdersSettingsStatusModel> filter, SortDefinition<OrdersSettingsStatusModel> sort, int limit = 0);
    Task<bool> Put<OrdersSettingsStatusModel>(FilterDefinition<OrdersSettingsStatusModel> filter, UpdateDefinition<OrdersSettingsStatusModel> updateDefinition);
    Task<bool> Delete<OrdersSettingsStatusModel>(FilterDefinition<OrdersSettingsStatusModel> filter);
}
