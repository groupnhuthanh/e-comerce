﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Orders.OrderStatusDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Orders.OrderStatusDetail;

public interface IOrderStatusDetailRepository : IRepositoryBase<OrderStatusDetailModel>
{
    Task<OrderStatusDetailModel> Post(OrderStatusDetailModel doc);
    Task<OrderStatusDetailModel> GetSingle(FilterDefinition<OrderStatusDetailModel> filter = null, SortDefinition<OrderStatusDetailModel> sort = null, int limit = 0);
    Task<List<OrderStatusDetailModel>> GetList(FilterDefinition<OrderStatusDetailModel> filter, SortDefinition<OrderStatusDetailModel> sort, int limit = 0);
    Task<bool> Put<OrderStatusDetailModel>(FilterDefinition<OrderStatusDetailModel> filter, UpdateDefinition<OrderStatusDetailModel> updateDefinition);
    Task<bool> Delete<OrderStatusDetailModel>(FilterDefinition<OrderStatusDetailModel> filter);
}
