﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Events.EventsOverview;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Events.EventsOverview;

public interface IEventsOverviewRepository : IRepositoryBase<EventsOverviewModel>
{
    Task<EventsOverviewModel> Post(EventsOverviewModel doc);
    Task<EventsOverviewModel> GetSingle(FilterDefinition<EventsOverviewModel> filter = null, SortDefinition<EventsOverviewModel> sort = null, int limit = 0);
    Task<List<EventsOverviewModel>> GetList(FilterDefinition<EventsOverviewModel> filter, SortDefinition<EventsOverviewModel> sort, int limit = 0);
    Task<bool> Put<EventsOverviewModel>(FilterDefinition<EventsOverviewModel> filter, UpdateDefinition<EventsOverviewModel> updateDefinition);
    Task<bool> Delete<EventsOverviewModel>(FilterDefinition<EventsOverviewModel> filter);
}
