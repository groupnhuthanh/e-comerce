﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Events.EventsInteraction;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Events.EventsInteraction;

public interface IEventsInteractionRepository : IRepositoryBase<EventsInteractionModel>
{
    Task<EventsInteractionModel> Post(EventsInteractionModel doc);
    Task<EventsInteractionModel> GetSingle(FilterDefinition<EventsInteractionModel> filter = null, SortDefinition<EventsInteractionModel> sort = null, int limit = 0);
    Task<List<EventsInteractionModel>> GetList(FilterDefinition<EventsInteractionModel> filter, SortDefinition<EventsInteractionModel> sort, int limit = 0);
    Task<bool> Put<EventsInteractionModel>(FilterDefinition<EventsInteractionModel> filter, UpdateDefinition<EventsInteractionModel> updateDefinition);
    Task<bool> Delete<EventsInteractionModel>(FilterDefinition<EventsInteractionModel> filter);
}
