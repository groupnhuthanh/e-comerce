﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Bundle_ApplyToItem;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Bundle_ApplyToItem;

public interface IBundle_ApplyToIteRepository : IRepositoryBase<Bundle_ApplyToItemModel>
{
    Task<Bundle_ApplyToItemModel> Post(Bundle_ApplyToItemModel doc);
    Task<Bundle_ApplyToItemModel> GetSingle(FilterDefinition<Bundle_ApplyToItemModel> filter = null, SortDefinition<Bundle_ApplyToItemModel> sort = null, int limit = 0);
    Task<List<Bundle_ApplyToItemModel>> GetList(FilterDefinition<Bundle_ApplyToItemModel> filter, SortDefinition<Bundle_ApplyToItemModel> sort, int limit = 0);
    Task<bool> Put<Bundle_ApplyToIteModel>(FilterDefinition<Bundle_ApplyToIteModel> filter, UpdateDefinition<Bundle_ApplyToIteModel> updateDefinition);
    Task<bool> Delete<Bundle_ApplyToIteModel>(FilterDefinition<Bundle_ApplyToIteModel> filter);
}
