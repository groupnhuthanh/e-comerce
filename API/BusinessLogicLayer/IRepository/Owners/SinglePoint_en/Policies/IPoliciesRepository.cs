﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Policies;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Policies;

public interface IPoliciesRepository : IRepositoryBase<PoliciesModel>
{
    Task<PoliciesModel> Post(PoliciesModel doc);
    Task<PoliciesModel> GetSingle(FilterDefinition<PoliciesModel> filter = null, SortDefinition<PoliciesModel> sort = null, int limit = 0);
    Task<List<PoliciesModel>> GetList(FilterDefinition<PoliciesModel> filter, SortDefinition<PoliciesModel> sort, int limit = 0);
    Task<bool> Put<PoliciesModel>(FilterDefinition<PoliciesModel> filter, UpdateDefinition<PoliciesModel> updateDefinition);
    Task<bool> Delete<PoliciesModel>(FilterDefinition<PoliciesModel> filter);
}
