﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Environment;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Environment;

public interface IEnvironmentRepository : IRepositoryBase<EnvironmentModel>
{
    Task<EnvironmentModel> Post(EnvironmentModel doc);
    Task<EnvironmentModel> GetSingle(FilterDefinition<EnvironmentModel> filter = null, SortDefinition<EnvironmentModel> sort = null, int limit = 0);
    Task<List<EnvironmentModel>> GetList(FilterDefinition<EnvironmentModel> filter, SortDefinition<EnvironmentModel> sort, int limit = 0);
    Task<bool> Put<EnvironmentModel>(FilterDefinition<EnvironmentModel> filter, UpdateDefinition<EnvironmentModel> updateDefinition);
    Task<bool> Delete<EnvironmentModel>(FilterDefinition<EnvironmentModel> filter);
}
