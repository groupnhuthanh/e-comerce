﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.OtpSms;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.OtpSms;

public interface IOtpSmsRepository : IRepositoryBase<OtpSmsModel>
{
    Task<OtpSmsModel> Post(OtpSmsModel doc);
    Task<OtpSmsModel> GetSingle(FilterDefinition<OtpSmsModel> filter = null, SortDefinition<OtpSmsModel> sort = null, int limit = 0);
    Task<List<OtpSmsModel>> GetList(FilterDefinition<OtpSmsModel> filter, SortDefinition<OtpSmsModel> sort, int limit = 0);
    Task<bool> Put<OtpSmsModel>(FilterDefinition<OtpSmsModel> filter, UpdateDefinition<OtpSmsModel> updateDefinition);
    Task<bool> Delete<OtpSmsModel>(FilterDefinition<OtpSmsModel> filter);
}
