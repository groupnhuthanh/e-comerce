﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.VoucherPayment;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.VoucherPayment;

public interface IVoucherPaymentRepository : IRepositoryBase<VoucherPaymentModel>
{
    Task<VoucherPaymentModel> Post(VoucherPaymentModel doc);
    Task<VoucherPaymentModel> GetSingle(FilterDefinition<VoucherPaymentModel> filter = null, SortDefinition<VoucherPaymentModel> sort = null, int limit = 0);
    Task<List<VoucherPaymentModel>> GetList(FilterDefinition<VoucherPaymentModel> filter, SortDefinition<VoucherPaymentModel> sort, int limit = 0);
    Task<bool> Put<VoucherPaymentModel>(FilterDefinition<VoucherPaymentModel> filter, UpdateDefinition<VoucherPaymentModel> updateDefinition);
    Task<bool> Delete<VoucherPaymentModel>(FilterDefinition<VoucherPaymentModel> filter);
}
