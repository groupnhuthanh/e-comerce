﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.EventOverviewImage;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.EventOverviewImage;

public interface IEventOverviewImageRepository : IRepositoryBase<EventOverviewImageModel>
{
    Task<EventOverviewImageModel> Post(EventOverviewImageModel doc);
    Task<EventOverviewImageModel> GetSingle(FilterDefinition<EventOverviewImageModel> filter = null, SortDefinition<EventOverviewImageModel> sort = null, int limit = 0);
    Task<List<EventOverviewImageModel>> GetList(FilterDefinition<EventOverviewImageModel> filter, SortDefinition<EventOverviewImageModel> sort, int limit = 0);
    Task<bool> Put<EventOverviewImageModel>(FilterDefinition<EventOverviewImageModel> filter, UpdateDefinition<EventOverviewImageModel> updateDefinition);
    Task<bool> Delete<EventOverviewImageModel>(FilterDefinition<EventOverviewImageModel> filter);
}
