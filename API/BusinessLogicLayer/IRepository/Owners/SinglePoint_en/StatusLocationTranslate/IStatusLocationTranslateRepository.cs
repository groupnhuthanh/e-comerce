﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.StatusLocationTranslate;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.StatusLocationTranslate;

public interface IStatusLocationTranslateRepository : IRepositoryBase<StatusLocationTranslateModel>
{
    Task<StatusLocationTranslateModel> Post(StatusLocationTranslateModel doc);
    Task<StatusLocationTranslateModel> GetSingle(FilterDefinition<StatusLocationTranslateModel> filter = null, SortDefinition<StatusLocationTranslateModel> sort = null, int limit = 0);
    Task<List<StatusLocationTranslateModel>> GetList(FilterDefinition<StatusLocationTranslateModel> filter, SortDefinition<StatusLocationTranslateModel> sort, int limit = 0);
    Task<bool> Put<StatusLocationTranslateModel>(FilterDefinition<StatusLocationTranslateModel> filter, UpdateDefinition<StatusLocationTranslateModel> updateDefinition);
    Task<bool> Delete<StatusLocationTranslateModel>(FilterDefinition<StatusLocationTranslateModel> filter);
}
