﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Settings.SettingsHardware;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Settings.SettingsHardware;

public interface ISettingsHardwareRepository : IRepositoryBase<SettingsHardwareModel>
{
    Task<SettingsHardwareModel> Post(SettingsHardwareModel doc);
    Task<SettingsHardwareModel> GetSingle(FilterDefinition<SettingsHardwareModel> filter = null, SortDefinition<SettingsHardwareModel> sort = null, int limit = 0);
    Task<List<SettingsHardwareModel>> GetList(FilterDefinition<SettingsHardwareModel> filter, SortDefinition<SettingsHardwareModel> sort, int limit = 0);
    Task<bool> Put<SettingsHardwareModel>(FilterDefinition<SettingsHardwareModel> filter, UpdateDefinition<SettingsHardwareModel> updateDefinition);
    Task<bool> Delete<SettingsHardwareModel>(FilterDefinition<SettingsHardwareModel> filter);
}
