﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Settings.SettingsDeliveryRole;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Settings.SettingsDeliveryRole;

public interface ISettingsDeliveryRoleRepository : IRepositoryBase<SettingsDeliveryRoleModel>
{
    Task<SettingsDeliveryRoleModel> Post(SettingsDeliveryRoleModel doc);
    Task<SettingsDeliveryRoleModel> GetSingle(FilterDefinition<SettingsDeliveryRoleModel> filter = null, SortDefinition<SettingsDeliveryRoleModel> sort = null, int limit = 0);
    Task<List<SettingsDeliveryRoleModel>> GetList(FilterDefinition<SettingsDeliveryRoleModel> filter, SortDefinition<SettingsDeliveryRoleModel> sort, int limit = 0);
    Task<bool> Put<SettingsDeliveryRoleModel>(FilterDefinition<SettingsDeliveryRoleModel> filter, UpdateDefinition<SettingsDeliveryRoleModel> updateDefinition);
    Task<bool> Delete<SettingsDeliveryRoleModel>(FilterDefinition<SettingsDeliveryRoleModel> filter);
}
