﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Loyaltys.LoyaltyReward;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Loyaltys.LoyaltyReward;

public interface ILoyaltyRewardRepository : IRepositoryBase<LoyaltyRewardModel>
{
    Task<LoyaltyRewardModel> Post(LoyaltyRewardModel doc);
    Task<LoyaltyRewardModel> GetSingle(FilterDefinition<LoyaltyRewardModel> filter = null, SortDefinition<LoyaltyRewardModel> sort = null, int limit = 0);
    Task<List<LoyaltyRewardModel>> GetList(FilterDefinition<LoyaltyRewardModel> filter, SortDefinition<LoyaltyRewardModel> sort, int limit = 0);
    Task<bool> Put<LoyaltyRewardModel>(FilterDefinition<LoyaltyRewardModel> filter, UpdateDefinition<LoyaltyRewardModel> updateDefinition);
    Task<bool> Delete<LoyaltyRewardModel>(FilterDefinition<LoyaltyRewardModel> filter);
}
