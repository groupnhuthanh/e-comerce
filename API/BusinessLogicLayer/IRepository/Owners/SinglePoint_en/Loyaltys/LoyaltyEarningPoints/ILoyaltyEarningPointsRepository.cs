﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Loyaltys.LoyaltyEarningPoints;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Loyaltys.LoyaltyEarningPoints;

public interface ILoyaltyEarningPointsRepository : IRepositoryBase<LoyaltyEarningPointsModel>
{
    Task<LoyaltyEarningPointsModel> Post(LoyaltyEarningPointsModel doc);
    Task<LoyaltyEarningPointsModel> GetSingle(FilterDefinition<LoyaltyEarningPointsModel> filter = null, SortDefinition<LoyaltyEarningPointsModel> sort = null, int limit = 0);
    Task<List<LoyaltyEarningPointsModel>> GetList(FilterDefinition<LoyaltyEarningPointsModel> filter, SortDefinition<LoyaltyEarningPointsModel> sort, int limit = 0);
    Task<bool> Put<LoyaltyEarningPointsModel>(FilterDefinition<LoyaltyEarningPointsModel> filter, UpdateDefinition<LoyaltyEarningPointsModel> updateDefinition);
    Task<bool> Delete<LoyaltyEarningPointsModel>(FilterDefinition<LoyaltyEarningPointsModel> filter);
}
