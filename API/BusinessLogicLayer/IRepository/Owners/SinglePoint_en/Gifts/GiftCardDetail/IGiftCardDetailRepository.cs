﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Gifts.GiftCardDetail;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Gifts.GiftCardDetail;

public interface IGiftCardDetailRepository : IRepositoryBase<GiftCardDetailModel>
{
    Task<GiftCardDetailModel> Post(GiftCardDetailModel doc);
    Task<GiftCardDetailModel> GetSingle(FilterDefinition<GiftCardDetailModel> filter = null, SortDefinition<GiftCardDetailModel> sort = null, int limit = 0);
    Task<List<GiftCardDetailModel>> GetList(FilterDefinition<GiftCardDetailModel> filter, SortDefinition<GiftCardDetailModel> sort, int limit = 0);
    Task<bool> Put<GiftCardDetailModel>(FilterDefinition<GiftCardDetailModel> filter, UpdateDefinition<GiftCardDetailModel> updateDefinition);
    Task<bool> Delete<GiftCardDetailModel>(FilterDefinition<GiftCardDetailModel> filter);
}
