﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Gifts.GiftCard;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Gifts.GiftCard;

public interface IGiftCardRepository : IRepositoryBase<GiftCardModel>
{
    Task<GiftCardModel> Post(GiftCardModel doc);
    Task<GiftCardModel> GetSingle(FilterDefinition<GiftCardModel> filter = null, SortDefinition<GiftCardModel> sort = null, int limit = 0);
    Task<List<GiftCardModel>> GetList(FilterDefinition<GiftCardModel> filter, SortDefinition<GiftCardModel> sort, int limit = 0);
    Task<bool> Put<GiftCardModel>(FilterDefinition<GiftCardModel> filter, UpdateDefinition<GiftCardModel> updateDefinition);
    Task<bool> Delete<GiftCardModel>(FilterDefinition<GiftCardModel> filter);
}
