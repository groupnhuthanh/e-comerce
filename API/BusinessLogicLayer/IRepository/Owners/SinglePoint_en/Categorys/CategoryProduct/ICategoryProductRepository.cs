﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Categorys.CategoryProduct;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Categorys.CategoryProduct;

public interface ICategoryProductRepository : IRepositoryBase<CategoryProductModel>
{
    Task<CategoryProductModel> Post(CategoryProductModel doc);
    Task<CategoryProductModel> GetSingle(FilterDefinition<CategoryProductModel> filter = null, SortDefinition<CategoryProductModel> sort = null, int limit = 0);
    Task<List<CategoryProductModel>> GetList(FilterDefinition<CategoryProductModel> filter, SortDefinition<CategoryProductModel> sort, int limit = 0);
    Task<bool> Put<CategoryProductModel>(FilterDefinition<CategoryProductModel> filter, UpdateDefinition<CategoryProductModel> updateDefinition);
    Task<bool> Delete<CategoryProductModel>(FilterDefinition<CategoryProductModel> filter);
}
