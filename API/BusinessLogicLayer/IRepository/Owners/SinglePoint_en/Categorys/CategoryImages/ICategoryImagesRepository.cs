﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Categorys.CategoryImages;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Categorys.CategoryImages;

public interface ICategoryImagesRepository : IRepositoryBase<CategoryImagesModel>
{
    Task<CategoryImagesModel> Post(CategoryImagesModel doc);
    Task<CategoryImagesModel> GetSingle(FilterDefinition<CategoryImagesModel> filter = null, SortDefinition<CategoryImagesModel> sort = null, int limit = 0);
    Task<List<CategoryImagesModel>> GetList(FilterDefinition<CategoryImagesModel> filter, SortDefinition<CategoryImagesModel> sort, int limit = 0);
    Task<bool> Put<CategoryImagesModel>(FilterDefinition<CategoryImagesModel> filter, UpdateDefinition<CategoryImagesModel> updateDefinition);
    Task<bool> Delete<CategoryImagesModel>(FilterDefinition<CategoryImagesModel> filter);
}
