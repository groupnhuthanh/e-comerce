﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Categorys.Category;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Categorys.Category;

public interface ICategoryRepository : IRepositoryBase<CategoryModel>
{
    Task<CategoryModel> Post(CategoryModel doc);
    Task<CategoryModel> GetSingle(FilterDefinition<CategoryModel> filter = null, SortDefinition<CategoryModel> sort = null, int limit = 0);
    Task<List<CategoryModel>> GetList(FilterDefinition<CategoryModel> filter, SortDefinition<CategoryModel> sort, int limit = 0);
    Task<bool> Put<CategoryModel>(FilterDefinition<CategoryModel> filter, UpdateDefinition<CategoryModel> updateDefinition);
    Task<bool> Delete<CategoryModel>(FilterDefinition<CategoryModel> filter);

}
