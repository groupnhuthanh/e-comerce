﻿using BusinessLogicLayer.IRepository.Base;
using Entities.Owners.SinglePoint_en.Domains;
using MongoDB.Driver;

namespace BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Domains;

public interface IDomainsRepository : IRepositoryBase<DomainsModel>
{
    Task<DomainsModel> Post(DomainsModel doc);
    Task<DomainsModel> GetSingle(FilterDefinition<DomainsModel> filter = null, SortDefinition<DomainsModel> sort = null, int limit = 0);
    Task<List<DomainsModel>> GetList(FilterDefinition<DomainsModel> filter, SortDefinition<DomainsModel> sort, int limit = 0);
    Task<bool> Put<DomainsModel>(FilterDefinition<DomainsModel> filter, UpdateDefinition<DomainsModel> updateDefinition);
    Task<bool> Delete<DomainsModel>(FilterDefinition<DomainsModel> filter);
}
