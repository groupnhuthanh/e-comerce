﻿using Entities.ExtendModels.Reports;

namespace BusinessLogicLayer.BusinessLogic.Owners.Report;

public interface IReportBLL
{
    Task<ReportResponseModel> ReportBase(Dictionary<string, object> dic);
}
