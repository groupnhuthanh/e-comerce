﻿using Entities.ExtendModels.Authentication.Captcha;
using Entities.ExtendModels.Response;
using Microsoft.AspNetCore.Http;

namespace BusinessLogicLayer.BusinessLogic.Owners.CaptchaCore;

public interface ICaptchaCoreBLL
{
    Task<ResponseAppModel> Get(string nftToken_id, HttpContext context);
    Task<ResponseAppModel> Post(CaptchaCoreModel doc, HttpContext context);
}
