﻿using Entities.ExtendModels.Tokens;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.BusinessLogic.Owners.Security
{
    public interface ITokenSecurityBLL
    {
        Task<bool> TokenValidate();
        Task<TokenValidate> TokenHMACValidate(HttpContext request, bool isRequiredHMAC = false, Encoding encoding = null);
        //Task<ORValidateModel> HeaderValidate(HttpContext request, bool isRequiredHMAC = false, Encoding encoding = null);
        //Task<RequestHeaderClsPayooModel> HeaderPayooValidate(HttpContext context, bool isRequiredHMAC = false, Encoding encoding = null);
    }
}
