﻿using Entities.ExtendModels.Authentication.Users;
using Entities.ExtendModels.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.BusinessLogic.Owners.Authentication
{
    public interface IAuthenticationBLL
    {
        Task<SignIn> SignIn(TokenView dicToken, Dictionary<string, object> dic);
    }
}
