namespace BusinessLogicLayer.BusinessLogic.Owners.Request;

public interface IRestfullRequest
{
    Task<T> Get<T>(string baseUrl, string param) where T : new();
    Task<T> GetURLExtension<T>(string baseUrl, string param) where T : new();
    Task<T> Post<T>(string baseUrl, string urlApi, object objDic) where T : new();
    Task<T> AuthenticationPost<T>(string baseUrl, string urlApi, object objDic) where T : new();
    Task<string> SendSMSPost(string baseUrl, string urlApi, object objDic);
}