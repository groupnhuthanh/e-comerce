﻿using Entities.ExtendModels.Authentication.Captcha;
using Entities.ExtendModels.Response;
using Microsoft.AspNetCore.Http;

namespace BusinessLogicLayer.ILogicCommon.IPipeLogic.Owners.CaptchaCores;

public interface ICaptchaCorePipe
{
    Task<ResponseAppModel> Generate(string nftsToken_id, HttpContext context);
    Task<ResponseAppModel> Verify(CaptchaCoreModel doc, HttpContext context);
}
