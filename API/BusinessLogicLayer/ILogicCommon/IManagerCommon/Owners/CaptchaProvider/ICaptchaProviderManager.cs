﻿
using CaptchaCore.Models;
using Entities.ExtendModels.Authentication.Captcha;

namespace BusinessLogicLayer.ILogicCommon.IManagerCommon.Owners.CaptchaProvider;

public interface ICaptchaProviderManager
{
    GenerateCaptchaModelResult Generate(int width, int height);
    CaptchaResult Verify(string clientInput, string clientKey);

}
