﻿namespace BusinessLogicLayer.ILogicCommon.IManagerCommon.Owners.LoggerManager;

public interface ILoggerManager
{
    void LogInfo(string message);
    void LogWarn(string message);
    void LogDebug(string message);
    void LogError(string message);
}
