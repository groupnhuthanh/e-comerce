﻿using BusinessLogicLayer.ILogicCommon.IManagerCommon.Wrapper;
using BusinessLogicLayer.ILogicCommon.IPipeLogic.Wrapper;
using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Wrapper;

namespace DataAccessLayer.LogicCommon.PipeLogic.Wrapper;

public class PipeLogicWrapper : IPipeLogicWrapper
{

    private readonly IManagerCommonWrapper _common;
    private readonly IRepositoryWrapper _repository;
    private readonly IQuery _query;

    public PipeLogicWrapper(IManagerCommonWrapper common, IRepositoryWrapper repository, IQuery query)
    {
        _common = common;
        _repository = repository;
        _query = query;

    }
  
}