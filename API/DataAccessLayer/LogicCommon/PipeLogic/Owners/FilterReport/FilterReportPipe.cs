﻿using BusinessLogicLayer.ILogicCommon.IManagerCommon.Wrapper;
using BusinessLogicLayer.ILogicCommon.IPipeLogic.Owners.FilterReport;
using BusinessLogicLayer.IRepository.Wrapper;
using Entities.ExtendModels.NullDySession;
using Entities.ExtendModels.Reports;
using Entities.ExtendModels.Tokens;
using Entities.Owners.SinglePoint_en.Devices.Device;
using Entities.Owners.SinglePoint_en.Locations.Location;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;
using Entities.Owners.SinglePoint_Orders_vi.Orders.Order;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;
using MongoDB.Driver;

namespace DataAccessLayer.LogicCommon.PipeLogic.Owners.FilterReport;

public class FilterReportPipe : IFilterReportPipe
{
    readonly TokenView tokenView = NullDySession.TokenUser;

    IManagerCommonWrapper _managerCommonWrapper;
    IRepositoryWrapper _repositoryWrapper;

    public FilterReportPipe(IManagerCommonWrapper managerCommonWrapper, IRepositoryWrapper repositoryWrapper)
    {
        _managerCommonWrapper = managerCommonWrapper;
        _repositoryWrapper = repositoryWrapper;
    }

    public async Task<FilterReportResponseModel> FilterOrderDetail(ReportRequestModel filter)
    {

        var FilterReportReponse = new FilterReportResponseModel();
        var LocationList = new List<LocationModel>();
        var DeviceList = new List<DeviceModel>();
        var CashDrawerList = new List<CashDrawerModel>();

        var filterLocation = Builders<LocationModel>.Filter.Empty;
        var filterDevice = Builders<DeviceModel>.Filter.Empty;
        var filterCashDrawer = Builders<CashDrawerModel>.Filter.Empty;

        var startDate = filter.StartDate;
        var endDate = filter.EndDate;
        var listStringLocations = filter.Location;
        var listStringDevices = filter.Device;
        var listStringCashDrawer = filter.CashDrawer;
        var userGuid = filter.UserGuid;



        if (listStringLocations?.Count() > 0)
        {
            filterLocation = Builders<LocationModel>.Filter.In(x => x._id, listStringLocations);
            
            filterDevice = Builders<DeviceModel>.Filter.In(x => x.Location, listStringLocations)
                         & Builders<DeviceModel>.Filter.Eq(x => x.UserGuid, userGuid);

            filterCashDrawer = Builders<CashDrawerModel>.Filter.In(x => x.LocationGuid, listStringLocations)
                             & Builders<CashDrawerModel>.Filter.In(x => x.DeviceGuid, listStringDevices);
        }
        LocationList = await _repositoryWrapper.locationRepository.GetList(filterLocation, null);
        DeviceList = await _repositoryWrapper.deviceRepository.GetList(filterDevice, null);
        CashDrawerList = await _repositoryWrapper.cashDrawerRepository.GetList(filterCashDrawer, null);

        if (LocationList.Count > 0)
        {
            var filterOrderDetail = Builders<OrderDetailModel>.Filter.Gte(q => q.CreateDate, startDate + " 00:00:00")
                                  & Builders<OrderDetailModel>.Filter.Lte(q => q.CreateDate, endDate + " 23:59:59");
            if (userGuid != null && userGuid.Any())
            {
                filterOrderDetail &= Builders<OrderDetailModel>.Filter.Eq(q => q.UserGuid, userGuid);
            }
            if (listStringLocations != null && listStringLocations.Any())
            {
                filterOrderDetail &= Builders<OrderDetailModel>.Filter.In(q => q.LocationGuid, listStringLocations);
            }

            if (listStringDevices != null && listStringDevices.Any())
            {
                filterOrderDetail &= Builders<OrderDetailModel>.Filter.In(q => q.DeviceGuid, listStringDevices);
            }

            if (listStringCashDrawer != null && listStringCashDrawer.Any())
            {
                filterOrderDetail &= Builders<OrderDetailModel>.Filter.In(q => q.CashDrawerId, listStringCashDrawer);
            }
            var OrderDetailList = await _repositoryWrapper.orderDetailRepository.GetList(filterOrderDetail, null);
            var orderDetailSuccess = new List<OrderDetailModel>();
            if (OrderDetailList != null && OrderDetailList.Any())
            {

                if (OrderDetailList != null && OrderDetailList.Any())
                {
                    foreach (var o in OrderDetailList)
                    {
                        var f = Builders<OrderModel>.Filter.Eq(x => x._id, o.Order_id);
                        var order = await _repositoryWrapper.orderRepository.GetSingle(f, null);
                        if (order.OrderStatusId == 10 && order != null)
                        {
                            orderDetailSuccess.Add(o);
                        }
                    }
                }
            }
            FilterReportReponse = new FilterReportResponseModel
            {
                ListOrderDetail = orderDetailSuccess,
                ListLocation = LocationList,
                ListDevice = DeviceList,
                ListCashDrawer = CashDrawerList
            };
        }
       
        return FilterReportReponse;
    }
}
