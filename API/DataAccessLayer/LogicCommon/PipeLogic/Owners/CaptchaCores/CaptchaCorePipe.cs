﻿using BusinessLogicLayer.ILogicCommon.IManagerCommon.Wrapper;
using BusinessLogicLayer.ILogicCommon.IPipeLogic.Owners.CaptchaCores;
using BusinessLogicLayer.IRepository.Wrapper;
using Entities.ExtendModels.Authentication.Captcha;
using Entities.ExtendModels.NullDySession;
using Entities.ExtendModels.Response;
using Entities.ExtendModels.Tokens;
using Entities.Owners.Heineken.CustomerNFTsWallet;
using Helpers.Convert;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DataAccessLayer.LogicCommon.PipeLogic.Owners.CaptchaCores;

/// <summary>
/// The captcha core pipe.
/// </summary>
public class CaptchaCorePipe : ICaptchaCorePipe
{
    readonly TokenView tokenView = NullDySession.TokenUser;

    private readonly IManagerCommonWrapper _common;
    IRepositoryWrapper _repository;
    /// <summary>
    /// Initializes a new instance of the <see cref="CaptchaCorePipe"/> class.
    /// </summary>
    /// <param name="provider">The provider.</param>
    public CaptchaCorePipe(IManagerCommonWrapper common, IRepositoryWrapper repository)
    {

        _common = common;
        _repository = repository;
    }

    public async Task<ResponseAppModel> Generate(string nftToken_id, HttpContext context)
    {
        var response = new ResponseAppModel { HttpStatusCode = 200, Status = false };
        var captcha = _common.CaptchaProviderManager.Generate(width: 675, height: 190);
        if (captcha != null && !string.IsNullOrEmpty(captcha.Code))
        {
            response.Status = true;

            var captchaModelResponse = new CaptchaModelResponse
            {
                Base64 = captcha.Base64,
                ClientKey = captcha.ClientKey
            };

            response.Model = captchaModelResponse;
        }
        return response;
    }
    public async Task<ResponseAppModel> Verify(CaptchaCoreModel doc, HttpContext context)
    {
        var response = new ResponseAppModel { HttpStatusCode = 200, Status = false };
        var input = doc?.CaptchaCode ?? "";
        if (string.IsNullOrEmpty(input))
        {
            response.Message = "CaptchaCode không tìm thấy";
            response.HttpStatusCode = 400;
            return response;
        }
        var key = doc?.ClientKey ?? "";
        if (string.IsNullOrEmpty(key))
        {
            response.Message = "ClientKey không tìm thấy";
            response.HttpStatusCode = 400;
            return response;
        }
        var nftToken_id = doc?.NftToken_id ?? "";

        if (string.IsNullOrEmpty(nftToken_id))
        {
            response.Message = "NFTsToken_id không tìm thấy";
            response.HttpStatusCode = 400;
            return response;
        }

        var result = _common.CaptchaProviderManager.Verify(input, key);
        if (!result.Succeeded)
        {
            response = await Generate(nftToken_id, context);
            response.Status = false;
            return response;
        }
        var userGuid = tokenView?.UserGuid ?? "";
        var customerGuid = tokenView?.CustomerGuid ?? "";
        if (!string.IsNullOrEmpty(userGuid) && !string.IsNullOrEmpty(customerGuid))
        {
            var customerNftsWalletFilter = Builders<CustomerNFTsWalletModel>.Filter.Eq("_id", ObjectId.Parse(nftToken_id))
                & Builders<CustomerNFTsWalletModel>.Filter.Eq(x => x.Owner, customerGuid);
            var customerNftsWallet = await _repository.customerNFTsWalletRepository.GetSingle(customerNftsWalletFilter);
            if (customerNftsWallet != null)
            {
                var createDate = LibConvert.Get_ByDateTimeNow();
                var customerNftsWalletUpdate = Builders<CustomerNFTsWalletModel>.Update.Set(x => x.IsVerify, true)
                    .Set("VerifyDate", createDate)
                    .Set("CaptchaCode", key)
                    .Set("ClientKey", key);
                var customerNftsWalletPut = await _repository.customerNFTsWalletRepository.Put(customerNftsWalletFilter, customerNftsWalletUpdate);
                if (customerNftsWalletPut)
                {
                    response.Status = true;
                    response.HttpStatusCode = 200;
                    response.Message = "Thành công";
                    return response;
                }
                else
                {
                    response.Message = "Cập nhật thất bại";
                }
            }
            else
            {
                response.Message = "Thông tin cập nhật không tìm thấy";
            }
        }
        else
        {
            response.Message = "Thông tin khách hàng không hợp lệ";
        }
        response = await Generate(nftToken_id, context);
        response.Status = false;
        return response;
    }
}






//internal static class Randomizer
//{
//    private static readonly Random Random;

//    static Randomizer()
//    {
//        Random = new Random();
//    }

//    public static int Number(int min, int max)
//    {
//        return Random.Next(min, max);
//    }

//    public static string String(int size, string permittedLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
//    {
//        if (size < 1)
//        {
//            throw new ArgumentOutOfRangeException("size");
//        }

//        StringBuilder stringBuilder = new StringBuilder(size);
//        for (int i = 0; i < size; i++)
//        {
//            char value = permittedLetters[Random.Next(permittedLetters.Length - 1)];
//            stringBuilder.Append(value);
//        }

//        return stringBuilder.ToString();
//    }

//    public static string Password()
//    {
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.Append(String(4));
//        stringBuilder.Append(Number(1000, 1001));
//        stringBuilder.Append(String(2));
//        return stringBuilder.ToString();
//    }
//}
