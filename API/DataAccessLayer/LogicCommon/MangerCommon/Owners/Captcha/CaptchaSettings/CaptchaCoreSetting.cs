﻿using System.Text;

namespace DataAccessLayer.LogicCommon.MangerCommon.Owners.Captcha.CaptchaSettings;

public class CaptchaCoreSetting
{
    public string Issuer { get; set; } = "Default";


    //public string CryptoKey { get; set; } = Randomizer.Password();
    public string CryptoKey { get; set; } = "J1f66U54BWikTPIIx3NOsqcHfP31QEkgFgybVQFgfH0QN+JlXX72eVN5ngb+MmNWHErCEi3yRQTvq9n5sca33Q==";


    public int ExpirationInSeconds { get; set; } = 30;


    public string PermittedLetters { get; set; } = "2346789ABCDEFGHJKLMNPRTUVWXYZabcdefghjklmnprtuvwxyz";


    public int CodeLength { get; set; } = 6;
}
internal static class Randomizer
{
    private static readonly Random Random;

    static Randomizer()
    {
        Random = new Random();
    }

    public static int Number(int min, int max)
    {
        return Random.Next(min, max);
    }

    public static string String(int size, string permittedLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
    {
        if (size < 1)
        {
            throw new ArgumentOutOfRangeException("size");
        }

        StringBuilder stringBuilder = new StringBuilder(size);
        for (int i = 0; i < size; i++)
        {
            char value = permittedLetters[Random.Next(permittedLetters.Length - 1)];
            stringBuilder.Append(value);
        }

        return stringBuilder.ToString();
    }

    public static string Password()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(String(4));
        stringBuilder.Append(Number(1000, 1001));
        stringBuilder.Append(String(2));
        return stringBuilder.ToString();
    }
}
