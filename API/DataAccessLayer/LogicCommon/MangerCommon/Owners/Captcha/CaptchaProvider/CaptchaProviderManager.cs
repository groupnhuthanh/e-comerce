﻿using BusinessLogicLayer.BusinessLogic.Owners.Request;
using BusinessLogicLayer.ILogicCommon.IManagerCommon.Owners.CaptchaProvider;
using CaptchaCore.Models;
using CaptchaCore.Providers.CodeGenerator;
using CaptchaCore.Providers.Crypto;
using CaptchaCore.Providers.ErrorDescriber;
using CaptchaCore.Providers.ImageCreator;
using CaptchaCore.Providers.ObjectSerializer;
using DataAccessLayer.LogicCommon.MangerCommon.Owners.Captcha.CaptchaSettings;
using Entities.ExtendModels.Authentication.Captcha;
using Entities.ExtendModels.System.Config;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Drawing;

namespace DataAccessLayer.LogicCommon.MangerCommon.Owners.Captcha.CaptchaProvider;

public class CaptchaProviderManager : ICaptchaProviderManager
{

    private readonly IRestfullRequest restfullRequest;
    private readonly IOptions<AppDomainSettings> _domainSettings;
    private readonly CaptchaCoreSetting Settings;
    protected readonly ICaptchaCodeGenerator CodeGenerator;
    protected readonly ICaptchaImageCreator ImageCreator;
    protected readonly ICaptchaCrypto Crypto;
    protected readonly ICaptchaObjectSerializer ObjectSerializer;
    protected readonly ICaptchaErrorDescriber ErrorDescriber;
    public CaptchaProviderManager(CaptchaCoreSetting settings, ICaptchaCodeGenerator codeGenerator, ICaptchaImageCreator imageCreator
, ICaptchaCrypto crypto, ICaptchaObjectSerializer objectSerializer, ICaptchaErrorDescriber errorDescriber, IRestfullRequest restfullRequest, IOptions<AppDomainSettings> domainSettings)
    {
        Settings = settings;
        CodeGenerator = codeGenerator;
        ImageCreator = imageCreator;
        Crypto = crypto;
        ObjectSerializer = objectSerializer;
        ErrorDescriber = errorDescriber;
        this.restfullRequest = restfullRequest;
        _domainSettings = domainSettings;
    }

    public virtual GenerateCaptchaModelResult Generate(CaptchaClientCredential credential, int width, int height)
    {
        string code = credential.Code;
        var domain_captcha = _domainSettings?.Value?.domain_captcha ?? "";
        var param = "?CaptchaCode=" + code + "&width=" + width + "&height=" + height;
        var result = restfullRequest.GetURLExtension<Edi.Captcha.CaptchaResult>(domain_captcha, param).Result;
        var base64 = result.CaptchaBase64Data;
        string input = ObjectSerializer.Serialize(credential);
        string clientKey = Crypto.Encrypt(input, Settings.CryptoKey);
        return new GenerateCaptchaModelResult(clientKey, code, base64);
    }

    public virtual GenerateCaptchaModelResult Generate(int width, int height)
    {

        string code = CodeGenerator.Generate(Settings.CodeLength, Settings.PermittedLetters);
        CaptchaClientCredential credential = new CaptchaClientCredential
        {
            UniqueIdentifier = Guid.NewGuid().ToString(),
            Issuer = Settings.Issuer,
            Code = code,
            ExpiredAt = DateTime.Now.AddSeconds(Settings.ExpirationInSeconds)
        };
        return Generate(credential, width, height);
    }

    public virtual CaptchaResult Verify(string clientInput, string clientKey)
    {
        try
        {
            string text = Crypto.Decrypt(clientKey, Settings.CryptoKey);
            if (text == null)
            {
                throw new Exception();
            }

            CaptchaClientCredential captchaClientCredential = ObjectSerializer.Deserialize(text);
            if (captchaClientCredential == null)
            {
                throw new Exception();
            }

            List<CaptchaError> list = new List<CaptchaError>();
            if (captchaClientCredential.Issuer != Settings.Issuer)
            {
                list.Add(ErrorDescriber.IssuerNotValid());
            }

            if (captchaClientCredential.Code != clientInput)
            {
                list.Add(ErrorDescriber.CodeNotMatch());
            }

            if (captchaClientCredential.ExpiredAt.CompareTo(DateTime.Now) == -1)
            {
                list.Add(ErrorDescriber.Expired());
            }

            if (list.Count > 0)
            {
                return CaptchaResult.Fail(list);
            }
            captchaClientCredential.ExpiredAt = captchaClientCredential.ExpiredAt.AddHours(-5);//expired after success
            return CaptchaResult.Success();
        }
        catch (Exception ex) when (ex is FormatException || ex is JsonReaderException)
        {
            return CaptchaResult.Fail(new List<CaptchaError> { ErrorDescriber.ClientKeyNotValid() });
        }
        catch
        {
            return CaptchaResult.Fail(new List<CaptchaError> { ErrorDescriber.UnhandledError() });
        }
    }

    public Bitmap Base64StringToBitmap(string
                                       base64String)
    {
        Bitmap bmpReturn = null;


        byte[] byteBuffer = Convert.FromBase64String(base64String);
        MemoryStream memoryStream = new MemoryStream(byteBuffer);


        memoryStream.Position = 0;


        bmpReturn = (Bitmap)Image.FromStream(memoryStream);


        memoryStream.Close();
        memoryStream = null;
        byteBuffer = null;

        return bmpReturn;

    }
}
