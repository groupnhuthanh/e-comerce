﻿using BusinessLogicLayer.BusinessLogic.Owners.Request;
using BusinessLogicLayer.ILogicCommon.IManagerCommon.Wrapper;
using BusinessLogicLayer.IRepository.Wrapper;
using CaptchaCore.Providers.CodeGenerator;
using CaptchaCore.Providers.Crypto;
using CaptchaCore.Providers.ErrorDescriber;
using CaptchaCore.Providers.ImageCreator;
using CaptchaCore.Providers.ObjectSerializer;
using Entities.ExtendModels.System.Config;
using Microsoft.Extensions.Options;

namespace DataAccessLayer.LogicCommon.MangerCommon.Wrapper;

public class ManagerCommonWrapper : IManagerCommonWrapper
{


    private readonly IRestfullRequest restfullRequest;
    private readonly IOptions<AppDomainSettings> _domainSettings;
    //protected readonly CaptchaCoreSetting Settings;
    protected readonly ICaptchaCodeGenerator CodeGenerator;
    protected readonly ICaptchaImageCreator ImageCreator;
    protected readonly ICaptchaCrypto Crypto;
    protected readonly ICaptchaObjectSerializer ObjectSerializer;
    protected readonly ICaptchaErrorDescriber ErrorDescriber;
    private readonly IRepositoryWrapper _repository;

    public ManagerCommonWrapper(
                                ICaptchaCodeGenerator codeGenerator,
                                ICaptchaImageCreator imageCreator,
                                ICaptchaCrypto crypto,
                                ICaptchaObjectSerializer objectSerializer,
                                ICaptchaErrorDescriber errorDescriber,
                                IRestfullRequest restfullRequest,
                                IOptions<AppDomainSettings> domainSettings,
                                IRepositoryWrapper repository)
    {
        //Settings = settings;
        CodeGenerator = codeGenerator;
        ImageCreator = imageCreator;
        Crypto = crypto;
        ObjectSerializer = objectSerializer;
        ErrorDescriber = errorDescriber;
        this.restfullRequest = restfullRequest;
        _domainSettings = domainSettings;
        _repository = repository;
    }
    

    
}
