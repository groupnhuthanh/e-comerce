﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.MongoQueryLanguage;

public class Query : IQuery
{
    private readonly IMongoClient _mongoClient;
    public Query(IMongoClient mongoClient)
    {
        _mongoClient = mongoClient;
    }
    public async Task<bool> Delete<TEntity>(string database, string table, FilterDefinition<TEntity> filter = null)
    {
        var db = _mongoClient.GetDatabase(database);
        var collection = db.GetCollection<TEntity>(table);
        var result = await collection.DeleteOneAsync(filter);
        return result.IsAcknowledged;
    }

    public async Task<List<TEntity>> GetList<TEntity>(string database, string table, FilterDefinition<TEntity> filter = null, SortDefinition<TEntity> sort = null, int limit = 0)
    {
        var list = new List<TEntity>();
        try
        {
            var db = _mongoClient.GetDatabase(database);
            var collection = db.GetCollection<TEntity>(table);
            filter ??= Builders<TEntity>.Filter.Empty;
            var condition = collection.Find(filter);
            if (sort != null)
            {
                condition.Sort(sort);
            }
            if (limit > 0)
            {
                condition.Limit(limit);
            }
            list = await condition.ToListAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        return list;
    }

    public async Task<TEntity> GetSingle<TEntity>(string database, string table, FilterDefinition<TEntity> filter = null, SortDefinition<TEntity> sort = null, int limit = 0) where TEntity : new()
    {
        var tEntity = new TEntity();
        try
        {
            var db = _mongoClient.GetDatabase(database);
            var collection = db.GetCollection<TEntity>(table);
            filter ??= Builders<TEntity>.Filter.Empty;
            var condition = collection.Find(filter);
            if (sort != null)
            {
                condition.Sort(sort);
            }
            if (limit > 0)
            {
                condition.Limit(limit);
            }
            tEntity = await condition.FirstOrDefaultAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return tEntity;
    }

    public async Task<TEntity> Post<TEntity>(string database, string table, TEntity doc)
    {
        try
        {
            var db = _mongoClient.GetDatabase(database);
            var collection = db.GetCollection<TEntity>(table.ToString());
            await collection.InsertOneAsync(doc);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        return doc;
    }

    public async Task<bool> Put<TEntity>(string database, string table, FilterDefinition<TEntity> filter, UpdateDefinition<TEntity> updateDefinition)
    {
        var db = _mongoClient.GetDatabase(database);
        var collection = db.GetCollection<TEntity>(table);
        var result = await collection.UpdateOneAsync(filter, updateDefinition);
        return result.IsAcknowledged;
    }
}
