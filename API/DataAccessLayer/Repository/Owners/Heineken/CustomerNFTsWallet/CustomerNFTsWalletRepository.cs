﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.Heineken.CustomerNFTsWallet;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.Heineken.CustomerNFTsWallet;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.Heineken.CustomerNFTsWallet;

public class CustomerNFTsWalletRepository : RepositoryBase<CustomerNFTsWalletModel>, ICustomerNFTsWalletRepository
{
    readonly IQuery query;
    private readonly string database = Databases.Heineken.ToString();
    private readonly string table = HeinekenTables.CustomerNFTsWallet.ToString();

    public CustomerNFTsWalletRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerNFTsWalletModel> Post(CustomerNFTsWalletModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerNFTsWalletModel> GetSingle(FilterDefinition<CustomerNFTsWalletModel> filter = null, SortDefinition<CustomerNFTsWalletModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerNFTsWalletModel>> GetList(FilterDefinition<CustomerNFTsWalletModel> filter, SortDefinition<CustomerNFTsWalletModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerNFTsWalletModel>(FilterDefinition<CustomerNFTsWalletModel> filter, UpdateDefinition<CustomerNFTsWalletModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerNFTsWalletModel>(FilterDefinition<CustomerNFTsWalletModel> filter = null) => await query.Delete(database, table, filter);

}
