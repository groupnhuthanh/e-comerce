﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Inventories.InventoryHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Inventories.InventoryHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Inventories.InventoryHistory;

public class InventoryHistoryRepository : RepositoryBase<InventoryHistoryModel>, IInventoryHistoryRepository

{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.InventoryHistory.ToString();
    public IQuery query;
    public InventoryHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<InventoryHistoryModel> Post(InventoryHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<InventoryHistoryModel> GetSingle(FilterDefinition<InventoryHistoryModel> filter = null, SortDefinition<InventoryHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<InventoryHistoryModel>> GetList(FilterDefinition<InventoryHistoryModel> filter, SortDefinition<InventoryHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<InventoryHistoryModel>(FilterDefinition<InventoryHistoryModel> filter, UpdateDefinition<InventoryHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<InventoryHistoryModel>(FilterDefinition<InventoryHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
