﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Inventories.Inventory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Inventories.Inventory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Inventories.Inventory;

public class InventoryRepository : RepositoryBase<InventoryModel>, IInventoryRepository
{

    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.Inventory.ToString();
    public IQuery query;
    public InventoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<InventoryModel> Post(InventoryModel doc) => await query.Post(database, table, doc);
    public async Task<InventoryModel> GetSingle(FilterDefinition<InventoryModel> filter = null, SortDefinition<InventoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<InventoryModel>> GetList(FilterDefinition<InventoryModel> filter, SortDefinition<InventoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<InventoryModel>(FilterDefinition<InventoryModel> filter, UpdateDefinition<InventoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<InventoryModel>(FilterDefinition<InventoryModel> filter = null) => await query.Delete(database, table, filter);

}
