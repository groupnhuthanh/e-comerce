﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Inventories.InventoryLedger;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Inventories.InventoryLedger;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Inventories.InventoryLedger;

public class InventoryLedgerRepository : RepositoryBase<InventoryLedgerModel>, IInventoryLedgerRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.InventoryLedger.ToString();
    public IQuery query;
    public InventoryLedgerRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<InventoryLedgerModel> Post(InventoryLedgerModel doc) => await query.Post(database, table, doc);
    public async Task<InventoryLedgerModel> GetSingle(FilterDefinition<InventoryLedgerModel> filter = null, SortDefinition<InventoryLedgerModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<InventoryLedgerModel>> GetList(FilterDefinition<InventoryLedgerModel> filter, SortDefinition<InventoryLedgerModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<InventoryLedgerModel>(FilterDefinition<InventoryLedgerModel> filter, UpdateDefinition<InventoryLedgerModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<InventoryLedgerModel>(FilterDefinition<InventoryLedgerModel> filter = null) => await query.Delete(database, table, filter);

}
