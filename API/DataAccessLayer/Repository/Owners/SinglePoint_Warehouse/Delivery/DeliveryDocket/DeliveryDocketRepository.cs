﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocket;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocket;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocket;

public class DeliveryDocketRepository : RepositoryBase<DeliveryDocketModel>, IDeliveryDocketRepository
{

    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.DeliveryDocket.ToString();
    public IQuery query;
    public DeliveryDocketRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeliveryDocketModel> Post(DeliveryDocketModel doc) => await query.Post(database, table, doc);
    public async Task<DeliveryDocketModel> GetSingle(FilterDefinition<DeliveryDocketModel> filter = null, SortDefinition<DeliveryDocketModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeliveryDocketModel>> GetList(FilterDefinition<DeliveryDocketModel> filter, SortDefinition<DeliveryDocketModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeliveryDocketModel>(FilterDefinition<DeliveryDocketModel> filter, UpdateDefinition<DeliveryDocketModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeliveryDocketModel>(FilterDefinition<DeliveryDocketModel> filter = null) => await query.Delete(database, table, filter);

}
