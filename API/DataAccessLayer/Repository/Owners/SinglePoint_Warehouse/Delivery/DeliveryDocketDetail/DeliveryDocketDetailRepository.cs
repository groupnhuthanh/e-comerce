﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocketDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocketDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocketDetail;

public class DeliveryDocketDetailRepository : RepositoryBase<DeliveryDocketDetailModel>, IDeliveryDocketDetailRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.DeliveryDocketDetail.ToString();
    public IQuery query;
    public DeliveryDocketDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeliveryDocketDetailModel> Post(DeliveryDocketDetailModel doc) => await query.Post(database, table, doc);
    public async Task<DeliveryDocketDetailModel> GetSingle(FilterDefinition<DeliveryDocketDetailModel> filter = null, SortDefinition<DeliveryDocketDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeliveryDocketDetailModel>> GetList(FilterDefinition<DeliveryDocketDetailModel> filter, SortDefinition<DeliveryDocketDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeliveryDocketDetailModel>(FilterDefinition<DeliveryDocketDetailModel> filter, UpdateDefinition<DeliveryDocketDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeliveryDocketDetailModel>(FilterDefinition<DeliveryDocketDetailModel> filter = null) => await query.Delete(database, table, filter);

}
