﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Works.WorkOrder;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Works.WorkOrder;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Works.WorkOrder;

public class WorkOrderRepository : RepositoryBase<WorkOrderModel>, IWorkOrderRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.WorkOrder.ToString();
    public IQuery query;
    public WorkOrderRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<WorkOrderModel> Post(WorkOrderModel doc) => await query.Post(database, table, doc);
    public async Task<WorkOrderModel> GetSingle(FilterDefinition<WorkOrderModel> filter = null, SortDefinition<WorkOrderModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<WorkOrderModel>> GetList(FilterDefinition<WorkOrderModel> filter, SortDefinition<WorkOrderModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<WorkOrderModel>(FilterDefinition<WorkOrderModel> filter, UpdateDefinition<WorkOrderModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<WorkOrderModel>(FilterDefinition<WorkOrderModel> filter = null) => await query.Delete(database, table, filter);

}
