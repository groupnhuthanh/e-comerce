﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisitionDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisitionDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisitionDetail;

public class PurchaseRequisitionDetailRepository : RepositoryBase<PurchaseRequisitionDetailModel>, IPurchaseRequisitionDetailRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.PurchaseRequisitionDetail.ToString();
    public IQuery query;
    public PurchaseRequisitionDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PurchaseRequisitionDetailModel> Post(PurchaseRequisitionDetailModel doc) => await query.Post(database, table, doc);
    public async Task<PurchaseRequisitionDetailModel> GetSingle(FilterDefinition<PurchaseRequisitionDetailModel> filter = null, SortDefinition<PurchaseRequisitionDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PurchaseRequisitionDetailModel>> GetList(FilterDefinition<PurchaseRequisitionDetailModel> filter, SortDefinition<PurchaseRequisitionDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PurchaseRequisitionDetailModel>(FilterDefinition<PurchaseRequisitionDetailModel> filter, UpdateDefinition<PurchaseRequisitionDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PurchaseRequisitionDetailModel>(FilterDefinition<PurchaseRequisitionDetailModel> filter = null) => await query.Delete(database, table, filter);

}
