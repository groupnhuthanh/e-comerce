﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedgerDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedgerDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedgerDetail;

public class PurchaseOrderLedgerDetailRepository : RepositoryBase<PurchaseOrderLedgerDetailModel>, IPurchaseOrderLedgerDetailRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.PurchaseOrderLedgerDetail.ToString();
    public IQuery query;
    public PurchaseOrderLedgerDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PurchaseOrderLedgerDetailModel> Post(PurchaseOrderLedgerDetailModel doc) => await query.Post(database, table, doc);
    public async Task<PurchaseOrderLedgerDetailModel> GetSingle(FilterDefinition<PurchaseOrderLedgerDetailModel> filter = null, SortDefinition<PurchaseOrderLedgerDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PurchaseOrderLedgerDetailModel>> GetList(FilterDefinition<PurchaseOrderLedgerDetailModel> filter, SortDefinition<PurchaseOrderLedgerDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PurchaseOrderLedgerDetailModel>(FilterDefinition<PurchaseOrderLedgerDetailModel> filter, UpdateDefinition<PurchaseOrderLedgerDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PurchaseOrderLedgerDetailModel>(FilterDefinition<PurchaseOrderLedgerDetailModel> filter = null) => await query.Delete(database, table, filter);

}
