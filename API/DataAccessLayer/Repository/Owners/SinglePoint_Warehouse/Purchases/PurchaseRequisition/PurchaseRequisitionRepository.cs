﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisition;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisition;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisition;

public class PurchaseRequisitionRepository : RepositoryBase<PurchaseRequisitionModel>, IPurchaseRequisitionRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.PurchaseRequisition.ToString();
    public IQuery query;
    public PurchaseRequisitionRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PurchaseRequisitionModel> Post(PurchaseRequisitionModel doc) => await query.Post(database, table, doc);
    public async Task<PurchaseRequisitionModel> GetSingle(FilterDefinition<PurchaseRequisitionModel> filter = null, SortDefinition<PurchaseRequisitionModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PurchaseRequisitionModel>> GetList(FilterDefinition<PurchaseRequisitionModel> filter, SortDefinition<PurchaseRequisitionModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PurchaseRequisitionModel>(FilterDefinition<PurchaseRequisitionModel> filter, UpdateDefinition<PurchaseRequisitionModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PurchaseRequisitionModel>(FilterDefinition<PurchaseRequisitionModel> filter = null) => await query.Delete(database, table, filter);

}
