﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedger;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedger;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderLedger;

public class PurchaseOrderLedgerRepository : RepositoryBase<PurchaseOrderLedgerModel>, IPurchaseOrderLedgerRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.PurchaseOrderLedger.ToString();
    public IQuery query;
    public PurchaseOrderLedgerRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PurchaseOrderLedgerModel> Post(PurchaseOrderLedgerModel doc) => await query.Post(database, table, doc);
    public async Task<PurchaseOrderLedgerModel> GetSingle(FilterDefinition<PurchaseOrderLedgerModel> filter = null, SortDefinition<PurchaseOrderLedgerModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PurchaseOrderLedgerModel>> GetList(FilterDefinition<PurchaseOrderLedgerModel> filter, SortDefinition<PurchaseOrderLedgerModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PurchaseOrderLedgerModel>(FilterDefinition<PurchaseOrderLedgerModel> filter, UpdateDefinition<PurchaseOrderLedgerModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PurchaseOrderLedgerModel>(FilterDefinition<PurchaseOrderLedgerModel> filter = null) => await query.Delete(database, table, filter);

}
