﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderDetail;

public class PurchaseOrderDetailRepository : RepositoryBase<PurchaseOrderDetailModel>, IPurchaseOrderDetailRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.PurchaseOrderDetail.ToString();
    public IQuery query;
    public PurchaseOrderDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PurchaseOrderDetailModel> Post(PurchaseOrderDetailModel doc) => await query.Post(database, table, doc);
    public async Task<PurchaseOrderDetailModel> GetSingle(FilterDefinition<PurchaseOrderDetailModel> filter = null, SortDefinition<PurchaseOrderDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PurchaseOrderDetailModel>> GetList(FilterDefinition<PurchaseOrderDetailModel> filter, SortDefinition<PurchaseOrderDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PurchaseOrderDetailModel>(FilterDefinition<PurchaseOrderDetailModel> filter, UpdateDefinition<PurchaseOrderDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PurchaseOrderDetailModel>(FilterDefinition<PurchaseOrderDetailModel> filter = null) => await query.Delete(database, table, filter);

}
