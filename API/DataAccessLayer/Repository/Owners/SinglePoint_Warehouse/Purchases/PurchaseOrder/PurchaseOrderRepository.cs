﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrder;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrder;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrder;

public class PurchaseOrderRepository : RepositoryBase<PurchaseOrderModel>, IPurchaseOrderRepository
{
    private readonly string database = Databases.SinglePoint_Warehouse.ToString();
    private readonly string table = SinglePoint_WarehouseTables.PurchaseOrder.ToString();
    public IQuery query;
    public PurchaseOrderRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PurchaseOrderModel> Post(PurchaseOrderModel doc) => await query.Post(database, table, doc);
    public async Task<PurchaseOrderModel> GetSingle(FilterDefinition<PurchaseOrderModel> filter = null, SortDefinition<PurchaseOrderModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PurchaseOrderModel>> GetList(FilterDefinition<PurchaseOrderModel> filter, SortDefinition<PurchaseOrderModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PurchaseOrderModel>(FilterDefinition<PurchaseOrderModel> filter, UpdateDefinition<PurchaseOrderModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PurchaseOrderModel>(FilterDefinition<PurchaseOrderModel> filter = null) => await query.Delete(database, table, filter);

}
