﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SingleWorker.Settings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SingleWorker.Settings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SingleWorker.Settings;

public class SettingsRepository : RepositoryBase<SettingsModel>, ISettingsRepository
{
    private readonly string database = Databases.SingleWorker.ToString();
    private readonly string table = System_WorkerTables.Settings.ToString();
    public IQuery query;
    public SettingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SettingsModel> Post(SettingsModel doc) => await query.Post(database, table, doc);
    public async Task<SettingsModel> GetSingle(FilterDefinition<SettingsModel> filter = null, SortDefinition<SettingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SettingsModel>> GetList(FilterDefinition<SettingsModel> filter, SortDefinition<SettingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SettingsModel>(FilterDefinition<SettingsModel> filter, UpdateDefinition<SettingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SettingsModel>(FilterDefinition<SettingsModel> filter = null) => await query.Delete(database, table, filter);

}
