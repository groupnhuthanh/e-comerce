﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SingleWorker.CustomerGuestsLogin;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SingleWorker.CustomerGuestsLogin;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SingleWorker.CustomerGuestsLogin;

public class CustomerGuestsLoginRepository : RepositoryBase<CustomerGuestsLoginModel>, ICustomerGuestsLoginRepository
{
    private readonly string database = Databases.SingleWorker.ToString();
    private readonly string table = SingleWorkerTables.CustomerGuestsLogin.ToString();
    public IQuery query;
    public CustomerGuestsLoginRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsLoginModel> Post(CustomerGuestsLoginModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsLoginModel> GetSingle(FilterDefinition<CustomerGuestsLoginModel> filter = null, SortDefinition<CustomerGuestsLoginModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsLoginModel>> GetList(FilterDefinition<CustomerGuestsLoginModel> filter, SortDefinition<CustomerGuestsLoginModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsLoginModel>(FilterDefinition<CustomerGuestsLoginModel> filter, UpdateDefinition<CustomerGuestsLoginModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsLoginModel>(FilterDefinition<CustomerGuestsLoginModel> filter = null) => await query.Delete(database, table, filter);

}
