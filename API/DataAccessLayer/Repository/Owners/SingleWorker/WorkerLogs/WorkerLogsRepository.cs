﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SingleWorker.WorkerLogs;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SingleWorker.WorkerLogs;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SingleWorker.WorkerLogs;

public class WorkerLogsRepository : RepositoryBase<WorkerLogsModel>, IWorkerLogsRepository
{
    private readonly string database = Databases.SingleWorker.ToString();
    private readonly string table = SingleWorkerTables.WorkerLogs.ToString();
    public IQuery query;
    public WorkerLogsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<WorkerLogsModel> Post(WorkerLogsModel doc) => await query.Post(database, table, doc);
    public async Task<WorkerLogsModel> GetSingle(FilterDefinition<WorkerLogsModel> filter = null, SortDefinition<WorkerLogsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<WorkerLogsModel>> GetList(FilterDefinition<WorkerLogsModel> filter, SortDefinition<WorkerLogsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<WorkerLogsModel>(FilterDefinition<WorkerLogsModel> filter, UpdateDefinition<WorkerLogsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<WorkerLogsModel>(FilterDefinition<WorkerLogsModel> filter = null) => await query.Delete(database, table, filter);

}
