﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SingleWorker.CustomerGuestsNoLogin;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SingleWorker.CustomerGuestsNoLogin;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SingleWorker.CustomerGuestsNoLogin;

public class CustomerGuestsNoLoginRepository : RepositoryBase<CustomerGuestsNoLoginModel>, ICustomerGuestsNoLoginRepository
{
    private readonly string database = Databases.SingleWorker.ToString();
    private readonly string table = SingleWorkerTables.CustomerGuestsNoLogin.ToString();
    public IQuery query;
    public CustomerGuestsNoLoginRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsNoLoginModel> Post(CustomerGuestsNoLoginModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsNoLoginModel> GetSingle(FilterDefinition<CustomerGuestsNoLoginModel> filter = null, SortDefinition<CustomerGuestsNoLoginModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsNoLoginModel>> GetList(FilterDefinition<CustomerGuestsNoLoginModel> filter, SortDefinition<CustomerGuestsNoLoginModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsNoLoginModel>(FilterDefinition<CustomerGuestsNoLoginModel> filter, UpdateDefinition<CustomerGuestsNoLoginModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsNoLoginModel>(FilterDefinition<CustomerGuestsNoLoginModel> filter = null) => await query.Delete(database, table, filter);

}
