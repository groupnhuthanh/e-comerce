﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_History.History_Brands;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_History.History_Brands;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_History.History_Brands;

public class History_BrandsRepository : RepositoryBase<History_BrandsModel>, IHistory_BrandsRepository
{
    private readonly string database = Databases.SinglePoint_History.ToString();
    private readonly string table = SinglePoint_HistoryTables.History_Brands.ToString();
    public IQuery query;
    public History_BrandsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<History_BrandsModel> Post(History_BrandsModel doc) => await query.Post(database, table, doc);
    public async Task<History_BrandsModel> GetSingle(FilterDefinition<History_BrandsModel> filter = null, SortDefinition<History_BrandsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<History_BrandsModel>> GetList(FilterDefinition<History_BrandsModel> filter, SortDefinition<History_BrandsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<History_BrandsModel>(FilterDefinition<History_BrandsModel> filter, UpdateDefinition<History_BrandsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<History_BrandsModel>(FilterDefinition<History_BrandsModel> filter = null) => await query.Delete(database, table, filter);

}
