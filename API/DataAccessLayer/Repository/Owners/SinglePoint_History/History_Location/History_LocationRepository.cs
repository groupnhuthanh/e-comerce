﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_History.History_Location;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_History.History_Location;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_History.History_Location;

public class History_LocationRepository : RepositoryBase<History_LocationModel>, IHistory_LocationRepository
{

    private readonly string database = Databases.SinglePoint_History.ToString();
    private readonly string table = SinglePoint_HistoryTables.History_Location.ToString();
    public IQuery query;
    public History_LocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<History_LocationModel> Post(History_LocationModel doc) => await query.Post(database, table, doc);
    public async Task<History_LocationModel> GetSingle(FilterDefinition<History_LocationModel> filter = null, SortDefinition<History_LocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<History_LocationModel>> GetList(FilterDefinition<History_LocationModel> filter, SortDefinition<History_LocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<History_LocationModel>(FilterDefinition<History_LocationModel> filter, UpdateDefinition<History_LocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<History_LocationModel>(FilterDefinition<History_LocationModel> filter = null) => await query.Delete(database, table, filter);

}
