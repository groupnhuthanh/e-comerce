using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Builder.PageBlocks;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Builder.PageBlocks;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Builder.PageBlocks;


public class PageBlocksRepository : RepositoryBase<PageBlocksModel>, IPageBlocksRepository
{
    private readonly string database = Databases.SinglePoint_Builder.ToString();
    private readonly string table = SinglePoint_BuilderTables.PageBlocks.ToString();
    public IQuery query;
    public PageBlocksRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PageBlocksModel> Post(PageBlocksModel doc) => await query.Post(database, table, doc);
    public async Task<PageBlocksModel> GetSingle(FilterDefinition<PageBlocksModel> filter = null, SortDefinition<PageBlocksModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PageBlocksModel>> GetList(FilterDefinition<PageBlocksModel> filter, SortDefinition<PageBlocksModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PageBlocksModel>(FilterDefinition<PageBlocksModel> filter, UpdateDefinition<PageBlocksModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PageBlocksModel>(FilterDefinition<PageBlocksModel> filter = null) => await query.Delete(database, table, filter);

}