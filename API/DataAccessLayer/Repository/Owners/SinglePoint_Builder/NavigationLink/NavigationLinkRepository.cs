using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Builder.NavigationLink;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Builder.NavigationLink;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Builder.NavigationLink;

public class NavigationLinkRepository : RepositoryBase<NavigationLinkModel>, INavigationLinkRepository
{
    private readonly string database = Databases.SinglePoint_Builder.ToString();
    private readonly string table = SinglePoint_BuilderTables.NavigationLink.ToString();
    public IQuery query;
    public NavigationLinkRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<NavigationLinkModel> Post(NavigationLinkModel doc) => await query.Post(database, table, doc);
    public async Task<NavigationLinkModel> GetSingle(FilterDefinition<NavigationLinkModel> filter = null, SortDefinition<NavigationLinkModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<NavigationLinkModel>> GetList(FilterDefinition<NavigationLinkModel> filter, SortDefinition<NavigationLinkModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<NavigationLinkModel>(FilterDefinition<NavigationLinkModel> filter, UpdateDefinition<NavigationLinkModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<NavigationLinkModel>(FilterDefinition<NavigationLinkModel> filter = null) => await query.Delete(database, table, filter);

}