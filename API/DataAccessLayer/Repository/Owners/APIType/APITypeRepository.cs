﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.APIType;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.APIType;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.APIType;

public class APITypeRepository : RepositoryBase<APITypeModel>, IAPITypeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.APIType.ToString();
    public IQuery query;
    public APITypeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<APITypeModel> Post(APITypeModel doc) => await query.Post(database, table, doc);
    public async Task<APITypeModel> GetSingle(FilterDefinition<APITypeModel> filter = null, SortDefinition<APITypeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<APITypeModel>> GetList(FilterDefinition<APITypeModel> filter, SortDefinition<APITypeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<APITypeModel>(FilterDefinition<APITypeModel> filter, UpdateDefinition<APITypeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<APITypeModel>(FilterDefinition<APITypeModel> filter = null) => await query.Delete(database, table, filter);

}
