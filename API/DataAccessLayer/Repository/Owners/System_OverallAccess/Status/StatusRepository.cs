﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.Status;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.Status;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.Status;

public class StatusRepository : RepositoryBase<StatusModel>, IStatusRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.Status.ToString();
    public IQuery query;
    public StatusRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<StatusModel> Post(StatusModel doc) => await query.Post(database, table, doc);
    public async Task<StatusModel> GetSingle(FilterDefinition<StatusModel> filter = null, SortDefinition<StatusModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<StatusModel>> GetList(FilterDefinition<StatusModel> filter, SortDefinition<StatusModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<StatusModel>(FilterDefinition<StatusModel> filter, UpdateDefinition<StatusModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<StatusModel>(FilterDefinition<StatusModel> filter = null) => await query.Delete(database, table, filter);

}
