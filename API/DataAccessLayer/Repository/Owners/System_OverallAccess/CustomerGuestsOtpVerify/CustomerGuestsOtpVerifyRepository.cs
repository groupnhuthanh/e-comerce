﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.CustomerGuestsOtpVerify;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.CustomerGuestsOtpVerify;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.CustomerGuestsOtpVerify;

public class CustomerGuestsOtpVerifyRepository : RepositoryBase<CustomerGuestsOtpVerifyModel>, ICustomerGuestsOtpVerifyRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.CustomerGuestsOtpVerify.ToString();
    public IQuery query;
    public CustomerGuestsOtpVerifyRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsOtpVerifyModel> Post(CustomerGuestsOtpVerifyModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsOtpVerifyModel> GetSingle(FilterDefinition<CustomerGuestsOtpVerifyModel> filter = null, SortDefinition<CustomerGuestsOtpVerifyModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsOtpVerifyModel>> GetList(FilterDefinition<CustomerGuestsOtpVerifyModel> filter, SortDefinition<CustomerGuestsOtpVerifyModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsOtpVerifyModel>(FilterDefinition<CustomerGuestsOtpVerifyModel> filter, UpdateDefinition<CustomerGuestsOtpVerifyModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsOtpVerifyModel>(FilterDefinition<CustomerGuestsOtpVerifyModel> filter = null) => await query.Delete(database, table, filter);

}
