﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.Variable;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.Variable;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.Variable;

public class VariableRepository : RepositoryBase<VariableModel>, IVariableRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.Variable.ToString();
    public IQuery query;
    public VariableRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<VariableModel> Post(VariableModel doc) => await query.Post(database, table, doc);
    public async Task<VariableModel> GetSingle(FilterDefinition<VariableModel> filter = null, SortDefinition<VariableModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<VariableModel>> GetList(FilterDefinition<VariableModel> filter, SortDefinition<VariableModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<VariableModel>(FilterDefinition<VariableModel> filter, UpdateDefinition<VariableModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<VariableModel>(FilterDefinition<VariableModel> filter = null) => await query.Delete(database, table, filter);

}
