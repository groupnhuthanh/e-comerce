﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.OtpSettings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.OtpSettings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.OtpSettings;

public class OtpSettingsRepository : RepositoryBase<OtpSettingsModel>, IOtpSettingsRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.Status.ToString();
    public IQuery query;
    public OtpSettingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }
    public async Task<OtpSettingsModel> Post(OtpSettingsModel doc) => await query.Post(database, table, doc);
    public async Task<OtpSettingsModel> GetSingle(FilterDefinition<OtpSettingsModel> filter = null, SortDefinition<OtpSettingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OtpSettingsModel>> GetList(FilterDefinition<OtpSettingsModel> filter, SortDefinition<OtpSettingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OtpSettingsModel>(FilterDefinition<OtpSettingsModel> filter, UpdateDefinition<OtpSettingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OtpSettingsModel>(FilterDefinition<OtpSettingsModel> filter = null) => await query.Delete(database, table, filter);

}
