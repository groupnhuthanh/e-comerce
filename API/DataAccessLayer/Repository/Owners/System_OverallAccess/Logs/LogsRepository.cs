﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.Logs;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.Logs;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.Logs;

public class LogsRepository : RepositoryBase<LogsModel>, ILogsRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.Status.ToString();
    public IQuery query;
    public LogsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<LogsModel> Post(LogsModel doc) => await query.Post(database, table, doc);
    public async Task<LogsModel> GetSingle(FilterDefinition<LogsModel> filter = null, SortDefinition<LogsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<LogsModel>> GetList(FilterDefinition<LogsModel> filter, SortDefinition<LogsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<LogsModel>(FilterDefinition<LogsModel> filter, UpdateDefinition<LogsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<LogsModel>(FilterDefinition<LogsModel> filter = null) => await query.Delete(database, table, filter);

}
