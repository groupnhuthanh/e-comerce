﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.VariableDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.VariableDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.VariableDetail;

public class VariableDetailRepository : RepositoryBase<VariableDetailModel>, IVariableDetailRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.VariableDetail.ToString();
    public IQuery query;
    public VariableDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<VariableDetailModel> Post(VariableDetailModel doc) => await query.Post(database, table, doc);
    public async Task<VariableDetailModel> GetSingle(FilterDefinition<VariableDetailModel> filter = null, SortDefinition<VariableDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<VariableDetailModel>> GetList(FilterDefinition<VariableDetailModel> filter, SortDefinition<VariableDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<VariableDetailModel>(FilterDefinition<VariableDetailModel> filter, UpdateDefinition<VariableDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<VariableDetailModel>(FilterDefinition<VariableDetailModel> filter = null) => await query.Delete(database, table, filter);

}
