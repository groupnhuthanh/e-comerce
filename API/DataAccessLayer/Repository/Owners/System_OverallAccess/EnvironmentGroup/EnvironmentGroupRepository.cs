﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.EnvironmentGroup;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.EnvironmentGroup;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.EnvironmentGroup;

public class EnvironmentGroupRepository : RepositoryBase<EnvironmentGroupModel>, IEnvironmentGroupRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.Status.ToString();
    public IQuery query;
    public EnvironmentGroupRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<EnvironmentGroupModel> Post(EnvironmentGroupModel doc) => await query.Post(database, table, doc);
    public async Task<EnvironmentGroupModel> GetSingle(FilterDefinition<EnvironmentGroupModel> filter = null, SortDefinition<EnvironmentGroupModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<EnvironmentGroupModel>> GetList(FilterDefinition<EnvironmentGroupModel> filter, SortDefinition<EnvironmentGroupModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<EnvironmentGroupModel>(FilterDefinition<EnvironmentGroupModel> filter, UpdateDefinition<EnvironmentGroupModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<EnvironmentGroupModel>(FilterDefinition<EnvironmentGroupModel> filter = null) => await query.Delete(database, table, filter);

}
