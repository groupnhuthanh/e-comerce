﻿
using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.AccessToken;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.AccessToken;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.AccessToken;

public class AccessTokenRepository : RepositoryBase<AccessTokenModel>, IAccessTokenRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.AccessToken.ToString();
    public IQuery query;
    public AccessTokenRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<AccessTokenModel> Post(AccessTokenModel doc) => await query.Post(database, table, doc);
    public async Task<AccessTokenModel> GetSingle(FilterDefinition<AccessTokenModel> filter = null, SortDefinition<AccessTokenModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<AccessTokenModel>> GetList(FilterDefinition<AccessTokenModel> filter, SortDefinition<AccessTokenModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<AccessTokenModel>(FilterDefinition<AccessTokenModel> filter, UpdateDefinition<AccessTokenModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<AccessTokenModel>(FilterDefinition<AccessTokenModel> filter = null) => await query.Delete(database, table, filter);

}
