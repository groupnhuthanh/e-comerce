﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.System_OverallAccess.TypeServer;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.System_OverallAccess.TypeServer;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.System_OverallAccess.TypeServer;

public class TypeServerRepository : RepositoryBase<TypeServerModel>, ITypeServerRepository
{
    private readonly string database = Databases.System_OverallAccess.ToString();
    private readonly string table = System_OverallAccessTables.TypeServer.ToString();
    public IQuery query;
    public TypeServerRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TypeServerModel> Post(TypeServerModel doc) => await query.Post(database, table, doc);
    public async Task<TypeServerModel> GetSingle(FilterDefinition<TypeServerModel> filter = null, SortDefinition<TypeServerModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TypeServerModel>> GetList(FilterDefinition<TypeServerModel> filter, SortDefinition<TypeServerModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TypeServerModel>(FilterDefinition<TypeServerModel> filter, UpdateDefinition<TypeServerModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TypeServerModel>(FilterDefinition<TypeServerModel> filter = null) => await query.Delete(database, table, filter);

}
