﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderCustomerItemHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderCustomerItemHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Orders.OrderCustomerItemHistory;

public class OrderCustomerItemHistoryRepository : RepositoryBase<OrderCustomerItemHistoryModel>, IOrderCustomerItemHistoryRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.OrderCustomerItemHistory.ToString();
    public IQuery query;
    public OrderCustomerItemHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderCustomerItemHistoryModel> Post(OrderCustomerItemHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<OrderCustomerItemHistoryModel> GetSingle(FilterDefinition<OrderCustomerItemHistoryModel> filter = null, SortDefinition<OrderCustomerItemHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderCustomerItemHistoryModel>> GetList(FilterDefinition<OrderCustomerItemHistoryModel> filter, SortDefinition<OrderCustomerItemHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderCustomerItemHistoryModel>(FilterDefinition<OrderCustomerItemHistoryModel> filter, UpdateDefinition<OrderCustomerItemHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderCustomerItemHistoryModel>(FilterDefinition<OrderCustomerItemHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
