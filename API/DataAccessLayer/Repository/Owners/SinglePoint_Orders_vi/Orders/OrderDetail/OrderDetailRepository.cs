﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;

public class OrderDetailRepository : RepositoryBase<OrderDetailModel>, IOrderDetailRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.OrderDetail.ToString();
    public IQuery query;
    public OrderDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderDetailModel> Post(OrderDetailModel doc) => await query.Post(database, table, doc);
    public async Task<OrderDetailModel> GetSingle(FilterDefinition<OrderDetailModel> filter = null, SortDefinition<OrderDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderDetailModel>> GetList(FilterDefinition<OrderDetailModel> filter, SortDefinition<OrderDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderDetailModel>(FilterDefinition<OrderDetailModel> filter, UpdateDefinition<OrderDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderDetailModel>(FilterDefinition<OrderDetailModel> filter = null) => await query.Delete(database, table, filter);

}
