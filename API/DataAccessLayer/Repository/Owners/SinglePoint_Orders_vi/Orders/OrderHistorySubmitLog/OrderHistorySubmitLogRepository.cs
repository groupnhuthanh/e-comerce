﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderHistorySubmitLog;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderHistorySubmitLog;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Orders.OrderHistorySubmitLog;

public class OrderHistorySubmitLogRepository : RepositoryBase<OrderHistorySubmitLogModel>, IOrderHistorySubmitLogRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.OrderHistorySubmitLog.ToString();
    public IQuery query;
    public OrderHistorySubmitLogRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderHistorySubmitLogModel> Post(OrderHistorySubmitLogModel doc) => await query.Post(database, table, doc);
    public async Task<OrderHistorySubmitLogModel> GetSingle(FilterDefinition<OrderHistorySubmitLogModel> filter = null, SortDefinition<OrderHistorySubmitLogModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderHistorySubmitLogModel>> GetList(FilterDefinition<OrderHistorySubmitLogModel> filter, SortDefinition<OrderHistorySubmitLogModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderHistorySubmitLogModel>(FilterDefinition<OrderHistorySubmitLogModel> filter, UpdateDefinition<OrderHistorySubmitLogModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderHistorySubmitLogModel>(FilterDefinition<OrderHistorySubmitLogModel> filter = null) => await query.Delete(database, table, filter);

}
