﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderStatusLog;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderStatusLog;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Orders.OrderStatusLog;

public class OrderStatusLogRepository : RepositoryBase<OrderStatusLogModel>, IOrderStatusLogRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.OrderStatusLog.ToString();
    public IQuery query;
    public OrderStatusLogRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderStatusLogModel> Post(OrderStatusLogModel doc) => await query.Post(database, table, doc);
    public async Task<OrderStatusLogModel> GetSingle(FilterDefinition<OrderStatusLogModel> filter = null, SortDefinition<OrderStatusLogModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderStatusLogModel>> GetList(FilterDefinition<OrderStatusLogModel> filter, SortDefinition<OrderStatusLogModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderStatusLogModel>(FilterDefinition<OrderStatusLogModel> filter, UpdateDefinition<OrderStatusLogModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderStatusLogModel>(FilterDefinition<OrderStatusLogModel> filter = null) => await query.Delete(database, table, filter);


}
