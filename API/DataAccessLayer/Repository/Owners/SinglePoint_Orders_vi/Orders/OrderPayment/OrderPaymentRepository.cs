﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderPayment;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderPayment;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Orders.OrderPayment;

public class OrderPaymentRepository : RepositoryBase<OrderPaymentModel>, IOrderPaymentRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.OrderPayment.ToString();
    public IQuery query;
    public OrderPaymentRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderPaymentModel> Post(OrderPaymentModel doc) => await query.Post(database, table, doc);
    public async Task<OrderPaymentModel> GetSingle(FilterDefinition<OrderPaymentModel> filter = null, SortDefinition<OrderPaymentModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderPaymentModel>> GetList(FilterDefinition<OrderPaymentModel> filter, SortDefinition<OrderPaymentModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderPaymentModel>(FilterDefinition<OrderPaymentModel> filter, UpdateDefinition<OrderPaymentModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderPaymentModel>(FilterDefinition<OrderPaymentModel> filter = null) => await query.Delete(database, table, filter);


}
