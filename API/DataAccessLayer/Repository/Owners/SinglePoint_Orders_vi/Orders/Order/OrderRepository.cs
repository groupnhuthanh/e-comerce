﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.Order;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Orders.Order;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Orders.Order;

public class OrderRepository : RepositoryBase<OrderModel>, IOrderRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.Order.ToString();
    public IQuery query;
    public OrderRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderModel> Post(OrderModel doc) => await query.Post(database, table, doc);
    public async Task<OrderModel> GetSingle(FilterDefinition<OrderModel> filter = null, SortDefinition<OrderModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderModel>> GetList(FilterDefinition<OrderModel> filter, SortDefinition<OrderModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderModel>(FilterDefinition<OrderModel> filter, UpdateDefinition<OrderModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderModel>(FilterDefinition<OrderModel> filter = null) => await query.Delete(database, table, filter);

}
