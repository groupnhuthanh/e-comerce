﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Orders.OrderHistory;

public class OrderHistoryRepository : RepositoryBase<OrderHistoryModel>, IOrderHistoryRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.OrderHistory.ToString();
    public IQuery query;
    public OrderHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderHistoryModel> Post(OrderHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<OrderHistoryModel> GetSingle(FilterDefinition<OrderHistoryModel> filter = null, SortDefinition<OrderHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderHistoryModel>> GetList(FilterDefinition<OrderHistoryModel> filter, SortDefinition<OrderHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderHistoryModel>(FilterDefinition<OrderHistoryModel> filter, UpdateDefinition<OrderHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderHistoryModel>(FilterDefinition<OrderHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
