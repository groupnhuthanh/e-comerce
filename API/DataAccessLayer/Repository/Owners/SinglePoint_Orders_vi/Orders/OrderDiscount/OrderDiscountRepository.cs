﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Orders.OrderDiscount;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDiscount;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Orders.OrderDiscount;

public class OrderDiscountRepository : RepositoryBase<OrderDiscountModel>, IOrderDiscountRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.OrderDiscount.ToString();
    public IQuery query;
    public OrderDiscountRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderDiscountModel> Post(OrderDiscountModel doc) => await query.Post(database, table, doc);
    public async Task<OrderDiscountModel> GetSingle(FilterDefinition<OrderDiscountModel> filter = null, SortDefinition<OrderDiscountModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderDiscountModel>> GetList(FilterDefinition<OrderDiscountModel> filter, SortDefinition<OrderDiscountModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderDiscountModel>(FilterDefinition<OrderDiscountModel> filter, UpdateDefinition<OrderDiscountModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderDiscountModel>(FilterDefinition<OrderDiscountModel> filter = null) => await query.Delete(database, table, filter);

}
