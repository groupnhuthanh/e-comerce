﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPLogOrder;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPLogOrder;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPLogOrder;

public class ORAMPLogOrderRepository : RepositoryBase<ORAMPLogOrderModel>, IORAMPLogOrderRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.ORAMPLogOrder.ToString();
    public IQuery query;
    public ORAMPLogOrderRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ORAMPLogOrderModel> Post(ORAMPLogOrderModel doc) => await query.Post(database, table, doc);
    public async Task<ORAMPLogOrderModel> GetSingle(FilterDefinition<ORAMPLogOrderModel> filter = null, SortDefinition<ORAMPLogOrderModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ORAMPLogOrderModel>> GetList(FilterDefinition<ORAMPLogOrderModel> filter, SortDefinition<ORAMPLogOrderModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ORAMPLogOrderModel>(FilterDefinition<ORAMPLogOrderModel> filter, UpdateDefinition<ORAMPLogOrderModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ORAMPLogOrderModel>(FilterDefinition<ORAMPLogOrderModel> filter = null) => await query.Delete(database, table, filter);

}
