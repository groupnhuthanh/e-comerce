﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.ORAMP.ORProductLog;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORProductLog;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.ORAMP.ORProductLog;

public class ORProductLogRepository : RepositoryBase<ORProductLogModel>, IORProductLogRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.ORProductLog.ToString();
    public IQuery query;
    public ORProductLogRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ORProductLogModel> Post(ORProductLogModel doc) => await query.Post(database, table, doc);
    public async Task<ORProductLogModel> GetSingle(FilterDefinition<ORProductLogModel> filter = null, SortDefinition<ORProductLogModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ORProductLogModel>> GetList(FilterDefinition<ORProductLogModel> filter, SortDefinition<ORProductLogModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ORProductLogModel>(FilterDefinition<ORProductLogModel> filter, UpdateDefinition<ORProductLogModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ORProductLogModel>(FilterDefinition<ORProductLogModel> filter = null) => await query.Delete(database, table, filter);

}
