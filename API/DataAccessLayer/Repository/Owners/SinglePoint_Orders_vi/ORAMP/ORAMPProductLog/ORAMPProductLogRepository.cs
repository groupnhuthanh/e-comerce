﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPProductLog;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPProductLog;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPProductLog;

public class ORAMPProductLogRepository : RepositoryBase<ORAMPProductLogModel>, IORAMPProductLogRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.ORAMPProductLog.ToString();
    public IQuery query;
    public ORAMPProductLogRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ORAMPProductLogModel> Post(ORAMPProductLogModel doc) => await query.Post(database, table, doc);
    public async Task<ORAMPProductLogModel> GetSingle(FilterDefinition<ORAMPProductLogModel> filter = null, SortDefinition<ORAMPProductLogModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ORAMPProductLogModel>> GetList(FilterDefinition<ORAMPProductLogModel> filter, SortDefinition<ORAMPProductLogModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ORAMPProductLogModel>(FilterDefinition<ORAMPProductLogModel> filter, UpdateDefinition<ORAMPProductLogModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ORAMPProductLogModel>(FilterDefinition<ORAMPProductLogModel> filter = null) => await query.Delete(database, table, filter);

}
