﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.HistoryOrderStatus;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.HistoryOrderStatus;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.HistoryOrderStatus;

public class HistoryOrderStatusRepository : RepositoryBase<HistoryOrderStatusModel>, IHistoryOrderStatusRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.HistoryOrderStatus.ToString();
    public IQuery query;
    public HistoryOrderStatusRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<HistoryOrderStatusModel> Post(HistoryOrderStatusModel doc) => await query.Post(database, table, doc);
    public async Task<HistoryOrderStatusModel> GetSingle(FilterDefinition<HistoryOrderStatusModel> filter = null, SortDefinition<HistoryOrderStatusModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<HistoryOrderStatusModel>> GetList(FilterDefinition<HistoryOrderStatusModel> filter, SortDefinition<HistoryOrderStatusModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<HistoryOrderStatusModel>(FilterDefinition<HistoryOrderStatusModel> filter, UpdateDefinition<HistoryOrderStatusModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<HistoryOrderStatusModel>(FilterDefinition<HistoryOrderStatusModel> filter = null) => await query.Delete(database, table, filter);

}
