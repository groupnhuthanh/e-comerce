﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;

public class CashDrawerRepository : RepositoryBase<CashDrawerModel>, ICashDrawerRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.CashDrawer.ToString();
    public IQuery query;
    public CashDrawerRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CashDrawerModel> Post(CashDrawerModel doc) => await query.Post(database, table, doc);
    public async Task<CashDrawerModel> GetSingle(FilterDefinition<CashDrawerModel> filter = null, SortDefinition<CashDrawerModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CashDrawerModel>> GetList(FilterDefinition<CashDrawerModel> filter, SortDefinition<CashDrawerModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CashDrawerModel>(FilterDefinition<CashDrawerModel> filter, UpdateDefinition<CashDrawerModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CashDrawerModel>(FilterDefinition<CashDrawerModel> filter = null) => await query.Delete(database, table, filter);

}
