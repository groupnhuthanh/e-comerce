﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerSaleOrders;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerSaleOrders;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerSaleOrders;

public class CashDrawerSaleOrdersRepository : RepositoryBase<CashDrawerSaleOrdersModel>, ICashDrawerSaleOrdersRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.CashDrawerSaleOrders.ToString();
    public IQuery query;
    public CashDrawerSaleOrdersRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CashDrawerSaleOrdersModel> Post(CashDrawerSaleOrdersModel doc) => await query.Post(database, table, doc);
    public async Task<CashDrawerSaleOrdersModel> GetSingle(FilterDefinition<CashDrawerSaleOrdersModel> filter = null, SortDefinition<CashDrawerSaleOrdersModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CashDrawerSaleOrdersModel>> GetList(FilterDefinition<CashDrawerSaleOrdersModel> filter, SortDefinition<CashDrawerSaleOrdersModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CashDrawerSaleOrdersModel>(FilterDefinition<CashDrawerSaleOrdersModel> filter, UpdateDefinition<CashDrawerSaleOrdersModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CashDrawerSaleOrdersModel>(FilterDefinition<CashDrawerSaleOrdersModel> filter = null) => await query.Delete(database, table, filter);

}
