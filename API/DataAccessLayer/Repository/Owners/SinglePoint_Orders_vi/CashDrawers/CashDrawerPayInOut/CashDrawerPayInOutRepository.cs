﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerPayInOut;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerPayInOut;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawerPayInOut;

public class CashDrawerPayInOutRepository : RepositoryBase<CashDrawerPayInOutModel>, ICashDrawerPayInOutRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.CashDrawerPayInOut.ToString();
    public IQuery query;
    public CashDrawerPayInOutRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CashDrawerPayInOutModel> Post(CashDrawerPayInOutModel doc) => await query.Post(database, table, doc);
    public async Task<CashDrawerPayInOutModel> GetSingle(FilterDefinition<CashDrawerPayInOutModel> filter = null, SortDefinition<CashDrawerPayInOutModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CashDrawerPayInOutModel>> GetList(FilterDefinition<CashDrawerPayInOutModel> filter, SortDefinition<CashDrawerPayInOutModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CashDrawerPayInOutModel>(FilterDefinition<CashDrawerPayInOutModel> filter, UpdateDefinition<CashDrawerPayInOutModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CashDrawerPayInOutModel>(FilterDefinition<CashDrawerPayInOutModel> filter = null) => await query.Delete(database, table, filter);

}
