﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Orders_vi.Refunds.RefundOrders;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Orders_vi.Refunds.RefundOrders;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Orders_vi.Refunds.RefundOrders;

public class RefundOrdersRepository : RepositoryBase<RefundOrdersModel>, IRefundOrdersRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_Orders_viTables.RefundOrders.ToString();
    public IQuery query;
    public RefundOrdersRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<RefundOrdersModel> Post(RefundOrdersModel doc) => await query.Post(database, table, doc);
    public async Task<RefundOrdersModel> GetSingle(FilterDefinition<RefundOrdersModel> filter = null, SortDefinition<RefundOrdersModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<RefundOrdersModel>> GetList(FilterDefinition<RefundOrdersModel> filter, SortDefinition<RefundOrdersModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<RefundOrdersModel>(FilterDefinition<RefundOrdersModel> filter, UpdateDefinition<RefundOrdersModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<RefundOrdersModel>(FilterDefinition<RefundOrdersModel> filter = null) => await query.Delete(database, table, filter);

}
