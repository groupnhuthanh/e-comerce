﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.TableType;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.TableType;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.TableType;

public class TableTypeRepository : RepositoryBase<TableTypeModel>, ITableTypeRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.TableType.ToString();
    public IQuery query;
    public TableTypeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TableTypeModel> Post(TableTypeModel doc) => await query.Post(database, table, doc);
    public async Task<TableTypeModel> GetSingle(FilterDefinition<TableTypeModel> filter = null, SortDefinition<TableTypeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TableTypeModel>> GetList(FilterDefinition<TableTypeModel> filter, SortDefinition<TableTypeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TableTypeModel>(FilterDefinition<TableTypeModel> filter, UpdateDefinition<TableTypeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TableTypeModel>(FilterDefinition<TableTypeModel> filter = null) => await query.Delete(database, table, filter);

}
