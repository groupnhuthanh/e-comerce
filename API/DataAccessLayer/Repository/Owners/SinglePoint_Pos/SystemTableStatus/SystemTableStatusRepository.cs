﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.SystemTableStatus;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.SystemTableStatus;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.SystemTableStatus;

public class SystemTableStatusRepository : RepositoryBase<SystemTableStatusModel>, ISystemTableStatusRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.SystemTableStatus.ToString();
    public IQuery query;
    public SystemTableStatusRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SystemTableStatusModel> Post(SystemTableStatusModel doc) => await query.Post(database, table, doc);
    public async Task<SystemTableStatusModel> GetSingle(FilterDefinition<SystemTableStatusModel> filter = null, SortDefinition<SystemTableStatusModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SystemTableStatusModel>> GetList(FilterDefinition<SystemTableStatusModel> filter, SortDefinition<SystemTableStatusModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SystemTableStatusModel>(FilterDefinition<SystemTableStatusModel> filter, UpdateDefinition<SystemTableStatusModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SystemTableStatusModel>(FilterDefinition<SystemTableStatusModel> filter = null) => await query.Delete(database, table, filter);

}
