﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSSecondaryScreenVideo;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.POS.POSSecondaryScreenVideo;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.POS.POSSecondaryScreenVideo;

public class POSSecondaryScreenVideoRepository : RepositoryBase<POSSecondaryScreenVideoModel>, IPOSSecondaryScreenVideoRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.POSSecondaryScreenVideo.ToString();
    public IQuery query;
    public POSSecondaryScreenVideoRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<POSSecondaryScreenVideoModel> Post(POSSecondaryScreenVideoModel doc) => await query.Post(database, table, doc);
    public async Task<POSSecondaryScreenVideoModel> GetSingle(FilterDefinition<POSSecondaryScreenVideoModel> filter = null, SortDefinition<POSSecondaryScreenVideoModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<POSSecondaryScreenVideoModel>> GetList(FilterDefinition<POSSecondaryScreenVideoModel> filter, SortDefinition<POSSecondaryScreenVideoModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<POSSecondaryScreenVideoModel>(FilterDefinition<POSSecondaryScreenVideoModel> filter, UpdateDefinition<POSSecondaryScreenVideoModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<POSSecondaryScreenVideoModel>(FilterDefinition<POSSecondaryScreenVideoModel> filter = null) => await query.Delete(database, table, filter);

}
