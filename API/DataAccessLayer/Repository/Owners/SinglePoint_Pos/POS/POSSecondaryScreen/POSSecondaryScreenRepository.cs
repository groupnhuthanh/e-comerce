﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSSecondaryScreen;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.POS.POSSecondaryScreen;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.POS.POSSecondaryScreen;

public class POSSecondaryScreenRepository : RepositoryBase<POSSecondaryScreenModel>, IPOSSecondaryScreenRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.POSSecondaryScreen.ToString();
    public IQuery query;
    public POSSecondaryScreenRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<POSSecondaryScreenModel> Post(POSSecondaryScreenModel doc) => await query.Post(database, table, doc);
    public async Task<POSSecondaryScreenModel> GetSingle(FilterDefinition<POSSecondaryScreenModel> filter = null, SortDefinition<POSSecondaryScreenModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<POSSecondaryScreenModel>> GetList(FilterDefinition<POSSecondaryScreenModel> filter, SortDefinition<POSSecondaryScreenModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<POSSecondaryScreenModel>(FilterDefinition<POSSecondaryScreenModel> filter, UpdateDefinition<POSSecondaryScreenModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<POSSecondaryScreenModel>(FilterDefinition<POSSecondaryScreenModel> filter = null) => await query.Delete(database, table, filter);

}
