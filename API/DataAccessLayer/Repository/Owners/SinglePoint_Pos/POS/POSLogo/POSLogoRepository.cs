﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSLogo;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.POS.POSLogo;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.POS.POSLogo;

public class POSLogoRepository : RepositoryBase<POSLogoModel>, IPOSLogoRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.POSLogo.ToString();
    public IQuery query;
    public POSLogoRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<POSLogoModel> Post(POSLogoModel doc) => await query.Post(database, table, doc);
    public async Task<POSLogoModel> GetSingle(FilterDefinition<POSLogoModel> filter = null, SortDefinition<POSLogoModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<POSLogoModel>> GetList(FilterDefinition<POSLogoModel> filter, SortDefinition<POSLogoModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<POSLogoModel>(FilterDefinition<POSLogoModel> filter, UpdateDefinition<POSLogoModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<POSLogoModel>(FilterDefinition<POSLogoModel> filter = null) => await query.Delete(database, table, filter);

}
