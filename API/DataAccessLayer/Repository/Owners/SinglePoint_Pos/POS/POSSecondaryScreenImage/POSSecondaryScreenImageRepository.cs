﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.POS.POSSecondaryScreenImage;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.POS.POSSecondaryScreenImage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.POS.POSSecondaryScreenImage;

public class POSSecondaryScreenImageRepository : RepositoryBase<POSSecondaryScreenImageModel>, IPOSSecondaryScreenImageRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.POSSecondaryScreenImage.ToString();
    public IQuery query;
    public POSSecondaryScreenImageRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<POSSecondaryScreenImageModel> Post(POSSecondaryScreenImageModel doc) => await query.Post(database, table, doc);
    public async Task<POSSecondaryScreenImageModel> GetSingle(FilterDefinition<POSSecondaryScreenImageModel> filter = null, SortDefinition<POSSecondaryScreenImageModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<POSSecondaryScreenImageModel>> GetList(FilterDefinition<POSSecondaryScreenImageModel> filter, SortDefinition<POSSecondaryScreenImageModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<POSSecondaryScreenImageModel>(FilterDefinition<POSSecondaryScreenImageModel> filter, UpdateDefinition<POSSecondaryScreenImageModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<POSSecondaryScreenImageModel>(FilterDefinition<POSSecondaryScreenImageModel> filter = null) => await query.Delete(database, table, filter);

}
