﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.Floors.FloorTable;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.Floors.FloorTable;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.Floors.FloorTable;

public class FloorTableRepository : RepositoryBase<FloorTableModel>, IFloorTableRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.FloorTable.ToString();
    public IQuery query;
    public FloorTableRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FloorTableModel> Post(FloorTableModel doc) => await query.Post(database, table, doc);
    public async Task<FloorTableModel> GetSingle(FilterDefinition<FloorTableModel> filter = null, SortDefinition<FloorTableModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FloorTableModel>> GetList(FilterDefinition<FloorTableModel> filter, SortDefinition<FloorTableModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FloorTableModel>(FilterDefinition<FloorTableModel> filter, UpdateDefinition<FloorTableModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FloorTableModel>(FilterDefinition<FloorTableModel> filter = null) => await query.Delete(database, table, filter);

}
