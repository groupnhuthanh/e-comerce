﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.Floors.Floor;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.Floors.Floor;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.Floors.Floor;

public class FloorRepository : RepositoryBase<FloorModel>, IFloorRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.Floor.ToString();
    public IQuery query;
    public FloorRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FloorModel> Post(FloorModel doc) => await query.Post(database, table, doc);
    public async Task<FloorModel> GetSingle(FilterDefinition<FloorModel> filter = null, SortDefinition<FloorModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FloorModel>> GetList(FilterDefinition<FloorModel> filter, SortDefinition<FloorModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FloorModel>(FilterDefinition<FloorModel> filter, UpdateDefinition<FloorModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FloorModel>(FilterDefinition<FloorModel> filter = null) => await query.Delete(database, table, filter);

}
