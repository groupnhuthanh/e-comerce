﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.NamingType;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.NamingType;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.NamingType;

public class NamingTypeRepository : RepositoryBase<NamingTypeModel>, INamingTypeRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.NamingType.ToString();
    public IQuery query;
    public NamingTypeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<NamingTypeModel> Post(NamingTypeModel doc) => await query.Post(database, table, doc);
    public async Task<NamingTypeModel> GetSingle(FilterDefinition<NamingTypeModel> filter = null, SortDefinition<NamingTypeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<NamingTypeModel>> GetList(FilterDefinition<NamingTypeModel> filter, SortDefinition<NamingTypeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<NamingTypeModel>(FilterDefinition<NamingTypeModel> filter, UpdateDefinition<NamingTypeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<NamingTypeModel>(FilterDefinition<NamingTypeModel> filter = null) => await query.Delete(database, table, filter);

}
