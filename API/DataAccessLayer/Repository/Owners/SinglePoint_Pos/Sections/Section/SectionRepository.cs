﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.Sections.Section;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.Sections.Section;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.Sections.Section;

public class SectionRepository : RepositoryBase<SectionModel>, ISectionRepository

{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.Section.ToString();
    public IQuery query;
    public SectionRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SectionModel> Post(SectionModel doc) => await query.Post(database, table, doc);
    public async Task<SectionModel> GetSingle(FilterDefinition<SectionModel> filter = null, SortDefinition<SectionModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SectionModel>> GetList(FilterDefinition<SectionModel> filter, SortDefinition<SectionModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SectionModel>(FilterDefinition<SectionModel> filter, UpdateDefinition<SectionModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SectionModel>(FilterDefinition<SectionModel> filter = null) => await query.Delete(database, table, filter);

}
