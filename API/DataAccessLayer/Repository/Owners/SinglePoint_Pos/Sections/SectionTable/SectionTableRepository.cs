﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Pos.Sections.SectionTable;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Pos.Sections.SectionTable;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Pos.Sections.SectionTable;

public class SectionTableRepository : RepositoryBase<SectionTableModel>, ISectionTableRepository
{
    private readonly string database = Databases.SinglePoint_Pos.ToString();
    private readonly string table = SinglePoint_PosTables.SectionTable.ToString();
    public IQuery query;
    public SectionTableRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SectionTableModel> Post(SectionTableModel doc) => await query.Post(database, table, doc);
    public async Task<SectionTableModel> GetSingle(FilterDefinition<SectionTableModel> filter = null, SortDefinition<SectionTableModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SectionTableModel>> GetList(FilterDefinition<SectionTableModel> filter, SortDefinition<SectionTableModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SectionTableModel>(FilterDefinition<SectionTableModel> filter, UpdateDefinition<SectionTableModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SectionTableModel>(FilterDefinition<SectionTableModel> filter = null) => await query.Delete(database, table, filter);

}
