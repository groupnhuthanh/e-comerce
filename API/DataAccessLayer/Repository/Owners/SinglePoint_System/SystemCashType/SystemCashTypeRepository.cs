﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_System.SystemCashType;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_System.SystemCashType;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_System.SystemCashType;

public class SystemCashTypeRepository : RepositoryBase<SystemCashTypeModel>, ISystemCashTypeRepository
{

    private readonly string database = Databases.SinglePoint_System.ToString();
    private readonly string table = SinglePoint_SystemTables.SystemCashType.ToString();
    public IQuery query;
    public SystemCashTypeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SystemCashTypeModel> Post(SystemCashTypeModel doc) => await query.Post(database, table, doc);
    public async Task<SystemCashTypeModel> GetSingle(FilterDefinition<SystemCashTypeModel> filter = null, SortDefinition<SystemCashTypeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SystemCashTypeModel>> GetList(FilterDefinition<SystemCashTypeModel> filter, SortDefinition<SystemCashTypeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SystemCashTypeModel>(FilterDefinition<SystemCashTypeModel> filter, UpdateDefinition<SystemCashTypeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SystemCashTypeModel>(FilterDefinition<SystemCashTypeModel> filter = null) => await query.Delete(database, table, filter);

}
