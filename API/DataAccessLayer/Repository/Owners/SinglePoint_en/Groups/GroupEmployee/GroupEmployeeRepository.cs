﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupEmployee;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Groups.GroupEmployee;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Groups.GroupEmployee;

public class GroupEmployeeRepository : RepositoryBase<GroupEmployeeModel>, IGroupEmployeeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.GroupEmployee.ToString();
    public IQuery query;
    public GroupEmployeeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<GroupEmployeeModel> Post(GroupEmployeeModel doc) => await query.Post(database, table, doc);
    public async Task<GroupEmployeeModel> GetSingle(FilterDefinition<GroupEmployeeModel> filter = null, SortDefinition<GroupEmployeeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<GroupEmployeeModel>> GetList(FilterDefinition<GroupEmployeeModel> filter, SortDefinition<GroupEmployeeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<GroupEmployeeModel>(FilterDefinition<GroupEmployeeModel> filter, UpdateDefinition<GroupEmployeeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<GroupEmployeeModel>(FilterDefinition<GroupEmployeeModel> filter = null) => await query.Delete(database, table, filter);

}

