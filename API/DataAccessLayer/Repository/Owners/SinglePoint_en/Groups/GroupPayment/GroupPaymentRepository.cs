﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupPayment;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Groups.GroupPayment;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Groups.GroupPayment;

public class GroupPaymentRepository : RepositoryBase<GroupPaymentModel>, IGroupPaymentRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.GroupPayment.ToString();
    public IQuery query;
    public GroupPaymentRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<GroupPaymentModel> Post(GroupPaymentModel doc) => await query.Post(database, table, doc);
    public async Task<GroupPaymentModel> GetSingle(FilterDefinition<GroupPaymentModel> filter = null, SortDefinition<GroupPaymentModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<GroupPaymentModel>> GetList(FilterDefinition<GroupPaymentModel> filter, SortDefinition<GroupPaymentModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<GroupPaymentModel>(FilterDefinition<GroupPaymentModel> filter, UpdateDefinition<GroupPaymentModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<GroupPaymentModel>(FilterDefinition<GroupPaymentModel> filter = null) => await query.Delete(database, table, filter);

}
