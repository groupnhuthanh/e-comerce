﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupEvent;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Groups.GroupEvent;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Groups.GroupEvent;

public class GroupEventRepository : RepositoryBase<GroupEventModel>, IGroupEventRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.GroupEvent.ToString();
    public IQuery query;
    public GroupEventRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<GroupEventModel> Post(GroupEventModel doc) => await query.Post(database, table, doc);
    public async Task<GroupEventModel> GetSingle(FilterDefinition<GroupEventModel> filter = null, SortDefinition<GroupEventModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<GroupEventModel>> GetList(FilterDefinition<GroupEventModel> filter, SortDefinition<GroupEventModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<GroupEventModel>(FilterDefinition<GroupEventModel> filter, UpdateDefinition<GroupEventModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<GroupEventModel>(FilterDefinition<GroupEventModel> filter = null) => await query.Delete(database, table, filter);

}
