﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupDiscount;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Groups.GroupDiscount;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Groups.GroupDiscount;

public class GroupDiscountRepository : RepositoryBase<GroupDiscountModel>, IGroupDiscountRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.GroupDiscount.ToString();
    public IQuery query;
    public GroupDiscountRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<GroupDiscountModel> Post(GroupDiscountModel doc) => await query.Post(database, table, doc);
    public async Task<GroupDiscountModel> GetSingle(FilterDefinition<GroupDiscountModel> filter = null, SortDefinition<GroupDiscountModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<GroupDiscountModel>> GetList(FilterDefinition<GroupDiscountModel> filter, SortDefinition<GroupDiscountModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<GroupDiscountModel>(FilterDefinition<GroupDiscountModel> filter, UpdateDefinition<GroupDiscountModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<GroupDiscountModel>(FilterDefinition<GroupDiscountModel> filter = null) => await query.Delete(database, table, filter);

}
