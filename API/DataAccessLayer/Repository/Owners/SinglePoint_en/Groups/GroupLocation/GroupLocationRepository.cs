﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupLocation;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Groups.GroupLocation;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Groups.GroupLocation;

public class GroupLocationRepository : RepositoryBase<GroupLocationModel>, IGroupLocationRepository
{

    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.GroupLocation.ToString();
    public IQuery query;
    public GroupLocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<GroupLocationModel> Post(GroupLocationModel doc) => await query.Post(database, table, doc);
    public async Task<GroupLocationModel> GetSingle(FilterDefinition<GroupLocationModel> filter = null, SortDefinition<GroupLocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<GroupLocationModel>> GetList(FilterDefinition<GroupLocationModel> filter, SortDefinition<GroupLocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<GroupLocationModel>(FilterDefinition<GroupLocationModel> filter, UpdateDefinition<GroupLocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<GroupLocationModel>(FilterDefinition<GroupLocationModel> filter = null) => await query.Delete(database, table, filter);

}
