﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Groups.GroupDiscountItem;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Groups.GroupDiscountItem;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Groups.GroupDiscountItem;

public class GroupDiscountItemRepository : RepositoryBase<GroupDiscountItemModel>, IGroupDiscountItemRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.GroupDiscountItem.ToString();
    public IQuery query;
    public GroupDiscountItemRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<GroupDiscountItemModel> Post(GroupDiscountItemModel doc) => await query.Post(database, table, doc);
    public async Task<GroupDiscountItemModel> GetSingle(FilterDefinition<GroupDiscountItemModel> filter = null, SortDefinition<GroupDiscountItemModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<GroupDiscountItemModel>> GetList(FilterDefinition<GroupDiscountItemModel> filter, SortDefinition<GroupDiscountItemModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<GroupDiscountItemModel>(FilterDefinition<GroupDiscountItemModel> filter, UpdateDefinition<GroupDiscountItemModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<GroupDiscountItemModel>(FilterDefinition<GroupDiscountItemModel> filter = null) => await query.Delete(database, table, filter);

}
