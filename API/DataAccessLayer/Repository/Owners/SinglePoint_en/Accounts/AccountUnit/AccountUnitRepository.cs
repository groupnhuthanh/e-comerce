﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Accounts.AccountUnit;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Accounts.AccountUnit;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Accounts.AccountUnit;

public class AccountUnitRepository : RepositoryBase<AccountUnitModel>, IAccountUnitRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.AccountUnit.ToString();
    public IQuery query;
    public AccountUnitRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<AccountUnitModel> Post(AccountUnitModel doc) => await query.Post(database, table, doc);
    public async Task<AccountUnitModel> GetSingle(FilterDefinition<AccountUnitModel> filter = null, SortDefinition<AccountUnitModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<AccountUnitModel>> GetList(FilterDefinition<AccountUnitModel> filter, SortDefinition<AccountUnitModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<AccountUnitModel>(FilterDefinition<AccountUnitModel> filter, UpdateDefinition<AccountUnitModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<AccountUnitModel>(FilterDefinition<AccountUnitModel> filter = null) => await query.Delete(database, table, filter);

}
