﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Accounts.AccountGroup;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Accounts.AccountGroup;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Accounts.AccountGroup;

public class AccountGroupRepository : RepositoryBase<AccountGroupModel>, IAccountGroupRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.AccountGroup.ToString();
    public IQuery query;
    public AccountGroupRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<AccountGroupModel> Post(AccountGroupModel doc) => await query.Post(database, table, doc);
    public async Task<AccountGroupModel> GetSingle(FilterDefinition<AccountGroupModel> filter = null, SortDefinition<AccountGroupModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<AccountGroupModel>> GetList(FilterDefinition<AccountGroupModel> filter, SortDefinition<AccountGroupModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<AccountGroupModel>(FilterDefinition<AccountGroupModel> filter, UpdateDefinition<AccountGroupModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<AccountGroupModel>(FilterDefinition<AccountGroupModel> filter = null) => await query.Delete(database, table, filter);

}
