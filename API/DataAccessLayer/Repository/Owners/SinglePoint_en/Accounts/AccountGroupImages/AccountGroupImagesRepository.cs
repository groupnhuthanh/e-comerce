﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Accounts.AccountGroupImages;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Accounts.AccountGroupImages;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Accounts.AccountGroupImages;

public class AccountGroupImagesRepository : RepositoryBase<AccountGroupImagesModel>, IAccountGroupImagesRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.AccountGroupImages.ToString();
    public IQuery query;
    public AccountGroupImagesRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<AccountGroupImagesModel> Post(AccountGroupImagesModel doc) => await query.Post(database, table, doc);
    public async Task<AccountGroupImagesModel> GetSingle(FilterDefinition<AccountGroupImagesModel> filter = null, SortDefinition<AccountGroupImagesModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<AccountGroupImagesModel>> GetList(FilterDefinition<AccountGroupImagesModel> filter, SortDefinition<AccountGroupImagesModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<AccountGroupImagesModel>(FilterDefinition<AccountGroupImagesModel> filter, UpdateDefinition<AccountGroupImagesModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<AccountGroupImagesModel>(FilterDefinition<AccountGroupImagesModel> filter = null) => await query.Delete(database, table, filter);

}
