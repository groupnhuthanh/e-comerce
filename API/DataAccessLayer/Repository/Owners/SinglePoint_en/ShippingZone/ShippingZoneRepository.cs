﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.ShippingZone;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.ShippingZone;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.ShippingZone;

public class ShippingZoneRepository : RepositoryBase<ShippingZoneModel>, IShippingZoneRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ShippingZone.ToString();
    public IQuery query;
    public ShippingZoneRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ShippingZoneModel> Post(ShippingZoneModel doc) => await query.Post(database, table, doc);
    public async Task<ShippingZoneModel> GetSingle(FilterDefinition<ShippingZoneModel> filter = null, SortDefinition<ShippingZoneModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ShippingZoneModel>> GetList(FilterDefinition<ShippingZoneModel> filter, SortDefinition<ShippingZoneModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ShippingZoneModel>(FilterDefinition<ShippingZoneModel> filter, UpdateDefinition<ShippingZoneModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ShippingZoneModel>(FilterDefinition<ShippingZoneModel> filter = null) => await query.Delete(database, table, filter);

}
