﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.DeliveryTime;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.ExtendModels.Times;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.DeliveryTime;

public class DeliveryTimeRepository : RepositoryBase<DeliveryTimeModel>, IDeliveryTimeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DeliveryTime.ToString();
    public IQuery query;
    public DeliveryTimeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeliveryTimeModel> Post(DeliveryTimeModel doc) => await query.Post(database, table, doc);
    public async Task<DeliveryTimeModel> GetSingle(FilterDefinition<DeliveryTimeModel> filter = null, SortDefinition<DeliveryTimeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeliveryTimeModel>> GetList(FilterDefinition<DeliveryTimeModel> filter, SortDefinition<DeliveryTimeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeliveryTimeModel>(FilterDefinition<DeliveryTimeModel> filter, UpdateDefinition<DeliveryTimeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeliveryTimeModel>(FilterDefinition<DeliveryTimeModel> filter = null) => await query.Delete(database, table, filter);

}
