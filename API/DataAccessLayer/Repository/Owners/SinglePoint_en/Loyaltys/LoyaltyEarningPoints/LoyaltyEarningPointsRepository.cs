﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Loyaltys.LoyaltyEarningPoints;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Loyaltys.LoyaltyEarningPoints;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Loyaltys.LoyaltyEarningPoints;

public class LoyaltyEarningPointsRepository : RepositoryBase<LoyaltyEarningPointsModel>, ILoyaltyEarningPointsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.LoyaltyEarningPoints.ToString();
    public IQuery query;
    public LoyaltyEarningPointsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<LoyaltyEarningPointsModel> Post(LoyaltyEarningPointsModel doc) => await query.Post(database, table, doc);
    public async Task<LoyaltyEarningPointsModel> GetSingle(FilterDefinition<LoyaltyEarningPointsModel> filter = null, SortDefinition<LoyaltyEarningPointsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<LoyaltyEarningPointsModel>> GetList(FilterDefinition<LoyaltyEarningPointsModel> filter, SortDefinition<LoyaltyEarningPointsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<LoyaltyEarningPointsModel>(FilterDefinition<LoyaltyEarningPointsModel> filter, UpdateDefinition<LoyaltyEarningPointsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<LoyaltyEarningPointsModel>(FilterDefinition<LoyaltyEarningPointsModel> filter = null) => await query.Delete(database, table, filter);

}
