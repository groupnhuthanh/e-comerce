﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Loyaltys.LoyaltyReward;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Loyaltys.LoyaltyReward;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Loyaltys.LoyaltyReward;

public class LoyaltyRewardRepository : RepositoryBase<LoyaltyRewardModel>, ILoyaltyRewardRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.LoyaltyReward.ToString();
    public IQuery query;
    public LoyaltyRewardRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<LoyaltyRewardModel> Post(LoyaltyRewardModel doc) => await query.Post(database, table, doc);
    public async Task<LoyaltyRewardModel> GetSingle(FilterDefinition<LoyaltyRewardModel> filter = null, SortDefinition<LoyaltyRewardModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<LoyaltyRewardModel>> GetList(FilterDefinition<LoyaltyRewardModel> filter, SortDefinition<LoyaltyRewardModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<LoyaltyRewardModel>(FilterDefinition<LoyaltyRewardModel> filter, UpdateDefinition<LoyaltyRewardModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<LoyaltyRewardModel>(FilterDefinition<LoyaltyRewardModel> filter = null) => await query.Delete(database, table, filter);

}
