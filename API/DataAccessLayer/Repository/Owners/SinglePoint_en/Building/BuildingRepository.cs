﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Building;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Building;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Building;

public class BuildingRepository : RepositoryBase<BuildingModel>, IBuildingRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Building.ToString();
    public IQuery query;
    public BuildingRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<BuildingModel> Post(BuildingModel doc) => await query.Post(database, table, doc);
    public async Task<BuildingModel> GetSingle(FilterDefinition<BuildingModel> filter = null, SortDefinition<BuildingModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<BuildingModel>> GetList(FilterDefinition<BuildingModel> filter, SortDefinition<BuildingModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<BuildingModel>(FilterDefinition<BuildingModel> filter, UpdateDefinition<BuildingModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<BuildingModel>(FilterDefinition<BuildingModel> filter = null) => await query.Delete(database, table, filter);

}
