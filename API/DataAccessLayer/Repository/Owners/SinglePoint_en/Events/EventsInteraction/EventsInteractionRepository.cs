﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Events.EventsInteraction;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Events.EventsInteraction;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Events.EventsInteraction;

public class EventsInteractionRepository : RepositoryBase<EventsInteractionModel>, IEventsInteractionRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.EventsInteraction.ToString();
    public IQuery query;
    public EventsInteractionRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<EventsInteractionModel> Post(EventsInteractionModel doc) => await query.Post(database, table, doc);
    public async Task<EventsInteractionModel> GetSingle(FilterDefinition<EventsInteractionModel> filter = null, SortDefinition<EventsInteractionModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<EventsInteractionModel>> GetList(FilterDefinition<EventsInteractionModel> filter, SortDefinition<EventsInteractionModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<EventsInteractionModel>(FilterDefinition<EventsInteractionModel> filter, UpdateDefinition<EventsInteractionModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<EventsInteractionModel>(FilterDefinition<EventsInteractionModel> filter = null) => await query.Delete(database, table, filter);

}
