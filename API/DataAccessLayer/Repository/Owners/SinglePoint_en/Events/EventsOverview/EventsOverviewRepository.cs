﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Events.EventsOverview;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Events.EventsOverview;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Events.EventsOverview;

public class EventsOverviewRepository : RepositoryBase<EventsOverviewModel>, IEventsOverviewRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.EventsOverview.ToString();
    public IQuery query;
    public EventsOverviewRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<EventsOverviewModel> Post(EventsOverviewModel doc) => await query.Post(database, table, doc);
    public async Task<EventsOverviewModel> GetSingle(FilterDefinition<EventsOverviewModel> filter = null, SortDefinition<EventsOverviewModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<EventsOverviewModel>> GetList(FilterDefinition<EventsOverviewModel> filter, SortDefinition<EventsOverviewModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<EventsOverviewModel>(FilterDefinition<EventsOverviewModel> filter, UpdateDefinition<EventsOverviewModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<EventsOverviewModel>(FilterDefinition<EventsOverviewModel> filter = null) => await query.Delete(database, table, filter);

}
