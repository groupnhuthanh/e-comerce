﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Categorys.CategoryProduct;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Categorys.CategoryProduct;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Categorys.CategoryProduct;

public class CategoryProductRepository : RepositoryBase<CategoryProductModel>, ICategoryProductRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CategoryProduct.ToString();
    public IQuery query;
    public CategoryProductRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CategoryProductModel> Post(CategoryProductModel doc) => await query.Post(database, table, doc);
    public async Task<CategoryProductModel> GetSingle(FilterDefinition<CategoryProductModel> filter = null, SortDefinition<CategoryProductModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CategoryProductModel>> GetList(FilterDefinition<CategoryProductModel> filter, SortDefinition<CategoryProductModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CategoryProductModel>(FilterDefinition<CategoryProductModel> filter, UpdateDefinition<CategoryProductModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CategoryProductModel>(FilterDefinition<CategoryProductModel> filter = null) => await query.Delete(database, table, filter);

}
