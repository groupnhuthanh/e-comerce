﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Categorys.Category;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Categorys.Category;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Categorys.Category;

public class CategoryRepository : RepositoryBase<CategoryModel>, ICategoryRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Category.ToString();
    public IQuery query;
    public CategoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CategoryModel> Post(CategoryModel doc) => await query.Post(database, table, doc);
    public async Task<CategoryModel> GetSingle(FilterDefinition<CategoryModel> filter = null, SortDefinition<CategoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CategoryModel>> GetList(FilterDefinition<CategoryModel> filter, SortDefinition<CategoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CategoryModel>(FilterDefinition<CategoryModel> filter, UpdateDefinition<CategoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CategoryModel>(FilterDefinition<CategoryModel> filter = null) => await query.Delete(database, table, filter);


}
