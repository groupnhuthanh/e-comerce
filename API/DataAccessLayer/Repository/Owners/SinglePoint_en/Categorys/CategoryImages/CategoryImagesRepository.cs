﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Categorys.CategoryImages;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Categorys.CategoryImages;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Categorys.CategoryImages;

public class CategoryImagesRepository : RepositoryBase<CategoryImagesModel>, ICategoryImagesRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CategoryImages.ToString();
    public IQuery query;
    public CategoryImagesRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CategoryImagesModel> Post(CategoryImagesModel doc) => await query.Post(database, table, doc);
    public async Task<CategoryImagesModel> GetSingle(FilterDefinition<CategoryImagesModel> filter = null, SortDefinition<CategoryImagesModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CategoryImagesModel>> GetList(FilterDefinition<CategoryImagesModel> filter, SortDefinition<CategoryImagesModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CategoryImagesModel>(FilterDefinition<CategoryImagesModel> filter, UpdateDefinition<CategoryImagesModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CategoryImagesModel>(FilterDefinition<CategoryImagesModel> filter = null) => await query.Delete(database, table, filter);

}
