﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Gifts.GiftCardDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Gifts.GiftCardDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Gifts.GiftCardDetail;

public class GiftCardDetailRepository : RepositoryBase<GiftCardDetailModel>, IGiftCardDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.GiftCardDetail.ToString();
    public IQuery query;
    public GiftCardDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<GiftCardDetailModel> Post(GiftCardDetailModel doc) => await query.Post(database, table, doc);
    public async Task<GiftCardDetailModel> GetSingle(FilterDefinition<GiftCardDetailModel> filter = null, SortDefinition<GiftCardDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<GiftCardDetailModel>> GetList(FilterDefinition<GiftCardDetailModel> filter, SortDefinition<GiftCardDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<GiftCardDetailModel>(FilterDefinition<GiftCardDetailModel> filter, UpdateDefinition<GiftCardDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<GiftCardDetailModel>(FilterDefinition<GiftCardDetailModel> filter = null) => await query.Delete(database, table, filter);

}
