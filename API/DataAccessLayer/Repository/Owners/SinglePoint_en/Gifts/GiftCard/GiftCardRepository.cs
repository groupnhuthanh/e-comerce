﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Gifts.GiftCard;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Gifts.GiftCard;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Gifts.GiftCard;

public class GiftCardRepository : RepositoryBase<GiftCardModel>, IGiftCardRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.GiftCard.ToString();
    public IQuery query;
    public GiftCardRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<GiftCardModel> Post(GiftCardModel doc) => await query.Post(database, table, doc);
    public async Task<GiftCardModel> GetSingle(FilterDefinition<GiftCardModel> filter = null, SortDefinition<GiftCardModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<GiftCardModel>> GetList(FilterDefinition<GiftCardModel> filter, SortDefinition<GiftCardModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<GiftCardModel>(FilterDefinition<GiftCardModel> filter, UpdateDefinition<GiftCardModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<GiftCardModel>(FilterDefinition<GiftCardModel> filter = null) => await query.Delete(database, table, filter);

}
