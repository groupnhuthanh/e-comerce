﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Assets;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Assets;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Assets;

public class AssetsRepository : RepositoryBase<AssetsModel>, IAssetsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Assets.ToString();
    public IQuery query;
    public AssetsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<AssetsModel> Post(AssetsModel doc) => await query.Post(database, table, doc);
    public async Task<AssetsModel> GetSingle(FilterDefinition<AssetsModel> filter = null, SortDefinition<AssetsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<AssetsModel>> GetList(FilterDefinition<AssetsModel> filter, SortDefinition<AssetsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<AssetsModel>(FilterDefinition<AssetsModel> filter, UpdateDefinition<AssetsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<AssetsModel>(FilterDefinition<AssetsModel> filter = null) => await query.Delete(database, table, filter);

}
