﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.UserRewards.UserReward;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.UserRewards.UserReward;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.UserRewards.UserReward;

public class UserRewardRepository : RepositoryBase<UserRewardModel>, IUserRewardRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.UserRewards.ToString();
    public IQuery query;
    public UserRewardRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<UserRewardModel> Post(UserRewardModel doc) => await query.Post(database, table, doc);
    public async Task<UserRewardModel> GetSingle(FilterDefinition<UserRewardModel> filter = null, SortDefinition<UserRewardModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<UserRewardModel>> GetList(FilterDefinition<UserRewardModel> filter, SortDefinition<UserRewardModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<UserRewardModel>(FilterDefinition<UserRewardModel> filter, UpdateDefinition<UserRewardModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<UserRewardModel>(FilterDefinition<UserRewardModel> filter = null) => await query.Delete(database, table, filter);

}
