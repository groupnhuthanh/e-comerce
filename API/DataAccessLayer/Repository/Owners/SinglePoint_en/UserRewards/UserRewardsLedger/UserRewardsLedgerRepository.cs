﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.UserRewards.UserRewardsLedger;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.UserRewards.UserRewardsLedger;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.UserRewards.UserRewardsLedger;

public class UserRewardsLedgerRepository : RepositoryBase<UserRewardsLedgerModel>, IUserRewardsLedgerRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.UserRewardsLedger.ToString();
    public IQuery query;
    public UserRewardsLedgerRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<UserRewardsLedgerModel> Post(UserRewardsLedgerModel doc) => await query.Post(database, table, doc);
    public async Task<UserRewardsLedgerModel> GetSingle(FilterDefinition<UserRewardsLedgerModel> filter = null, SortDefinition<UserRewardsLedgerModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<UserRewardsLedgerModel>> GetList(FilterDefinition<UserRewardsLedgerModel> filter, SortDefinition<UserRewardsLedgerModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<UserRewardsLedgerModel>(FilterDefinition<UserRewardsLedgerModel> filter, UpdateDefinition<UserRewardsLedgerModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<UserRewardsLedgerModel>(FilterDefinition<UserRewardsLedgerModel> filter = null) => await query.Delete(database, table, filter);

}
