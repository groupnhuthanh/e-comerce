﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.UserRewards.UserRewardsList;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.UserRewards.UserRewardsList;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.UserRewards.UserRewardsList;

public class UserRewardsListRepository : RepositoryBase<UserRewardsListModel>, IUserRewardsListRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.UserRewardsList.ToString();
    public IQuery query;
    public UserRewardsListRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<UserRewardsListModel> Post(UserRewardsListModel doc) => await query.Post(database, table, doc);
    public async Task<UserRewardsListModel> GetSingle(FilterDefinition<UserRewardsListModel> filter = null, SortDefinition<UserRewardsListModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<UserRewardsListModel>> GetList(FilterDefinition<UserRewardsListModel> filter, SortDefinition<UserRewardsListModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<UserRewardsListModel>(FilterDefinition<UserRewardsListModel> filter, UpdateDefinition<UserRewardsListModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<UserRewardsListModel>(FilterDefinition<UserRewardsListModel> filter = null) => await query.Delete(database, table, filter);

}
