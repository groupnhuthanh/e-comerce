﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.CompressFile;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.CompressFile;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.CompressFile;

public class CompressFileRepository : RepositoryBase<CompressFileModel>, ICompressFileRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CompressFile.ToString();
    public IQuery query;
    public CompressFileRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CompressFileModel> Post(CompressFileModel doc) => await query.Post(database, table, doc);
    public async Task<CompressFileModel> GetSingle(FilterDefinition<CompressFileModel> filter = null, SortDefinition<CompressFileModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CompressFileModel>> GetList(FilterDefinition<CompressFileModel> filter, SortDefinition<CompressFileModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CompressFileModel>(FilterDefinition<CompressFileModel> filter, UpdateDefinition<CompressFileModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CompressFileModel>(FilterDefinition<CompressFileModel> filter = null) => await query.Delete(database, table, filter);

}
