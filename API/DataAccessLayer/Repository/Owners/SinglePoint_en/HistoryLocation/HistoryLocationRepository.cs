﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.HistoryLocation;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.HistoryLocation;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.HistoryLocation;

public class HistoryLocationRepository : RepositoryBase<HistoryLocationModel>, IHistoryLocationRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.HistoryLocation.ToString();
    public IQuery query;
    public HistoryLocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<HistoryLocationModel> Post(HistoryLocationModel doc) => await query.Post(database, table, doc);
    public async Task<HistoryLocationModel> GetSingle(FilterDefinition<HistoryLocationModel> filter = null, SortDefinition<HistoryLocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<HistoryLocationModel>> GetList(FilterDefinition<HistoryLocationModel> filter, SortDefinition<HistoryLocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<HistoryLocationModel>(FilterDefinition<HistoryLocationModel> filter, UpdateDefinition<HistoryLocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<HistoryLocationModel>(FilterDefinition<HistoryLocationModel> filter = null) => await query.Delete(database, table, filter);

}
