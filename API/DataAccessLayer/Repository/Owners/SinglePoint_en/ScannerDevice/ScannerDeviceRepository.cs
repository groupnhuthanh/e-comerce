﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.ScannerDevice;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.ScannerDevice;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.ScannerDevice;

public class ScannerDeviceRepository : RepositoryBase<ScannerDeviceModel>, IScannerDeviceRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ScannerDevice.ToString();
    public IQuery query;
    public ScannerDeviceRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ScannerDeviceModel> Post(ScannerDeviceModel doc) => await query.Post(database, table, doc);
    public async Task<ScannerDeviceModel> GetSingle(FilterDefinition<ScannerDeviceModel> filter = null, SortDefinition<ScannerDeviceModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ScannerDeviceModel>> GetList(FilterDefinition<ScannerDeviceModel> filter, SortDefinition<ScannerDeviceModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ScannerDeviceModel>(FilterDefinition<ScannerDeviceModel> filter, UpdateDefinition<ScannerDeviceModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ScannerDeviceModel>(FilterDefinition<ScannerDeviceModel> filter = null) => await query.Delete(database, table, filter);

}
