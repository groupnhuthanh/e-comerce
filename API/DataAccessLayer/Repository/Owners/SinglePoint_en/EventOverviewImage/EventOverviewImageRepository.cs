﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.EventOverviewImage;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.EventOverviewImage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.EventOverviewImage;

public class EventOverviewImageRepository : RepositoryBase<EventOverviewImageModel>, IEventOverviewImageRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.EventOverviewImage.ToString();
    public IQuery query;
    public EventOverviewImageRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<EventOverviewImageModel> Post(EventOverviewImageModel doc) => await query.Post(database, table, doc);
    public async Task<EventOverviewImageModel> GetSingle(FilterDefinition<EventOverviewImageModel> filter = null, SortDefinition<EventOverviewImageModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<EventOverviewImageModel>> GetList(FilterDefinition<EventOverviewImageModel> filter, SortDefinition<EventOverviewImageModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<EventOverviewImageModel>(FilterDefinition<EventOverviewImageModel> filter, UpdateDefinition<EventOverviewImageModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<EventOverviewImageModel>(FilterDefinition<EventOverviewImageModel> filter = null) => await query.Delete(database, table, filter);

}
