﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Orders.OrdersSettingsStatus;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Orders.OrdersSettingsStatus;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Orders.OrdersSettingsStatus;

public class OrdersSettingsStatusRepository : RepositoryBase<OrdersSettingsStatusModel>, IOrdersSettingsStatusRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.OrdersSettingsStatus.ToString();
    public IQuery query;
    public OrdersSettingsStatusRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrdersSettingsStatusModel> Post(OrdersSettingsStatusModel doc) => await query.Post(database, table, doc);
    public async Task<OrdersSettingsStatusModel> GetSingle(FilterDefinition<OrdersSettingsStatusModel> filter = null, SortDefinition<OrdersSettingsStatusModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrdersSettingsStatusModel>> GetList(FilterDefinition<OrdersSettingsStatusModel> filter, SortDefinition<OrdersSettingsStatusModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrdersSettingsStatusModel>(FilterDefinition<OrdersSettingsStatusModel> filter, UpdateDefinition<OrdersSettingsStatusModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrdersSettingsStatusModel>(FilterDefinition<OrdersSettingsStatusModel> filter = null) => await query.Delete(database, table, filter);

}
