﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Orders.OrderStatusDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Orders.OrderStatusDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Orders.OrderStatusDetail;

public class OrderStatusDetailRepository : RepositoryBase<OrderStatusDetailModel>, IOrderStatusDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.OrderStatusDetail.ToString();
    public IQuery query;
    public OrderStatusDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderStatusDetailModel> Post(OrderStatusDetailModel doc) => await query.Post(database, table, doc);
    public async Task<OrderStatusDetailModel> GetSingle(FilterDefinition<OrderStatusDetailModel> filter = null, SortDefinition<OrderStatusDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderStatusDetailModel>> GetList(FilterDefinition<OrderStatusDetailModel> filter, SortDefinition<OrderStatusDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderStatusDetailModel>(FilterDefinition<OrderStatusDetailModel> filter, UpdateDefinition<OrderStatusDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderStatusDetailModel>(FilterDefinition<OrderStatusDetailModel> filter = null) => await query.Delete(database, table, filter);

}
