﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FormatSettings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.FormatSettings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.FormatSettings;

public class FormatSettingsRepository : RepositoryBase<FormatSettingsModel>, IFormatSettingsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.FormatSettings.ToString();
    public IQuery query;
    public FormatSettingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FormatSettingsModel> Post(FormatSettingsModel doc) => await query.Post(database, table, doc);
    public async Task<FormatSettingsModel> GetSingle(FilterDefinition<FormatSettingsModel> filter = null, SortDefinition<FormatSettingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FormatSettingsModel>> GetList(FilterDefinition<FormatSettingsModel> filter, SortDefinition<FormatSettingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FormatSettingsModel>(FilterDefinition<FormatSettingsModel> filter, UpdateDefinition<FormatSettingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FormatSettingsModel>(FilterDefinition<FormatSettingsModel> filter = null) => await query.Delete(database, table, filter);

}
