﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Marketing.MarketingMessage;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Marketing.MarketingMessage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Marketing.MarketingMessage;

public class MarketingMessageRepository : RepositoryBase<MarketingMessageModel>, IMarketingMessageRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.MarketingMessage.ToString();
    public IQuery query;
    public MarketingMessageRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<MarketingMessageModel> Post(MarketingMessageModel doc) => await query.Post(database, table, doc);
    public async Task<MarketingMessageModel> GetSingle(FilterDefinition<MarketingMessageModel> filter = null, SortDefinition<MarketingMessageModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<MarketingMessageModel>> GetList(FilterDefinition<MarketingMessageModel> filter, SortDefinition<MarketingMessageModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<MarketingMessageModel>(FilterDefinition<MarketingMessageModel> filter, UpdateDefinition<MarketingMessageModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<MarketingMessageModel>(FilterDefinition<MarketingMessageModel> filter = null) => await query.Delete(database, table, filter);

}
