﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Marketing.MarketingPoll;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Marketing.MarketingPoll;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Marketing.MarketingPoll;

public class MarketingPollRepository : RepositoryBase<MarketingPollModel>, IMarketingPollRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.MarketingPoll.ToString();
    public IQuery query;
    public MarketingPollRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<MarketingPollModel> Post(MarketingPollModel doc) => await query.Post(database, table, doc);
    public async Task<MarketingPollModel> GetSingle(FilterDefinition<MarketingPollModel> filter = null, SortDefinition<MarketingPollModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<MarketingPollModel>> GetList(FilterDefinition<MarketingPollModel> filter, SortDefinition<MarketingPollModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<MarketingPollModel>(FilterDefinition<MarketingPollModel> filter, UpdateDefinition<MarketingPollModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<MarketingPollModel>(FilterDefinition<MarketingPollModel> filter = null) => await query.Delete(database, table, filter);

}
