﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Modifiers.ModifierItemPriceOverride;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Modifiers.ModifierItemPriceOverride;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Modifiers.ModifierItemPriceOverride;

public class ModifierItemPriceOverrideRepository : RepositoryBase<ModifierItemPriceOverrideModel>, IModifierItemPriceOverrideRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ModifierItemPriceOverride.ToString();
    public IQuery query;
    public ModifierItemPriceOverrideRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ModifierItemPriceOverrideModel> Post(ModifierItemPriceOverrideModel doc) => await query.Post(database, table, doc);
    public async Task<ModifierItemPriceOverrideModel> GetSingle(FilterDefinition<ModifierItemPriceOverrideModel> filter = null, SortDefinition<ModifierItemPriceOverrideModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ModifierItemPriceOverrideModel>> GetList(FilterDefinition<ModifierItemPriceOverrideModel> filter, SortDefinition<ModifierItemPriceOverrideModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ModifierItemPriceOverrideModel>(FilterDefinition<ModifierItemPriceOverrideModel> filter, UpdateDefinition<ModifierItemPriceOverrideModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ModifierItemPriceOverrideModel>(FilterDefinition<ModifierItemPriceOverrideModel> filter = null) => await query.Delete(database, table, filter);

}
