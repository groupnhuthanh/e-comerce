﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Modifiers.ModifierItem;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Modifiers.ModifierItem;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Modifiers.ModifierItem;

public class ModifierItemRepository : RepositoryBase<ModifierItemModel>, IModifierItemRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ModifierItem.ToString();
    public IQuery query;
    public ModifierItemRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ModifierItemModel> Post(ModifierItemModel doc) => await query.Post(database, table, doc);
    public async Task<ModifierItemModel> GetSingle(FilterDefinition<ModifierItemModel> filter = null, SortDefinition<ModifierItemModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ModifierItemModel>> GetList(FilterDefinition<ModifierItemModel> filter, SortDefinition<ModifierItemModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ModifierItemModel>(FilterDefinition<ModifierItemModel> filter, UpdateDefinition<ModifierItemModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ModifierItemModel>(FilterDefinition<ModifierItemModel> filter = null) => await query.Delete(database, table, filter);

}
