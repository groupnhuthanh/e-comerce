﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Modifiers.Modifier;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Modifiers.Modifier;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Modifiers.Modifier;

public class ModifierRepository : RepositoryBase<ModifierModel>, IModifierRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Modifier.ToString();
    public IQuery query;
    public ModifierRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ModifierModel> Post(ModifierModel doc) => await query.Post(database, table, doc);
    public async Task<ModifierModel> GetSingle(FilterDefinition<ModifierModel> filter = null, SortDefinition<ModifierModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ModifierModel>> GetList(FilterDefinition<ModifierModel> filter, SortDefinition<ModifierModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ModifierModel>(FilterDefinition<ModifierModel> filter, UpdateDefinition<ModifierModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ModifierModel>(FilterDefinition<ModifierModel> filter = null) => await query.Delete(database, table, filter);

}
