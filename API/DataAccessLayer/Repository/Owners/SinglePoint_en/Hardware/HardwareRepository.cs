﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Hardware;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Hardware;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Hardware;

public class HardwareRepository : RepositoryBase<HardwareModel>, IHardwareRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Hardware.ToString();
    public IQuery query;
    public HardwareRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<HardwareModel> Post(HardwareModel doc) => await query.Post(database, table, doc);
    public async Task<HardwareModel> GetSingle(FilterDefinition<HardwareModel> filter = null, SortDefinition<HardwareModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<HardwareModel>> GetList(FilterDefinition<HardwareModel> filter, SortDefinition<HardwareModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<HardwareModel>(FilterDefinition<HardwareModel> filter, UpdateDefinition<HardwareModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<HardwareModel>(FilterDefinition<HardwareModel> filter = null) => await query.Delete(database, table, filter);

}
