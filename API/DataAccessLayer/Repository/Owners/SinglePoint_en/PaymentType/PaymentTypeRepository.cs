﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.PaymentType;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.PaymentType;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.PaymentType;

public class PaymentTypeRepository : RepositoryBase<PaymentTypeModel>, IPaymentTypeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.PaymentType.ToString();
    public IQuery query;
    public PaymentTypeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PaymentTypeModel> Post(PaymentTypeModel doc) => await query.Post(database, table, doc);
    public async Task<PaymentTypeModel> GetSingle(FilterDefinition<PaymentTypeModel> filter = null, SortDefinition<PaymentTypeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PaymentTypeModel>> GetList(FilterDefinition<PaymentTypeModel> filter, SortDefinition<PaymentTypeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PaymentTypeModel>(FilterDefinition<PaymentTypeModel> filter, UpdateDefinition<PaymentTypeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PaymentTypeModel>(FilterDefinition<PaymentTypeModel> filter = null) => await query.Delete(database, table, filter);

}
