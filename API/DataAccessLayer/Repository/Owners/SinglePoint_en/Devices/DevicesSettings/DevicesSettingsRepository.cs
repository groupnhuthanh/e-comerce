﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DevicesSettings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.DevicesSettings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.DevicesSettings;

public class DevicesSettingsRepository : RepositoryBase<DevicesSettingsModel>, IDevicesSettingsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DevicesSettings.ToString();
    public IQuery query;
    public DevicesSettingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DevicesSettingsModel> Post(DevicesSettingsModel doc) => await query.Post(database, table, doc);
    public async Task<DevicesSettingsModel> GetSingle(FilterDefinition<DevicesSettingsModel> filter = null, SortDefinition<DevicesSettingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DevicesSettingsModel>> GetList(FilterDefinition<DevicesSettingsModel> filter, SortDefinition<DevicesSettingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DevicesSettingsModel>(FilterDefinition<DevicesSettingsModel> filter, UpdateDefinition<DevicesSettingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DevicesSettingsModel>(FilterDefinition<DevicesSettingsModel> filter = null) => await query.Delete(database, table, filter);

}
