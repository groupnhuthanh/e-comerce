﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceSettingIdsMultiple;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.DeviceSettingIdsMultiple;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.DeviceSettingIdsMultiple;

public class DeviceSettingIdsMultipleRepository : RepositoryBase<DeviceSettingIdsMultipleModel>, IDeviceSettingIdsMultipleRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DeviceSettingIdsMultiple.ToString();
    public IQuery query;
    public DeviceSettingIdsMultipleRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeviceSettingIdsMultipleModel> Post(DeviceSettingIdsMultipleModel doc) => await query.Post(database, table, doc);
    public async Task<DeviceSettingIdsMultipleModel> GetSingle(FilterDefinition<DeviceSettingIdsMultipleModel> filter = null, SortDefinition<DeviceSettingIdsMultipleModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeviceSettingIdsMultipleModel>> GetList(FilterDefinition<DeviceSettingIdsMultipleModel> filter, SortDefinition<DeviceSettingIdsMultipleModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeviceSettingIdsMultipleModel>(FilterDefinition<DeviceSettingIdsMultipleModel> filter, UpdateDefinition<DeviceSettingIdsMultipleModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeviceSettingIdsMultipleModel>(FilterDefinition<DeviceSettingIdsMultipleModel> filter = null) => await query.Delete(database, table, filter);

}
