﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceImage;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.DeviceImage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.DeviceImage;

public class DeviceImageRepository : RepositoryBase<DeviceImageModel>, IDeviceImageRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DeviceImage.ToString();
    public IQuery query;
    public DeviceImageRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeviceImageModel> Post(DeviceImageModel doc) => await query.Post(database, table, doc);
    public async Task<DeviceImageModel> GetSingle(FilterDefinition<DeviceImageModel> filter = null, SortDefinition<DeviceImageModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeviceImageModel>> GetList(FilterDefinition<DeviceImageModel> filter, SortDefinition<DeviceImageModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeviceImageModel>(FilterDefinition<DeviceImageModel> filter, UpdateDefinition<DeviceImageModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeviceImageModel>(FilterDefinition<DeviceImageModel> filter = null) => await query.Delete(database, table, filter);

}
