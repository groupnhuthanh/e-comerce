﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceSettingsIds;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.DeviceSettingsIds;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.DeviceSettingsIds;

public class DeviceSettingsIdsRepository : RepositoryBase<DeviceSettingsIdsModel>, IDeviceSettingsIdsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DeviceSettingsIds.ToString();
    public IQuery query;
    public DeviceSettingsIdsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeviceSettingsIdsModel> Post(DeviceSettingsIdsModel doc) => await query.Post(database, table, doc);
    public async Task<DeviceSettingsIdsModel> GetSingle(FilterDefinition<DeviceSettingsIdsModel> filter = null, SortDefinition<DeviceSettingsIdsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeviceSettingsIdsModel>> GetList(FilterDefinition<DeviceSettingsIdsModel> filter, SortDefinition<DeviceSettingsIdsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeviceSettingsIdsModel>(FilterDefinition<DeviceSettingsIdsModel> filter, UpdateDefinition<DeviceSettingsIdsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeviceSettingsIdsModel>(FilterDefinition<DeviceSettingsIdsModel> filter = null) => await query.Delete(database, table, filter);

}
