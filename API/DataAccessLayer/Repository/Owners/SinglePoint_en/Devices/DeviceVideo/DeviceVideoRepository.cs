﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceVideo;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.DeviceVideo;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.DeviceVideo;

public class DeviceVideoRepository : RepositoryBase<DeviceVideoModel>, IDeviceVideoRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DeviceVideo.ToString();
    public IQuery query;
    public DeviceVideoRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeviceVideoModel> Post(DeviceVideoModel doc) => await query.Post(database, table, doc);
    public async Task<DeviceVideoModel> GetSingle(FilterDefinition<DeviceVideoModel> filter = null, SortDefinition<DeviceVideoModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeviceVideoModel>> GetList(FilterDefinition<DeviceVideoModel> filter, SortDefinition<DeviceVideoModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeviceVideoModel>(FilterDefinition<DeviceVideoModel> filter, UpdateDefinition<DeviceVideoModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeviceVideoModel>(FilterDefinition<DeviceVideoModel> filter = null) => await query.Delete(database, table, filter);

}
