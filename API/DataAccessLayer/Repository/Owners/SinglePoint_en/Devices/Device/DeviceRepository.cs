﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.Device;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.Device;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.Device;

public class DeviceRepository : RepositoryBase<DeviceModel>, IDeviceRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Device.ToString();
    public IQuery query;
    public DeviceRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeviceModel> Post(DeviceModel doc) => await query.Post(database, table, doc);
    public async Task<DeviceModel> GetSingle(FilterDefinition<DeviceModel> filter = null, SortDefinition<DeviceModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeviceModel>> GetList(FilterDefinition<DeviceModel> filter, SortDefinition<DeviceModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeviceModel>(FilterDefinition<DeviceModel> filter, UpdateDefinition<DeviceModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeviceModel>(FilterDefinition<DeviceModel> filter = null) => await query.Delete(database, table, filter);

}
