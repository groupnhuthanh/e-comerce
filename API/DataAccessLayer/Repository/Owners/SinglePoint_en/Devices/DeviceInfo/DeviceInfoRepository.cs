﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceInfo;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.DeviceInfo;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.DeviceInfo;

public class DeviceInfoRepository : RepositoryBase<DeviceInfoModel>, IDeviceInfoRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DeviceInfo.ToString();
    public IQuery query;
    public DeviceInfoRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeviceInfoModel> Post(DeviceInfoModel doc) => await query.Post(database, table, doc);
    public async Task<DeviceInfoModel> GetSingle(FilterDefinition<DeviceInfoModel> filter = null, SortDefinition<DeviceInfoModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeviceInfoModel>> GetList(FilterDefinition<DeviceInfoModel> filter, SortDefinition<DeviceInfoModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeviceInfoModel>(FilterDefinition<DeviceInfoModel> filter, UpdateDefinition<DeviceInfoModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeviceInfoModel>(FilterDefinition<DeviceInfoModel> filter = null) => await query.Delete(database, table, filter);

}
