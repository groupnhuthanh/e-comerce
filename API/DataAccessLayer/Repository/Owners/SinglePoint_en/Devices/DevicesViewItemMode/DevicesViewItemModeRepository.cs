﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DevicesViewItemMode;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.DevicesViewItemMode;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.DevicesViewItemMode;

public class DevicesViewItemModeRepository : RepositoryBase<DevicesViewItemModeModel>, IDevicesViewItemModeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DevicesViewItemMode.ToString();
    public IQuery query;
    public DevicesViewItemModeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DevicesViewItemModeModel> Post(DevicesViewItemModeModel doc) => await query.Post(database, table, doc);
    public async Task<DevicesViewItemModeModel> GetSingle(FilterDefinition<DevicesViewItemModeModel> filter = null, SortDefinition<DevicesViewItemModeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DevicesViewItemModeModel>> GetList(FilterDefinition<DevicesViewItemModeModel> filter, SortDefinition<DevicesViewItemModeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DevicesViewItemModeModel>(FilterDefinition<DevicesViewItemModeModel> filter, UpdateDefinition<DevicesViewItemModeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DevicesViewItemModeModel>(FilterDefinition<DevicesViewItemModeModel> filter = null) => await query.Delete(database, table, filter);

}
