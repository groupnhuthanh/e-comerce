﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Devices.DeviceRequestHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Devices.DeviceRequestHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Devices.DeviceRequestHistory;

public class DeviceRequestHistoryRepository : RepositoryBase<DeviceRequestHistoryModel>, IDeviceRequestHistoryRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DeviceRequestHistory.ToString();
    public IQuery query;
    public DeviceRequestHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DeviceRequestHistoryModel> Post(DeviceRequestHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<DeviceRequestHistoryModel> GetSingle(FilterDefinition<DeviceRequestHistoryModel> filter = null, SortDefinition<DeviceRequestHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DeviceRequestHistoryModel>> GetList(FilterDefinition<DeviceRequestHistoryModel> filter, SortDefinition<DeviceRequestHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DeviceRequestHistoryModel>(FilterDefinition<DeviceRequestHistoryModel> filter, UpdateDefinition<DeviceRequestHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DeviceRequestHistoryModel>(FilterDefinition<DeviceRequestHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
