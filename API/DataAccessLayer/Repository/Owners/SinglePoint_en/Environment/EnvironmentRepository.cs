﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Environment;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Environment;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Environment;

public class EnvironmentRepository : RepositoryBase<EnvironmentModel>, IEnvironmentRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Environment.ToString();
    public IQuery query;
    public EnvironmentRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<EnvironmentModel> Post(EnvironmentModel doc) => await query.Post(database, table, doc);
    public async Task<EnvironmentModel> GetSingle(FilterDefinition<EnvironmentModel> filter = null, SortDefinition<EnvironmentModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<EnvironmentModel>> GetList(FilterDefinition<EnvironmentModel> filter, SortDefinition<EnvironmentModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<EnvironmentModel>(FilterDefinition<EnvironmentModel> filter, UpdateDefinition<EnvironmentModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<EnvironmentModel>(FilterDefinition<EnvironmentModel> filter = null) => await query.Delete(database, table, filter);

}
