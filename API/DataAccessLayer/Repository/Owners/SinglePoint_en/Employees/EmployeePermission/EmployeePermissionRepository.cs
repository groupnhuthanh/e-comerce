﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Employees.EmployeePermission;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Employees.EmployeePermission;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Employees.EmployeePermission;

public class EmployeePermissionRepository : RepositoryBase<EmployeePermissionModel>, IEmployeePermissionRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.EmployeePermission.ToString();
    public IQuery query;
    public EmployeePermissionRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<EmployeePermissionModel> Post(EmployeePermissionModel doc) => await query.Post(database, table, doc);
    public async Task<EmployeePermissionModel> GetSingle(FilterDefinition<EmployeePermissionModel> filter = null, SortDefinition<EmployeePermissionModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<EmployeePermissionModel>> GetList(FilterDefinition<EmployeePermissionModel> filter, SortDefinition<EmployeePermissionModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<EmployeePermissionModel>(FilterDefinition<EmployeePermissionModel> filter, UpdateDefinition<EmployeePermissionModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<EmployeePermissionModel>(FilterDefinition<EmployeePermissionModel> filter = null) => await query.Delete(database, table, filter);

}
