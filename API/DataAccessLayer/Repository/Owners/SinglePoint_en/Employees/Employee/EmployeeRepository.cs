﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Employees.Employee;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Employees.Employee;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Employees.Employee;

public class EmployeeRepository : RepositoryBase<EmployeeModel>, IEmployeeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Employee.ToString();
    public IQuery query;
    public EmployeeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<EmployeeModel> Post(EmployeeModel doc) => await query.Post(database, table, doc);
    public async Task<EmployeeModel> GetSingle(FilterDefinition<EmployeeModel> filter = null, SortDefinition<EmployeeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<EmployeeModel>> GetList(FilterDefinition<EmployeeModel> filter, SortDefinition<EmployeeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<EmployeeModel>(FilterDefinition<EmployeeModel> filter, UpdateDefinition<EmployeeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<EmployeeModel>(FilterDefinition<EmployeeModel> filter = null) => await query.Delete(database, table, filter);

}
