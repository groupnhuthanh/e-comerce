﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.ReportsOrdersHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.ReportsOrdersHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.ReportsOrdersHistory;

public class ReportsOrdersHistoryRepository : RepositoryBase<ReportsOrdersHistoryModel>, IReportsOrdersHistoryRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ReportsOrdersHistory.ToString();
    public IQuery query;
    public ReportsOrdersHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ReportsOrdersHistoryModel> Post(ReportsOrdersHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<ReportsOrdersHistoryModel> GetSingle(FilterDefinition<ReportsOrdersHistoryModel> filter = null, SortDefinition<ReportsOrdersHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ReportsOrdersHistoryModel>> GetList(FilterDefinition<ReportsOrdersHistoryModel> filter, SortDefinition<ReportsOrdersHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ReportsOrdersHistoryModel>(FilterDefinition<ReportsOrdersHistoryModel> filter, UpdateDefinition<ReportsOrdersHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ReportsOrdersHistoryModel>(FilterDefinition<ReportsOrdersHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
