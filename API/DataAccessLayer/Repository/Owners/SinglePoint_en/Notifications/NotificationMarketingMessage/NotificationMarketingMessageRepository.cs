﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Notifications.NotificationMarketingMessage;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Notifications.NotificationMarketingMessage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Notifications.NotificationMarketingMessage;

public class NotificationMarketingMessageRepository : RepositoryBase<NotificationMarketingMessageModel>, INotificationMarketingMessageRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.NotificationMarketingMessage.ToString();
    public IQuery query;
    public NotificationMarketingMessageRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<NotificationMarketingMessageModel> Post(NotificationMarketingMessageModel doc) => await query.Post(database, table, doc);
    public async Task<NotificationMarketingMessageModel> GetSingle(FilterDefinition<NotificationMarketingMessageModel> filter = null, SortDefinition<NotificationMarketingMessageModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<NotificationMarketingMessageModel>> GetList(FilterDefinition<NotificationMarketingMessageModel> filter, SortDefinition<NotificationMarketingMessageModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<NotificationMarketingMessageModel>(FilterDefinition<NotificationMarketingMessageModel> filter, UpdateDefinition<NotificationMarketingMessageModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<NotificationMarketingMessageModel>(FilterDefinition<NotificationMarketingMessageModel> filter = null) => await query.Delete(database, table, filter);

}
