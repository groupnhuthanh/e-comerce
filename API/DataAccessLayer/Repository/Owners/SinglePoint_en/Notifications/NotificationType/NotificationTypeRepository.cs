﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Notifications.NotificationType;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Notifications.NotificationType;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Notifications.NotificationType;

public class NotificationTypeRepository : RepositoryBase<NotificationTypeModel>, INotificationTypeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.NotificationType.ToString();
    public IQuery query;
    public NotificationTypeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<NotificationTypeModel> Post(NotificationTypeModel doc) => await query.Post(database, table, doc);
    public async Task<NotificationTypeModel> GetSingle(FilterDefinition<NotificationTypeModel> filter = null, SortDefinition<NotificationTypeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<NotificationTypeModel>> GetList(FilterDefinition<NotificationTypeModel> filter, SortDefinition<NotificationTypeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<NotificationTypeModel>(FilterDefinition<NotificationTypeModel> filter, UpdateDefinition<NotificationTypeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<NotificationTypeModel>(FilterDefinition<NotificationTypeModel> filter = null) => await query.Delete(database, table, filter);

}
