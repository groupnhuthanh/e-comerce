﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_Combo;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_Combo;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Discounts.Discounts_Combo;

public class Discounts_ComboRepository : RepositoryBase<Discounts_ComboModel>, IDiscounts_ComboRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Discounts_Combo.ToString();
    public IQuery query;
    public Discounts_ComboRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<Discounts_ComboModel> Post(Discounts_ComboModel doc) => await query.Post(database, table, doc);
    public async Task<Discounts_ComboModel> GetSingle(FilterDefinition<Discounts_ComboModel> filter = null, SortDefinition<Discounts_ComboModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<Discounts_ComboModel>> GetList(FilterDefinition<Discounts_ComboModel> filter, SortDefinition<Discounts_ComboModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<Discounts_ComboModel>(FilterDefinition<Discounts_ComboModel> filter, UpdateDefinition<Discounts_ComboModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<Discounts_ComboModel>(FilterDefinition<Discounts_ComboModel> filter = null) => await query.Delete(database, table, filter);

}
