﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_Percentage;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_Percentage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Discounts.Discounts_Percentage;

public class Discounts_PercentageRepository : RepositoryBase<Discounts_PercentageModel>, IDiscounts_PercentageRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Discounts_Percentage.ToString();
    public IQuery query;
    public Discounts_PercentageRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<Discounts_PercentageModel> Post(Discounts_PercentageModel doc) => await query.Post(database, table, doc);
    public async Task<Discounts_PercentageModel> GetSingle(FilterDefinition<Discounts_PercentageModel> filter = null, SortDefinition<Discounts_PercentageModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<Discounts_PercentageModel>> GetList(FilterDefinition<Discounts_PercentageModel> filter, SortDefinition<Discounts_PercentageModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<Discounts_PercentageModel>(FilterDefinition<Discounts_PercentageModel> filter, UpdateDefinition<Discounts_PercentageModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<Discounts_PercentageModel>(FilterDefinition<Discounts_PercentageModel> filter = null) => await query.Delete(database, table, filter);

}
