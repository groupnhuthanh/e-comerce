﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_BuyXgetY;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_BuyXgetY;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Discounts.Discounts_BuyXgetY;

public class Discounts_BuyXgetYRepository : RepositoryBase<Discounts_BuyXgetYModel>, IDiscounts_BuyXgetYRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Discounts_BuyXgetY.ToString();
    public IQuery query;
    public Discounts_BuyXgetYRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<Discounts_BuyXgetYModel> Post(Discounts_BuyXgetYModel doc) => await query.Post(database, table, doc);
    public async Task<Discounts_BuyXgetYModel> GetSingle(FilterDefinition<Discounts_BuyXgetYModel> filter = null, SortDefinition<Discounts_BuyXgetYModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<Discounts_BuyXgetYModel>> GetList(FilterDefinition<Discounts_BuyXgetYModel> filter, SortDefinition<Discounts_BuyXgetYModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<Discounts_BuyXgetYModel>(FilterDefinition<Discounts_BuyXgetYModel> filter, UpdateDefinition<Discounts_BuyXgetYModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<Discounts_BuyXgetYModel>(FilterDefinition<Discounts_BuyXgetYModel> filter = null) => await query.Delete(database, table, filter);

}
