﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_FixedAmount;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_FixedAmount;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Discounts.Discounts_FixedAmount;

public class Discounts_FixedAmountRepository : RepositoryBase<Discounts_FixedAmountModel>, IDiscounts_FixedAmountRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Discounts_FixedAmount.ToString();
    public IQuery query;
    public Discounts_FixedAmountRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<Discounts_FixedAmountModel> Post(Discounts_FixedAmountModel doc) => await query.Post(database, table, doc);
    public async Task<Discounts_FixedAmountModel> GetSingle(FilterDefinition<Discounts_FixedAmountModel> filter = null, SortDefinition<Discounts_FixedAmountModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<Discounts_FixedAmountModel>> GetList(FilterDefinition<Discounts_FixedAmountModel> filter, SortDefinition<Discounts_FixedAmountModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<Discounts_FixedAmountModel>(FilterDefinition<Discounts_FixedAmountModel> filter, UpdateDefinition<Discounts_FixedAmountModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<Discounts_FixedAmountModel>(FilterDefinition<Discounts_FixedAmountModel> filter = null) => await query.Delete(database, table, filter);

}
