﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.DiscountsImages;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Discounts.DiscountsImages;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Discounts.DiscountsImages;

public class DiscountsImagesRepository : RepositoryBase<DiscountsImagesModel>, IDiscountsImagesRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DiscountsImages.ToString();
    public IQuery query;
    public DiscountsImagesRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DiscountsImagesModel> Post(DiscountsImagesModel doc) => await query.Post(database, table, doc);
    public async Task<DiscountsImagesModel> GetSingle(FilterDefinition<DiscountsImagesModel> filter = null, SortDefinition<DiscountsImagesModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DiscountsImagesModel>> GetList(FilterDefinition<DiscountsImagesModel> filter, SortDefinition<DiscountsImagesModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DiscountsImagesModel>(FilterDefinition<DiscountsImagesModel> filter, UpdateDefinition<DiscountsImagesModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DiscountsImagesModel>(FilterDefinition<DiscountsImagesModel> filter = null) => await query.Delete(database, table, filter);

}
