﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.DiscountCode;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Discounts.DiscountCode;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Discounts.DiscountCode;

public class DiscountCodeRepository : RepositoryBase<DiscountCodeModel>, IDiscountCodeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DiscountCode.ToString();
    public IQuery query;
    public DiscountCodeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DiscountCodeModel> Post(DiscountCodeModel doc) => await query.Post(database, table, doc);
    public async Task<DiscountCodeModel> GetSingle(FilterDefinition<DiscountCodeModel> filter = null, SortDefinition<DiscountCodeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DiscountCodeModel>> GetList(FilterDefinition<DiscountCodeModel> filter, SortDefinition<DiscountCodeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DiscountCodeModel>(FilterDefinition<DiscountCodeModel> filter, UpdateDefinition<DiscountCodeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DiscountCodeModel>(FilterDefinition<DiscountCodeModel> filter = null) => await query.Delete(database, table, filter);

}
