﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Discounts.Discounts_ApplyToItem;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Discounts.Discounts_ApplyToItem;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Discounts.Discounts_ApplyToItem;

public class Discounts_ApplyToItemRepository : RepositoryBase<Discounts_ApplyToItemModel>, IDiscounts_ApplyToItemRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Discounts_ApplyToItem.ToString();
    public IQuery query;
    public Discounts_ApplyToItemRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<Discounts_ApplyToItemModel> Post(Discounts_ApplyToItemModel doc) => await query.Post(database, table, doc);
    public async Task<Discounts_ApplyToItemModel> GetSingle(FilterDefinition<Discounts_ApplyToItemModel> filter = null, SortDefinition<Discounts_ApplyToItemModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<Discounts_ApplyToItemModel>> GetList(FilterDefinition<Discounts_ApplyToItemModel> filter, SortDefinition<Discounts_ApplyToItemModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<Discounts_ApplyToItemModel>(FilterDefinition<Discounts_ApplyToItemModel> filter, UpdateDefinition<Discounts_ApplyToItemModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<Discounts_ApplyToItemModel>(FilterDefinition<Discounts_ApplyToItemModel> filter = null) => await query.Delete(database, table, filter);

}
