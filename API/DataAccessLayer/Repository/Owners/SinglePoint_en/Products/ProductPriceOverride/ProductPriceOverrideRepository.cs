﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Products.ProductPriceOverride;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Products.ProductPriceOverride;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Products.ProductPriceOverride;

public class ProductPriceOverrideRepository : RepositoryBase<ProductPriceOverrideModel>, IProductPriceOverrideRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ProductPriceOverride.ToString();
    public IQuery query;
    public ProductPriceOverrideRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ProductPriceOverrideModel> Post(ProductPriceOverrideModel doc) => await query.Post(database, table, doc);
    public async Task<ProductPriceOverrideModel> GetSingle(FilterDefinition<ProductPriceOverrideModel> filter = null, SortDefinition<ProductPriceOverrideModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ProductPriceOverrideModel>> GetList(FilterDefinition<ProductPriceOverrideModel> filter, SortDefinition<ProductPriceOverrideModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ProductPriceOverrideModel>(FilterDefinition<ProductPriceOverrideModel> filter, UpdateDefinition<ProductPriceOverrideModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ProductPriceOverrideModel>(FilterDefinition<ProductPriceOverrideModel> filter = null) => await query.Delete(database, table, filter);

}
