﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Products.ProductImages;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Products.ProductImages;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Products.ProductImages;

public class ProductImagesRepository : RepositoryBase<ProductImagesModel>, IProductImagesRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ProductImages.ToString();
    public IQuery query;
    public ProductImagesRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ProductImagesModel> Post(ProductImagesModel doc) => await query.Post(database, table, doc);
    public async Task<ProductImagesModel> GetSingle(FilterDefinition<ProductImagesModel> filter = null, SortDefinition<ProductImagesModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ProductImagesModel>> GetList(FilterDefinition<ProductImagesModel> filter, SortDefinition<ProductImagesModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ProductImagesModel>(FilterDefinition<ProductImagesModel> filter, UpdateDefinition<ProductImagesModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ProductImagesModel>(FilterDefinition<ProductImagesModel> filter = null) => await query.Delete(database, table, filter);

}
