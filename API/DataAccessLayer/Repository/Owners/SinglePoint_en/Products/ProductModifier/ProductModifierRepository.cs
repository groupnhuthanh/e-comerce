﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Products.ProductModifier;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Products.ProductModifier;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Products.ProductModifier;

public class ProductModifierRepository : RepositoryBase<ProductModifierModel>, IProductModifierRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ProductModifier.ToString();
    public IQuery query;
    public ProductModifierRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ProductModifierModel> Post(ProductModifierModel doc) => await query.Post(database, table, doc);
    public async Task<ProductModifierModel> GetSingle(FilterDefinition<ProductModifierModel> filter = null, SortDefinition<ProductModifierModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ProductModifierModel>> GetList(FilterDefinition<ProductModifierModel> filter, SortDefinition<ProductModifierModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ProductModifierModel>(FilterDefinition<ProductModifierModel> filter, UpdateDefinition<ProductModifierModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ProductModifierModel>(FilterDefinition<ProductModifierModel> filter = null) => await query.Delete(database, table, filter);

}
