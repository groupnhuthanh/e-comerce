﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Products.Product;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Products.Product;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Products.Product;

public class ProductRepository : RepositoryBase<ProductModel>, IProductRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Product.ToString();
    public IQuery query;
    public ProductRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ProductModel> Post(ProductModel doc) => await query.Post(database, table, doc);
    public async Task<ProductModel> GetSingle(FilterDefinition<ProductModel> filter = null, SortDefinition<ProductModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ProductModel>> GetList(FilterDefinition<ProductModel> filter, SortDefinition<ProductModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ProductModel>(FilterDefinition<ProductModel> filter, UpdateDefinition<ProductModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ProductModel>(FilterDefinition<ProductModel> filter = null) => await query.Delete(database, table, filter);

}
