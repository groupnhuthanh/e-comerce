﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FriendRequests;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.FriendRequests;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.FriendRequests;

public class FriendRequestsRepository : RepositoryBase<FriendRequestsModel>, IFriendRequestsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.FriendRequests.ToString();
    public IQuery query;
    public FriendRequestsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FriendRequestsModel> Post(FriendRequestsModel doc) => await query.Post(database, table, doc);
    public async Task<FriendRequestsModel> GetSingle(FilterDefinition<FriendRequestsModel> filter = null, SortDefinition<FriendRequestsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FriendRequestsModel>> GetList(FilterDefinition<FriendRequestsModel> filter, SortDefinition<FriendRequestsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FriendRequestsModel>(FilterDefinition<FriendRequestsModel> filter, UpdateDefinition<FriendRequestsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FriendRequestsModel>(FilterDefinition<FriendRequestsModel> filter = null) => await query.Delete(database, table, filter);

}
