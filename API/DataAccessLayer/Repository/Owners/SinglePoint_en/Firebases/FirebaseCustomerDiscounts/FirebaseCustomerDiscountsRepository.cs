﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Firebases.FirebaseCustomerDiscounts;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Firebases.FirebaseCustomerDiscounts;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Firebases.FirebaseCustomerDiscounts;

public class FirebaseCustomerDiscountsRepository : RepositoryBase<FirebaseCustomerDiscountsModel>, IFirebaseCustomerDiscountsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.FirebaseCustomerDiscounts.ToString();
    public IQuery query;
    public FirebaseCustomerDiscountsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FirebaseCustomerDiscountsModel> Post(FirebaseCustomerDiscountsModel doc) => await query.Post(database, table, doc);
    public async Task<FirebaseCustomerDiscountsModel> GetSingle(FilterDefinition<FirebaseCustomerDiscountsModel> filter = null, SortDefinition<FirebaseCustomerDiscountsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FirebaseCustomerDiscountsModel>> GetList(FilterDefinition<FirebaseCustomerDiscountsModel> filter, SortDefinition<FirebaseCustomerDiscountsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FirebaseCustomerDiscountsModel>(FilterDefinition<FirebaseCustomerDiscountsModel> filter, UpdateDefinition<FirebaseCustomerDiscountsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FirebaseCustomerDiscountsModel>(FilterDefinition<FirebaseCustomerDiscountsModel> filter = null) => await query.Delete(database, table, filter);

}
