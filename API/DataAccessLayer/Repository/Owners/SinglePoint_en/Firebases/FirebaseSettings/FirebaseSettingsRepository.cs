﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Firebases.FirebaseSettings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Firebases.FirebaseSettings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Firebases.FirebaseSettings;

public class FirebaseSettingsRepository : RepositoryBase<FirebaseSettingsModel>, IFirebaseSettingsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.FirebaseSettings.ToString();
    public IQuery query;
    public FirebaseSettingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FirebaseSettingsModel> Post(FirebaseSettingsModel doc) => await query.Post(database, table, doc);
    public async Task<FirebaseSettingsModel> GetSingle(FilterDefinition<FirebaseSettingsModel> filter = null, SortDefinition<FirebaseSettingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FirebaseSettingsModel>> GetList(FilterDefinition<FirebaseSettingsModel> filter, SortDefinition<FirebaseSettingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FirebaseSettingsModel>(FilterDefinition<FirebaseSettingsModel> filter, UpdateDefinition<FirebaseSettingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FirebaseSettingsModel>(FilterDefinition<FirebaseSettingsModel> filter = null) => await query.Delete(database, table, filter);

}
