﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Firebases.FirebaseMenus;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Firebases.FirebaseMenus;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Firebases.FirebaseMenus;

public class FirebaseMenusRepository : RepositoryBase<FirebaseMenusModel>, IFirebaseMenusRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.FirebaseMenus.ToString();
    public IQuery query;
    public FirebaseMenusRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FirebaseMenusModel> Post(FirebaseMenusModel doc) => await query.Post(database, table, doc);
    public async Task<FirebaseMenusModel> GetSingle(FilterDefinition<FirebaseMenusModel> filter = null, SortDefinition<FirebaseMenusModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FirebaseMenusModel>> GetList(FilterDefinition<FirebaseMenusModel> filter, SortDefinition<FirebaseMenusModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FirebaseMenusModel>(FilterDefinition<FirebaseMenusModel> filter, UpdateDefinition<FirebaseMenusModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FirebaseMenusModel>(FilterDefinition<FirebaseMenusModel> filter = null) => await query.Delete(database, table, filter);

}
