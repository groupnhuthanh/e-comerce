﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslatePolicies;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslatePolicies;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslatePolicies;

public class TranslatePoliciesRepository : RepositoryBase<TranslatePoliciesModel>, ITranslatePoliciesRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslatePolicies.ToString();
    public IQuery query;
    public TranslatePoliciesRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslatePoliciesModel> Post(TranslatePoliciesModel doc) => await query.Post(database, table, doc);
    public async Task<TranslatePoliciesModel> GetSingle(FilterDefinition<TranslatePoliciesModel> filter = null, SortDefinition<TranslatePoliciesModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslatePoliciesModel>> GetList(FilterDefinition<TranslatePoliciesModel> filter, SortDefinition<TranslatePoliciesModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslatePoliciesModel>(FilterDefinition<TranslatePoliciesModel> filter, UpdateDefinition<TranslatePoliciesModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslatePoliciesModel>(FilterDefinition<TranslatePoliciesModel> filter = null) => await query.Delete(database, table, filter);

}
