﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateModifierItem;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslateModifierItem;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslateModifierItem;

public class TranslateModifierItemRepository : RepositoryBase<TranslateModifierItemModel>, ITranslateModifierItemRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslateModifier.ToString();
    public IQuery query;
    public TranslateModifierItemRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslateModifierItemModel> Post(TranslateModifierItemModel doc) => await query.Post(database, table, doc);
    public async Task<TranslateModifierItemModel> GetSingle(FilterDefinition<TranslateModifierItemModel> filter = null, SortDefinition<TranslateModifierItemModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslateModifierItemModel>> GetList(FilterDefinition<TranslateModifierItemModel> filter, SortDefinition<TranslateModifierItemModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslateModifierItemModel>(FilterDefinition<TranslateModifierItemModel> filter, UpdateDefinition<TranslateModifierItemModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslateModifierItemModel>(FilterDefinition<TranslateModifierItemModel> filter = null) => await query.Delete(database, table, filter);

}
