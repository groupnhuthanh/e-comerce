﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateModifier;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslateModifier;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslateModifier;

public class TranslateModifierRepository : RepositoryBase<TranslateModifierModel>, ITranslateModifierRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslateModifier.ToString();
    public IQuery query;
    public TranslateModifierRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslateModifierModel> Post(TranslateModifierModel doc) => await query.Post(database, table, doc);
    public async Task<TranslateModifierModel> GetSingle(FilterDefinition<TranslateModifierModel> filter = null, SortDefinition<TranslateModifierModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslateModifierModel>> GetList(FilterDefinition<TranslateModifierModel> filter, SortDefinition<TranslateModifierModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslateModifierModel>(FilterDefinition<TranslateModifierModel> filter, UpdateDefinition<TranslateModifierModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslateModifierModel>(FilterDefinition<TranslateModifierModel> filter = null) => await query.Delete(database, table, filter);

}
