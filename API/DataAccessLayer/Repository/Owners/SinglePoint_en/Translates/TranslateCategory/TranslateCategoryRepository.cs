﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateCategory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslateCategory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslateCategory;

public class TranslateCategoryRepository : RepositoryBase<TranslateCategoryModel>, ITranslateCategoryRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslateCategory.ToString();
    public IQuery query;
    public TranslateCategoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslateCategoryModel> Post(TranslateCategoryModel doc) => await query.Post(database, table, doc);
    public async Task<TranslateCategoryModel> GetSingle(FilterDefinition<TranslateCategoryModel> filter = null, SortDefinition<TranslateCategoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslateCategoryModel>> GetList(FilterDefinition<TranslateCategoryModel> filter, SortDefinition<TranslateCategoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslateCategoryModel>(FilterDefinition<TranslateCategoryModel> filter, UpdateDefinition<TranslateCategoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslateCategoryModel>(FilterDefinition<TranslateCategoryModel> filter = null) => await query.Delete(database, table, filter);

}
