﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateGroupDiscount;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslateGroupDiscount;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslateGroupDiscount;

public class TranslateGroupDiscountRepository : RepositoryBase<TranslateGroupDiscountModel>, ITranslateGroupDiscountRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslateGroupDiscount.ToString();
    public IQuery query;
    public TranslateGroupDiscountRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslateGroupDiscountModel> Post(TranslateGroupDiscountModel doc) => await query.Post(database, table, doc);
    public async Task<TranslateGroupDiscountModel> GetSingle(FilterDefinition<TranslateGroupDiscountModel> filter = null, SortDefinition<TranslateGroupDiscountModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslateGroupDiscountModel>> GetList(FilterDefinition<TranslateGroupDiscountModel> filter, SortDefinition<TranslateGroupDiscountModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslateGroupDiscountModel>(FilterDefinition<TranslateGroupDiscountModel> filter, UpdateDefinition<TranslateGroupDiscountModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslateGroupDiscountModel>(FilterDefinition<TranslateGroupDiscountModel> filter = null) => await query.Delete(database, table, filter);

}
