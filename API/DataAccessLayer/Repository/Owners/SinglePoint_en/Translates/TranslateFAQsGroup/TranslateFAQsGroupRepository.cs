﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateFAQsGroup;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslateFAQsGroup;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslateFAQsGroup;

public class TranslateFAQsGroupRepository : RepositoryBase<TranslateFAQsGroupModel>, ITranslateFAQsGroupRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslateFAQsGroup.ToString();
    public IQuery query;
    public TranslateFAQsGroupRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslateFAQsGroupModel> Post(TranslateFAQsGroupModel doc) => await query.Post(database, table, doc);
    public async Task<TranslateFAQsGroupModel> GetSingle(FilterDefinition<TranslateFAQsGroupModel> filter = null, SortDefinition<TranslateFAQsGroupModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslateFAQsGroupModel>> GetList(FilterDefinition<TranslateFAQsGroupModel> filter, SortDefinition<TranslateFAQsGroupModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslateFAQsGroupModel>(FilterDefinition<TranslateFAQsGroupModel> filter, UpdateDefinition<TranslateFAQsGroupModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslateFAQsGroupModel>(FilterDefinition<TranslateFAQsGroupModel> filter = null) => await query.Delete(database, table, filter);

}
