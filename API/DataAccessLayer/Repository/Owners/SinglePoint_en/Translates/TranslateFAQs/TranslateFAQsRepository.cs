﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateFAQs;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslateFAQs;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslateFAQs;

public class TranslateFAQsRepository : RepositoryBase<TranslateFAQsModel>, ITranslateFAQsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslateFAQs.ToString();
    public IQuery query;
    public TranslateFAQsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslateFAQsModel> Post(TranslateFAQsModel doc) => await query.Post(database, table, doc);
    public async Task<TranslateFAQsModel> GetSingle(FilterDefinition<TranslateFAQsModel> filter = null, SortDefinition<TranslateFAQsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslateFAQsModel>> GetList(FilterDefinition<TranslateFAQsModel> filter, SortDefinition<TranslateFAQsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslateFAQsModel>(FilterDefinition<TranslateFAQsModel> filter, UpdateDefinition<TranslateFAQsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslateFAQsModel>(FilterDefinition<TranslateFAQsModel> filter = null) => await query.Delete(database, table, filter);

}
