﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateOrderStatusDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslateOrderStatusDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslateOrderStatusDetail;

public class TranslateOrderStatusDetailRepository : RepositoryBase<TranslateOrderStatusDetailModel>, ITranslateOrderStatusDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslateOrderStatusDetail.ToString();
    public IQuery query;
    public TranslateOrderStatusDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslateOrderStatusDetailModel> Post(TranslateOrderStatusDetailModel doc) => await query.Post(database, table, doc);
    public async Task<TranslateOrderStatusDetailModel> GetSingle(FilterDefinition<TranslateOrderStatusDetailModel> filter = null, SortDefinition<TranslateOrderStatusDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslateOrderStatusDetailModel>> GetList(FilterDefinition<TranslateOrderStatusDetailModel> filter, SortDefinition<TranslateOrderStatusDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslateOrderStatusDetailModel>(FilterDefinition<TranslateOrderStatusDetailModel> filter, UpdateDefinition<TranslateOrderStatusDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslateOrderStatusDetailModel>(FilterDefinition<TranslateOrderStatusDetailModel> filter = null) => await query.Delete(database, table, filter);

}
