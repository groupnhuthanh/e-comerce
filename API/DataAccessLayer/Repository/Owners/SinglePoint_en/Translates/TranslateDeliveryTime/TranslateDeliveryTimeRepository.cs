﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Translates.TranslateDeliveryTime;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Translates.TranslateDeliveryTime;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Translates.TranslateDeliveryTime;

public class TranslateDeliveryTimeRepository : RepositoryBase<TranslateDeliveryTimeModel>, ITranslateDeliveryTimeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TranslateDeliveryTime.ToString();
    public IQuery query;
    public TranslateDeliveryTimeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TranslateDeliveryTimeModel> Post(TranslateDeliveryTimeModel doc) => await query.Post(database, table, doc);
    public async Task<TranslateDeliveryTimeModel> GetSingle(FilterDefinition<TranslateDeliveryTimeModel> filter = null, SortDefinition<TranslateDeliveryTimeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TranslateDeliveryTimeModel>> GetList(FilterDefinition<TranslateDeliveryTimeModel> filter, SortDefinition<TranslateDeliveryTimeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TranslateDeliveryTimeModel>(FilterDefinition<TranslateDeliveryTimeModel> filter, UpdateDefinition<TranslateDeliveryTimeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TranslateDeliveryTimeModel>(FilterDefinition<TranslateDeliveryTimeModel> filter = null) => await query.Delete(database, table, filter);

}
