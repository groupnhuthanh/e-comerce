﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.AppVersion;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.AppVersion;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.AppVersion;

public class AppVersionRepository : RepositoryBase<AppVersionModel>, IAppVersionRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.AppVersion.ToString();
    public IQuery query;
    public AppVersionRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<AppVersionModel> Post(AppVersionModel doc) => await query.Post(database, table, doc);
    public async Task<AppVersionModel> GetSingle(FilterDefinition<AppVersionModel> filter = null, SortDefinition<AppVersionModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<AppVersionModel>> GetList(FilterDefinition<AppVersionModel> filter, SortDefinition<AppVersionModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<AppVersionModel>(FilterDefinition<AppVersionModel> filter, UpdateDefinition<AppVersionModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<AppVersionModel>(FilterDefinition<AppVersionModel> filter = null) => await query.Delete(database, table, filter);

}
