﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Campaigns.CampaignImage;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Campaigns.CampaignImage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Campaigns.CampaignImage;

public class CampaignImageRepository : RepositoryBase<CampaignImageModel>, ICampaignImageRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CampaignImage.ToString();
    public IQuery query;
    public CampaignImageRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CampaignImageModel> Post(CampaignImageModel doc) => await query.Post(database, table, doc);
    public async Task<CampaignImageModel> GetSingle(FilterDefinition<CampaignImageModel> filter = null, SortDefinition<CampaignImageModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CampaignImageModel>> GetList(FilterDefinition<CampaignImageModel> filter, SortDefinition<CampaignImageModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CampaignImageModel>(FilterDefinition<CampaignImageModel> filter, UpdateDefinition<CampaignImageModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CampaignImageModel>(FilterDefinition<CampaignImageModel> filter = null) => await query.Delete(database, table, filter);

}
