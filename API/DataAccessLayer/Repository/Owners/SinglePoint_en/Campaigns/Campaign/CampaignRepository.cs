﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Campaigns.Campaign;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Campaigns.Campaign;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Campaigns.Campaign;

public class CampaignRepository : RepositoryBase<CampaignModel>, ICampaignRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Campaign.ToString();
    public IQuery query;
    public CampaignRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CampaignModel> Post(CampaignModel doc) => await query.Post(database, table, doc);
    public async Task<CampaignModel> GetSingle(FilterDefinition<CampaignModel> filter = null, SortDefinition<CampaignModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CampaignModel>> GetList(FilterDefinition<CampaignModel> filter, SortDefinition<CampaignModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CampaignModel>(FilterDefinition<CampaignModel> filter, UpdateDefinition<CampaignModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CampaignModel>(FilterDefinition<CampaignModel> filter = null) => await query.Delete(database, table, filter);

}

