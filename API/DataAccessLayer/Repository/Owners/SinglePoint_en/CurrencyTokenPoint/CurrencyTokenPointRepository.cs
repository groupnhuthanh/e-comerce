﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.CurrencyTokenPoint;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.CurrencyTokenPoint;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.CurrencyTokenPoint;

public class CurrencyTokenPointRepository : RepositoryBase<CurrencyTokenPointModel>, ICurrencyTokenPointRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CurrencyTokenPoint.ToString();
    public IQuery query;
    public CurrencyTokenPointRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CurrencyTokenPointModel> Post(CurrencyTokenPointModel doc) => await query.Post(database, table, doc);
    public async Task<CurrencyTokenPointModel> GetSingle(FilterDefinition<CurrencyTokenPointModel> filter = null, SortDefinition<CurrencyTokenPointModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CurrencyTokenPointModel>> GetList(FilterDefinition<CurrencyTokenPointModel> filter, SortDefinition<CurrencyTokenPointModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CurrencyTokenPointModel>(FilterDefinition<CurrencyTokenPointModel> filter, UpdateDefinition<CurrencyTokenPointModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CurrencyTokenPointModel>(FilterDefinition<CurrencyTokenPointModel> filter = null) => await query.Delete(database, table, filter);

}
