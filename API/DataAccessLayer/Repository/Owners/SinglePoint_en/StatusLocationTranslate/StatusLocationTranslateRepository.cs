﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.StatusLocationTranslate;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.StatusLocationTranslate;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.StatusLocationTranslate;

public class StatusLocationTranslateRepository : RepositoryBase<StatusLocationTranslateModel>, IStatusLocationTranslateRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.StatusLocationTranslate.ToString();
    public IQuery query;
    public StatusLocationTranslateRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<StatusLocationTranslateModel> Post(StatusLocationTranslateModel doc) => await query.Post(database, table, doc);
    public async Task<StatusLocationTranslateModel> GetSingle(FilterDefinition<StatusLocationTranslateModel> filter = null, SortDefinition<StatusLocationTranslateModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<StatusLocationTranslateModel>> GetList(FilterDefinition<StatusLocationTranslateModel> filter, SortDefinition<StatusLocationTranslateModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<StatusLocationTranslateModel>(FilterDefinition<StatusLocationTranslateModel> filter, UpdateDefinition<StatusLocationTranslateModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<StatusLocationTranslateModel>(FilterDefinition<StatusLocationTranslateModel> filter = null) => await query.Delete(database, table, filter);

}
