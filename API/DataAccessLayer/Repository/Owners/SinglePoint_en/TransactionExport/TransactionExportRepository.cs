﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.TransactionExport;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.TransactionExport;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.TransactionExport;

public class TransactionExportRepository : RepositoryBase<TransactionExportModel>, ITransactionExportRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.TransactionExport.ToString();
    public IQuery query;
    public TransactionExportRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<TransactionExportModel> Post(TransactionExportModel doc) => await query.Post(database, table, doc);
    public async Task<TransactionExportModel> GetSingle(FilterDefinition<TransactionExportModel> filter = null, SortDefinition<TransactionExportModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<TransactionExportModel>> GetList(FilterDefinition<TransactionExportModel> filter, SortDefinition<TransactionExportModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<TransactionExportModel>(FilterDefinition<TransactionExportModel> filter, UpdateDefinition<TransactionExportModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<TransactionExportModel>(FilterDefinition<TransactionExportModel> filter = null) => await query.Delete(database, table, filter);

}
