﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Suppliers.Supplier;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Suppliers.Supplier;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Suppliers.Supplier;

public class SupplierRepository : RepositoryBase<SupplierModel>, ISupplierRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Supplier.ToString();
    public IQuery query;
    public SupplierRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SupplierModel> Post(SupplierModel doc) => await query.Post(database, table, doc);
    public async Task<SupplierModel> GetSingle(FilterDefinition<SupplierModel> filter = null, SortDefinition<SupplierModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SupplierModel>> GetList(FilterDefinition<SupplierModel> filter, SortDefinition<SupplierModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SupplierModel>(FilterDefinition<SupplierModel> filter, UpdateDefinition<SupplierModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SupplierModel>(FilterDefinition<SupplierModel> filter = null) => await query.Delete(database, table, filter);

}
