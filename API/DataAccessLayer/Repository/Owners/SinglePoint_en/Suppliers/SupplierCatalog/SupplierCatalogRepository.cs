﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Suppliers.SupplierCatalog;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Suppliers.SupplierCatalog;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Suppliers.SupplierCatalog;

public class SupplierCatalogRepository : RepositoryBase<SupplierCatalogModel>, ISupplierCatalogRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.SupplierCatalog.ToString();
    public IQuery query;
    public SupplierCatalogRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SupplierCatalogModel> Post(SupplierCatalogModel doc) => await query.Post(database, table, doc);
    public async Task<SupplierCatalogModel> GetSingle(FilterDefinition<SupplierCatalogModel> filter = null, SortDefinition<SupplierCatalogModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SupplierCatalogModel>> GetList(FilterDefinition<SupplierCatalogModel> filter, SortDefinition<SupplierCatalogModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SupplierCatalogModel>(FilterDefinition<SupplierCatalogModel> filter, UpdateDefinition<SupplierCatalogModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SupplierCatalogModel>(FilterDefinition<SupplierCatalogModel> filter = null) => await query.Delete(database, table, filter);

}
