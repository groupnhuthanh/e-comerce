﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Bundle_ApplyToItem;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Bundle_ApplyToItem;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Bundle_ApplyToItem;

public class Bundle_ApplyToIteRepository : RepositoryBase<Bundle_ApplyToItemModel>, IBundle_ApplyToIteRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Bundle_ApplyToItem.ToString();
    public IQuery query;
    public Bundle_ApplyToIteRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<Bundle_ApplyToItemModel> Post(Bundle_ApplyToItemModel doc) => await query.Post(database, table, doc);
    public async Task<Bundle_ApplyToItemModel> GetSingle(FilterDefinition<Bundle_ApplyToItemModel> filter = null, SortDefinition<Bundle_ApplyToItemModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<Bundle_ApplyToItemModel>> GetList(FilterDefinition<Bundle_ApplyToItemModel> filter, SortDefinition<Bundle_ApplyToItemModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<Bundle_ApplyToItemModel>(FilterDefinition<Bundle_ApplyToItemModel> filter, UpdateDefinition<Bundle_ApplyToItemModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<Bundle_ApplyToItemModel>(FilterDefinition<Bundle_ApplyToItemModel> filter = null) => await query.Delete(database, table, filter);

}
