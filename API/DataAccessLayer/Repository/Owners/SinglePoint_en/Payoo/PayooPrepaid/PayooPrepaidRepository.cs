﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Payoo.PayooPrepaid;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Payoo.PayooPrepaid;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Payoo.PayooPrepaid;

public class PayooPrepaidRepository : RepositoryBase<PayooPrepaidModel>, IPayooPrepaidRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.PayooPrepaid.ToString();
    public IQuery query;
    public PayooPrepaidRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PayooPrepaidModel> Post(PayooPrepaidModel doc) => await query.Post(database, table, doc);
    public async Task<PayooPrepaidModel> GetSingle(FilterDefinition<PayooPrepaidModel> filter = null, SortDefinition<PayooPrepaidModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PayooPrepaidModel>> GetList(FilterDefinition<PayooPrepaidModel> filter, SortDefinition<PayooPrepaidModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PayooPrepaidModel>(FilterDefinition<PayooPrepaidModel> filter, UpdateDefinition<PayooPrepaidModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PayooPrepaidModel>(FilterDefinition<PayooPrepaidModel> filter = null) => await query.Delete(database, table, filter);

}
