﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Payoo.PayooPrepaidDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Payoo.PayooPrepaidDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Payoo.PayooPrepaidDetail;

public class PayooPrepaidDetailRepository : RepositoryBase<PayooPrepaidDetailModel>, IPayooPrepaidDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.PayooPrepaidDetail.ToString();
    public IQuery query;
    public PayooPrepaidDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PayooPrepaidDetailModel> Post(PayooPrepaidDetailModel doc) => await query.Post(database, table, doc);
    public async Task<PayooPrepaidDetailModel> GetSingle(FilterDefinition<PayooPrepaidDetailModel> filter = null, SortDefinition<PayooPrepaidDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PayooPrepaidDetailModel>> GetList(FilterDefinition<PayooPrepaidDetailModel> filter, SortDefinition<PayooPrepaidDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PayooPrepaidDetailModel>(FilterDefinition<PayooPrepaidDetailModel> filter, UpdateDefinition<PayooPrepaidDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PayooPrepaidDetailModel>(FilterDefinition<PayooPrepaidDetailModel> filter = null) => await query.Delete(database, table, filter);

}
