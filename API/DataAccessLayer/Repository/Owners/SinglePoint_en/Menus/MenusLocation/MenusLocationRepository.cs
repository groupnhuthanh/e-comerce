﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.MenusLocation;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Menus.MenusLocation;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Menus.MenusLocation;

public class MenusLocationRepository : RepositoryBase<MenusLocationModel>, IMenusLocationRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.MenusLocation.ToString();
    public IQuery query;
    public MenusLocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<MenusLocationModel> Post(MenusLocationModel doc) => await query.Post(database, table, doc);
    public async Task<MenusLocationModel> GetSingle(FilterDefinition<MenusLocationModel> filter = null, SortDefinition<MenusLocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<MenusLocationModel>> GetList(FilterDefinition<MenusLocationModel> filter, SortDefinition<MenusLocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<MenusLocationModel>(FilterDefinition<MenusLocationModel> filter, UpdateDefinition<MenusLocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<MenusLocationModel>(FilterDefinition<MenusLocationModel> filter = null) => await query.Delete(database, table, filter);

}
