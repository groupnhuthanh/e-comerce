﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.MenusGroupItem;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Menus.MenusGroupItem;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Menus.MenusGroupItem;

public class MenusGroupItemRepository : RepositoryBase<MenusGroupItemModel>, IMenusGroupItemRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.MenusGroupItem.ToString();
    public IQuery query;
    public MenusGroupItemRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<MenusGroupItemModel> Post(MenusGroupItemModel doc) => await query.Post(database, table, doc);
    public async Task<MenusGroupItemModel> GetSingle(FilterDefinition<MenusGroupItemModel> filter = null, SortDefinition<MenusGroupItemModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<MenusGroupItemModel>> GetList(FilterDefinition<MenusGroupItemModel> filter, SortDefinition<MenusGroupItemModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<MenusGroupItemModel>(FilterDefinition<MenusGroupItemModel> filter, UpdateDefinition<MenusGroupItemModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<MenusGroupItemModel>(FilterDefinition<MenusGroupItemModel> filter = null) => await query.Delete(database, table, filter);

}
