﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.MenusGroup;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Menus.MenusGroup;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Menus.MenusGroup;

public class MenusGroupRepository : RepositoryBase<MenusGroupModel>, IMenusGroupRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.MenusGroup.ToString();
    public IQuery query;
    public MenusGroupRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<MenusGroupModel> Post(MenusGroupModel doc) => await query.Post(database, table, doc);
    public async Task<MenusGroupModel> GetSingle(FilterDefinition<MenusGroupModel> filter = null, SortDefinition<MenusGroupModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<MenusGroupModel>> GetList(FilterDefinition<MenusGroupModel> filter, SortDefinition<MenusGroupModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<MenusGroupModel>(FilterDefinition<MenusGroupModel> filter, UpdateDefinition<MenusGroupModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<MenusGroupModel>(FilterDefinition<MenusGroupModel> filter = null) => await query.Delete(database, table, filter);

}
