﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.MenusRearrange;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Menus.MenusRearrange;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Menus.MenusRearrange;

public class MenusRearrangeRepository : RepositoryBase<MenusRearrangeModel>, IMenusRearrangeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.MenusRearrange.ToString();
    public IQuery query;
    public MenusRearrangeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<MenusRearrangeModel> Post(MenusRearrangeModel doc) => await query.Post(database, table, doc);
    public async Task<MenusRearrangeModel> GetSingle(FilterDefinition<MenusRearrangeModel> filter = null, SortDefinition<MenusRearrangeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<MenusRearrangeModel>> GetList(FilterDefinition<MenusRearrangeModel> filter, SortDefinition<MenusRearrangeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<MenusRearrangeModel>(FilterDefinition<MenusRearrangeModel> filter, UpdateDefinition<MenusRearrangeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<MenusRearrangeModel>(FilterDefinition<MenusRearrangeModel> filter = null) => await query.Delete(database, table, filter);

}
