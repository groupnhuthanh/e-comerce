﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Menus.Menu;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Menus.Menu;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Menus.Menu;

public class MenuRepository : RepositoryBase<MenuModel>, IMenuRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Menu.ToString();
    public IQuery query;
    public MenuRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<MenuModel> Post(MenuModel doc) => await query.Post(database, table, doc);
    public async Task<MenuModel> GetSingle(FilterDefinition<MenuModel> filter = null, SortDefinition<MenuModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<MenuModel>> GetList(FilterDefinition<MenuModel> filter, SortDefinition<MenuModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<MenuModel>(FilterDefinition<MenuModel> filter, UpdateDefinition<MenuModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<MenuModel>(FilterDefinition<MenuModel> filter = null) => await query.Delete(database, table, filter);

}
