﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Domains;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Domains;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Domains;

public class DomainsRepository : RepositoryBase<DomainsModel>, IDomainsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Domains.ToString();
    public IQuery query;
    public DomainsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DomainsModel> Post(DomainsModel doc) => await query.Post(database, table, doc);
    public async Task<DomainsModel> GetSingle(FilterDefinition<DomainsModel> filter = null, SortDefinition<DomainsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DomainsModel>> GetList(FilterDefinition<DomainsModel> filter, SortDefinition<DomainsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DomainsModel>(FilterDefinition<DomainsModel> filter, UpdateDefinition<DomainsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DomainsModel>(FilterDefinition<DomainsModel> filter = null) => await query.Delete(database, table, filter);

}
