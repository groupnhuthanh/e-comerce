﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FCMToken;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.FCMToken;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.FCMToken;

public class FCMTokenRepository : RepositoryBase<FCMTokenModel>, IFCMTokenRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.FCMToken.ToString();
    public IQuery query;
    public FCMTokenRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FCMTokenModel> Post(FCMTokenModel doc) => await query.Post(database, table, doc);
    public async Task<FCMTokenModel> GetSingle(FilterDefinition<FCMTokenModel> filter = null, SortDefinition<FCMTokenModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FCMTokenModel>> GetList(FilterDefinition<FCMTokenModel> filter, SortDefinition<FCMTokenModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FCMTokenModel>(FilterDefinition<FCMTokenModel> filter, UpdateDefinition<FCMTokenModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FCMTokenModel>(FilterDefinition<FCMTokenModel> filter = null) => await query.Delete(database, table, filter);

}
