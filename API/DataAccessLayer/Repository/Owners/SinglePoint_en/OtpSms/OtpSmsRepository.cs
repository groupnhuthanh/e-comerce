﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.OtpSms;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.OtpSms;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.OtpSms;

public class OtpSmsRepository : RepositoryBase<OtpSmsModel>, IOtpSmsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.OtpSms.ToString();
    public IQuery query;
    public OtpSmsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OtpSmsModel> Post(OtpSmsModel doc) => await query.Post(database, table, doc);
    public async Task<OtpSmsModel> GetSingle(FilterDefinition<OtpSmsModel> filter = null, SortDefinition<OtpSmsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OtpSmsModel>> GetList(FilterDefinition<OtpSmsModel> filter, SortDefinition<OtpSmsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OtpSmsModel>(FilterDefinition<OtpSmsModel> filter, UpdateDefinition<OtpSmsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OtpSmsModel>(FilterDefinition<OtpSmsModel> filter = null) => await query.Delete(database, table, filter);

}
