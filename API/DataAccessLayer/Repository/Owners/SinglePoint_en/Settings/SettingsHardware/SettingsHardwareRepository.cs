﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Settings.SettingsHardware;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Settings.SettingsHardware;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Settings.SettingsHardware;

public class SettingsHardwareRepository : RepositoryBase<SettingsHardwareModel>, ISettingsHardwareRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.SettingsHardware.ToString();
    public IQuery query;
    public SettingsHardwareRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SettingsHardwareModel> Post(SettingsHardwareModel doc) => await query.Post(database, table, doc);
    public async Task<SettingsHardwareModel> GetSingle(FilterDefinition<SettingsHardwareModel> filter = null, SortDefinition<SettingsHardwareModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SettingsHardwareModel>> GetList(FilterDefinition<SettingsHardwareModel> filter, SortDefinition<SettingsHardwareModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SettingsHardwareModel>(FilterDefinition<SettingsHardwareModel> filter, UpdateDefinition<SettingsHardwareModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SettingsHardwareModel>(FilterDefinition<SettingsHardwareModel> filter = null) => await query.Delete(database, table, filter);

}
