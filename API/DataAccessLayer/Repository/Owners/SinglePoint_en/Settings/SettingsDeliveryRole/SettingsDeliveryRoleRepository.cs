﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Settings.SettingsDeliveryRole;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Settings.SettingsDeliveryRole;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Settings.SettingsDeliveryRole;

public class SettingsDeliveryRoleRepository : RepositoryBase<SettingsDeliveryRoleModel>, ISettingsDeliveryRoleRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.SettingsDeliveryRole.ToString();
    public IQuery query;
    public SettingsDeliveryRoleRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SettingsDeliveryRoleModel> Post(SettingsDeliveryRoleModel doc) => await query.Post(database, table, doc);
    public async Task<SettingsDeliveryRoleModel> GetSingle(FilterDefinition<SettingsDeliveryRoleModel> filter = null, SortDefinition<SettingsDeliveryRoleModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SettingsDeliveryRoleModel>> GetList(FilterDefinition<SettingsDeliveryRoleModel> filter, SortDefinition<SettingsDeliveryRoleModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SettingsDeliveryRoleModel>(FilterDefinition<SettingsDeliveryRoleModel> filter, UpdateDefinition<SettingsDeliveryRoleModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SettingsDeliveryRoleModel>(FilterDefinition<SettingsDeliveryRoleModel> filter = null) => await query.Delete(database, table, filter);

}
