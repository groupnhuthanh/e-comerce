﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Permission;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Permission;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Permission;

public class PermissionRepository : RepositoryBase<PermissionModel>, IPermissionRepository
{

    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Permission.ToString();
    public IQuery query;
    public PermissionRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PermissionModel> Post(PermissionModel doc) => await query.Post(database, table, doc);
    public async Task<PermissionModel> GetSingle(FilterDefinition<PermissionModel> filter = null, SortDefinition<PermissionModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PermissionModel>> GetList(FilterDefinition<PermissionModel> filter, SortDefinition<PermissionModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PermissionModel>(FilterDefinition<PermissionModel> filter, UpdateDefinition<PermissionModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PermissionModel>(FilterDefinition<PermissionModel> filter = null) => await query.Delete(database, table, filter);

}
