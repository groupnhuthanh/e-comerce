﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FAQ.FAQsGroup;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.FAQ.FAQsGroup;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.FAQ.FAQsGroup;

public class FAQsGroupRepository : RepositoryBase<FAQsGroupModel>, IFAQsGroupRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.FAQsGroup.ToString();
    public IQuery query;
    public FAQsGroupRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FAQsGroupModel> Post(FAQsGroupModel doc) => await query.Post(database, table, doc);
    public async Task<FAQsGroupModel> GetSingle(FilterDefinition<FAQsGroupModel> filter = null, SortDefinition<FAQsGroupModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FAQsGroupModel>> GetList(FilterDefinition<FAQsGroupModel> filter, SortDefinition<FAQsGroupModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FAQsGroupModel>(FilterDefinition<FAQsGroupModel> filter, UpdateDefinition<FAQsGroupModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FAQsGroupModel>(FilterDefinition<FAQsGroupModel> filter = null) => await query.Delete(database, table, filter);

}
