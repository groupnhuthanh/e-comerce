﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.FAQ.FAQs;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.FAQ.FAQs;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.FAQ.FAQs;

public class FAQsRepository : RepositoryBase<FAQsModel>, IFAQsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.FAQs.ToString();
    public IQuery query;
    public FAQsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<FAQsModel> Post(FAQsModel doc) => await query.Post(database, table, doc);
    public async Task<FAQsModel> GetSingle(FilterDefinition<FAQsModel> filter = null, SortDefinition<FAQsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<FAQsModel>> GetList(FilterDefinition<FAQsModel> filter, SortDefinition<FAQsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<FAQsModel>(FilterDefinition<FAQsModel> filter, UpdateDefinition<FAQsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<FAQsModel>(FilterDefinition<FAQsModel> filter = null) => await query.Delete(database, table, filter);

}
