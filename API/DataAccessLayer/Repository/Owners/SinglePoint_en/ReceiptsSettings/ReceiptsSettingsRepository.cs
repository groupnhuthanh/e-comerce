﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.ReceiptsSettings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.ReceiptsSettings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.ReceiptsSettings;

public class ReceiptsSettingsRepository : RepositoryBase<ReceiptsSettingsModel>, IReceiptsSettingsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.ReceiptsSettings.ToString();
    public IQuery query;
    public ReceiptsSettingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ReceiptsSettingsModel> Post(ReceiptsSettingsModel doc) => await query.Post(database, table, doc);
    public async Task<ReceiptsSettingsModel> GetSingle(FilterDefinition<ReceiptsSettingsModel> filter = null, SortDefinition<ReceiptsSettingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ReceiptsSettingsModel>> GetList(FilterDefinition<ReceiptsSettingsModel> filter, SortDefinition<ReceiptsSettingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ReceiptsSettingsModel>(FilterDefinition<ReceiptsSettingsModel> filter, UpdateDefinition<ReceiptsSettingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ReceiptsSettingsModel>(FilterDefinition<ReceiptsSettingsModel> filter = null) => await query.Delete(database, table, filter);

}
