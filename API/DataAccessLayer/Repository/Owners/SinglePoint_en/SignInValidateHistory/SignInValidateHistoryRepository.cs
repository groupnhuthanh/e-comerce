﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.SignInValidateHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.SignInValidateHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.SignInValidateHistory;

public class SignInValidateHistoryRepository : RepositoryBase<SignInValidateHistoryModel>, ISignInValidateHistoryRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.SignInValidateHistory.ToString();
    public IQuery query;
    public SignInValidateHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<SignInValidateHistoryModel> Post(SignInValidateHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<SignInValidateHistoryModel> GetSingle(FilterDefinition<SignInValidateHistoryModel> filter = null, SortDefinition<SignInValidateHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<SignInValidateHistoryModel>> GetList(FilterDefinition<SignInValidateHistoryModel> filter, SortDefinition<SignInValidateHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<SignInValidateHistoryModel>(FilterDefinition<SignInValidateHistoryModel> filter, UpdateDefinition<SignInValidateHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<SignInValidateHistoryModel>(FilterDefinition<SignInValidateHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
