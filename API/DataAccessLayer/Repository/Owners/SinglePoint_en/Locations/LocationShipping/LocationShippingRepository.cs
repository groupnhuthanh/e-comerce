﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Locations.LocationShipping;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Locations.LocationShipping;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Locations.LocationShipping;

public class LocationShippingRepository : RepositoryBase<LocationShippingModel>, ILocationShippingRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.LocationShipping.ToString();
    public IQuery query;
    public LocationShippingRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<LocationShippingModel> Post(LocationShippingModel doc) => await query.Post(database, table, doc);
    public async Task<LocationShippingModel> GetSingle(FilterDefinition<LocationShippingModel> filter = null, SortDefinition<LocationShippingModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<LocationShippingModel>> GetList(FilterDefinition<LocationShippingModel> filter, SortDefinition<LocationShippingModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<LocationShippingModel>(FilterDefinition<LocationShippingModel> filter, UpdateDefinition<LocationShippingModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<LocationShippingModel>(FilterDefinition<LocationShippingModel> filter = null) => await query.Delete(database, table, filter);

}
