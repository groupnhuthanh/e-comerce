﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Locations.Location;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Locations.Location;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Locations.Location;

public class LocationRepository : RepositoryBase<LocationModel>, ILocationRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Location.ToString();
    public IQuery query;
    public LocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<LocationModel> Post(LocationModel doc) => await query.Post(database, table, doc);
    public async Task<LocationModel> GetSingle(FilterDefinition<LocationModel> filter = null, SortDefinition<LocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<LocationModel>> GetList(FilterDefinition<LocationModel> filter, SortDefinition<LocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<LocationModel>(FilterDefinition<LocationModel> filter, UpdateDefinition<LocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<LocationModel>(FilterDefinition<LocationModel> filter = null) => await query.Delete(database, table, filter);

}
