﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Locations.LocationSettingsIds;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Locations.LocationSettingsIds;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Locations.LocationSettingsIds;

public class LocationSettingsIdsRepository : RepositoryBase<LocationSettingsIdsModel>, ILocationSettingsIdsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.LocationSettingsIds.ToString();
    public IQuery query;
    public LocationSettingsIdsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<LocationSettingsIdsModel> Post(LocationSettingsIdsModel doc) => await query.Post(database, table, doc);
    public async Task<LocationSettingsIdsModel> GetSingle(FilterDefinition<LocationSettingsIdsModel> filter = null, SortDefinition<LocationSettingsIdsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<LocationSettingsIdsModel>> GetList(FilterDefinition<LocationSettingsIdsModel> filter, SortDefinition<LocationSettingsIdsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<LocationSettingsIdsModel>(FilterDefinition<LocationSettingsIdsModel> filter, UpdateDefinition<LocationSettingsIdsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<LocationSettingsIdsModel>(FilterDefinition<LocationSettingsIdsModel> filter = null) => await query.Delete(database, table, filter);

}
