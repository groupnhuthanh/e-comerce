﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.DateSettings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.DateSettings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.DateSettings;

public class DateSettingsRepository : RepositoryBase<DateSettingsModel>, IDateSettingsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.DateSettings.ToString();
    public IQuery query;
    public DateSettingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<DateSettingsModel> Post(DateSettingsModel doc) => await query.Post(database, table, doc);
    public async Task<DateSettingsModel> GetSingle(FilterDefinition<DateSettingsModel> filter = null, SortDefinition<DateSettingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<DateSettingsModel>> GetList(FilterDefinition<DateSettingsModel> filter, SortDefinition<DateSettingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<DateSettingsModel>(FilterDefinition<DateSettingsModel> filter, UpdateDefinition<DateSettingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<DateSettingsModel>(FilterDefinition<DateSettingsModel> filter = null) => await query.Delete(database, table, filter);

}
