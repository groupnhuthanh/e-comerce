﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Policies;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Policies;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Policies;

public class PoliciesRepository : RepositoryBase<PoliciesModel>, IPoliciesRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Policies.ToString();
    public IQuery query;
    public PoliciesRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PoliciesModel> Post(PoliciesModel doc) => await query.Post(database, table, doc);
    public async Task<PoliciesModel> GetSingle(FilterDefinition<PoliciesModel> filter = null, SortDefinition<PoliciesModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PoliciesModel>> GetList(FilterDefinition<PoliciesModel> filter, SortDefinition<PoliciesModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PoliciesModel>(FilterDefinition<PoliciesModel> filter, UpdateDefinition<PoliciesModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PoliciesModel>(FilterDefinition<PoliciesModel> filter = null) => await query.Delete(database, table, filter);

}
