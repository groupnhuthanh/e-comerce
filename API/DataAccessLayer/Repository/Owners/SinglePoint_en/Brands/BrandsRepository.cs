﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Brands;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Brands;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Brands;

public class BrandsRepository : RepositoryBase<BrandsModel>, IBrandsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Brands.ToString();
    public IQuery query;
    public BrandsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<BrandsModel> Post(BrandsModel doc) => await query.Post(database, table, doc);
    public async Task<BrandsModel> GetSingle(FilterDefinition<BrandsModel> filter = null, SortDefinition<BrandsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<BrandsModel>> GetList(FilterDefinition<BrandsModel> filter, SortDefinition<BrandsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<BrandsModel>(FilterDefinition<BrandsModel> filter, UpdateDefinition<BrandsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<BrandsModel>(FilterDefinition<BrandsModel> filter = null) => await query.Delete(database, table, filter);

}
