﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.MembershipLevel;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.MembershipLevel;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.MembershipLevel;

public class MembershipLevelRepository : RepositoryBase<MembershipLevelModel>, IMembershipLevelRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.MembershipLevel.ToString();
    public IQuery query;
    public MembershipLevelRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<MembershipLevelModel> Post(MembershipLevelModel doc) => await query.Post(database, table, doc);
    public async Task<MembershipLevelModel> GetSingle(FilterDefinition<MembershipLevelModel> filter = null, SortDefinition<MembershipLevelModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<MembershipLevelModel>> GetList(FilterDefinition<MembershipLevelModel> filter, SortDefinition<MembershipLevelModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<MembershipLevelModel>(FilterDefinition<MembershipLevelModel> filter, UpdateDefinition<MembershipLevelModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<MembershipLevelModel>(FilterDefinition<MembershipLevelModel> filter = null) => await query.Delete(database, table, filter);

}
