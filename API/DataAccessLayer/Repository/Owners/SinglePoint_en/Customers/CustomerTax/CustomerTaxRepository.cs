﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerTax;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerTax;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerTax;

public class CustomerTaxRepository : RepositoryBase<CustomerTaxModel>, ICustomerTaxRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerTax.ToString();
    public IQuery query;
    public CustomerTaxRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerTaxModel> Post(CustomerTaxModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerTaxModel> GetSingle(FilterDefinition<CustomerTaxModel> filter = null, SortDefinition<CustomerTaxModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerTaxModel>> GetList(FilterDefinition<CustomerTaxModel> filter, SortDefinition<CustomerTaxModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerTaxModel>(FilterDefinition<CustomerTaxModel> filter, UpdateDefinition<CustomerTaxModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerTaxModel>(FilterDefinition<CustomerTaxModel> filter = null) => await query.Delete(database, table, filter);

}
