﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerLedgerDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerLedgerDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerLedgerDetail;

public class CustomerLedgerDetailRepository : RepositoryBase<CustomerLedgerDetailModel>, ICustomerLedgerDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerLedgerDetail.ToString();
    public IQuery query;
    public CustomerLedgerDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerLedgerDetailModel> Post(CustomerLedgerDetailModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerLedgerDetailModel> GetSingle(FilterDefinition<CustomerLedgerDetailModel> filter = null, SortDefinition<CustomerLedgerDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerLedgerDetailModel>> GetList(FilterDefinition<CustomerLedgerDetailModel> filter, SortDefinition<CustomerLedgerDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerLedgerDetailModel>(FilterDefinition<CustomerLedgerDetailModel> filter, UpdateDefinition<CustomerLedgerDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerLedgerDetailModel>(FilterDefinition<CustomerLedgerDetailModel> filter = null) => await query.Delete(database, table, filter);

}
