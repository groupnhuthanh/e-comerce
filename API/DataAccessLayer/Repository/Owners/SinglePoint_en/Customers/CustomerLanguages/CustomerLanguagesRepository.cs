﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerLanguages;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerLanguages;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerLanguages;

public class CustomerLanguagesRepository : RepositoryBase<CustomerLanguagesModel>, ICustomerLanguagesRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerLanguages.ToString();
    public IQuery query;
    public CustomerLanguagesRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerLanguagesModel> Post(CustomerLanguagesModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerLanguagesModel> GetSingle(FilterDefinition<CustomerLanguagesModel> filter = null, SortDefinition<CustomerLanguagesModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerLanguagesModel>> GetList(FilterDefinition<CustomerLanguagesModel> filter, SortDefinition<CustomerLanguagesModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerLanguagesModel>(FilterDefinition<CustomerLanguagesModel> filter, UpdateDefinition<CustomerLanguagesModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerLanguagesModel>(FilterDefinition<CustomerLanguagesModel> filter = null) => await query.Delete(database, table, filter);

}
