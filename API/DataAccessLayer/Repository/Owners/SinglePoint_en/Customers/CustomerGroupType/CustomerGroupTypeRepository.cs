﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGroupType;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGroupType;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGroupType;

public class CustomerGroupTypeRepository : RepositoryBase<CustomerGroupTypeModel>, ICustomerGroupTypeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGroupType.ToString();
    public IQuery query;
    public CustomerGroupTypeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGroupTypeModel> Post(CustomerGroupTypeModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGroupTypeModel> GetSingle(FilterDefinition<CustomerGroupTypeModel> filter = null, SortDefinition<CustomerGroupTypeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGroupTypeModel>> GetList(FilterDefinition<CustomerGroupTypeModel> filter, SortDefinition<CustomerGroupTypeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGroupTypeModel>(FilterDefinition<CustomerGroupTypeModel> filter, UpdateDefinition<CustomerGroupTypeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGroupTypeModel>(FilterDefinition<CustomerGroupTypeModel> filter = null) => await query.Delete(database, table, filter);

}
