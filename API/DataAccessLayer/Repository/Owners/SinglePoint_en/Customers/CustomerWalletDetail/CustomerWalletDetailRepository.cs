﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerWalletDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerWalletDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerWalletDetail;

public class CustomerWalletDetailRepository : RepositoryBase<CustomerWalletDetailModel>, ICustomerWalletDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerWalletDetail.ToString();
    public IQuery query;
    public CustomerWalletDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerWalletDetailModel> Post(CustomerWalletDetailModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerWalletDetailModel> GetSingle(FilterDefinition<CustomerWalletDetailModel> filter = null, SortDefinition<CustomerWalletDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerWalletDetailModel>> GetList(FilterDefinition<CustomerWalletDetailModel> filter, SortDefinition<CustomerWalletDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerWalletDetailModel>(FilterDefinition<CustomerWalletDetailModel> filter, UpdateDefinition<CustomerWalletDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerWalletDetailModel>(FilterDefinition<CustomerWalletDetailModel> filter = null) => await query.Delete(database, table, filter);

}
