﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerTaxProduct;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerTaxProduct;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerTaxProduct;

public class CustomerTaxProductRepository : RepositoryBase<CustomerTaxProductModel>, ICustomerTaxProductRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerTaxProduct.ToString();
    public IQuery query;
    public CustomerTaxProductRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerTaxProductModel> Post(CustomerTaxProductModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerTaxProductModel> GetSingle(FilterDefinition<CustomerTaxProductModel> filter = null, SortDefinition<CustomerTaxProductModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerTaxProductModel>> GetList(FilterDefinition<CustomerTaxProductModel> filter, SortDefinition<CustomerTaxProductModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerTaxProductModel>(FilterDefinition<CustomerTaxProductModel> filter, UpdateDefinition<CustomerTaxProductModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerTaxProductModel>(FilterDefinition<CustomerTaxProductModel> filter = null) => await query.Delete(database, table, filter);

}
