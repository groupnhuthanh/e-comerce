﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsVerify;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsVerify;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsVerify;

public class CustomerGuestsVerifyRepository : RepositoryBase<CustomerGuestsVerifyModel>, ICustomerGuestsVerifyRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsVerify.ToString();
    public IQuery query;
    public CustomerGuestsVerifyRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsVerifyModel> Post(CustomerGuestsVerifyModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsVerifyModel> GetSingle(FilterDefinition<CustomerGuestsVerifyModel> filter = null, SortDefinition<CustomerGuestsVerifyModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsVerifyModel>> GetList(FilterDefinition<CustomerGuestsVerifyModel> filter, SortDefinition<CustomerGuestsVerifyModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsVerifyModel>(FilterDefinition<CustomerGuestsVerifyModel> filter, UpdateDefinition<CustomerGuestsVerifyModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsVerifyModel>(FilterDefinition<CustomerGuestsVerifyModel> filter = null) => await query.Delete(database, table, filter);

}
