﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerWallet;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerWallet;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerWallet;

public class CustomerWalletRepository : RepositoryBase<CustomerWalletModel>, ICustomerWalletRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerWallet.ToString();
    public IQuery query;
    public CustomerWalletRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerWalletModel> Post(CustomerWalletModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerWalletModel> GetSingle(FilterDefinition<CustomerWalletModel> filter = null, SortDefinition<CustomerWalletModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerWalletModel>> GetList(FilterDefinition<CustomerWalletModel> filter, SortDefinition<CustomerWalletModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerWalletModel>(FilterDefinition<CustomerWalletModel> filter, UpdateDefinition<CustomerWalletModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerWalletModel>(FilterDefinition<CustomerWalletModel> filter = null) => await query.Delete(database, table, filter);

}
