﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerTaxLocation;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerTaxLocation;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerTaxLocation;

public class CustomerTaxLocationRepository : RepositoryBase<CustomerTaxLocationModel>, ICustomerTaxLocationRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerTaxLocation.ToString();
    public IQuery query;
    public CustomerTaxLocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerTaxLocationModel> Post(CustomerTaxLocationModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerTaxLocationModel> GetSingle(FilterDefinition<CustomerTaxLocationModel> filter = null, SortDefinition<CustomerTaxLocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerTaxLocationModel>> GetList(FilterDefinition<CustomerTaxLocationModel> filter, SortDefinition<CustomerTaxLocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerTaxLocationModel>(FilterDefinition<CustomerTaxLocationModel> filter, UpdateDefinition<CustomerTaxLocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerTaxLocationModel>(FilterDefinition<CustomerTaxLocationModel> filter = null) => await query.Delete(database, table, filter);

}
