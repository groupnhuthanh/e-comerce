﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerOthersPaymentsDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerOthersPaymentsDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerOthersPaymentsDetail;

public class CustomerOthersPaymentsDetailRepository : RepositoryBase<CustomerOthersPaymentsDetailModel>, ICustomerOthersPaymentsDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerOthersPaymentsDetail.ToString();
    public IQuery query;
    public CustomerOthersPaymentsDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerOthersPaymentsDetailModel> Post(CustomerOthersPaymentsDetailModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerOthersPaymentsDetailModel> GetSingle(FilterDefinition<CustomerOthersPaymentsDetailModel> filter = null, SortDefinition<CustomerOthersPaymentsDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerOthersPaymentsDetailModel>> GetList(FilterDefinition<CustomerOthersPaymentsDetailModel> filter, SortDefinition<CustomerOthersPaymentsDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerOthersPaymentsDetailModel>(FilterDefinition<CustomerOthersPaymentsDetailModel> filter, UpdateDefinition<CustomerOthersPaymentsDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerOthersPaymentsDetailModel>(FilterDefinition<CustomerOthersPaymentsDetailModel> filter = null) => await query.Delete(database, table, filter);

}
