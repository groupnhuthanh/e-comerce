﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsDiningOption;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsDiningOption;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsDiningOption;

public class CustomerGuestsDiningOptionRepository : RepositoryBase<CustomerGuestsDiningOptionModel>, ICustomerGuestsDiningOptionRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsDiningOption.ToString();
    public IQuery query;
    public CustomerGuestsDiningOptionRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsDiningOptionModel> Post(CustomerGuestsDiningOptionModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsDiningOptionModel> GetSingle(FilterDefinition<CustomerGuestsDiningOptionModel> filter = null, SortDefinition<CustomerGuestsDiningOptionModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsDiningOptionModel>> GetList(FilterDefinition<CustomerGuestsDiningOptionModel> filter, SortDefinition<CustomerGuestsDiningOptionModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsDiningOptionModel>(FilterDefinition<CustomerGuestsDiningOptionModel> filter, UpdateDefinition<CustomerGuestsDiningOptionModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsDiningOptionModel>(FilterDefinition<CustomerGuestsDiningOptionModel> filter = null) => await query.Delete(database, table, filter);

}
