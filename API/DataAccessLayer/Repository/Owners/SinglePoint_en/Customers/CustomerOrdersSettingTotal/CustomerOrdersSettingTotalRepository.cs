﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerOrdersSettingTotal;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerOrdersSettingTotal;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerOrdersSettingTotal;

public class CustomerOrdersSettingTotalRepository : RepositoryBase<CustomerOrdersSettingTotalModel>, ICustomerOrdersSettingTotalRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerOrdersSettingTotal.ToString();
    public IQuery query;
    public CustomerOrdersSettingTotalRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerOrdersSettingTotalModel> Post(CustomerOrdersSettingTotalModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerOrdersSettingTotalModel> GetSingle(FilterDefinition<CustomerOrdersSettingTotalModel> filter = null, SortDefinition<CustomerOrdersSettingTotalModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerOrdersSettingTotalModel>> GetList(FilterDefinition<CustomerOrdersSettingTotalModel> filter, SortDefinition<CustomerOrdersSettingTotalModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerOrdersSettingTotalModel>(FilterDefinition<CustomerOrdersSettingTotalModel> filter, UpdateDefinition<CustomerOrdersSettingTotalModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerOrdersSettingTotalModel>(FilterDefinition<CustomerOrdersSettingTotalModel> filter = null) => await query.Delete(database, table, filter);

}
