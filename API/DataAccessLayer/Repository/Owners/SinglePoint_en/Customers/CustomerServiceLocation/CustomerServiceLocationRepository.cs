﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerServiceLocation;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerServiceLocation;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerServiceLocation;

public class CustomerServiceLocationRepository : RepositoryBase<CustomerServiceLocationModel>, ICustomerServiceLocationRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerServiceLocation.ToString();
    public IQuery query;
    public CustomerServiceLocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerServiceLocationModel> Post(CustomerServiceLocationModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerServiceLocationModel> GetSingle(FilterDefinition<CustomerServiceLocationModel> filter = null, SortDefinition<CustomerServiceLocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerServiceLocationModel>> GetList(FilterDefinition<CustomerServiceLocationModel> filter, SortDefinition<CustomerServiceLocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerServiceLocationModel>(FilterDefinition<CustomerServiceLocationModel> filter, UpdateDefinition<CustomerServiceLocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerServiceLocationModel>(FilterDefinition<CustomerServiceLocationModel> filter = null) => await query.Delete(database, table, filter);

}
