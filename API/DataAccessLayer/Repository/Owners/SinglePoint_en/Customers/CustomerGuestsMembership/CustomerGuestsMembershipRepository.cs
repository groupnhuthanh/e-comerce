﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsMembership;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsMembership;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsMembership;

public class CustomerGuestsMembershipRepository : RepositoryBase<CustomerGuestsMembershipModel>, ICustomerGuestsMembershipRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsMembership.ToString();
    public IQuery query;
    public CustomerGuestsMembershipRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsMembershipModel> Post(CustomerGuestsMembershipModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsMembershipModel> GetSingle(FilterDefinition<CustomerGuestsMembershipModel> filter = null, SortDefinition<CustomerGuestsMembershipModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsMembershipModel>> GetList(FilterDefinition<CustomerGuestsMembershipModel> filter, SortDefinition<CustomerGuestsMembershipModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsMembershipModel>(FilterDefinition<CustomerGuestsMembershipModel> filter, UpdateDefinition<CustomerGuestsMembershipModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsMembershipModel>(FilterDefinition<CustomerGuestsMembershipModel> filter = null) => await query.Delete(database, table, filter);

}
