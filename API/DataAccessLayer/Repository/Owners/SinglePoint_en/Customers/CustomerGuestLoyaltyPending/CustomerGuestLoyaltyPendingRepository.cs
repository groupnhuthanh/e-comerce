﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyPending;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyPending;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyPending;

public class CustomerGuestLoyaltyPendingRepository : RepositoryBase<CustomerGuestLoyaltyPendingModel>, ICustomerGuestLoyaltyPendingRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestLoyaltyPending.ToString();
    public IQuery query;
    public CustomerGuestLoyaltyPendingRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestLoyaltyPendingModel> Post(CustomerGuestLoyaltyPendingModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestLoyaltyPendingModel> GetSingle(FilterDefinition<CustomerGuestLoyaltyPendingModel> filter = null, SortDefinition<CustomerGuestLoyaltyPendingModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestLoyaltyPendingModel>> GetList(FilterDefinition<CustomerGuestLoyaltyPendingModel> filter, SortDefinition<CustomerGuestLoyaltyPendingModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestLoyaltyPendingModel>(FilterDefinition<CustomerGuestLoyaltyPendingModel> filter, UpdateDefinition<CustomerGuestLoyaltyPendingModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestLoyaltyPendingModel>(FilterDefinition<CustomerGuestLoyaltyPendingModel> filter = null) => await query.Delete(database, table, filter);

}
