﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerSurcharge;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerSurcharge;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerSurcharge;

public class CustomerSurchargeRepository : RepositoryBase<CustomerSurchargeModel>, ICustomerSurchargeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerSurcharge.ToString();
    public IQuery query;
    public CustomerSurchargeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerSurchargeModel> Post(CustomerSurchargeModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerSurchargeModel> GetSingle(FilterDefinition<CustomerSurchargeModel> filter = null, SortDefinition<CustomerSurchargeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerSurchargeModel>> GetList(FilterDefinition<CustomerSurchargeModel> filter, SortDefinition<CustomerSurchargeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerSurchargeModel>(FilterDefinition<CustomerSurchargeModel> filter, UpdateDefinition<CustomerSurchargeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerSurchargeModel>(FilterDefinition<CustomerSurchargeModel> filter = null) => await query.Delete(database, table, filter);

}
