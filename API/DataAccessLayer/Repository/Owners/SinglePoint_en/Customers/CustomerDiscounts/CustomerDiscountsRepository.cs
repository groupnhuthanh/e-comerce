﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerDiscounts;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerDiscounts;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerDiscounts;

public class CustomerDiscountsRepository : RepositoryBase<CustomerDiscountsModel>, ICustomerDiscountsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerDiscounts.ToString();
    public IQuery query;
    public CustomerDiscountsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerDiscountsModel> Post(CustomerDiscountsModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerDiscountsModel> GetSingle(FilterDefinition<CustomerDiscountsModel> filter = null, SortDefinition<CustomerDiscountsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerDiscountsModel>> GetList(FilterDefinition<CustomerDiscountsModel> filter, SortDefinition<CustomerDiscountsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerDiscountsModel>(FilterDefinition<CustomerDiscountsModel> filter, UpdateDefinition<CustomerDiscountsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerDiscountsModel>(FilterDefinition<CustomerDiscountsModel> filter = null) => await query.Delete(database, table, filter);


}
