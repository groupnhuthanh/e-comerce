﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerCreditCard;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerCreditCard;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerCreditCard;

public class CustomerCreditCardRepository : RepositoryBase<CustomerCreditCardModel>, ICustomerCreditCardRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerCreditCard.ToString();
    public IQuery query;
    public CustomerCreditCardRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerCreditCardModel> Post(CustomerCreditCardModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerCreditCardModel> GetSingle(FilterDefinition<CustomerCreditCardModel> filter = null, SortDefinition<CustomerCreditCardModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerCreditCardModel>> GetList(FilterDefinition<CustomerCreditCardModel> filter, SortDefinition<CustomerCreditCardModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerCreditCardModel>(FilterDefinition<CustomerCreditCardModel> filter, UpdateDefinition<CustomerCreditCardModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerCreditCardModel>(FilterDefinition<CustomerCreditCardModel> filter = null) => await query.Delete(database, table, filter);

}
