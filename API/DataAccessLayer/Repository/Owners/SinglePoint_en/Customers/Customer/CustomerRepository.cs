﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.Customer;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.Customer;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.Customer;

public class CustomerRepository : RepositoryBase<CustomerModel>, ICustomerRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.Customer.ToString();
    public IQuery query;
    public CustomerRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerModel> Post(CustomerModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerModel> GetSingle(FilterDefinition<CustomerModel> filter = null, SortDefinition<CustomerModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerModel>> GetList(FilterDefinition<CustomerModel> filter, SortDefinition<CustomerModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerModel>(FilterDefinition<CustomerModel> filter, UpdateDefinition<CustomerModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerModel>(FilterDefinition<CustomerModel> filter = null) => await query.Delete(database, table, filter);

}
