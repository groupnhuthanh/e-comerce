﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsBillings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsBillings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsBillings;

public class CustomerGuestsPaymentsBillingsRepository : RepositoryBase<CustomerGuestsPaymentsBillingsModel>, ICustomerGuestsPaymentsBillingsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsPaymentsBillings.ToString();
    public IQuery query;
    public CustomerGuestsPaymentsBillingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsPaymentsBillingsModel> Post(CustomerGuestsPaymentsBillingsModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsPaymentsBillingsModel> GetSingle(FilterDefinition<CustomerGuestsPaymentsBillingsModel> filter = null, SortDefinition<CustomerGuestsPaymentsBillingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsPaymentsBillingsModel>> GetList(FilterDefinition<CustomerGuestsPaymentsBillingsModel> filter, SortDefinition<CustomerGuestsPaymentsBillingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsPaymentsBillingsModel>(FilterDefinition<CustomerGuestsPaymentsBillingsModel> filter, UpdateDefinition<CustomerGuestsPaymentsBillingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsPaymentsBillingsModel>(FilterDefinition<CustomerGuestsPaymentsBillingsModel> filter = null) => await query.Delete(database, table, filter);

}
