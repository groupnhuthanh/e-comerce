﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsMembershipDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsMembershipDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsMembershipDetail;

public class CustomerGuestsMembershipDetailRepository : RepositoryBase<CustomerGuestsMembershipDetailModel>, ICustomerGuestsMembershipDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsMembershipDetail.ToString();
    public IQuery query;
    public CustomerGuestsMembershipDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsMembershipDetailModel> Post(CustomerGuestsMembershipDetailModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsMembershipDetailModel> GetSingle(FilterDefinition<CustomerGuestsMembershipDetailModel> filter = null, SortDefinition<CustomerGuestsMembershipDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsMembershipDetailModel>> GetList(FilterDefinition<CustomerGuestsMembershipDetailModel> filter, SortDefinition<CustomerGuestsMembershipDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsMembershipDetailModel>(FilterDefinition<CustomerGuestsMembershipDetailModel> filter, UpdateDefinition<CustomerGuestsMembershipDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsMembershipDetailModel>(FilterDefinition<CustomerGuestsMembershipDetailModel> filter = null) => await query.Delete(database, table, filter);

}
