﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsLedger;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsLedger;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsLedger;

public class CustomerGuestsLedgerRepository : RepositoryBase<CustomerGuestsLedgerModel>, ICustomerGuestsLedgerRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsLedger.ToString();
    public IQuery query;
    public CustomerGuestsLedgerRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsLedgerModel> Post(CustomerGuestsLedgerModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsLedgerModel> GetSingle(FilterDefinition<CustomerGuestsLedgerModel> filter = null, SortDefinition<CustomerGuestsLedgerModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsLedgerModel>> GetList(FilterDefinition<CustomerGuestsLedgerModel> filter, SortDefinition<CustomerGuestsLedgerModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsLedgerModel>(FilterDefinition<CustomerGuestsLedgerModel> filter, UpdateDefinition<CustomerGuestsLedgerModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsLedgerModel>(FilterDefinition<CustomerGuestsLedgerModel> filter = null) => await query.Delete(database, table, filter);

}
