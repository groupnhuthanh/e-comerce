﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsLoginHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsLoginHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsLoginHistory;

public class CustomerGuestsLoginHistoryRepository : RepositoryBase<CustomerGuestsLoginHistoryModel>, ICustomerGuestsLoginHistoryRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsLoginHistory.ToString();
    public IQuery query;
    public CustomerGuestsLoginHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsLoginHistoryModel> Post(CustomerGuestsLoginHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsLoginHistoryModel> GetSingle(FilterDefinition<CustomerGuestsLoginHistoryModel> filter = null, SortDefinition<CustomerGuestsLoginHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsLoginHistoryModel>> GetList(FilterDefinition<CustomerGuestsLoginHistoryModel> filter, SortDefinition<CustomerGuestsLoginHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsLoginHistoryModel>(FilterDefinition<CustomerGuestsLoginHistoryModel> filter, UpdateDefinition<CustomerGuestsLoginHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsLoginHistoryModel>(FilterDefinition<CustomerGuestsLoginHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
