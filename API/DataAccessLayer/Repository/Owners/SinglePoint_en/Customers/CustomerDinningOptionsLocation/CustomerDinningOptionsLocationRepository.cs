﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerDinningOptionsLocation;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerDinningOptionsLocation;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerDinningOptionsLocation;

public class CustomerDinningOptionsLocationRepository : RepositoryBase<CustomerDinningOptionsLocationModel>, ICustomerDinningOptionsLocationRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerDinningOptionsLocation.ToString();
    public IQuery query;
    public CustomerDinningOptionsLocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerDinningOptionsLocationModel> Post(CustomerDinningOptionsLocationModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerDinningOptionsLocationModel> GetSingle(FilterDefinition<CustomerDinningOptionsLocationModel> filter = null, SortDefinition<CustomerDinningOptionsLocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerDinningOptionsLocationModel>> GetList(FilterDefinition<CustomerDinningOptionsLocationModel> filter, SortDefinition<CustomerDinningOptionsLocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerDinningOptionsLocationModel>(FilterDefinition<CustomerDinningOptionsLocationModel> filter, UpdateDefinition<CustomerDinningOptionsLocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerDinningOptionsLocationModel>(FilterDefinition<CustomerDinningOptionsLocationModel> filter = null) => await query.Delete(database, table, filter);

}
