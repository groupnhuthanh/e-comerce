﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerSurchargeProduct;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerSurchargeProduct;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerSurchargeProduct;

public class CustomerSurchargeProductRepository : RepositoryBase<CustomerSurchargeProductModel>, ICustomerSurchargeProductRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerSurchargeProduct.ToString();
    public IQuery query;
    public CustomerSurchargeProductRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerSurchargeProductModel> Post(CustomerSurchargeProductModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerSurchargeProductModel> GetSingle(FilterDefinition<CustomerSurchargeProductModel> filter = null, SortDefinition<CustomerSurchargeProductModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerSurchargeProductModel>> GetList(FilterDefinition<CustomerSurchargeProductModel> filter, SortDefinition<CustomerSurchargeProductModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerSurchargeProductModel>(FilterDefinition<CustomerSurchargeProductModel> filter, UpdateDefinition<CustomerSurchargeProductModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerSurchargeProductModel>(FilterDefinition<CustomerSurchargeProductModel> filter = null) => await query.Delete(database, table, filter);

}
