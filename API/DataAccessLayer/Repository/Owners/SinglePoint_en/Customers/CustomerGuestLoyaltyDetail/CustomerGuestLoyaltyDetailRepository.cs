﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyDetail;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyDetail;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyDetail;

public class CustomerGuestLoyaltyDetailRepository : RepositoryBase<CustomerGuestLoyaltyDetailModel>, ICustomerGuestLoyaltyDetailRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsGroup.ToString();
    public IQuery query;
    public CustomerGuestLoyaltyDetailRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestLoyaltyDetailModel> Post(CustomerGuestLoyaltyDetailModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestLoyaltyDetailModel> GetSingle(FilterDefinition<CustomerGuestLoyaltyDetailModel> filter = null, SortDefinition<CustomerGuestLoyaltyDetailModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestLoyaltyDetailModel>> GetList(FilterDefinition<CustomerGuestLoyaltyDetailModel> filter, SortDefinition<CustomerGuestLoyaltyDetailModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestLoyaltyDetailModel>(FilterDefinition<CustomerGuestLoyaltyDetailModel> filter, UpdateDefinition<CustomerGuestLoyaltyDetailModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestLoyaltyDetailModel>(FilterDefinition<CustomerGuestLoyaltyDetailModel> filter = null) => await query.Delete(database, table, filter);

}
