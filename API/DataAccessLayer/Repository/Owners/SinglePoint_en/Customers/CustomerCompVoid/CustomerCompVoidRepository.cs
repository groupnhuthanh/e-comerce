﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerCompVoid;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerCompVoid;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerCompVoid;

public class CustomerCompVoidRepository : RepositoryBase<CustomerCompVoidModel>, ICustomerCompVoidRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerCompVoid.ToString();
    public IQuery query;
    public CustomerCompVoidRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerCompVoidModel> Post(CustomerCompVoidModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerCompVoidModel> GetSingle(FilterDefinition<CustomerCompVoidModel> filter = null, SortDefinition<CustomerCompVoidModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerCompVoidModel>> GetList(FilterDefinition<CustomerCompVoidModel> filter, SortDefinition<CustomerCompVoidModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerCompVoidModel>(FilterDefinition<CustomerCompVoidModel> filter, UpdateDefinition<CustomerCompVoidModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerCompVoidModel>(FilterDefinition<CustomerCompVoidModel> filter = null) => await query.Delete(database, table, filter);

}
