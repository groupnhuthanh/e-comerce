﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerOthersPayments;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerOthersPayments;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerOthersPayments;

public class CustomerOthersPaymentsRepository : RepositoryBase<CustomerOthersPaymentsModel>, ICustomerOthersPaymentsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerOthersPayments.ToString();
    public IQuery query;
    public CustomerOthersPaymentsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerOthersPaymentsModel> Post(CustomerOthersPaymentsModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerOthersPaymentsModel> GetSingle(FilterDefinition<CustomerOthersPaymentsModel> filter = null, SortDefinition<CustomerOthersPaymentsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerOthersPaymentsModel>> GetList(FilterDefinition<CustomerOthersPaymentsModel> filter, SortDefinition<CustomerOthersPaymentsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerOthersPaymentsModel>(FilterDefinition<CustomerOthersPaymentsModel> filter, UpdateDefinition<CustomerOthersPaymentsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerOthersPaymentsModel>(FilterDefinition<CustomerOthersPaymentsModel> filter = null) => await query.Delete(database, table, filter);

}
