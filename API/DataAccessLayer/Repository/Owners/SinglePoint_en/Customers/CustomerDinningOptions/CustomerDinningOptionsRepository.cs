﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerDinningOptions;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerDinningOptions;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerDinningOptions;

public class CustomerDinningOptionsRepository : RepositoryBase<CustomerDinningOptionsModel>, ICustomerDinningOptionsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerDinningOptions.ToString();
    public IQuery query;
    public CustomerDinningOptionsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerDinningOptionsModel> Post(CustomerDinningOptionsModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerDinningOptionsModel> GetSingle(FilterDefinition<CustomerDinningOptionsModel> filter = null, SortDefinition<CustomerDinningOptionsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerDinningOptionsModel>> GetList(FilterDefinition<CustomerDinningOptionsModel> filter, SortDefinition<CustomerDinningOptionsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerDinningOptionsModel>(FilterDefinition<CustomerDinningOptionsModel> filter, UpdateDefinition<CustomerDinningOptionsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerDinningOptionsModel>(FilterDefinition<CustomerDinningOptionsModel> filter = null) => await query.Delete(database, table, filter);


}
