﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerSurchargeLocation;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerSurchargeLocation;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerSurchargeLocation;

public class CustomerSurchargeLocationRepository : RepositoryBase<CustomerSurchargeLocationModel>, ICustomerSurchargeLocationRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerSurchargeLocation.ToString();
    public IQuery query;
    public CustomerSurchargeLocationRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerSurchargeLocationModel> Post(CustomerSurchargeLocationModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerSurchargeLocationModel> GetSingle(FilterDefinition<CustomerSurchargeLocationModel> filter = null, SortDefinition<CustomerSurchargeLocationModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerSurchargeLocationModel>> GetList(FilterDefinition<CustomerSurchargeLocationModel> filter, SortDefinition<CustomerSurchargeLocationModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerSurchargeLocationModel>(FilterDefinition<CustomerSurchargeLocationModel> filter, UpdateDefinition<CustomerSurchargeLocationModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerSurchargeLocationModel>(FilterDefinition<CustomerSurchargeLocationModel> filter = null) => await query.Delete(database, table, filter);

}
