﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerCash;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerCash;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerCash;

public class CustomerCashRepository : RepositoryBase<CustomerCashModel>, ICustomerCashRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerCash.ToString();
    public IQuery query;
    public CustomerCashRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerCashModel> Post(CustomerCashModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerCashModel> GetSingle(FilterDefinition<CustomerCashModel> filter = null, SortDefinition<CustomerCashModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerCashModel>> GetList(FilterDefinition<CustomerCashModel> filter, SortDefinition<CustomerCashModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerCashModel>(FilterDefinition<CustomerCashModel> filter, UpdateDefinition<CustomerCashModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerCashModel>(FilterDefinition<CustomerCashModel> filter = null) => await query.Delete(database, table, filter);

}
