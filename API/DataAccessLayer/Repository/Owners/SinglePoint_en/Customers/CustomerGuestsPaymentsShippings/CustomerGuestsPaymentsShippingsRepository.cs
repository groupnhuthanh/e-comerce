﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsShippings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsShippings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsShippings;

public class CustomerGuestsPaymentsShippingsRepository : RepositoryBase<CustomerGuestsPaymentsShippingsModel>, ICustomerGuestsPaymentsShippingsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsPaymentsShippings.ToString();
    public IQuery query;
    public CustomerGuestsPaymentsShippingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsPaymentsShippingsModel> Post(CustomerGuestsPaymentsShippingsModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsPaymentsShippingsModel> GetSingle(FilterDefinition<CustomerGuestsPaymentsShippingsModel> filter = null, SortDefinition<CustomerGuestsPaymentsShippingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsPaymentsShippingsModel>> GetList(FilterDefinition<CustomerGuestsPaymentsShippingsModel> filter, SortDefinition<CustomerGuestsPaymentsShippingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsPaymentsShippingsModel>(FilterDefinition<CustomerGuestsPaymentsShippingsModel> filter, UpdateDefinition<CustomerGuestsPaymentsShippingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsPaymentsShippingsModel>(FilterDefinition<CustomerGuestsPaymentsShippingsModel> filter = null) => await query.Delete(database, table, filter);

}
