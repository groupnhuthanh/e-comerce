﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuests;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuests;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuests;

public class CustomerGuestsRepository : RepositoryBase<CustomerGuestsModel>, ICustomerGuestsRepository
{

    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuests.ToString();
    public IQuery query;
    public CustomerGuestsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsModel> Post(CustomerGuestsModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsModel> GetSingle(FilterDefinition<CustomerGuestsModel> filter = null, SortDefinition<CustomerGuestsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsModel>> GetList(FilterDefinition<CustomerGuestsModel> filter, SortDefinition<CustomerGuestsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsModel>(FilterDefinition<CustomerGuestsModel> filter, UpdateDefinition<CustomerGuestsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsModel>(FilterDefinition<CustomerGuestsModel> filter = null) => await query.Delete(database, table, filter);

}