﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsDeleteHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsDeleteHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsDeleteHistory;

public class CustomerGuestsDeleteHistoryRepository : RepositoryBase<CustomerGuestsDeleteHistoryModel>, ICustomerGuestsDeleteHistoryRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsDeleteHistory.ToString();
    public IQuery query;
    public CustomerGuestsDeleteHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsDeleteHistoryModel> Post(CustomerGuestsDeleteHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsDeleteHistoryModel> GetSingle(FilterDefinition<CustomerGuestsDeleteHistoryModel> filter = null, SortDefinition<CustomerGuestsDeleteHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsDeleteHistoryModel>> GetList(FilterDefinition<CustomerGuestsDeleteHistoryModel> filter, SortDefinition<CustomerGuestsDeleteHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsDeleteHistoryModel>(FilterDefinition<CustomerGuestsDeleteHistoryModel> filter, UpdateDefinition<CustomerGuestsDeleteHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsDeleteHistoryModel>(FilterDefinition<CustomerGuestsDeleteHistoryModel> filter = null) => await query.Delete(database, table, filter);


}
