﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerLedger;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerLedger;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerLedger;

public class CustomerLedgerRepository : RepositoryBase<CustomerLedgerModel>, ICustomerLedgerRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerLedger.ToString();
    public IQuery query;
    public CustomerLedgerRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerLedgerModel> Post(CustomerLedgerModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerLedgerModel> GetSingle(FilterDefinition<CustomerLedgerModel> filter = null, SortDefinition<CustomerLedgerModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerLedgerModel>> GetList(FilterDefinition<CustomerLedgerModel> filter, SortDefinition<CustomerLedgerModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerLedgerModel>(FilterDefinition<CustomerLedgerModel> filter, UpdateDefinition<CustomerLedgerModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerLedgerModel>(FilterDefinition<CustomerLedgerModel> filter = null) => await query.Delete(database, table, filter);

}
