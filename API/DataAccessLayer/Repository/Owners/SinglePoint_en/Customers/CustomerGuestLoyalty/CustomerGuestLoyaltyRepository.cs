﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestLoyalty;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyalty;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestLoyalty;

public class CustomerGuestLoyaltyRepository : RepositoryBase<CustomerGuestLoyaltyModel>, ICustomerGuestLoyaltyRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestLoyalty.ToString();
    public IQuery query;
    public CustomerGuestLoyaltyRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestLoyaltyModel> Post(CustomerGuestLoyaltyModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestLoyaltyModel> GetSingle(FilterDefinition<CustomerGuestLoyaltyModel> filter = null, SortDefinition<CustomerGuestLoyaltyModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestLoyaltyModel>> GetList(FilterDefinition<CustomerGuestLoyaltyModel> filter, SortDefinition<CustomerGuestLoyaltyModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestLoyaltyModel>(FilterDefinition<CustomerGuestLoyaltyModel> filter, UpdateDefinition<CustomerGuestLoyaltyModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestLoyaltyModel>(FilterDefinition<CustomerGuestLoyaltyModel> filter = null) => await query.Delete(database, table, filter);

}
