﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerSettings;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerSettings;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerSettings;

public class CustomerSettingsRepository : RepositoryBase<CustomerSettingsModel>, ICustomerSettingsRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerSettings.ToString();
    public IQuery query;
    public CustomerSettingsRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerSettingsModel> Post(CustomerSettingsModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerSettingsModel> GetSingle(FilterDefinition<CustomerSettingsModel> filter = null, SortDefinition<CustomerSettingsModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerSettingsModel>> GetList(FilterDefinition<CustomerSettingsModel> filter, SortDefinition<CustomerSettingsModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerSettingsModel>(FilterDefinition<CustomerSettingsModel> filter, UpdateDefinition<CustomerSettingsModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerSettingsModel>(FilterDefinition<CustomerSettingsModel> filter = null) => await query.Delete(database, table, filter);

}
