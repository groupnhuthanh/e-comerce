﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsVerifyExchange;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsVerifyExchange;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsVerifyExchange;

public class CustomerGuestsVerifyExchangeRepository : RepositoryBase<CustomerGuestsVerifyExchangeModel>, ICustomerGuestsVerifyExchangeRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsVerifyExchange.ToString();
    public IQuery query;
    public CustomerGuestsVerifyExchangeRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsVerifyExchangeModel> Post(CustomerGuestsVerifyExchangeModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsVerifyExchangeModel> GetSingle(FilterDefinition<CustomerGuestsVerifyExchangeModel> filter = null, SortDefinition<CustomerGuestsVerifyExchangeModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsVerifyExchangeModel>> GetList(FilterDefinition<CustomerGuestsVerifyExchangeModel> filter, SortDefinition<CustomerGuestsVerifyExchangeModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsVerifyExchangeModel>(FilterDefinition<CustomerGuestsVerifyExchangeModel> filter, UpdateDefinition<CustomerGuestsVerifyExchangeModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsVerifyExchangeModel>(FilterDefinition<CustomerGuestsVerifyExchangeModel> filter = null) => await query.Delete(database, table, filter);

}
