﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerService;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerService;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerService;

public class CustomerServiceRepository : RepositoryBase<CustomerServiceModel>, ICustomerServiceRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerService.ToString();
    public IQuery query;
    public CustomerServiceRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerServiceModel> Post(CustomerServiceModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerServiceModel> GetSingle(FilterDefinition<CustomerServiceModel> filter = null, SortDefinition<CustomerServiceModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerServiceModel>> GetList(FilterDefinition<CustomerServiceModel> filter, SortDefinition<CustomerServiceModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerServiceModel>(FilterDefinition<CustomerServiceModel> filter, UpdateDefinition<CustomerServiceModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerServiceModel>(FilterDefinition<CustomerServiceModel> filter = null) => await query.Delete(database, table, filter);

}
