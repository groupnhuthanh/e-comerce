﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerGuestsGroup;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuestsGroup;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerGuestsGroup;

public class CustomerGuestsGroupRepository : RepositoryBase<CustomerGuestsGroupModel>, ICustomerGuestsGroupRepository

{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerGuestsGroup.ToString();
    public IQuery query;
    public CustomerGuestsGroupRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerGuestsGroupModel> Post(CustomerGuestsGroupModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerGuestsGroupModel> GetSingle(FilterDefinition<CustomerGuestsGroupModel> filter = null, SortDefinition<CustomerGuestsGroupModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerGuestsGroupModel>> GetList(FilterDefinition<CustomerGuestsGroupModel> filter, SortDefinition<CustomerGuestsGroupModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerGuestsGroupModel>(FilterDefinition<CustomerGuestsGroupModel> filter, UpdateDefinition<CustomerGuestsGroupModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerGuestsGroupModel>(FilterDefinition<CustomerGuestsGroupModel> filter = null) => await query.Delete(database, table, filter);


}
