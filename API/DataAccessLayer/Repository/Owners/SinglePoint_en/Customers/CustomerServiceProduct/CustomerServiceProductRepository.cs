﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.Customers.CustomerServiceProduct;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.Customers.CustomerServiceProduct;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.Customers.CustomerServiceProduct;

public class CustomerServiceProductRepository : RepositoryBase<CustomerServiceProductModel>, ICustomerServiceProductRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.CustomerServiceProduct.ToString();
    public IQuery query;
    public CustomerServiceProductRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<CustomerServiceProductModel> Post(CustomerServiceProductModel doc) => await query.Post(database, table, doc);
    public async Task<CustomerServiceProductModel> GetSingle(FilterDefinition<CustomerServiceProductModel> filter = null, SortDefinition<CustomerServiceProductModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<CustomerServiceProductModel>> GetList(FilterDefinition<CustomerServiceProductModel> filter, SortDefinition<CustomerServiceProductModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<CustomerServiceProductModel>(FilterDefinition<CustomerServiceProductModel> filter, UpdateDefinition<CustomerServiceProductModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<CustomerServiceProductModel>(FilterDefinition<CustomerServiceProductModel> filter = null) => await query.Delete(database, table, filter);

}
