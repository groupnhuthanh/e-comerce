﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.VariationPriceOverride;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.VariationPriceOverride;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.VariationPriceOverride;

public class VariationPriceOverrideRepository : RepositoryBase<VariationPriceOverrideModel>, IVariationPriceOverrideRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.VariationPriceOverride.ToString();
    public IQuery query;
    public VariationPriceOverrideRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<VariationPriceOverrideModel> Post(VariationPriceOverrideModel doc) => await query.Post(database, table, doc);
    public async Task<VariationPriceOverrideModel> GetSingle(FilterDefinition<VariationPriceOverrideModel> filter = null, SortDefinition<VariationPriceOverrideModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<VariationPriceOverrideModel>> GetList(FilterDefinition<VariationPriceOverrideModel> filter, SortDefinition<VariationPriceOverrideModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<VariationPriceOverrideModel>(FilterDefinition<VariationPriceOverrideModel> filter, UpdateDefinition<VariationPriceOverrideModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<VariationPriceOverrideModel>(FilterDefinition<VariationPriceOverrideModel> filter = null) => await query.Delete(database, table, filter);

}
