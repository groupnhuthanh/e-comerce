﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_en.VoucherPayment;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_en.VoucherPayment;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_en.VoucherPayment;

public class VoucherPaymentRepository : RepositoryBase<VoucherPaymentModel>, IVoucherPaymentRepository
{
    private readonly string database = Databases.SinglePoint_en.ToString();
    private readonly string table = SinglePoint_enTables.VoucherPayment.ToString();
    public IQuery query;
    public VoucherPaymentRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<VoucherPaymentModel> Post(VoucherPaymentModel doc) => await query.Post(database, table, doc);
    public async Task<VoucherPaymentModel> GetSingle(FilterDefinition<VoucherPaymentModel> filter = null, SortDefinition<VoucherPaymentModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<VoucherPaymentModel>> GetList(FilterDefinition<VoucherPaymentModel> filter, SortDefinition<VoucherPaymentModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<VoucherPaymentModel>(FilterDefinition<VoucherPaymentModel> filter, UpdateDefinition<VoucherPaymentModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<VoucherPaymentModel>(FilterDefinition<VoucherPaymentModel> filter = null) => await query.Delete(database, table, filter);

}
