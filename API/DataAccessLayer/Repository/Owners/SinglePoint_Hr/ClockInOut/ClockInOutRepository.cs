﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Hr.ClockInOut;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Hr.ClockInOut;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Hr.ClockInOut;

public class ClockInOutRepository : RepositoryBase<ClockInOutModel>, IClockInOutRepository
{
    private readonly string database = Databases.SinglePoint_Hr.ToString();
    private readonly string table = SinglePoint_HrTables.ClockInOut.ToString();
    public IQuery query;
    public ClockInOutRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ClockInOutModel> Post(ClockInOutModel doc) => await query.Post(database, table, doc);
    public async Task<ClockInOutModel> GetSingle(FilterDefinition<ClockInOutModel> filter = null, SortDefinition<ClockInOutModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ClockInOutModel>> GetList(FilterDefinition<ClockInOutModel> filter, SortDefinition<ClockInOutModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ClockInOutModel>(FilterDefinition<ClockInOutModel> filter, UpdateDefinition<ClockInOutModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ClockInOutModel>(FilterDefinition<ClockInOutModel> filter = null) => await query.Delete(database, table, filter);

}
