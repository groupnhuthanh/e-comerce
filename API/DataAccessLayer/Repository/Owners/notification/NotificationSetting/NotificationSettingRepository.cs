﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.notification.NotificationSetting;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.notification.NotificationSetting;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.notification.NotificationSetting;

public class NotificationSettingRepository : RepositoryBase<NotificationSettingModel>, INotificationSettingRepository
{
    private readonly string database = Databases.notification.ToString();
    private readonly string table = notificationTables.NotificationSetting.ToString();
    public IQuery query;
    public NotificationSettingRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<NotificationSettingModel> Post(NotificationSettingModel doc) => await query.Post(database, table, doc);
    public async Task<NotificationSettingModel> GetSingle(FilterDefinition<NotificationSettingModel> filter = null, SortDefinition<NotificationSettingModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<NotificationSettingModel>> GetList(FilterDefinition<NotificationSettingModel> filter, SortDefinition<NotificationSettingModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<NotificationSettingModel>(FilterDefinition<NotificationSettingModel> filter, UpdateDefinition<NotificationSettingModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<NotificationSettingModel>(FilterDefinition<NotificationSettingModel> filter = null) => await query.Delete(database, table, filter);
}
