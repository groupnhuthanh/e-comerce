﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.notification.EmailSetting;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.notification.EmailSetting;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.notification.EmailSetting;

public class EmailSettingRepository : RepositoryBase<EmailSettingModel>, IEmailSettingRepository
{
    private readonly string database = Databases.notification.ToString();
    private readonly string table = notificationTables.EmailSetting.ToString();
    public IQuery query;
    public EmailSettingRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<EmailSettingModel> Post(EmailSettingModel doc) => await query.Post(database, table, doc);
    public async Task<EmailSettingModel> GetSingle(FilterDefinition<EmailSettingModel> filter = null, SortDefinition<EmailSettingModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<EmailSettingModel>> GetList(FilterDefinition<EmailSettingModel> filter, SortDefinition<EmailSettingModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<EmailSettingModel>(FilterDefinition<EmailSettingModel> filter, UpdateDefinition<EmailSettingModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<EmailSettingModel>(FilterDefinition<EmailSettingModel> filter = null) => await query.Delete(database, table, filter);

}
