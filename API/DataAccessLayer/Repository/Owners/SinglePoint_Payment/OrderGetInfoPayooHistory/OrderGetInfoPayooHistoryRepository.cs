﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.OrderGetInfoPayooHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Payment.OrderGetInfoPayooHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Payment.OrderGetInfoPayooHistory;

public class OrderGetInfoPayooHistoryRepository : RepositoryBase<OrderGetInfoPayooHistoryModel>, IOrderGetInfoPayooHistoryRepository
{
    private readonly string database = Databases.SinglePoint_Payment.ToString();
    private readonly string table = SinglePoint_PaymentTables.OrderGetInfoPayooHistory.ToString();
    public IQuery query;
    public OrderGetInfoPayooHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderGetInfoPayooHistoryModel> Post(OrderGetInfoPayooHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<OrderGetInfoPayooHistoryModel> GetSingle(FilterDefinition<OrderGetInfoPayooHistoryModel> filter = null, SortDefinition<OrderGetInfoPayooHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderGetInfoPayooHistoryModel>> GetList(FilterDefinition<OrderGetInfoPayooHistoryModel> filter, SortDefinition<OrderGetInfoPayooHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderGetInfoPayooHistoryModel>(FilterDefinition<OrderGetInfoPayooHistoryModel> filter, UpdateDefinition<OrderGetInfoPayooHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderGetInfoPayooHistoryModel>(FilterDefinition<OrderGetInfoPayooHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
