﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.ClSOrderPaymentPrepaid;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Payment.ClSOrderPaymentPrepaid;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Payment.ClSOrderPaymentPrepaid;

public class ClSOrderPaymentPrepaidRepository : RepositoryBase<ClSOrderPaymentPrepaidModel>, IClSOrderPaymentPrepaidRepository
{
    private readonly string database = Databases.SinglePoint_Payment.ToString();
    private readonly string table = SinglePoint_PaymentTables.ClSOrderPaymentPrepaid.ToString();
    public IQuery query;
    public ClSOrderPaymentPrepaidRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<ClSOrderPaymentPrepaidModel> Post(ClSOrderPaymentPrepaidModel doc) => await query.Post(database, table, doc);
    public async Task<ClSOrderPaymentPrepaidModel> GetSingle(FilterDefinition<ClSOrderPaymentPrepaidModel> filter = null, SortDefinition<ClSOrderPaymentPrepaidModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<ClSOrderPaymentPrepaidModel>> GetList(FilterDefinition<ClSOrderPaymentPrepaidModel> filter, SortDefinition<ClSOrderPaymentPrepaidModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<ClSOrderPaymentPrepaidModel>(FilterDefinition<ClSOrderPaymentPrepaidModel> filter, UpdateDefinition<ClSOrderPaymentPrepaidModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<ClSOrderPaymentPrepaidModel>(FilterDefinition<ClSOrderPaymentPrepaidModel> filter = null) => await query.Delete(database, table, filter);

}
