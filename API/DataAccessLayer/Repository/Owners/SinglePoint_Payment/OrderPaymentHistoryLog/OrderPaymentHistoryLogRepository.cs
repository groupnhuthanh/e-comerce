﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.OrderPaymentHistoryLog;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Payment.OrderPaymentHistoryLog;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Payment.OrderPaymentHistoryLog;

public class OrderPaymentHistoryLogRepository : RepositoryBase<OrderPaymentHistoryLogModel>, IOrderPaymentHistoryLogRepository
{
    private readonly string database = Databases.SinglePoint_Payment.ToString();
    private readonly string table = SinglePoint_PaymentTables.OrderPaymentHistoryLog.ToString();
    public IQuery query;
    public OrderPaymentHistoryLogRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderPaymentHistoryLogModel> Post(OrderPaymentHistoryLogModel doc) => await query.Post(database, table, doc);
    public async Task<OrderPaymentHistoryLogModel> GetSingle(FilterDefinition<OrderPaymentHistoryLogModel> filter = null, SortDefinition<OrderPaymentHistoryLogModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderPaymentHistoryLogModel>> GetList(FilterDefinition<OrderPaymentHistoryLogModel> filter, SortDefinition<OrderPaymentHistoryLogModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderPaymentHistoryLogModel>(FilterDefinition<OrderPaymentHistoryLogModel> filter, UpdateDefinition<OrderPaymentHistoryLogModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderPaymentHistoryLogModel>(FilterDefinition<OrderPaymentHistoryLogModel> filter = null) => await query.Delete(database, table, filter);

}
