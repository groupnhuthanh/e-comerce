﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.OrderPaymentPayooNotify;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Payment.OrderPaymentPayooNotify;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Payment.OrderPaymentPayooNotify;

public class OrderPaymentPayooNotifyRepository : RepositoryBase<OrderPaymentPayooNotifyModel>, IOrderPaymentPayooNotifyRepository
{
    private readonly string database = Databases.SinglePoint_Payment.ToString();
    private readonly string table = SinglePoint_PaymentTables.OrderPaymentPayooNotify.ToString();
    public IQuery query;
    public OrderPaymentPayooNotifyRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderPaymentPayooNotifyModel> Post(OrderPaymentPayooNotifyModel doc) => await query.Post(database, table, doc);
    public async Task<OrderPaymentPayooNotifyModel> GetSingle(FilterDefinition<OrderPaymentPayooNotifyModel> filter = null, SortDefinition<OrderPaymentPayooNotifyModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderPaymentPayooNotifyModel>> GetList(FilterDefinition<OrderPaymentPayooNotifyModel> filter, SortDefinition<OrderPaymentPayooNotifyModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderPaymentPayooNotifyModel>(FilterDefinition<OrderPaymentPayooNotifyModel> filter, UpdateDefinition<OrderPaymentPayooNotifyModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderPaymentPayooNotifyModel>(FilterDefinition<OrderPaymentPayooNotifyModel> filter = null) => await query.Delete(database, table, filter);


}
