﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.PayooXML;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Payment.PayooXML;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Payment.PayooXML;

public class PayooXMLRepository : RepositoryBase<PayooXMLModel>, IPayooXMLRepository
{
    private readonly string database = Databases.SinglePoint_Payment.ToString();
    private readonly string table = SinglePoint_PaymentTables.PayooXML.ToString();
    public IQuery query;
    public PayooXMLRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<PayooXMLModel> Post(PayooXMLModel doc) => await query.Post(database, table, doc);
    public async Task<PayooXMLModel> GetSingle(FilterDefinition<PayooXMLModel> filter = null, SortDefinition<PayooXMLModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<PayooXMLModel>> GetList(FilterDefinition<PayooXMLModel> filter, SortDefinition<PayooXMLModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<PayooXMLModel>(FilterDefinition<PayooXMLModel> filter, UpdateDefinition<PayooXMLModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<PayooXMLModel>(FilterDefinition<PayooXMLModel> filter = null) => await query.Delete(database, table, filter);

}
