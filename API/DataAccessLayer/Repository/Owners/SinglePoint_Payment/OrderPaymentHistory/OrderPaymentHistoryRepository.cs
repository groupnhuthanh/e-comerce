﻿using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Owners.SinglePoint_Payment.OrderPaymentHistory;
using DataAccessLayer.Repository.Base;
using Entities.Enums.Databases;
using Entities.Enums.Tables;
using Entities.Owners.SinglePoint_Payment.OrderPaymentHistory;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Owners.SinglePoint_Payment.OrderPaymentHistory;

public class OrderPaymentHistoryRepository : RepositoryBase<OrderPaymentHistoryModel>, IOrderPaymentHistoryRepository
{
    private readonly string database = Databases.SinglePoint_Orders_vi.ToString();
    private readonly string table = SinglePoint_PaymentTables.OrderPaymentHistory.ToString();
    public IQuery query;
    public OrderPaymentHistoryRepository(IQuery query) : base(query)
    {
        this.query = query;
    }

    public async Task<OrderPaymentHistoryModel> Post(OrderPaymentHistoryModel doc) => await query.Post(database, table, doc);
    public async Task<OrderPaymentHistoryModel> GetSingle(FilterDefinition<OrderPaymentHistoryModel> filter = null, SortDefinition<OrderPaymentHistoryModel> sort = null, int limit = 0) => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<List<OrderPaymentHistoryModel>> GetList(FilterDefinition<OrderPaymentHistoryModel> filter, SortDefinition<OrderPaymentHistoryModel> sort, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<bool> Put<OrderPaymentHistoryModel>(FilterDefinition<OrderPaymentHistoryModel> filter, UpdateDefinition<OrderPaymentHistoryModel> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<bool> Delete<OrderPaymentHistoryModel>(FilterDefinition<OrderPaymentHistoryModel> filter = null) => await query.Delete(database, table, filter);

}
