﻿using BusinessLogicLayer.IRepository.Base;
using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using MongoDB.Driver;

namespace DataAccessLayer.Repository.Base;

public abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity>
{
    readonly IQuery query;
    /// <summary>
    /// Initializes a new instance of the <see cref="RepositoryBase"/> class.
    /// </summary>
    /// <param name="query">The query.</param>
    protected RepositoryBase(IQuery query)
    {
        this.query = query;
    }
    public async Task<List<TEntity>> GetList<TEntity>(string database, string table, FilterDefinition<TEntity> filter = null, SortDefinition<TEntity> sort = null, int limit = 0) => await query.GetList(database, table, filter, sort, limit);
    public async Task<TEntity> GetSingle<TEntity>(string database, string table, FilterDefinition<TEntity> filter = null, SortDefinition<TEntity> sort = null, int limit = 0) where TEntity : new() => await query.GetSingle(database, table, filter, sort, limit);
    public async Task<bool> Delete<TEntity>(string database, string table, FilterDefinition<TEntity> filter) => await query.Delete<TEntity>(database, table, filter);
    public async Task<bool> Put<TEntity>(string database, string table, FilterDefinition<TEntity> filter, UpdateDefinition<TEntity> updateDefinition) => await query.Put(database, table, filter, updateDefinition);
    public async Task<TEntity> Post<TEntity>(string database, string table, TEntity doc) => await query.Post(database, table, doc);
}
