﻿using BusinessLogicLayer.BusinessLogic.Wrapper;
using BusinessLogicLayer.ILogicCommon.IManagerCommon.Wrapper;
using BusinessLogicLayer.ILogicCommon.IPipeLogic.Wrapper;
using BusinessLogicLayer.IRepository.Wrapper;
using Entities.ExtendModels.System.Config;
using Microsoft.Extensions.Options;

namespace DataAccessLayer.DataAccess.Wrapper;

public class DataAccessWrapper : BusinessLogicWrapper
{

    private readonly IManagerCommonWrapper _common;
    private readonly IPipeLogicWrapper _pipe;
    private readonly IRepositoryWrapper _repository;
    IOptions<AppDomainSettings> _domainSettings;




    public DataAccessWrapper(IPipeLogicWrapper pipe, IManagerCommonWrapper common, IRepositoryWrapper repository, IOptions<AppDomainSettings> domainSettings)
    {

        _pipe = pipe;
        _common = common;
        _repository = repository;
        _domainSettings = domainSettings;

    }

}
