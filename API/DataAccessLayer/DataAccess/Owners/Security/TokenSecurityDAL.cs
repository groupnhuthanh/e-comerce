﻿using BusinessLogicLayer.BusinessLogic.Owners.Security;
using BusinessLogicLayer.IRepository.Wrapper;
using Entities.ExtendModels.Files;
using Entities.ExtendModels.NullDySession;
using Entities.ExtendModels.System.Config;
using Entities.ExtendModels.Tokens;
using Entities.Owners.SinglePoint_en.Customers.CustomerGuests;
using Entities.Owners.SinglePoint_en.Devices.Device;
using Entities.Owners.SinglePoint_en.Devices.DeviceRequestHistory;
using Helpers.Convert;
using Helpers.Encryption;
using Helpers.Files;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Text;
using System.Web;

namespace DataAccessLayer.DataAccess.Owners.Security
{
    public class TokenSecurityDAL : ITokenSecurityBLL
    {
        TokenView tokenView = NullDySession.TokenUser ?? new TokenView();
        private readonly IRepositoryWrapper _repository;
        private readonly IOptions<AppDomainSettings> _domainSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenSecurityDal"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public TokenSecurityDAL(IRepositoryWrapper repository, IOptions<AppDomainSettings> domainSettings)
        {
            _repository = repository;
            _domainSettings = domainSettings;
        }

        public async Task<bool> TokenValidate()
        {
            var result = true;
            if (tokenView != null)
            {
                var device_id = tokenView.DeviceGuid;
                var user_id = tokenView.UserGuid;
                var location_id = tokenView.LocationGuid;
                var customer_id = tokenView.CustomerGuid;
                if (string.IsNullOrEmpty(device_id) || string.IsNullOrEmpty(user_id) || string.IsNullOrEmpty(location_id))
                {
                    result = false;
                }
                else
                {
                    if (!string.IsNullOrEmpty(customer_id))
                    {
                        var filter = Builders<CustomerGuestsModel>.Filter.Eq("Visible", 1)
                            & Builders<CustomerGuestsModel>.Filter.Eq("UserGuid", user_id)
                            & Builders<CustomerGuestsModel>.Filter.Eq("_id", customer_id);
                        var ieCustomerGuestList = await _repository.customerGuestsRepository.GetList(filter, null);
                        if (ieCustomerGuestList.Count() == 0)
                        {
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Tokens the h m a c validate.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="isRequiredHMAC">If true, is required h m a c.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns>A Task.</returns>
        public async Task<TokenValidate> TokenHMACValidate(HttpContext context, bool isRequiredHMAC = false, Encoding encoding = null)
        {
            var result = new TokenValidate();
            try
            {
                //isRequiredHMAC = false;//only test
                if (isRequiredHMAC)
                {
                    #region Read Body Request data
                    var request = context.Request;
                    request.Body.Position = 0;
                    var reader = new StreamReader(request.Body, encoding ?? Encoding.UTF8);
                    var body = await reader.ReadToEndAsync().ConfigureAwait(false);
                    request.Body.Position = 0;
                    var decode = "";
                    //if (body == "" || body.ToLower() == "null")
                    //{
                    //    result.message = "Nội dung yêu cầu không tìm thấy";
                    //    return result;
                    //}
                    if (body != "")
                    {
                        if (body.Contains('+'))
                        {
                            var specialChar = HttpUtility.UrlEncode("+");
                            body = body.Replace("+", specialChar);
                        }
                        decode = HttpUtility.UrlDecode(body.Trim());//lỗi html - kiểm tra cách cho phép html lưu xuống
                    }
                    #endregion
                    //if (tokenView == null || tokenView.UserGuid == null || tokenView.DeviceGuid == null)
                    //{

                    //}

                    var header = request.Headers;
                    var userItem = context.Items.ContainsKey("User") ? ConvertHelper.Serialize(context.Items["User"]) : "";
                    if (userItem != "")
                    {
                        var item = ConvertHelper.Deserialize<Dictionary<string, object>>(userItem);
                        if (item != null && item.Any())
                        {
                            if (string.IsNullOrEmpty(tokenView.DeviceGuid))
                            {
                                tokenView.DeviceGuid = item["DeviceGuid"].ToString() ?? "";
                            }
                            if (string.IsNullOrEmpty(tokenView.LocationGuid))
                            {
                                tokenView.LocationGuid = item["LocationGuid"].ToString() ?? "";
                            }
                            if (string.IsNullOrEmpty(tokenView.UserGuid))
                            {
                                tokenView.UserGuid = item["UserGuid"].ToString() ?? "";
                            }
                            if (string.IsNullOrEmpty(tokenView.CustomerGuid))
                            {
                                tokenView.CustomerGuid = item.ContainsKey("CustomerGuid") ? item["CustomerGuid"].ToString() ?? "" : "";
                            }
                            if (string.IsNullOrEmpty(tokenView.X_AUTH_DeviceCode))
                            {
                                tokenView.X_AUTH_DeviceCode = item["X-AUTH-DeviceCode"].ToString() ?? "";
                            }
                            tokenView.X_AUTH_Nonce = item["X-AUTH-Nonce"].ToString() ?? "";
                            if (string.IsNullOrEmpty(tokenView.X_AUTH_Signature))
                            {
                                tokenView.X_AUTH_Signature = item["X-AUTH-Signature"].ToString() ?? "";
                            }
                            tokenView.X_AUTH_Checksum = item["X-AUTH-Checksum"].ToString() ?? "";
                            tokenView.X_AUTH_Timestamp = item["X-AUTH-Timestamp"].ToString() ?? "";
                        }
                    }
                    //if (deviceCode == "CXC4-5Z2Z-K6MR")
                    //{
                    var deviceCode = tokenView?.X_AUTH_DeviceCode;
                    var device_id = tokenView?.DeviceGuid;
                    var user_id = tokenView?.UserGuid;
                    var location_id = tokenView?.LocationGuid;

                    //if (string.IsNullOrEmpty(device_id) || string.IsNullOrEmpty(user_id) || string.IsNullOrEmpty(location_id))
                    //{
                    //    result.message = "Lỗi 1110";
                    //    return result;
                    //}
                    //else
                    //{
                    //if (!string.IsNullOrEmpty(customer_id))
                    //{
                    //var customerGuestId = tokenView.X_AUTH_CustomerGuest_id ?? "";
                    //
                    var customer_id = tokenView?.CustomerGuid ?? "";
                    var deviceRequestHistoryFile = PathFile.deviceRequestHistoryFile;
                    if (string.IsNullOrEmpty(deviceCode) || deviceCode == "")
                    {
                        result.errorCode = "DC100";
                        result.message = "Lỗi DC100";
                        return result;
                    }
                    var timestamp = tokenView.X_AUTH_Timestamp ?? "0";
                    if (string.IsNullOrEmpty(timestamp) || timestamp == "0")
                    {
                        result.errorCode = "1121";
                        result.message = "Lỗi 1121";
                        return result;
                    }
                    var nonce = tokenView.X_AUTH_Nonce ?? "0";
                    if (string.IsNullOrEmpty(nonce) || nonce == "0")
                    {
                        result.errorCode = "1150";
                        result.message = "Lỗi 1150";
                        return result;
                    }
                    else
                    {
                        var contentList = await FileHelper.ReadFile(deviceRequestHistoryFile);
                        if (contentList != null && contentList.Count() > 0)
                        {
                            var nonceList = contentList.Find(x => !string.IsNullOrEmpty(x) && x.Equals(nonce));
                            if (!string.IsNullOrEmpty(nonceList))
                            {
                                result.errorCode = "1151";
                                //result.message = "Lỗi 1151--"+ConvertHelper.Serialize(contentList)+"---"+nonce+"path:"+ deviceRequestHistoryFile;
                                result.message = "Lỗi 1151";
                                return result;
                            }
                        }
                    }
                    var checksum = tokenView.X_AUTH_Checksum ?? "";
                    if (string.IsNullOrEmpty(checksum) && !string.IsNullOrEmpty(body))
                    {
                        result.errorCode = "1131";
                        result.message = "Lỗi 1131";
                        return result;
                    }
                    var signature = tokenView.X_AUTH_Signature ?? "";
                    if (string.IsNullOrEmpty(signature))
                    {
                        result.errorCode = "1141";
                        result.message = "Lỗi 1141";
                        return result;
                    }
                    //if (!string.IsNullOrEmpty(deviceCode))
                    //{
                    var filter = Builders<DeviceModel>.Filter.Eq("Visible", 1)
                    //& Builders<DeviceModel>.Filter.Eq("UserGuid", user_id)
                    & Builders<DeviceModel>.Filter.Eq("AppCode", deviceCode);
                    var device = await _repository.deviceRepository.GetSingle(filter);
                    if (device != null && device._id != "")
                    {
                        var secretKey = device.SecretKey ?? "";
                        if (secretKey != "")
                        {
                            var baseURL = _domainSettings.Value.domain;
                            baseURL = baseURL[..baseURL.LastIndexOf("/")];
                            var Path = request.Path.Value ?? "";
                            var verb = request.Method;
                            decode = decode != "" ? decode : "''";
                            var sha256Body = "";
                            if (!string.IsNullOrEmpty(body))
                            {
                                sha256Body = HMAC_SHA256Helper.CalcSha256Hash(decode);
                                //var sha256 = sha256Body + verb + baseURL + Path + timestamp + nonce + customer_id;
                                if (checksum != sha256Body)
                                {
                                    result.errorCode = "1130";
                                    result.message = "Lỗi 1130";
                                    return result;
                                }
                            }
                            //var sha256 = sha256Body + verb + baseURL + Path + timestamp + nonce + customer_id;//tạm khóa account user
                            var sha256 = sha256Body + verb + baseURL + Path + timestamp + nonce + customer_id;
                            var hmacSha256 = HMAC_SHA256Helper.GenerateHMACSignature(sha256, secretKey);
                            if (string.IsNullOrEmpty(hmacSha256))
                            {
                                result.errorCode = "1000";
                                result.message = "Lỗi 1000";
                                return result;
                            }
                            if (hmacSha256 != signature)
                            {
                                result.errorCode = "1140";
                                result.message = "Lỗi 1140";
                                return result;
                            }

                            var deviceRequestHistoryModel = new DeviceRequestHistoryModel
                            {
                                UserGuid = user_id,
                                DeviceGuid = device_id,
                                LocationGuid = location_id,
                                DeviceCode = deviceCode,
                                CustomerGuest_id = customer_id,
                                BaseUrl = baseURL,
                                Checksum = checksum,
                                Nonce = nonce,
                                Path = Path,
                                Payload = decode,
                                Verb = verb,
                                Timestamp = timestamp
                            };
                            var deviceRequestHistoryPost = await _repository.deviceRequestHistoryRepository.Post(deviceRequestHistoryModel);

                            await FileHelper.WriteToFile(deviceRequestHistoryFile, nonce, true);
                            result.errorCode = "200";
                            return result;
                        }
                        else
                        {
                            result.errorCode = "2140";
                            result.message = "Lỗi 2140";
                            return result;
                        }
                    }
                    else
                    {
                        result.errorCode = "DC101";
                        result.message = "Lỗi DC101";
                        return result;
                    }
                }
                else
                {
                    result.errorCode = "200";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " ---- " + ex.StackTrace);
            }
            return result;
        }

        //public async Task<ORValidateModel> HeaderValidate(HttpContext context, bool isRequiredHMAC = false, Encoding encoding = null)
        //{
        //    var result = new ORValidateModel();
        //    //isRequiredHMAC = false;//only test
        //    if (isRequiredHMAC)
        //    {
        //        var request = context.Request;
        //        var header = request.Headers;
        //        var publicKey = header["X-AUTH-PublicKey"].ToString();
        //        var deviceCode = header["X-AUTH-DeviceCode"].ToString();
        //        var timeStamp = header["X-AUTH-Timestamp"].ToString();
        //        if (string.IsNullOrEmpty(publicKey))
        //        {
        //            result.errorCode = "1100";
        //            result.message = "Lỗi 1100";
        //            return result;
        //        }
        //        if (string.IsNullOrEmpty(deviceCode))
        //        {
        //            result.errorCode = "DC101";
        //            result.message = "Lỗi DC101";
        //            return result;
        //        }
        //        if (string.IsNullOrEmpty(timeStamp))
        //        {
        //            result.errorCode = "1121";
        //            result.message = "Lỗi 1121";
        //            return result;
        //        }
        //        var filter = Builders<DeviceModel>.Filter.Eq("Visible", 1)
        //       //& Builders<DeviceModel>.Filter.Eq("UserGuid", user_id)
        //       & Builders<DeviceModel>.Filter.Eq("AppCode", deviceCode);
        //        var device = await _repository.DeviceRepository.Single(filter);
        //        if (device != null && !string.IsNullOrEmpty(device._id))
        //        {
        //            //Kiểm tra thêm public key có hợp lệ nữa hông?
        //            var locationGuid = device?.Location ?? "";
        //            var deviceGuid = device?._id ?? "";
        //            if (!string.IsNullOrEmpty(locationGuid) && !string.IsNullOrEmpty(deviceGuid))
        //            {
        //                var headerModel = new OROrderHeaderModel
        //                {
        //                    DeviceCode = deviceCode,
        //                    PublicKey = publicKey,
        //                    Timestamp = timeStamp,
        //                    DeviceGuid = deviceGuid,
        //                    LocationGuid = locationGuid,
        //                };
        //                result.Header = headerModel;
        //                result.errorCode = "200";
        //            }
        //            else
        //            {
        //                result.errorCode = "404";
        //                result.message = "Lỗi 404";
        //                return result;
        //            }
        //        }
        //        else
        //        {
        //            result.errorCode = "404";
        //            result.message = "Lỗi 404";
        //            return result;
        //        }
        //    }
        //    else
        //    {
        //        result.errorCode = "200";
        //    }
        //    return result;
        //}


        //public async Task<RequestHeaderClsPayooModel> HeaderPayooValidate(HttpContext context, bool isRequiredHMAC = false, Encoding encoding = null)
        //{
        //    var result = new RequestHeaderClsPayooModel
        //    {
        //        errorCode = "400",
        //        message = ""
        //    };
        //    //isRequiredHMAC = false;//only test
        //    if (isRequiredHMAC)
        //    {
        //        var request = context.Request;
        //        var header = request.Headers;
        //        var partnerCode = header["PartnerCode"].ToString();
        //        var credential = header["Credential"].ToString();
        //        var signature = header["Signature"].ToString();
        //        if (string.IsNullOrEmpty(partnerCode))
        //        {
        //            result.errorCode = "1100";
        //            result.message = "Lỗi 1100";
        //            return result;
        //        }
        //        if (string.IsNullOrEmpty(credential))
        //        {
        //            result.errorCode = "DC101";
        //            result.message = "Lỗi DC101";
        //            return result;
        //        }
        //        if (string.IsNullOrEmpty(signature))
        //        {
        //            result.errorCode = "1121";
        //            result.message = "Lỗi 1121";
        //            return result;
        //        }

        //        var headerCls = new HeaderClsPayooModel
        //        {
        //            Credential = credential,
        //            Signature = signature,
        //            PartnerCode = partnerCode
        //        };
        //        result.Header = headerCls;
        //        result.errorCode = "200";
        //    }
        //    else
        //    {
        //        result.errorCode = "200";
        //    }
        //    return result;
        //}
    }
}
