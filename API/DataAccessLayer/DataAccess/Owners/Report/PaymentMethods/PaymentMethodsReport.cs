﻿using BusinessLogicLayer.ILogicCommon.IPipeLogic.Wrapper;
using BusinessLogicLayer.IRepository.Wrapper;
using Entities.ExtendModels.Reports;
using Entities.Owners.SinglePoint_en.PaymentType;
using Entities.Owners.SinglePoint_System.SystemCashType;
using Helpers.Filter;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DataAccess.Owners.Report.PaymentMethods;

public static class PaymentMethodsReport
{
    public static async Task<ReportModel<DataPaymentMethodModel>> ReportPaymentMenthodsPage(this ReportRequestModel filter, IRepositoryWrapper _repositoryWrapper, IPipeLogicWrapper _pipeLogicWrapper)
    {
        var report = new ReportModel<DataPaymentMethodModel>();
        var dataPaymentMethodList = new List<DataPaymentMethodModel>();
        var totalPage = new DataPaymentMethodModel();

        //FilterOrderDetail response list location, list orderDetail, list device , list cashdrawer
        var resultFilterList = await _pipeLogicWrapper.filterReportPipe.FilterOrderDetail(filter);

        var OrderDetailList = resultFilterList.ListOrderDetail;
        var LocationList = resultFilterList.ListLocation.Select(x => x.NickName).ToList();
        var DeviceList = resultFilterList.ListDevice.Select(x => x.Nickname).ToList();
        var CashDrawerList = resultFilterList.ListCashDrawer.Select(x => x._id).ToList();


        var filterCashType = Builders<SystemCashTypeModel>.Filter.Empty;
        var CashTypeList = await _repositoryWrapper.systemCashTypeRepository.GetList(filterCashType, null);

        if (OrderDetailList != null && OrderDetailList.Any())
        {
            var filterPaymentType = Builders<PaymentTypeModel>.Filter.Eq(x => x.UserGuid, filter.UserGuid);
            var PaymentTypeList = await _repositoryWrapper.paymentTypeRepository.GetList(filterPaymentType, null);
            if (PaymentTypeList != null && PaymentTypeList.Any())
            {
                //var paymentTypeList = paymentSaleList?.Select(x => x.PaymentTypeId).ToList();
                var paymentSaleList = OrderDetailList?.SelectMany(x => x.PaymentList);
                var orderRefundList = OrderDetailList?.Where(x => x.isRefund.Equals(1)).ToList();
                //PaymentTypeList = PaymentTypeList.Where(x => x.PaymentTypeId.Equals(1)).ToList();
                PaymentTypeList?.ForEach(paymentType =>
                {

                    #region Sale
                    var paymentSale = paymentSaleList?.Where(x => x._id.Equals(paymentType._id)).ToList();
                    var saleQuantity = paymentSale?.Count();
                    var salePaymentAmount = paymentSale?.Sum(x => x.Payable);
                    #endregion
                    #region Refund
                    var paymentRefund = orderRefundList?.SelectMany(x => x.OrderRefund)?.Where(x => x.PaymentTypeGuid != null && x.PaymentTypeGuid.Equals(paymentType._id));
                    var refundAmount = paymentRefund?.Sum(x => x.RefundAmount);
                    var refundQuanty = paymentRefund?.Select(x => x.OrderGuid).Distinct().Count();
                    #endregion
                    var title = paymentType.Title_en ?? "";
                    var systemCashType = CashTypeList?.Where(x => x.Id.Equals(paymentType?.CashTypeId)).FirstOrDefault();
                    var cashType = systemCashType?.Title_en;
                    var total = salePaymentAmount - refundAmount;

                    var payment = new DataPaymentMethodModel
                    {
                        _id = paymentType._id,
                        CashType = cashType,
                        PaymentAmount = salePaymentAmount,
                        RefundAmount = refundAmount,
                        RefundQty = refundQuanty,
                        PaymentMethods = title,
                        Qty = saleQuantity,
                        Total = total
                    };
                    if (saleQuantity != 0)
                    {
                        dataPaymentMethodList.Add(payment);
                    }
                });
            }
            #region TotalPage
            totalPage = new DataPaymentMethodModel
            {
                _id = "",
                PaymentMethods = "Total",
                CashType = "",
                PaymentAmount = dataPaymentMethodList.Sum(x => x.PaymentAmount),
                RefundAmount = dataPaymentMethodList.Sum(x => x.RefundAmount),
                RefundQty = dataPaymentMethodList.Sum(x => x.RefundQty),
                Qty = dataPaymentMethodList.Sum(x => x.Qty),
                Total = dataPaymentMethodList.Sum(x => x.Total),
            };
            #endregion
        }
        dataPaymentMethodList.Add(totalPage);
        report.filterResponse = new FilterResponseModel
        {
            locationList = LocationList,
            deviceList = DeviceList,
            cashdrawerList = CashDrawerList
        };
        report.reportTableData = dataPaymentMethodList;
        return report;
    }
}
