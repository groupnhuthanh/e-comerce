﻿using BusinessLogicLayer.BusinessLogic.Owners.Report;
using BusinessLogicLayer.ILogicCommon.IPipeLogic.Wrapper;
using BusinessLogicLayer.IRepository.Wrapper;
using DataAccessLayer.DataAccess.Owners.Report.PaymentMethods;
using DataAccessLayer.DataAccess.Owners.Report.TimeReporting;
using Entities.ExtendModels.Reports;
using Helpers.Filter;

namespace DataAccessLayer.DataAccess.Owners.Report;

public class ReportsDAL : IReportBLL
{
    IPipeLogicWrapper _pipeLogicWrapper;
    IRepositoryWrapper _repositoryWrapper;

    public ReportsDAL(IPipeLogicWrapper pipeLogicWrapper, IRepositoryWrapper repositoryWrapper)
    {
        _pipeLogicWrapper = pipeLogicWrapper;
        _repositoryWrapper = repositoryWrapper;
    }

    public async Task<ReportResponseModel> ReportBase(Dictionary<string, object> dic)
    {
        var response = new ReportResponseModel();
        response.HttpStatusCode = 400;
        response.isSuccess = false;
        response.data = dic;
        response.message = "fail!";
        var filter = dic.filterReport();
        var valid = filter.valid;
        if (valid == true)
        {
            switch (filter.TypeReport)
            {
                case 1:
                    response.nameReport = "Payment Methods";
                    var paymentMethods = await filter.ReportPaymentMenthodsPage(_repositoryWrapper, _pipeLogicWrapper);
                    if (paymentMethods != null)
                    {

                        response.HttpStatusCode = 200;
                        response.isSuccess = true;
                        response.data = paymentMethods;
                        response.message = "success!";
                        return response;
                    }
                    return response;
                case 2:
                    response.nameReport = "Time Reporting";
                    var timeReporting = await filter.TimeReportPage(_pipeLogicWrapper);
                    if (timeReporting != null)
                    {

                        response.HttpStatusCode = 200;
                        response.isSuccess = true;
                        response.data = timeReporting;
                        response.message = "success!";
                        return response;
                    }
                    return response;
                case 3:
                    break;
               
                default: break;
            }
        }
        response.message = "Invalid filter request! Please check back";
        return response;
    }
}