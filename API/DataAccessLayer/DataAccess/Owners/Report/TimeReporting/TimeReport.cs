﻿using BusinessLogicLayer.ILogicCommon.IPipeLogic.Wrapper;
using BusinessLogicLayer.IRepository.Wrapper;
using DataAccessLayer.LogicCommon.PipeLogic.Wrapper;
using Entities.ExtendModels.Reports;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;
using MongoDB.Driver;

namespace DataAccessLayer.DataAccess.Owners.Report.TimeReporting;

public static class TimeReport
{
    public static async Task<ReportModel<DataTimeReportModel>> TimeReportPage(this ReportRequestModel filter,
                                                         IPipeLogicWrapper _pipeLogicWrapper)
    {
        var y = 0;
        var period = "";
        var startDate = filter.StartDate;
        var report = new ReportModel<DataTimeReportModel>();
        var dataTimeList = new List<DataTimeReportModel>();

        //FilterOrderDetail response list location, list orderDetail, list device , list cashdrawer
        var resultFilterList = await _pipeLogicWrapper.filterReportPipe.FilterOrderDetail(filter);

        var OrderDetailListInDay = resultFilterList.ListOrderDetail;
        var LocationList = resultFilterList.ListLocation.Select(x => x.NickName).ToList();
        var DeviceList = resultFilterList.ListDevice.Select(x => x.Nickname).ToList();
        var CashDrawerList = resultFilterList.ListCashDrawer.Select(x => x._id).ToList();

        if (OrderDetailListInDay != null && OrderDetailListInDay.Any())
        {
            for (int i = 1; i <= 24; i++)
            {
                #region Period
                var hourEnd = i.ToString().PadLeft(24.ToString().Length, '0');
                var hourStart = y.ToString().PadLeft(24.ToString().Length, '0');
                if (i == 24) period = hourStart + ":00" + " - 00:00";
                else period = hourStart + ":00" + " - " + hourEnd + ":00";
                #endregion
                #region handle data
                var OrderDetailList = OrderDetailListInDay?.Where(x => DateTime.Parse(x.CreateDate) >= DateTime.Parse(startDate + " " + hourStart + ":00:00")
               && DateTime.Parse(x.CreateDate) <= DateTime.Parse(startDate + " " + hourStart + ":59:00")).ToList();
                var orderProductList = OrderDetailList?.SelectMany(x => x.OrderProducts).ToList();
                var orderList = OrderDetailList?.Select(x => x.Order).ToList();
                var orderRefundList = OrderDetailList?.Where(x => x.isRefund.Equals(1))?.SelectMany(x => x.OrderRefund).ToList();
                var grossSales = orderProductList?.Sum(x => x.GrossPrice);
                //double? NetSales = GrossPrice - RefundAmount - Discount - Comp;
                var refundAmount = orderRefundList?.Sum(x => x.RefundAmount);
                var discount = orderList?.Sum(x => x.Discount) + orderProductList?.Sum(x => x.DiscountTotalPrice);
                var netSales = grossSales - refundAmount - discount;
                var transaction = orderProductList?.Count();
                var orderQuantity = OrderDetailList?.Count();
                var dataTimeReport = new DataTimeReportModel
                {
                    Period = period,
                    Transaction = transaction,
                    Orders = orderQuantity,
                    GrossSales = grossSales,
                    NetSales = netSales,
                    Rate = 0
                };
                if (orderQuantity != 0)
                {
                    dataTimeList.Add(dataTimeReport);
                }
                y++;
                #endregion
            }
            #region total
            var transTotal = dataTimeList.Sum(x => x.Transaction);
            var orderTotal = dataTimeList.Sum(x => x.Orders);
            var grossTotal = dataTimeList.Sum(x => x.GrossSales);
            var netTotal = dataTimeList.Sum(x => x.NetSales);
            var totalPage = new DataTimeReportModel
            {
                Period = "Total",
                Transaction = transTotal,
                Orders = orderTotal,
                GrossSales = grossTotal,
                NetSales = netTotal,
                Rate = 100,
            };
            dataTimeList.Add(totalPage);
            #endregion

            dataTimeList.ForEach(x =>
            {
                if (totalPage.Transaction != 0)
                {
                    decimal? rate = x.Transaction / totalPage.Transaction * 100;
                    x.Rate = rate;
                }
            });

        }
        report.filterResponse = new FilterResponseModel
        {
            locationList = LocationList,
            deviceList = DeviceList,
            cashdrawerList = CashDrawerList
        };
        report.reportTableData = dataTimeList;
        return report;


    }
}
