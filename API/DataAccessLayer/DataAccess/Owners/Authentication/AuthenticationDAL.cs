﻿
using BusinessLogicLayer.BusinessLogic.Owners.Authentication;
using BusinessLogicLayer.ILogicCommon.IManagerCommon.Wrapper;
using BusinessLogicLayer.ILogicCommon.IPipeLogic.Wrapper;
using BusinessLogicLayer.IRepository.Wrapper;
using Entities.ExtendModels.Authentication.Users;
using Entities.ExtendModels.Tokens;

namespace DataAccessLayer.DataAccess.Owners.Authentication
{
    public class AuthenticationDAL : IAuthenticationBLL
    {

        IPipeLogicWrapper _pipeLogicWrapper;
        IManagerCommonWrapper _managerCommonWrapper;
        IRepositoryWrapper _repositoryWrapper;

        public AuthenticationDAL(IPipeLogicWrapper pipeLogicWrapper, IRepositoryWrapper repositoryWrapper)
        {
            _pipeLogicWrapper = pipeLogicWrapper;
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<SignIn> SignIn(TokenView dicToken,Dictionary<string, object> doc)
        {
            var result = new SignIn();
            var userAccount = doc.ContainsKey("UserAccount") ? doc["UserAccount"]?.ToString().Trim().ToLower() ?? "" : "";
            var password = doc.ContainsKey("Password") ? doc["Password"]?.ToString().Trim() ?? "" : "";
            var fcmToken = doc.ContainsKey("FcmToken") ? doc["FcmToken"]?.ToString() : "";


            return result;
        }
    }
}
