using BusinessLogicLayer.BusinessLogic.Owners.Request;
using DataAccessLayer.DataAccess.Owners.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace DataAccessLayer.DataAccess.Owners.Request;

public class RestfullRequest : IRestfullRequest
{
    /// <summary>
    /// Initializes a new instance of the <see cref="RestfullRequest"/> class.
    /// </summary>
    public RestfullRequest()
    {
    }

    public async Task<T> Get<T>(string baseUrl, string param) where T : new()
    {
        var json = new T();
        using var client = new HttpClient();
        var url = baseUrl + param;
        client.BaseAddress = new Uri(url);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        var send = await client.GetAsync(url);
        if (send.IsSuccessStatusCode)
        {
            var result = send.Content.ReadAsStringAsync().Result;
            if (result != "")
            {
                var response = JsonConvert.DeserializeObject<SingleResponse<object>>(JsonConvert.DeserializeObject(result).ToString());
                if (response.Model != null)
                {
                    var model = response.Model;
                    if (model != null && model.ToString() != "" && model.ToString() != "[]")
                    {
                        json = JsonConvert.DeserializeObject<T>(model.ToString() ?? "");
                    }
                }
            }
        }
        return json;
    }


    public async Task<T> GetURLExtension<T>(string baseUrl, string param) where T : new()
    {
        var json = new T();
        using var client = new HttpClient();
        var url = baseUrl + param;
        client.BaseAddress = new Uri(url);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        var send = await client.GetAsync(url);
        if (send.IsSuccessStatusCode)
        {
            var result = send.Content.ReadAsStringAsync().Result;
            if (result != "")
            {
                json = JsonConvert.DeserializeObject<T>(result);
            }
        }
        return json;
    }

    //public async Task<List<T>> Post<T>(string baseUrl, string urlApi, object objDic) where T : new()
    //{
    //    var json = new List<T>();
    //    using var client = new HttpClient();
    //    var url = baseUrl + urlApi;
    //    client.BaseAddress = new Uri(url);
    //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    //    var send = await client.PostAsJsonAsync(url, objDic);
    //    if (send.IsSuccessStatusCode)
    //    {
    //        var result = send.Content.ReadAsStringAsync().Result;
    //        if (result != "")
    //        {
    //            var token = JsonConvert.DeserializeObject<JToken>(result);
    //            if (token != null && token.ToString() != "")
    //            {
    //                var response = JsonConvert.DeserializeObject<ListResponse<T>>(token.ToString());
    //                if (response != null && response.Model != null && response.Model.Any())
    //                {
    //                    var model = JsonConvert.SerializeObject(response.Model);
    //                    if (model != "" && model != "[]")
    //                    {
    //                        json = JsonConvert.DeserializeObject<List<T>>(model);
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    return json;
    //}

    /// <summary>
    /// Posts the.
    /// </summary>
    /// <param name="baseUrl">The base url.</param>
    /// <param name="urlApi">The url api.</param>
    /// <param name="objDic">The obj dic.</param>
    /// <returns>A Task.</returns>
    public async Task<T> Post<T>(string baseUrl, string urlApi, object objDic) where T : new()
    {
        var json = new T();
        using var client = new HttpClient();
        var url = baseUrl + urlApi;
        client.BaseAddress = new Uri(url);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //client.Timeout = TimeSpan.FromMilliseconds(10000);
        var send = await client.PostAsJsonAsync(url, objDic);
        if (send.IsSuccessStatusCode)
        {
            var result = send.Content.ReadAsStringAsync().Result;
            if (result != "")
            {
                var token = JsonConvert.DeserializeObject<JToken>(result);
                if (token != null && token.ToString() != "")
                {
                    var response = JsonConvert.DeserializeObject<SingleResponse<T>>(token.ToString());
                    if (response != null && response.Model != null)
                    {
                        var model = JsonConvert.SerializeObject(response.Model);
                        if (model != "" && model != "[]")
                        {
                            json = JsonConvert.DeserializeObject<T>(model);
                        }
                    }
                }
            }
        }
        return json;
    }

    public async Task<T> AuthenticationPost<T>(string baseUrl, string urlApi, object objDic) where T : new()
    {
        var json = new T();
        using var client = new HttpClient();
        var url = baseUrl + urlApi;
        client.BaseAddress = new Uri(url);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        client.Timeout = TimeSpan.FromMilliseconds(10000);
        var send = await client.PostAsJsonAsync(url, objDic);
        if (send.IsSuccessStatusCode)
        {
            var result = send.Content.ReadAsStringAsync().Result;
            if (result != "")
            {
                var token = JsonConvert.DeserializeObject<JToken>(result);
                if (token != null && token.ToString() != "")
                {
                    var response = JsonConvert.DeserializeObject<ListResponse<T>>(token.ToString());
                    if (response != null && response.Model != null && response.Model.Any())
                    {
                        var model = JsonConvert.SerializeObject(response.Model);
                        if (model != "" && model != "[]")
                        {
                            json = JsonConvert.DeserializeObject<T>(model);
                        }
                    }
                }
            }
        }
        return json;
    }


    /// <summary>
    /// Posts the.
    /// </summary>
    /// <param name="baseUrl">The base url.</param>
    /// <param name="urlApi">The url api.</param>
    /// <param name="objDic">The obj dic.</param>
    /// <returns>A Task.</returns>
    public async Task<string> SendSMSPost(string baseUrl, string urlApi, object objDic)
    {
        var result = "";
        using var client = new HttpClient();
        //var url = baseUrl + urlApi;
        client.BaseAddress = new Uri(baseUrl);
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        client.Timeout = TimeSpan.FromMilliseconds(10000);
        var send = await client.PostAsJsonAsync(urlApi, objDic);
        if (send.IsSuccessStatusCode)
        {
            result = await send.Content.ReadAsStringAsync();
        }
        return result;
    }
}