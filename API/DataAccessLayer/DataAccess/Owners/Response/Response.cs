using Newtonsoft.Json;

namespace DataAccessLayer.DataAccess.Owners.Response;

#pragma warning disable CS1591
public interface IResponse
{
    string Message { get; set; }

    //int Code { get; set; }

}

public interface ISingleResponse<TModel> : IResponse
{
    TModel Model { get; set; }
}

public interface IListResponse<TModel> : IResponse
{
    IEnumerable<TModel> Model { get; set; }
}

public interface IPagedResponse<TModel> : IListResponse<TModel>
{
    int ItemsCount { get; set; }

    double PageCount { get; }
}

public class Response : IResponse
{
    public string Message { get; set; }

    //public int Code { get; set; }

}

public class SingleResponse<TModel> : ISingleResponse<TModel>
{
    public string Message { get; set; }
    public bool DidError { get; set; }

    public string ErrorMessage { get; set; }

    //public int Code { get; set; }


    public TModel Model { get; set; }
}

public class ListResponse<TModel> : IListResponse<TModel>
{
    public int Code { get; set; }
    public string Message { get; set; }

    public bool DidError { get; set; }

    public string ErrorMessage { get; set; }

    public IEnumerable<TModel> Model { get; set; }
}

public class PagedResponse<TModel> : IPagedResponse<TModel>
{
    public string Message { get; set; }

    //public int Code { get; set; }
    public bool DidError { get; set; }


    public IEnumerable<TModel> Model { get; set; }

    public int PageSize { get; set; }

    public int PageNumber { get; set; }

    public int ItemsCount { get; set; }

    public double PageCount
        => ItemsCount < PageSize ? 1 : (int)((double)ItemsCount / PageSize + 1);
}

public class Paging<TModel>
{
    public int PageNo { get; set; }
    public int? TotalPage { get; set; }
    public long? Total { get; set; }
    public int? PageSize { get; set; }
    public IEnumerable<TModel> List { get; set; }
}


public static class ResponseExtensions
{
    public static int StatusCode { get; private set; }
    public static string ToHttpResponse(this IResponse response)
    {
        //var status = !response.Code.Equals(200) ? HttpStatusCode.InternalServerError : HttpStatusCode.OK;

        var okResult = JsonConvert.SerializeObject(response);
        //if (okResult != "")
        //{
        //    StatusCode = (int)status;
        //}
        //response.Code = StatusCode;
        return okResult;
    }

    public static string ToHttpResponse<TModel>(this ISingleResponse<TModel> response)
    {
        //var status = HttpStatusCode.OK;

        //if (!response.Code.Equals(200))
        //    status = HttpStatusCode.InternalServerError;
        //else if (response.Model == null)
        //    status = HttpStatusCode.NotFound;

        var okResult = JsonConvert.SerializeObject(response);
        //if (okResult != "")
        //{
        //    StatusCode = (int)status;
        //}

        //response.Code = StatusCode;
        return okResult;
    }

    public static string ToHttpResponse<TModel>(this IListResponse<TModel> response)
    {
        //var status = HttpStatusCode.OK;

        //if (!response.Code.Equals(200))
        //    status = HttpStatusCode.InternalServerError;
        //else if (response.Model == null)
        //    status = HttpStatusCode.NoContent;
        var okResult = JsonConvert.SerializeObject(response);
        //if (okResult != "")
        //{
        //    StatusCode = (int)status;
        //}
        //response.Code = StatusCode;
        return okResult;
    }
}
#pragma warning restore CS1591
