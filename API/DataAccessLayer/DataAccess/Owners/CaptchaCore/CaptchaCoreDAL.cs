﻿using BusinessLogicLayer.BusinessLogic.Owners.CaptchaCore;
using BusinessLogicLayer.ILogicCommon.IPipeLogic.Wrapper;
using Entities.ExtendModels.Authentication.Captcha;
using Entities.ExtendModels.Response;
using Microsoft.AspNetCore.Http;

namespace DataAccessLayer.DataAccess.Owners.CaptchaCore;

public class CaptchaCoreDAL : ICaptchaCoreBLL
{
    IPipeLogicWrapper _pipeLogicWrapper;

    public CaptchaCoreDAL(IPipeLogicWrapper pipeLogicWrapper)
    {
        _pipeLogicWrapper = pipeLogicWrapper;
    }

    public async Task<ResponseAppModel> Get(string nftToken_id, HttpContext context)
    {
        return await _pipeLogicWrapper.captchaCorePipe.Generate(nftToken_id, context);
    }

    public async Task<ResponseAppModel> Post(CaptchaCoreModel doc, HttpContext context)
    {
        return await _pipeLogicWrapper.captchaCorePipe.Verify(doc, context);
    }
}
