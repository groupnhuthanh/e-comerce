﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.ExtendModels.Files
{
    public class PathFile
    {
        public static PathFile Instance { get; } = new PathFile();
        private PathFile()
        {

        }
        /// <summary>
        /// Gets the device request history file.
        /// </summary>
        public static string deviceRequestHistoryFile
        {
            get
            {
                var now = DateTime.Now;
                var timeSpanNow = now.ToString("HH:mm:ss");
                var timeSpanFrom = TimeSpan.Parse("00:00:00");
                var timeSpanTo = TimeSpan.Parse(timeSpanNow);
                //var subTimeSpan = Math.Ceiling(timeSpanTo.Subtract(timeSpanFrom).TotalMinutes).ToString();
                //var subTimeSpan = Math.Ceiling(timeSpanTo.Subtract(timeSpanFrom).TotalHours).ToString();
                int subTimeSpan = now.Hour >= 0 && now.Hour <= 6 ? 0 : now.Hour > 6 && now.Hour <= 12 ? 6 : now.Hour > 12 && now.Hour <= 18 ? 12 : now.Hour > 18 && now.Hour <= 24 ? 18 : 0;
                string path = DateTime.Now.ToString("yyyy/MM/dd/") + subTimeSpan;
                return "device/request/history/" + path + ".txt";
            }
        }
    }
}
