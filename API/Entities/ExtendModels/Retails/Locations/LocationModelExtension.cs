using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.ExtendModels.Retails.Locations;

[BsonIgnoreExtraElements]
public class LocationModelExtension : BaseModel
{
    public string StreetName { get; set; }
    public string Name { get; set; }
    public string Company { get; set; }
    public string AddressName { get; set; }
    public int? Status { get; set; }
    public string ListLanguage { get; set; }
    public string Url { get; set; }
}