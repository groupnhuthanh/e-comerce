using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Entities.ExtendModels.Retails.Locations;

[BsonIgnoreExtraElements]
public class LocationModel : LocationModelExtension
{
    public string LegalName { get; set; }
    public string NickName { get; set; }
    public string OwnershipType { get; set; }
    public int? AddressType { get; set; }
    public int? Country { get; set; }
    public int? City { get; set; }
    public string AddressTypeName { get; set; }
    public int? District { get; set; }
    public int? Ward { get; set; }
    public string Address_1 { get; set; }
    public string Address_2 { get; set; }
    public string Email { get; set; }
    public string CityName { get; set; }
    public string DistrictName { get; set; }
    public string WardName { get; set; }
    public string Phone { get; set; }
    public string Facebook { get; set; }
    public string Website { get; set; }
    public string LanguageId { get; set; }
    public string TimeZoneDisplayName { get; set; }
    public string FullAddress { get; set; }
    public string ReferenceId { get; set; }
    public string OpenTime { get; set; }
    public string Holidays { get; set; }
    public double? Latitude { get; set; }
    public double? Longitude { get; set; }
    public int? TypeLocationId { get; set; }
    public string Handle { get; set; }
    public string EmpGuid { get; set; }
    public int Visible { get; set; }
    public int LocationId { get; set; }
    public int? OnlyToday { get; set; }
    public string TimeZoneId { get; set; }
    public string RegionCode { get; set; }
    public string StoreCode { get; set; }
    public LocModel loc { get; set; }
    public string calculated { get; set; }

    public string ReferenceId2 { get; set; }
}

public partial class LocModel
{
[JsonProperty("type", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
public string type { get; set; }

[JsonProperty("coordinates", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
public List<double> coordinates { get; set; }
}

public partial class LocationOpenTime
{
// [JsonProperty("Id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
// public int? Id { get; set; }

[JsonProperty("Day", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
public string Day { get; set; }

[JsonProperty("Time", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
public List<Time> Time { get; set; }

[JsonProperty("Visible", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
public int? Visible { get; set; }

[JsonProperty("OrderNo", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
public int? OrderNo { get; set; }
}

public partial class Time
{
public int? TimeId { get; set; }

[JsonProperty("TimeOn", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
public string TimeOn { get; set; }

[JsonProperty("TimeOff", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
public string TimeOff { get; set; }
public BsonArray PerLocation { get; set; }
}

public class PerLocation
{
public int? PerLocationId { get; set; }
public string TimeOn { get; set; }
public string TimeOff { get; set; }
public string Location { get; set; }
public int? OrderNo { get; set; }
public int? Visible { get; set; }
}