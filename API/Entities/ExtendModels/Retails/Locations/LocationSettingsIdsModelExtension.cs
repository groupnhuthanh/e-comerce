using Entities.Base;

namespace Entities.ExtendModels.Retails.Locations;

public class LocationSettingsIdsModelExtension : BaseModel
{
    public string DeviceGuid { get; set; }
    public long? NumberIncrement { get; set; }
    public string NumberRandom { get; set; }
    public string RandomPrefix { get; set; }
}
