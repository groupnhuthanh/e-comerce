using Entities.ExtendModels.Extension.Tokens;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Entities.ExtendModels.Authentication.Tokens;

[BsonIgnoreExtraElements]
public class AccessTokenModel : AccessTokenModelExtension
{
    [JsonProperty("AccessToken", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string AccessToken { get; set; }
    [JsonProperty("RefreshToken", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string RefreshToken { get; set; }
    [JsonProperty("ExpiresIn", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public int? ExpiresIn { get; set; }
    [JsonProperty("SystemExpiresIn", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public int? SystemExpiresIn { get; set; }
    [JsonProperty("UserGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string UserGuid { get; set; }
    [JsonProperty("LocationGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string LocationGuid { get; set; }
    [JsonProperty("DeviceGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string DeviceGuid { get; set; }
    [JsonProperty("CustomerGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string CustomerGuid { get; set; }
    [JsonProperty("ExpiredDate", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public DateTime ExpiredDate { get; set; }
}