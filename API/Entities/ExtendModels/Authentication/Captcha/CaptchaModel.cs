﻿using Edi.Captcha;
using System.ComponentModel.DataAnnotations;

namespace Entities.ExtendModels.Authentication.Captcha;

public class CaptchaModel : ICaptchable
{
    public string NftToken_id { get; set; }
    [Required]
    [StringLength(4)]
    public string CaptchaCode { get; set; }
}

public class CaptchaCoreModel : ICaptchable
{
    public string NftToken_id { get; set; }
    [Required]
    [StringLength(5)]
    public string CaptchaCode { get; set; }
    public string ClientKey { get; set; }
}

public class GenerateCaptchaModelResult
{
    public string ClientKey { get; set; }

    public string Code { get; set; }

    public string Base64 { get; set; }

    public GenerateCaptchaModelResult(string clientKey, string code, string base64)
    {
        ClientKey = clientKey;
        Code = code;
        Base64 = base64;
    }
}

public class CaptchaModelResponse
{
    public string ClientKey { get; set; }

    public string Base64 { get; set; }
}
