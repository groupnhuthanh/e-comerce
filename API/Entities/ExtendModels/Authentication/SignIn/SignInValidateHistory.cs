using Entities.ExtendModels.Extension.SignIn;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Entities.ExtendModels.Authentication.SignIn;

[BsonIgnoreExtraElements]
public class SignInValidateHistory : SignInValidateHistoryExtension
{
    [JsonProperty("UserGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string UserGuid { get; set; }
    [JsonProperty("LocationGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string LocationGuid { get; set; }
    [JsonProperty("DeviceGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string DeviceGuid { get; set; }
    [JsonProperty("Email", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Email { get; set; }
    [JsonProperty("Otp", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Otp { get; set; }
    [JsonProperty("ExpiredIn", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public int ExpiredIn { get; set; }
    [JsonProperty("ExpiredDate", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public DateTime ExpiredDate { get; set; }
    [JsonProperty("SignupTypeId", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public int? SignupTypeId { get; set; } = 2;
}