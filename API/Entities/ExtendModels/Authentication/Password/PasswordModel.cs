﻿namespace Entities.ExtendModels.Authentication.Password;

public partial class PasswordModel
{
    public string Password { get; set; }
    public double Calc { get; set; }
    public int Score { get; set; }
}
