﻿using Entities.ThirdParty.HelperModels;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.ExtendModels.Authentication.Users
{
    public class UserModel
    {
       
    }
    [BsonIgnoreExtraElements]
    public partial class DevMode
    {
        [JsonProperty("Title", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }
        [JsonProperty("Body", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Body { get; set; }
    }
    [BsonIgnoreExtraElements]
    public partial class ValidateOtp
    {
        public int Validation { get; set; } = 4;//1: thành công, >2: lỗi.
        public string Message { get; set; }
        public string DialingCode { get; set; }
        public string VerifyToken { get; set; }
        public UserViewProfilesModel Profile { get; set; }
    }
    [BsonIgnoreExtraElements]
    public partial class SignIn : ValidateOtp
    {
        [JsonProperty("RefreshToken", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string RefreshToken { get; set; }

        [JsonProperty("AccessToken", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string AccessToken { get; set; }

        [JsonProperty("ExpiresIn", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public double? ExpiresIn { get; set; }

        [JsonProperty("LastestLoginTime", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string LastestLoginTime { get; set; }
        [JsonProperty("SignInTypeId", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public int? SignInTypeId { get; set; }
        [JsonProperty("FirstName", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string FirstName { get; set; }

        [JsonProperty("LastName", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string LastName { get; set; }

        [JsonProperty("FullName", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string FullName { get; set; }
        [JsonProperty("Email", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }

        [JsonProperty("PhoneNumber", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string PhoneNumber { get; set; }
        [JsonProperty("AvatarUrl", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string AvatarUrl { get; set; }

        [JsonProperty("CustomerGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string CustomerGuid { get; set; }
        public long _key { get; set; }
        [JsonProperty("GroupTypeId", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public int? GroupTypeId { get; set; }
        public DevMode DevMode { get; set; }

        //public string NameImage { get; set; }
        //public string Type { get; set; }
        //public int Width { get; set; }
        //public int Height { get; set; }
        public string Uri { get; set; }
        //public string UrlRewrite { get; set; }        
    }
    [BsonIgnoreExtraElements]
    public class UserViewProfilesModel
    {
        public string _id { get; set; }
        public long _key { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string FullName_Ansi { get; set; }
        public string Acronymn { get; set; }
        public string Logo { get; set; }
        public string Avatar { get; set; }
        public string Phone { get; set; }
        public string E164Format { get; set; }
        public string PhoneMasker { get; set; }
        public string MemberSince { get; set; }
        public long? Points { get; set; }
        public long? GiftCard { get; set; }
        public decimal? WalletBalance { get; set; }
        public int? MemberType { get; set; }
        public string MemberText { get; set; }
        public string MemberBackground { get; set; }
        public string MemberIcon { get; set; }
        public string PointsText { get; set; }
        public string GiftCardText { get; set; }
        public string WalletBalanceText { get; set; }
        public string Company { get; set; }
        public string DialingCode { get; set; }
        public string Email { get; set; }
        public string EmailMasker { get; set; }
        //public bool? EmailVerified { get; set; }
        //public bool? PhoneVerified { get; set; }
        public int? Gender { get; set; }
        public string SubFacebook { get; set; }
        public string SubGoogle { get; set; }
        public string SubTwitter { get; set; }
        public string SubGithub { get; set; }
        public string SubEmail { get; set; }
        public string SubPhone { get; set; }
        public string SubMicrosoft { get; set; }
        public string SubApple { get; set; }
        public string Grant_type { get; set; }
        public bool? IsVerifyPhone { get; set; }
        public bool? IsVerifyEmail { get; set; }
        public bool? IsLinkedFacebook { get; set; }
        public bool? IsLinkedGoogle { get; set; }
        public bool? IsLinkedApple { get; set; }
        public bool? IsLinkedTwitter { get; set; }
        public bool? IsLinkedGithub { get; set; }
        public bool? IsLinkedMicrosoft { get; set; }
        public bool? IsLinkedEmail { get; set; }
        public bool? IsLinkedPhone { get; set; }
        public bool? IsMultipleLinked { get; set; }
        public int? GroupTypeId { get; set; }
        public DevMode DevMode { get; set; }
        public string UserName { get; set; }
        public bool? HasPassword { get; set; } = false;
        public int? Visible { get; set; }
        public int? StatusRequest { get; set; }
        public int CurrentLevelId { get; set; }
        public string HashId { get; set; }

        [JsonProperty("Birthday", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Birthday { get; set; }

        [JsonProperty("AllowEditNames", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public bool AllowEditNames { get; set; }

        [JsonProperty("AllowEditBirthdate", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]

        public bool AllowEditBirthdate { get; set; }
        public int PrepaidStatus { get; set; }
        public bool AllowPrepaid { get; set; }
        public string NameAcronymn // THÊM 2023/03/27
        {
            get
            {
                return LIB_convert_entity.FirstCharacter(FirstName) + LIB_convert_entity.FirstCharacter(LastName);
            }
            set { _ = value; }
        }

        [JsonProperty("DateFommatBirday", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string DateFommatBirday { get; set; }

    }
    
}
