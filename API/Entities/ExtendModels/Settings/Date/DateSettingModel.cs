using Entities.Base;
using Entities.ExtendModels.Retails.Locations;

namespace Entities.ExtendModels.Settings.Date;

public class DateSettingModel : BaseModel
{
    public string UserGuid { get; set; }
    public string OpenTime { get; set; }
    public string Holidays { get; set; }
    public string DaySegments { get; set; }
}
public class TimeExclude
{
    public TimeSpan TimeOn { get; set; }
    public TimeSpan TimeOff { get; set; }
    public List<TimeSpan> TimeArangeList { get; set; }
}

public class OpenTime
{
    public int? Id { get; set; }
    public string Day { get; set; }
    public List<Time> Time { get; set; }
    public int? OrderNo { get; set; }
    public int? Visible { get; set; }
}

public class Holidays
{
    public int? Id { get; set; }
    public string Title { get; set; }
    public string Start { get; set; }
    public string End { get; set; }
    public int? Visible { get; set; }
    public int? OrderNo { get; set; }
    public List<HolidaysPerLocation> PerLocation { get; set; } = null!;
}

public class HolidaysPerLocation
{
    public int? PerLocationId { get; set; }
    public string Start { get; set; }
    public string End { get; set; }
    public string Location { get; set; }
    public int? OrderNo { get; set; }
    public int? Visible { get; set; }
}