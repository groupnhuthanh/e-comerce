﻿namespace Entities.ExtendModels.Globals.Path;

public class ORPath
{
    private static readonly string version = "hooks/";
    public static string twoOrderCreate = version + "two/orders/create";
    public static string onrampMasterDataUpdate = "onramp/master-data/update";
    public static string onrampOrderUpdate = "onramp/orders/update";
}

public static class GOEnvironment
{
    public static string TWO_Firebase = "TWO_PATH_APP_FIREBASE";
    public static string TWO_Mongo = "TWO_PATH_APP_MONGO";
    public static string TWO_Nonce = "TWO_PATH_APP_NONE";
    public static string TWO_Mongo_Database = "SystemGender";
}
