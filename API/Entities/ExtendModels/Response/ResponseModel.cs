﻿using Newtonsoft.Json;

namespace Entities.ExtendModels.Response;

public class ResponseModel
{
    public int? HttpStatusCode { get; set; }
    [JsonProperty("DidError", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public bool DidError { get; set; }
    public string Message { get; set; }
    public string ErrorMessage { get; set; }
    public object Model { get; set; }

    //[JsonProperty("CustomerGuests_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    //public string CustomerGuests_id  { get; set; }

}



public partial class TEntityPaging<TEntity>
{
    public string Keyword { get; set; }
    public int? PageIndex { get; set; }
    public int? PageSize { get; set; }
    public decimal? PageTotal { get; set; }
    public long? ItemTotal { get; set; }
    public int? ReturnItemTotal { get; set; }
    public List<TEntity> List { get; set; }
    //public Pages Pages { get; set; }
}

public partial class TEntityPagingApp<TEntity>
{
    public string Keyword { get; set; }
    public int? PageNo { get; set; }
    public int? PageSize { get; set; }
    public decimal? TotalPage { get; set; }
    public long? Total { get; set; }
    public int? ReturnItemTotal { get; set; }
    public List<TEntity> List { get; set; }
}

public partial class TEntityList<TEntity>
{
    public List<TEntity> List { get; set; }
}

public class ResponseEntityModel
{
    public string _id { get; set; }
    public bool IsOrderPaymentCompleted { get; set; }
    public decimal? PaymentChangeAmount { get; set; } = 0;
    public decimal? PaymentReceivedAmount { get; set; } = 0;
    public decimal LoyaltyPointsReceived { get; set; }
    public object Order { get; set; }
}

public class ResponseAppModel
{
    public int? HttpStatusCode { get; set; } = 200;
    public string Message { get; set; }
    public bool Status { get; set; } = false;
    public int MessageTypeId { get; set; } = 0;
    public object Model { get; set; }

    //[JsonProperty("CustomerGuests_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    //public string CustomerGuests_id  { get; set; }

}

public partial class PostModel
{
    public int Status { get; set; }
    public string _id { get; set; }
    public string CreateDate { get; set; }
}
