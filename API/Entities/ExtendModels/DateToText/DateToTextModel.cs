﻿namespace Entities.ExtendModels.DateToText;

public partial class DateToTextModel
{
    public string DateText { get; set; }
    public string DateOfWeek { get; set; }
    public bool HasEnded { get; set; } = false;
    public string DateStartText { get; set; }
    public string DateEndText { get; set; }
}
