﻿using Entities.Owners.SinglePoint_en.Devices.Device;
using Entities.Owners.SinglePoint_en.Locations.Location;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.ExtendModels.Reports;

[BsonIgnoreExtraElements]
public record ReportModel<TEntity>
{
    
    public FilterResponseModel? filterResponse { get; set; }
    public List<TEntity>? reportTableData { get; set; }
}

[BsonIgnoreExtraElements]
public class FilterResponseModel
{
   
    public List<string> locationList { get; set; }
    public List<string> deviceList { get; set; }
    public List<string> cashdrawerList { get; set; }


}


[BsonIgnoreExtraElements]
public class DataPaymentMethodModel
{

    [BsonElement("_id")]
    [BsonIgnoreIfNull]
    public string _id { get; set; }
    public string PaymentMethods { get; set; }
    public string CashType { get; set; }
    public int? RefundQty { get; set; } = 0;
    public decimal? RefundAmount { get; set; } = 0;
    public int? Qty { get; set; } = 0;
    public decimal? PaymentAmount { get; set; } = 0;
    public decimal? Total { get; set; } = 0;
}
public class DataTimeReportModel
{
    public string Period { get; set; }
    public decimal? Transaction { get; set; }
    public int? Orders { get; set; } = 0;
    public decimal? GrossSales { get; set; } = 0;
    public decimal? NetSales { get; set; } = 0;
    public decimal? Rate { get; set; } = 0;

}
