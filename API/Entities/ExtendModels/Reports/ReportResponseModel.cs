﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.ExtendModels.Reports
{
    public class ReportResponseModel
    {
        public string nameReport { get; set; }
        public string message { get; set; }
        public int? HttpStatusCode { get; set; }
        public bool? isSuccess { get; set; }
        public dynamic? data { get; set; }
    }
}
