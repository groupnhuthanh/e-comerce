﻿

using Entities.Owners.SinglePoint_en.Devices.Device;
using Entities.Owners.SinglePoint_en.Locations.Location;
using Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;
using Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.ExtendModels.Reports;

[BsonIgnoreExtraElements]
public class ReportRequestModel : FilterBaseModel
{

    public List<string> Location { get; set; }
    public List<string> Device { get; set; }
    public List<string> CashDrawer { get; set; }


}
[BsonIgnoreExtraElements]
public class FilterReportResponseModel
{
    public List<OrderDetailModel> ListOrderDetail { get; set; }
    public List<LocationModel> ListLocation { get; set; }
    public List<DeviceModel> ListDevice { get; set; }
    public List<CashDrawerModel> ListCashDrawer { get; set; }


}

[BsonIgnoreExtraElements]
public class FilterBaseModel
{
    public bool? valid { get; set; }
    public int? TypeReport { get; set; }
    public string DateRange { get; set; }
    public string StartDate { get; set; }
    public string EndDate { get; set; }
    public string UserGuid { get; set; }

}
