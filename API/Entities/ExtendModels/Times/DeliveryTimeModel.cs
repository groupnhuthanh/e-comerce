﻿using MongoDB.Bson.Serialization.Attributes;

namespace Entities.ExtendModels.Times;

[BsonIgnoreExtraElements]
public partial class DeliveryTimeModel
{
    public string _id { get; set; }
    public DateModel Date { get; set; }
    public string Time { get; set; }
    public TimesModel Times { get; set; }
    public int? Duration { get; set; }
    public string UserGuid { get; set; }
    public string Code { get; set; }
    public int LanguageId { get; set; }
    public long? _key { get; set; }
    public string _rev { get; set; }
}
[BsonIgnoreExtraElements]
public partial class DateModel
{
    public string Date1 { get; set; }
    public string Date2 { get; set; }
    public string Date3 { get; set; }
}

[BsonIgnoreExtraElements]
public partial class TimesModel
{
    public string Time1 { get; set; }
    public string Time2 { get; set; }
    public string Time3 { get; set; }
}
