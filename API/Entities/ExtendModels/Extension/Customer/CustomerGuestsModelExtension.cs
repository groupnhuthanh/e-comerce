using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Entities.ExtendModels.Extension.Customer;

[BsonNoId]
[BsonIgnoreExtraElements]
public class CustomerGuestsModelExtension : BaseModel
{
    public string DialingCode { get; set; }
    public bool IsSignIn { get; set; }
    public string LastLoginDate { get; set; }
    public string Token { get; set; }
    public List<string> TokenList { get; set; }
    public string Password { get; set; }
    public string LastUpdateDate { get; set; }
    public string UnlinkDate { get; set; }
    public string UserName { get; set; }
    public string Email { get; set; }

    public string Phone { get; set; }
    public string E164Format { get; set; }
    public int? Gender { get; set; }
    public string Company { get; set; }
    public string EmailVerifyDate { get; set; }
    public string Avatar { get; set; }
    public string Grant_type { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string Iss { get; set; }
    public string Aud { get; set; }
    public string Firebase { get; set; }
    public bool? IsVerifyPhone { get; set; }
    public bool? IsVerifyEmail { get; set; }
    public string Sub { get; set; }

    public string SubGoogle { get; set; }
    public bool? IsLinkedGoogle { get; set; }
    public string SubFacebook { get; set; }
    public bool? IsLinkedFacebook { get; set; }
    public string SubApple { get; set; }
    public bool? IsLinkedApple { get; set; }
    public string SubTwitter { get; set; }
    public bool? IsLinkedTwitter { get; set; }
    public string SubGithub { get; set; }
    public bool? IsLinkedGithub { get; set; }   
    public string SubMicrosoft { get; set; }
    public bool? IsLinkedMicrosoft { get; set; }
    public string SubEmail { get; set; }
    public bool? IsLinkedEmail { get; set; }
    public string SubPhone { get; set; }
    public bool? IsLinkedPhone { get; set; }
    public string UnlinkCustomerGuest_id { get; set; }
    public string CustomerGuestGuid { get; set; }
    public string SecurityCode { get; set; }
    public int ExpiredCode { get; set; }
    public DateTime ExpiredDate { get; set; }
    public string SecurityCodeMask { get; set; }

    public bool EmailVerified { get; set; }
    
    public bool PhoneVerified { get; set; }
    public string Birthday { get; set; }
    public string Address1 { get; set; }
    public string EmployeeGuid { get; set; }
    public int? Source { get; set; }
    public string CityName { get; set; }
    public string LastDateSignOut { get; set; }
    public int? CountryId { get; set; }
    public string Latitude { get; set; }
    public string Longitude { get; set; }
    public int? CityId { get; set; }
    public int? AddressTypeId { get; set; } = 0;
    public string Name { get; set; }
    public string AddressTypeName { get; set; }
    public string Address2 { get; set; }
    public int? DistrictId { get; set; }
    public string DistrictName { get; set; }
    public int? WardId { get; set; }
    public string WardName { get; set; }
    public string Note { get; set; }
    public string grant_type { get; set; }
    public string NickName { get; set; }
    public int? Active { get; set; }
    public int? Locker { get; set; }
    public string LockerDate { get; set; }
    public string HashId { get; set; }
    public bool IsSystemCheck { get; set; }
    public string FirebaseTokenLog { get; set; }
    public string Version { get; set; }
    public bool? IsLinkedPrepaidPayoo { get; set; }
    public string PrepaidPayooDate { get; set; }
    public string PrepaidPayooCode { get; set; }
}