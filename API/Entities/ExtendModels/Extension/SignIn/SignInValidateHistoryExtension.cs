using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Entities.ExtendModels.Extension.SignIn;

[BsonIgnoreExtraElements]
public class SignInValidateHistoryExtension : BaseModel
{
    [JsonProperty("OtpToken", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string OtpToken { get; set; }
    [JsonProperty("Visible", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public int Visible { get; set; }
    [JsonProperty("CustomerGuestGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string CustomerGuestGuid { get; set; }
    [JsonProperty("Phone", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Phone { get; set; }
    public string PhoneE164 { get; set; }
}