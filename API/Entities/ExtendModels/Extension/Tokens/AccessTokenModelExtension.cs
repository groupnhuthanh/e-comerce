using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Entities.ExtendModels.Extension.Tokens;

[BsonIgnoreExtraElements]
public class AccessTokenModelExtension : BaseModel
{
    [JsonProperty("Token", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Token { get; set; }
}