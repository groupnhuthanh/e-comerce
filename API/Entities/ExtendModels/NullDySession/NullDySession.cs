﻿using Entities.ExtendModels.Tokens;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Entities.ExtendModels.NullDySession;

/// 
///Current session object
/// 
/// <summary>
/// The null dy session.
/// </summary>
public class NullDySession
{
    /// 
    ///Get dysession instance
    /// 
    /// <summary>
    /// Gets the instance.
    /// </summary>
    public static NullDySession Instance { get; set; } = new NullDySession();
    /// 
    ///Get current user information
    /// 
    /// <summary>
    /// Gets the token user.
    /// </summary>
    public static TokenView TokenUser
    {

        get
        {
            var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            var claimsIdentity = claimsPrincipal?.Identity as ClaimsIdentity;
            var device_id = claimsIdentity?.Claims.FirstOrDefault(c => c.Type == "DeviceGuid");
            if (device_id == null || string.IsNullOrEmpty(device_id.Value))
            {
                var token_id = claimsIdentity?.Claims.FirstOrDefault(c => c.Type == "Token_id");
                if (token_id == null)
                    return null;
                TokenView tokenViewForRefr = null;
                var userClaimForRefr = claimsIdentity?.Claims.ToList();
                if (userClaimForRefr != null && userClaimForRefr.Any())
                {
                    var claim = JsonConvert.SerializeObject(userClaimForRefr.ToDictionary(x => x.Type, y => y.Value));
                    tokenViewForRefr = JsonConvert.DeserializeObject<TokenView>(claim);
                }
                return tokenViewForRefr;
            }
            TokenView tokenView = null;
            var userClaim = claimsIdentity?.Claims.ToList();
            if (userClaim != null && userClaim.Any())
            {
                var claim = JsonConvert.SerializeObject(userClaim.ToDictionary(x => x.Type, y => y.Value));
                tokenView = JsonConvert.DeserializeObject<TokenView>(claim);
            }
            return tokenView;
        }
        //set
        //{
        //    var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
        //    var claimsIdentity = claimsPrincipal?.Identity as ClaimsIdentity;
        //    var userClaim = claimsIdentity?.Claims.ToList();
        //    if (userClaim != null && userClaim.Any())
        //    {
        //        var claim = JsonConvert.SerializeObject(userClaim.ToDictionary(x => x.Type, y => y.Value));
        //        NullDySession.TokenUser = JsonConvert.DeserializeObject<TokenView>(claim);
        //    }
        //}
    }

    /// <summary>
    /// Prevents a default instance of the <see cref="NullDySession"/> class from being created.
    /// </summary>
    private NullDySession()
    {
    }
}
