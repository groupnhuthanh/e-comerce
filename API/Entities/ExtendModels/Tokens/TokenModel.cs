﻿namespace Entities.ExtendModels.Tokens;

public class TokenModel
{
    public string Token { get; set; }
    public string RefreshToken { get; set; }
    public int ExpireIn { get; set; } = 2;
    public Define Define { get; set; } = new Define();
}

public class Define
{
    public string ExpireIn = "Thời gian hết hạn của token tính từ: 2 Hrs";
}

public partial class PostToken
{
    public string Code { get; set; }
    public object Secrect { get; set; }
}

public class TokenView
{
    public string User_id { get; set; }
    public string Location_id { get; set; }
    public string Device_id { get; set; }
    public string Customer_id { get; set; }
    public string Staff_id { get; set; }
    public string CustomerGuid { get; set; }
    public string Token_id { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string Store { get; set; }
    public string Handle { get; set; }
    public string Language { get; set; }
    public string X_AUTH_CustomerGuest_id { get; set; }
    public string X_AUTH_DeviceCode { get; set; }
    public string X_AUTH_Timestamp { get; set; }
    public string X_AUTH_Nonce { get; set; }
    public string X_AUTH_Checksum { get; set; }
    public string X_AUTH_Signature { get; set; }
}

public class TokenValidate
{
    public string errorCode { get; set; } = "401";
    public string message { get; set; }
}
