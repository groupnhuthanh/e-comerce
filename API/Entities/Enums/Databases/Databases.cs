﻿namespace Entities.Enums.Databases;

public enum Databases
{
    Heineken,
    SinglePoint_Builder,
    SinglePoint_History,
    SinglePoint_Hr,
    SinglePoint_Orders_vi,
    SinglePoint_Payment,
    SinglePoint_Pos,
    SinglePoint_Staverse,
    SinglePoint_System,
    SinglePoint_Warehouse,
    SinglePoint_en,
    SingleWorker,
    System_OverallAccess,
    System_ui,
    admin,
    config,
    db_Identity,
    local,
    notification,
    test
}
