﻿namespace Entities.Enums.Tables;

/// <summary>
/// The tables.
/// </summary>
public enum TablesManagement
{
    ManagementIndex
}
public enum HeinekenTables
{
    InventoryItem,
    Challenge,
    ChallengeDetail,
    Avatar,
    NFTsToken,
    CustomerNFTsWallet
, Scores
, Games
, Song
, Leaderboard
, LeaderboardStarbeatz
, HighScores
, HighScoresStarbeatz
, RankWallet
, LeaderboardWeekday
, LeaderboardWeekdayStarbeatz
, LeaderboardAllsong
, LeaderboardAllsongStarbeatz
, RankWalletHistory
, StarbeatzChallengeScores
, PartnerNotifications,
    TicketCode,
    TicketCodeDetail,
    CustomerTicketCode
, CustomerNFTsWalletHistory,
    InvitationCode,
    InvitationCodeDetail,
    CustomerInvitationCode
}
public enum notificationTables
{
    EmailSetting,
    NotificationSetting
}
public enum SinglePoint_BuilderTables
{
    NavigationLink,
    PageBlocks

}
public enum SinglePoint_enTables
{
    APIType,
    AccountGroup,
    AccountGroupImages,
    AccountUnit,
    AppVersion,
    Assets,
    BillOfMaterial,
    Brands,
    Building,
    Bundle_ApplyToItem,
    Campaign,
    CampaignImage,
    Category,
    CategoryImages,
    CategoryProduct,
    CompressFile,
    CurrencyTokenPoint,
    Customer,
    CustomerCash,
    CustomerCashDetail,
    CustomerCompVoid,
    CustomerCreditCard,
    CustomerDinningOptions,
    CustomerDinningOptionsLocation,
    CustomerDiscounts,
    CustomerGroupType,
    CustomerGuestLoyalty,
    CustomerGuestLoyaltyDetail,
    CustomerGuestLoyaltyPending,
    CustomerGuests,
    CustomerGuestsDeleteHistory,
    CustomerGuestsDiningOption,
    CustomerGuestsGroup,
    CustomerGuestsLedger,
    CustomerGuestsLoginHistory,
    CustomerGuestsMembership,
    CustomerGuestsMembershipDetail,
    CustomerGuestsPaymentsBillings,
    CustomerGuestsPaymentsShippings,
    CustomerGuestsVerify,
    CustomerGuestsVerifyExchange,
    CustomerLanguages,
    CustomerLedger,
    CustomerLedgerDetail,
    CustomerOrdersSettingTotal,
    CustomerOthersPayments,
    CustomerOthersPaymentsDetail,
    CustomerService,
    CustomerServiceLocation,
    CustomerServiceProduct,
    CustomerSettings,
    CustomerSurcharge,
    CustomerSurchargeLocation,
    CustomerSurchargeProduct,
    CustomerTax,
    CustomerTaxLocation,
    CustomerTaxProduct,
    CustomerWallet,
    CustomerWalletDetail,
    DateSettings,
    DeliveryTime,
    Device,
    DeviceImage,
    DeviceInfo,
    DeviceRequestHistory,
    DeviceSettingIdsMultiple,
    DeviceSettingsIds,
    DeviceVideo,
    DevicesSettings,
    DevicesViewItemMode,
    DiscountCode,
    DiscountsImages,
    Discounts_ApplyToItem,
    Discounts_BuyXgetY,
    Discounts_Combo,
    Discounts_FixedAmount,
    Discounts_Percentage,
    Discounts_SpecificPrice,
    Domains,
    Employee,
    EmployeePermission,
    Environment,
    EventOverviewImage,
    EventsInteraction,
    EventsOverview,
    FAQs,
    FAQsGroup,
    FCMToken,
    FirebaseCustomerDiscounts,
    FirebaseMenus,
    FirebaseSettings,
    FormatSettings,
    FriendRequests,
    FriendsList,
    GiftCard,
    GiftCardDetail,
    GroupDiscount,
    GroupDiscountItem,
    GroupEmployee,
    GroupEvent,
    GroupLocation,
    GroupPayment,
    Hardware,
    HistoryLocation,
    Location,
    LocationSettingsIds,
    LocationShipping,
    LoyaltyEarningPoints,
    LoyaltyReward,
    ManagementIndex,
    MarketingMessage,
    MarketingPoll,
    MembershipLevel,
    Menu,
    MenusGroup,
    MenusGroupItem,
    MenusLocation,
    MenusRearrange,
    Modifier,
    ModifierItem,
    ModifierItemPriceOverride,
    NotificationMarketingMessage,
    NotificationType,
    OrderStatusDetail,
    OrdersSettingsStatus,
    OtpSms,
    PaymentType,
    PayooPrepaid,
    PayooPrepaidDetail,
    Permission,
    Policies,
    Product,
    ProductImages,
    ProductModifier,
    ProductPriceOverride,
    ReceiptsSettings,
    ReportsOrdersHistory,
    ScannerDevice,
    SettingsDeliveryRole,
    SettingsHardware,
    ShippingZone,
    SignInValidateHistory,
    StatusLocationTranslate,
    Supplier,
    SupplierCatalog,
    TransactionExport,
    TranslateAccountGroup,
    TranslateCategory,
    TranslateDeliveryTime,
    TranslateFAQs,
    TranslateFAQsGroup,
    TranslateGroupDiscount,
    TranslateModifier,
    TranslateModifierItem,
    TranslateOrderStatusDetail,
    TranslatePolicies,
    UserRewards,
    UserRewardsLedger,
    UserRewardsList,
    VariationPriceOverride,
    VoucherPayment

}
public enum SinglePoint_HistoryTables
{
    History_Brands,
    History_Location
}
public enum SinglePoint_HrTables
{
    ClockInOut
}
public enum SinglePoint_Orders_viTables
{
    CashDrawer,
    CashDrawerPayInOut,
    CashDrawerSaleOrders,
    HistoryOrderStatus,
    ManagementIndex,
    ORAMPLogOrder,
    ORAMPProductLog,
    ORProductLog,
    Order,
    OrderCustomerItemHistory,
    OrderDetail,
    OrderDiscount,
    OrderHistory,
    OrderHistorySubmitLog,
    OrderPayment,
    OrderStatusLog,
    RefundMethodOrder,
    RefundOrders,
}
public enum SinglePoint_PaymentTables
{
    APIType,
    ClSOrderPaymentPrepaid,
    OrderGetInfoPayooHistory,
    OrderPaymentHistory,
    OrderPaymentHistoryLog,
    OrderPaymentPayooNotify,
    PayooXML
}
public enum SinglePoint_PosTables
{
    Floor,
    FloorTable,
    ManagementIndex,
    NamingType,
    POSLogo,
    POSSecondaryScreen,
    POSSecondaryScreenImage,
    POSSecondaryScreenVideo,
    POSVideos,
    Section,
    SectionTable,
    SystemTableStatus,
    TableType
}
public enum SinglePoint_SystemTables
{
    SystemDeleteReason,
    SystemRawProductType,
    SystemStatusOrders,
    SystemTypeLocation,
    SystemInventoryReceiveItemTypes,
    SystemUnit,
    SystemSupplierProductType,
    SystemEventTypes,
    SystemQRPrefix,
    SystemDiscountsType
, SystemRefundReason,
    SystemCashType
}
public enum SinglePoint_WarehouseTables
{
    DeliveryDocket,
    DeliveryDocketDetail,
    Inventory,
    InventoryHistory,
    InventoryLedger,
    ManagementIndex,
    PurchaseOrder,
    PurchaseOrderDetail,
    PurchaseOrderLedger,
    PurchaseOrderLedgerDetail,
    PurchaseRequisition,
    PurchaseRequisitionDetail,
    StocksTransfer,
    StocksTransferDetail,
    WorkOrder,
    WorkOrderDetail
}
public enum SingleWorkerTables
{
    CustomerGuestsNoLogin
, CustomerGuestsLogin
, CustomerGuestsAcronymn
, Order
, WorkerLogs
}
public enum System_WorkerTables
{
    Settings,
    WorkerLogs
}
public enum System_OverallAccessTables
{
    AccessToken,
    BackgroundServiceSetting,
    CustomerGuestsOtpVerify,
    Environment,
    EnvironmentGroup,
    Logs,
    ManagementIndex,
    OtpSettings,
    Status,
    TypeServer,
    Variable,
    VariableDetail,
}
