﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_History.History_Brands;

public class History_BrandsModel : BaseModel
{
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public long? TypeHistoryId { get; set; }
    public string Guid { get; set; }
    public long? Visible { get; set; }
    public List<HistoryList> HistoryList { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}

public partial class HistoryList
{
    public string Column { get; set; }
    public string Old { get; set; }
    public string New { get; set; }
    public string Note { get; set; }

}
