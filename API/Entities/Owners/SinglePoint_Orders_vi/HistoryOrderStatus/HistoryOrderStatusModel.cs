﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.HistoryOrderStatus;

public class HistoryOrderStatusModel : BaseModel
{
    public DateTimeOffset? CreateDate { get; set; }
    public long? SystemId { get; set; }
    public string EmployeeGuid { get; set; }
    public string OrderGuid { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public long? ReasonId { get; set; }
    public string Reason { get; set; }
    public long? OrderStatusId { get; set; }
    public long? PaymentStatusId { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
