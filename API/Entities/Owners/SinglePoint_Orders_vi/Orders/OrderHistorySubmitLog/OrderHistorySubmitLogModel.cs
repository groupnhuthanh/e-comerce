﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.Orders.OrderHistorySubmitLog;

public class OrderHistorySubmitLogModel : BaseModel
{
    public string AppVersion { get; set; }
    public WelcomeOrder Order { get; set; }
    public OrderDetail OrderDetail { get; set; }
    public bool? IsCompletedOrder { get; set; }
    public List<object> CommonErrorMessages { get; set; }
    public object OrderCustomerItemHistoryList { get; set; }
    public object QueueChecksum { get; set; }
    public bool? IsMerchantStore { get; set; }
    public object FromMerchantStore { get; set; }
}

public partial class WelcomeOrder
{
    public object LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public object Checksum { get; set; }
    public object Step { get; set; }
    public object UpdateAt { get; set; }
    public object DiningOptionId { get; set; }
    public object CustomerGuestGuid { get; set; }
    public object BaseUtcOffSet { get; set; }
    public object MenuLocationGuid { get; set; }
    public object AssignToDeviceGuid { get; set; }
    public object CreateDateAssign { get; set; }
    public object CashDrawerId { get; set; }
    public object AssignToLocationGuid { get; set; }
    public object AssignToEmployeeGuid { get; set; }
    public object CurrencySymbol { get; set; }
    public object CurrencyId { get; set; }
    public object CashDrawerGuid { get; set; }
    public object AppVersion { get; set; }
    public object IsRefund { get; set; }
    public long? OrderWalletPayment { get; set; }
    public object OrderStatusId { get; set; }
    public object PaymentStatusId { get; set; }
    public object UserGuid { get; set; }
    public object EmployeeGuid { get; set; }
    public object LocationGuid { get; set; }
    public object DeviceGuid { get; set; }
    public object Source { get; set; }
    public object DeliveryTypeId { get; set; }
    public object DeliveryTime { get; set; }
    public object UserForBillInformation { get; set; }
    public long? QuantityPeopleTable { get; set; }
    public object TableId { get; set; }
    public object TableName { get; set; }
    public object Code { get; set; }
    public object OrderCode { get; set; }
    public object OrderTime { get; set; }
    public object OrderWait { get; set; }
    public object OrderTimeLater { get; set; }
    public object HourPickup { get; set; }
    public object OrderDate { get; set; }
    public object EstimateTime { get; set; }
    public object CreateDateSync { get; set; }
    public long? IsOfflineSync { get; set; }
    public object DeliveryCode { get; set; }
    public string CustomerGuid { get; set; }
    public object IsNew { get; set; }
    public object Version { get; set; }
    public object Uuid { get; set; }
    public object OrderKey { get; set; }
    public object IsRead { get; set; }
    public object OrderParentGuid { get; set; }
    public object OrderRefundType { get; set; }
    public object EmployeeRefund { get; set; }
    public object UserRefund { get; set; }
    public long? ReasonId { get; set; }
    public object ReasonText { get; set; }
    public object DateRefund { get; set; }
    public object PayooDate { get; set; }
    public object PayooSignature { get; set; }
    public long? SendToPayooTotal { get; set; }
    public long? IsSyncToOnramp { get; set; }
    public bool? IsLoyalty { get; set; }
    public bool? IsLoyaltySuccess { get; set; }
    public object IsSyncToOnrampDate { get; set; }
    public bool? IsCustomerNew { get; set; }
    public long? UseTarget { get; set; }
    public object AccountNo { get; set; }
}

public partial class OrderDetail
{
    public object LocationId { get; set; }
    public object CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public object CashDrawerId { get; set; }
    public List<object> TableList { get; set; }
    public List<object> SurchargeFeeList { get; set; }
    public List<object> ShippingFeeList { get; set; }
    public List<object> ServiceFeeList { get; set; }
    public List<object> TaxFeeList { get; set; }
    public object LoyaltyPointsList { get; set; }
    public object OrderRefund { get; set; }
    public long? TotalQuantity { get; set; }
    public object UserGuid { get; set; }
    public object CustomerGuestGuid { get; set; }
    public object LocationGuid { get; set; }
    public object DeviceGuid { get; set; }
    public object CompVoidList { get; set; }
    public OrderDetailOrder Order { get; set; }
    public DiningOption DiningOption { get; set; }
    public Shipping Shipping { get; set; }
    public Billing Billing { get; set; }
    public DeliveryTime DeliveryTime { get; set; }
    public List<object> DiscountUsedList { get; set; }
    public object DeletingDiscount { get; set; }
    public List<PaymentList> PaymentList { get; set; }
    public List<object> DiscountList { get; set; }
    public List<OrderProduct> OrderProducts { get; set; }
    public object OrderId { get; set; }
    public object PurchaseRequisitionId { get; set; }
    public object OrderParentGuid { get; set; }
    public object IsRefund { get; set; }
    public object DiscountListAll { get; set; }
    public object DateRefund { get; set; }
    public object IsCheckInsertHighlands { get; set; }
    public object ReasonList { get; set; }
}

public partial class Billing
{
    public string Id { get; set; }
    public long? Key { get; set; }
    public object Rev { get; set; }
    public object CustomerGuestGuid { get; set; }
    public string FullName { get; set; }
    public object Acronymn { get; set; }
    public object Avatar { get; set; }
    public long? MembershipLevelType { get; set; }
    public object MembershipLevel { get; set; }
    public string Phone { get; set; }
    public object Email { get; set; }
    public string Company { get; set; }
    public string AddressName { get; set; }
    public long? WardId { get; set; }
    public long? DistrictId { get; set; }
    public long? CityId { get; set; }
    public string WardName { get; set; }
    public string DistrictName { get; set; }
    public string CityName { get; set; }
    public long? AddressTypeId { get; set; }
    public object AddressTypeName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string Note { get; set; }
    public long? Visible { get; set; }
    public object UserGuid { get; set; }
}

public partial class DeliveryTime
{
    public string DateText { get; set; }
    public string TimeText { get; set; }
    public string DateValue { get; set; }
    public string TimeValue { get; set; }
    public bool? IsNow { get; set; }
    public long? EstimateTime { get; set; }
    public string TimeDisplay { get; set; }
    public string DeliveryAt { get; set; }
}

public partial class DiningOption
{
    public long? Id { get; set; }
    public string Title { get; set; }
    public long? TypeId { get; set; }
    public string Acronymn { get; set; }
}

public partial class OrderDetailOrder
{
    public long? Subtotal { get; set; }
    public long? OtherFee { get; set; }
    public long? Discount { get; set; }
    public long? Grandtotal { get; set; }
    public long? PaymentAmount { get; set; }
    public long? Balance { get; set; }
    public long? Change { get; set; }
    public long? Received { get; set; }
    public long? Points { get; set; }
    public long? Service { get; set; }
    public long? Surcharge { get; set; }
    public long? Shipping { get; set; }
    public long? Tax { get; set; }
    public long? Tip { get; set; }
    public string Note { get; set; }
    public object SubtotalFormatter { get; set; }
    public object DiscountFormatter { get; set; }
    public object OtherFeeFormatter { get; set; }
    public object GrandtotalFormatter { get; set; }
    public object PaymentAmountFormatter { get; set; }
    public object BalanceFormatter { get; set; }
    public long? IsRefund { get; set; }
    public object OrderId { get; set; }
    public object CompOrder { get; set; }
    public object Total { get; set; }
    public object DiscountTotal { get; set; }
    public object DiscountTotalPriceFormatter { get; set; }
    public object TotalFormatter { get; set; }
    public object RefundAmount { get; set; }
}

public partial class OrderProduct
{
    public object OrderGuid { get; set; }
    public object DeviceGuid { get; set; }
    public long? IsRefund { get; set; }
    public object CreateDate { get; set; }
    public long? Price { get; set; }
    public long? ModSubtotal { get; set; }
    public object ServiceTotalPrice { get; set; }
    public object SurchargeTotalPrice { get; set; }
    public object TaxTotalPrice { get; set; }
    public object ShippingTotalPrice { get; set; }
    public string CategoryId { get; set; }
    public string GroupId { get; set; }
    public object ParentName { get; set; }
    public object DiningOption { get; set; }
    public List<VariantList> VariantList { get; set; }
    public List<object> ModifierList { get; set; }
    public List<object> SurchargeFeeList { get; set; }
    public List<object> ShippingFeeList { get; set; }
    public List<object> ServiceFeeList { get; set; }
    public List<object> TaxFeeList { get; set; }
    public List<object> ProductChoosedList { get; set; }
    public object BasePriceFormatter { get; set; }
    public string PriceFormatter { get; set; }
    public object BuyXGetY { get; set; }
    public bool? IsProduct { get; set; }
    public object CompVoidList { get; set; }
    public string Id { get; set; }
    public string Name1 { get; set; }
    public string Name2 { get; set; }
    public string Sku { get; set; }
    public string Variant { get; set; }
    public long? ProductTypeId { get; set; }
    public object ProductApplyTo { get; set; }
    public string Note { get; set; }
    public long? OrderDetailId { get; set; }
    public long? ComparePrice { get; set; }
    public long? ModifierTotalPrice { get; set; }
    public object TotalPrice { get; set; }
    public long? Quantity { get; set; }
    public object ProductSubtotal { get; set; }
    public object ProdModSubtotal { get; set; }
    public long? Subtotal { get; set; }
    public long? LineTotal { get; set; }
    public long? OtherFee { get; set; }
    public long? DiscountTotalPrice { get; set; }
    public long? GrossPrice { get; set; }
    public string ParentId { get; set; }
    public long? ParentIndex { get; set; }
    public object Url { get; set; }
    public List<object> DiscountList { get; set; }
    public object SubtotalFormatter { get; set; }
    public object LineTotalFormatter { get; set; }
    public object OtherFeeFormatter { get; set; }
    public long? SalesItemTypeId { get; set; }
    public long? RawProductTypeId { get; set; }
    public object TotalReceivedQuantity { get; set; }
    public object SupplierCode { get; set; }
    public object BillOfMaterial { get; set; }
    public bool? IsReward { get; set; }
    public object RewardId { get; set; }
    public long? RefundAmount { get; set; }
    public object ReferenceId1 { get; set; }
    public object ReferenceId2 { get; set; }
}

public partial class VariantList
{
    public object Variant { get; set; }
    public long? Id { get; set; }
    public string Value { get; set; }
    public long? Level { get; set; }
    public bool? Visible { get; set; }
    public string GroupValue { get; set; }
    public long? ComparePrice { get; set; }
    public long? CostPerItem { get; set; }
    public long? Price { get; set; }
    public string Sku { get; set; }
    public long? Inventory { get; set; }
    public long? Discount { get; set; }
    public string Barcode { get; set; }
    public object ReferenceId1 { get; set; }
    public object ReferenceId2 { get; set; }
}

public partial class PaymentList
{
    public string Id { get; set; }
    public long? PaymentTypeId { get; set; }
    public long? ApplyToId { get; set; }
    public long? SubId { get; set; }
    public string Title { get; set; }
    public string Icon { get; set; }
    public long? Payable { get; set; }
    public long? OverPay { get; set; }
    public string EmployeeName { get; set; }
    public string CardNo { get; set; }
    public List<object> VoucherList { get; set; }
    public object CreateDate { get; set; }
    public long? PaymentStatus { get; set; }
    public object PaymentSource { get; set; }
    public object BankName { get; set; }
}

public partial class Shipping
{
    public string Id { get; set; }
    public object CustomerGuestId { get; set; }
    public string FullName { get; set; }
    public string StoreName { get; set; }
    public string Phone { get; set; }
    public object Email { get; set; }
    public string AddressName { get; set; }
    public string WardName { get; set; }
    public string DistrictName { get; set; }
    public string CityName { get; set; }
    public long? AddressTypeId { get; set; }
    public string AddressTypeName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string Note { get; set; }
    public long? DistanceValue { get; set; }
    public string DistanceText { get; set; }
    public long? DurationValue { get; set; }
    public string DurationText { get; set; }
    public double? Latitude { get; set; }
    public double? Longitude { get; set; }
    public string Company { get; set; }
    public object IsComplete { get; set; }
}
