﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.Orders.OrderCustomerItemHistory;

public class OrderCustomerItemHistoryModel : BaseModel
{
    public string UserId { get; set; }
    public string LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string OrderId { get; set; }
    public string CustomerId { get; set; }
    public string CategoryId { get; set; }
    public string ProductId { get; set; }
    public string ProductName { get; set; }
    public string ParentProductId { get; set; }
    public long? ProductTypeId { get; set; }
    public string Variant { get; set; }
    public string Sku { get; set; }
    public long? DigitalTypeId { get; set; }
    public long? Quantity { get; set; }
    public long? AllowQuantityMaxAll { get; set; }
    public long? MaximumQuantityPerDay { get; set; }
    public long? MaximumQuantityPerOrder { get; set; }
    public long? MaximumPurchasePerCustomer { get; set; }
    public bool? IsLock { get; set; }
    public long? SalesItemTypeId { get; set; }
    public string OrderParentGuid { get; set; }
}
