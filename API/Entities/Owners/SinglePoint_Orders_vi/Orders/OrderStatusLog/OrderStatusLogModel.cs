﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.Orders.OrderStatusLog;

public class OrderStatusLogModel : BaseModel
{
    public string OrderId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public List<OrderStatusList> OrderStatusList { get; set; }
}

public partial class OrderStatusList
{
    public long? OrderStatusId { get; set; }
    public string OrderStatusReference1 { get; set; }
    public string OrderStatusReference2 { get; set; }
    public string DeviceGuid { get; set; }
    public string LocationGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public List<ReasonList> ReasonList { get; set; }
    public Metadata Metadata { get; set; }
}

public partial class Metadata
{
    public bool? Success { get; set; }
    public List<object> Details { get; set; }
}

public partial class ReasonList
{
    public long? ReasonId { get; set; }
    public string ReasonNote { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string EmployeeGuid { get; set; }

}
