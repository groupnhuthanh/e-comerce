﻿using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Owners.SinglePoint_Orders_vi.Orders.Order;

[BsonIgnoreExtraElements]
public class OrderModel : BaseModel
{
    public object LocationId { get; set; }
    public string Checksum { get; set; }
    public long? Step { get; set; }
    public long? UpdateAt { get; set; }
    public long? DiningOptionId { get; set; }
    public string CustomerGuestGuid { get; set; }
    public string BaseUtcOffSet { get; set; }
    public string MenuLocationGuid { get; set; }
    public object AssignToDeviceGuid { get; set; }
    public object CreateDateAssign { get; set; }
    public string CashDrawerId { get; set; }
    public string AssignToLocationGuid { get; set; }
    public object AssignToEmployeeGuid { get; set; }
    public string CurrencySymbol { get; set; }
    public long? CurrencyId { get; set; }
    public string CashDrawerGuid { get; set; }
    public string AppVersion { get; set; }
    public long? IsRefund { get; set; }
    public long? OrderWalletPayment { get; set; }
    public long? OrderStatusId { get; set; }
    public long? PaymentStatusId { get; set; }
    public string UserGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public int? Source { get; set; }
    public int? DeliveryTypeId { get; set; }
    public string DeliveryTime { get; set; }
    public bool? UserForBillInformation { get; set; }
    public long? QuantityPeopleTable { get; set; }
    public int? TableId { get; set; }
    public string TableName { get; set; }
    public string Code { get; set; }
    public string OrderCode { get; set; }
    public string OrderTime { get; set; }
    public int? OrderWait { get; set; }
    public string OrderTimeLater { get; set; }
    public string HourPickup { get; set; }
    public string OrderDate { get; set; }
    public int? EstimateTime { get; set; }
    public string CreateDateSync { get; set; }
    public long? IsOfflineSync { get; set; }
    public long? DeliveryCode { get; set; }
    public string CustomerGuid { get; set; }
    public bool? IsNew { get; set; }
    public string Version { get; set; }
    public string Uuid { get; set; }
    public string OrderKey { get; set; }
    public bool? IsRead { get; set; }
    public string OrderParentGuid { get; set; }
    public int? OrderRefundType { get; set; }
    public string EmployeeRefund { get; set; }
    public object UserRefund { get; set; }
    public long? ReasonId { get; set; }
    public string ReasonText { get; set; }
    public string DateRefund { get; set; }
    public string PayooDate { get; set; }
    public string PayooSignature { get; set; }
    public long? SendToPayooTotal { get; set; }
    public long? IsSyncToOnramp { get; set; }
    public bool? IsLoyalty { get; set; }
    public bool? IsLoyaltySuccess { get; set; }
    public string IsSyncToOnrampDate { get; set; }
    public bool? IsCustomerNew { get; set; }
    public long? UseTarget { get; set; }
    public string AccountNo { get; set; }
    public bool? IsPayooSdk { get; set; }
    public long? PayooSdk { get; set; }
    public string Loyalty { get; set; }
    public bool? IsWorker { get; set; }
    public string WorkerDate { get; set; }
}
