﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.Orders.OrderHistory;

public class OrderHistoryModel : BaseModel
{
    public string EmployeeGuid { get; set; }
    public long? OrderStatusId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public CreateDate CreateDate { get; set; }
    public string CashDrawerId { get; set; }
}

public partial class CreateDate
{
    public string T { get; set; }
    public List<double> V { get; set; }
}
