﻿using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDetail;

[BsonIgnoreExtraElements]
public class OrderDetailModel : BaseModel
{
    public object LocationId { get; set; }
    public string CashDrawerId { get; set; }
    public List<TableList> TableList { get; set; }
    public List<object> SurchargeFeeList { get; set; }
    public object ShippingFeeList { get; set; }
    public List<object> ServiceFeeList { get; set; }
    public List<object> TaxFeeList { get; set; }
    public object LoyaltyPointsList { get; set; }
    public List<OrderRefundModel> OrderRefund { get; set; }
    public long? TotalQuantity { get; set; }
    public string UserGuid { get; set; }
    public object CustomerGuestGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public List<object> CompVoidList { get; set; }
    public Order Order { get; set; }
    public DiningOption DiningOption { get; set; }
    public object Shipping { get; set; }
    public object Billing { get; set; }
    public object DeliveryTime { get; set; }
    public List<DiscountUsed> DiscountUsedList { get; set; }
    public object DeletingDiscount { get; set; }
    public List<PaymentList> PaymentList { get; set; }
    public List<DiscountModel> DiscountList { get; set; }
    public List<OrderProduct> OrderProducts { get; set; }
    public string Order_id { get; set; }
    public object PurchaseRequisitionId { get; set; }
    public object OrderParentGuid { get; set; }
    public int? isRefund { get; set; }
    public object DiscountListAll { get; set; }
    public object DateRefund { get; set; }
    public object IsCheckInsertHighlands { get; set; }
    public object ReasonList { get; set; }
    public string Creator { get; set; }
    public string CreatorName { get; set; }
    public string CreatorAcronymn { get; set; }
    public string CreatorAvatar { get; set; }
    public string StatusName { get; set; }
    public string StatusColor { get; set; }
    public string StatusAcronymn { get; set; }
    public decimal SubTotal { get; set; }
}
[BsonIgnoreExtraElements]
public partial class DiningOption
{
    public long? Id { get; set; }
    public string Title { get; set; }
    public long? TypeId { get; set; }
    public string Acronymn { get; set; }
}
[BsonIgnoreExtraElements]
public partial class Order
{
    public decimal Subtotal { get; set; }
    public decimal OtherFee { get; set; }
    public decimal Discount { get; set; }
    public decimal Grandtotal { get; set; }
    public decimal PaymentAmount { get; set; }
    public decimal Balance { get; set; }
    public decimal Change { get; set; }
    public decimal Received { get; set; }
    public long? Points { get; set; }
    public long? Service { get; set; }
    public long? Surcharge { get; set; }
    public long? Shipping { get; set; }
    public long? Tax { get; set; }
    public long? Tip { get; set; }
    public string Note { get; set; }
    public object SubtotalFormatter { get; set; }
    public object DiscountFormatter { get; set; }
    public object OtherFeeFormatter { get; set; }
    public object GrandtotalFormatter { get; set; }
    public object PaymentAmountFormatter { get; set; }
    public object BalanceFormatter { get; set; }
    public long? IsRefund { get; set; }
    public object OrderId { get; set; }
    public object CompOrder { get; set; }
    public object Total { get; set; }
    public object DiscountTotal { get; set; }
    public object DiscountTotalPriceFormatter { get; set; }
    public object TotalFormatter { get; set; }
    public object RefundAmount { get; set; }
}
[BsonIgnoreExtraElements]
public partial class OrderRefundModel
{
    public string PaymentTypeGuid { get; set; }
    public string OrderGuid { get; set; }
    public int? RefundType { get; set; }
    public int? ReasonId { get; set; }
    public string ReasonText { get; set; }
    public string ProductGuid { get; set; }
    public int? OrderDetailId { get; set; }
    public int? MethodId { get; set; }
    public string NumberAccount { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string CustomerGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string CashDrawerGuid { get; set; }
    public string CreateDate { get; set; }
    public decimal? RefundAmount { get; set; }
}
[BsonIgnoreExtraElements]
public partial class OrderProduct
{
    public object OrderGuid { get; set; }
    public object DeviceGuid { get; set; }
    public long? IsRefund { get; set; }
    public object CreateDate { get; set; }
    public decimal? Price { get; set; }
    public decimal? ModSubtotal { get; set; }
    public decimal? ServiceTotalPrice { get; set; }
    public decimal? SurchargeTotalPrice { get; set; }
    public decimal? TaxTotalPrice { get; set; }
    public decimal? ShippingTotalPrice { get; set; }
    public string CategoryId { get; set; }
    public string GroupId { get; set; }
    public object ParentName { get; set; }
    public DiningOption DiningOption { get; set; }
    public List<VariantList> VariantList { get; set; }
    public List<object> ModifierList { get; set; }
    public List<object> SurchargeFeeList { get; set; }
    public object ShippingFeeList { get; set; }
    public List<object> ServiceFeeList { get; set; }
    public List<object> TaxFeeList { get; set; }
    public object ProductChoosedList { get; set; }
    public object BasePriceFormatter { get; set; }
    public object PriceFormatter { get; set; }
    public object BuyXGetY { get; set; }
    public bool? IsProduct { get; set; }
    public List<object> CompVoidList { get; set; }
    public string Id { get; set; }
    public string Name1 { get; set; }
    public string Name2 { get; set; }
    public object Sku { get; set; }
    public object Variant { get; set; }
    public long? ProductTypeId { get; set; }
    public long? ProductApplyTo { get; set; }
    public object Note { get; set; }
    public long? OrderDetailId { get; set; }
    public object ComparePrice { get; set; }
    public decimal? ModifierTotalPrice { get; set; }
    public decimal? TotalPrice { get; set; }
    public int? Quantity { get; set; }
    public decimal? ProductSubtotal { get; set; }
    public decimal? ProdModSubtotal { get; set; }
    public decimal? Subtotal { get; set; }
    public decimal? LineTotal { get; set; }
    public decimal? OtherFee { get; set; }
    public decimal? DiscountTotalPrice { get; set; }
    public decimal? GrossPrice { get; set; }
    public string ParentId { get; set; }
    public int? ParentIndex { get; set; }
    public string Url { get; set; }
    public List<object> DiscountList { get; set; }
    public object SubtotalFormatter { get; set; }
    public object LineTotalFormatter { get; set; }
    public object OtherFeeFormatter { get; set; }
    public object SalesItemTypeId { get; set; }
    public long? RawProductTypeId { get; set; }
    public object TotalReceivedQuantity { get; set; }
    public object SupplierCode { get; set; }
    public object BillOfMaterial { get; set; }
    public bool? IsReward { get; set; }
    public object RewardId { get; set; }
    public long? RefundAmount { get; set; }
    public object ReferenceId1 { get; set; }
    public object ReferenceId2 { get; set; }
}
[BsonIgnoreExtraElements]
public partial class DiscountModel
{
    public string Id { get; set; }
    public string DiscountName { get; set; }
    public string DiscountCode { get; set; }
    public bool? DiscountAutomatic { get; set; }
    public int? OnlyApplyDiscountOncePerOrder { get; set; }
    public decimal? DiscountTotalPrice { get; set; }
    public int? DiscountQuantity { get; set; }
    public decimal? DiscountLineTotalPrice { get; set; }
    public long DiscountType { get; set; }
    public object DiscountValueType { get; set; }
    public int? ParentTypeId { get; set; }
    public decimal? DiscountValue { get; set; }
    public decimal? MaximumAmount { get; set; }
    public object DiscountLineTotalPriceFormatter { get; set; }
    public object OrderId { get; set; }
    public object Discount { get; set; }
    public object DiscountApplyTo { get; set; }
    public object OrderDetailId { get; set; }
    public object ProductId { get; set; }
}
[BsonIgnoreExtraElements]
public partial class VariantList
{
    public object Variant { get; set; }
    public long? Id { get; set; }
    public string Value { get; set; }
    public long? Level { get; set; }
    public bool? Visible { get; set; }
    public object GroupValue { get; set; }
    public object ComparePrice { get; set; }
    public object CostPerItem { get; set; }
    public object Price { get; set; }
    public object Sku { get; set; }
    public object Inventory { get; set; }
    public object Discount { get; set; }
    public object Barcode { get; set; }
    public object ReferenceId1 { get; set; }
    public object ReferenceId2 { get; set; }
}
[BsonIgnoreExtraElements]
public partial class PaymentList
{
    public string _id { get; set; }
    public int? PaymentTypeId { get; set; }
    public int? ApplyToId { get; set; }
    public int? SubId { get; set; }
    public string Title { get; set; }
    public string Icon { get; set; }
    public decimal Payable { get; set; }
    public decimal OverPay { get; set; }
    public string EmployeeName { get; set; }
    public string CardNo { get; set; }
    public List<object> VoucherList { get; set; }
    public string CreateDate { get; set; }
    public long? PaymentStatus { get; set; }
    public object PaymentSource { get; set; }
    public object BankName { get; set; }
}
[BsonIgnoreExtraElements]
public partial class TableList
{
    public string Id { get; set; }
    public long? TableName { get; set; }
    public long? PeopleQuantity { get; set; }
    public List<LogList> LogList { get; set; }
}
[BsonIgnoreExtraElements]
public partial class LogList
{
    public long? Id { get; set; }
    public string Title { get; set; }
    public string DateTime { get; set; }
}
[BsonIgnoreExtraElements]
public partial class DiscountUsed
{
    public string _id { get; set; }
    public string DiscountName { get; set; }
    public string Reward_id { get; set; }
    public bool? IsReward { get; set; }
    public string DiscountCode { get; set; }
    public int? Quantity { get; set; }
    public bool? IsFromDeals { get; set; }
    public int? UseSkipProduct { get; set; }
    public int? DiscountType { get; set; }
    public string Acronymn { get; set; }
}
