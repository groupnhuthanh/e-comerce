﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.Orders.OrderDiscount;

public class OrderDiscountModel : BaseModel
{
    public object LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string OrderGuid { get; set; }
    public string DiscountGuid { get; set; }
    public string CustomerGuid { get; set; }
    public bool? DiscountAutomatic { get; set; }
    public string DiscountCode { get; set; }
    public long? DiscountType { get; set; }
    public string DiscountValue { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public long? OrderStatusId { get; set; }
    public long? PaymentStatusId { get; set; }
}
