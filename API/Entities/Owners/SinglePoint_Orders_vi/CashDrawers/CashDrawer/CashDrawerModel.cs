﻿using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;
using System.Reflection.Metadata;

namespace Entities.Owners.SinglePoint_Orders_vi.CashDrawers.CashDrawer;


[BsonIgnoreExtraElements]
public class CashDrawerModel : BaseModel
{
    public object LocationId { get; set; }
    public string DateIn { get; set; }
    public string DateOut { get; set; }
    public object DateClosed { get; set; }
    public decimal? StartingCash { get; set; }
    public decimal? EndCash { get; set; }
    public decimal? PaidIn { get; set; }
    public decimal? PaidOut { get; set; }
    public decimal? CashSales { get; set; }
    public decimal? CashRefunds { get; set; }
    public decimal? ExpectedInDrawer { get; set; }
    public string DrawerDescription { get; set; }
    public decimal? ActualInDrawer { get; set; }
    public decimal? Difference { get; set; }
    public int? StatusId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string Starter { get; set; }
    public object Ender { get; set; }
    public string Closer { get; set; }
    public int? TypeStartId { get; set; }
    public int? TypeEndId { get; set; }
    public int? TypeCloseId { get; set; }
    public string Code { get; set; }
    public string LastDate { get; set; }
    public int? EndSource { get; set; }
    public int? Source { get; set; }
    public string Init { get; set; }
}
