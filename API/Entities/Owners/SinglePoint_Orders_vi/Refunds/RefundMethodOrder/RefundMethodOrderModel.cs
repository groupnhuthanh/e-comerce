﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.Refunds.RefundMethodOrder;

public class RefundMethodOrderModel : BaseModel
{
    public string UserGuid { get; set; }
    public string RefundAmount { get; set; }
    public string OrderGuid { get; set; }
    public long? RefundType { get; set; }
    public long? ReasonId { get; set; }
    public string ReasonText { get; set; }
    public long? MethodId { get; set; }
    public string PaymentTypeGuid { get; set; }
    public string NumberAccount { get; set; }
    public string CashDrawerGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public string Rev { get; set; }
    public long? Key { get; set; }
}
