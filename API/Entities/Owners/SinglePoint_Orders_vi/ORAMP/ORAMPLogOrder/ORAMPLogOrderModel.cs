﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPLogOrder;

public class ORAMPLogOrderModel : BaseModel
{
    public OrOrderRequest OrOrderRequest { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? LogTypeId { get; set; }
    public OrOrderHeader OrOrderHeader { get; set; }
    public object OrResponse { get; set; }
}

public partial class OrOrderHeader
{
    public string PublicKey { get; set; }
    public string DeviceCode { get; set; }
    public long? Timestamp { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
}

public partial class OrOrderRequest
{
    public string OrderId { get; set; }
    public long? OrderStatusId { get; set; }
    public long? ReasonType { get; set; }
    public object ReasonNotes { get; set; }
}
