﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORProductLog;

public class ORProductLogModel : BaseModel
{
    public OrProductRequest OrProductRequest { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? LogTypeId { get; set; }
    public OrOrderHeader OrOrderHeader { get; set; }
    public OrResponse OrResponse { get; set; }
}

public partial class OrOrderHeader
{
    public string PublicKey { get; set; }
    public string DeviceCode { get; set; }
    public long? Timestamp { get; set; }
}

public partial class OrProductRequest
{
    public Location Location { get; set; }
    public List<object> Product { get; set; }
}

public partial class Location
{
    public string Id { get; set; }
    public long? StatusId { get; set; }
}

public partial class OrResponse
{
    public string T { get; set; }
    public long? HttpStatusCode { get; set; }
    public string Message { get; set; }
    public bool? Status { get; set; }
    public long? MessageTypeId { get; set; }
    public object Model { get; set; }
}
