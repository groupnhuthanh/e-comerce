﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Orders_vi.ORAMP.ORAMPProductLog;

public class ORAMPProductLogModel : BaseModel
{
    public OrProductRequest OrProductRequest { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? LogTypeId { get; set; }
    public OrOrderHeader OrOrderHeader { get; set; }
    public object OrResponse { get; set; }
}

public partial class OrOrderHeader
{
    public string PublicKey { get; set; }
    public string DeviceCode { get; set; }
    public string Timestamp { get; set; }
}

public partial class OrProductRequest
{
    public Location Location { get; set; }
    public List<Product> Product { get; set; }
}

public partial class Location
{
    public string Id { get; set; }
    public long? StatusId { get; set; }
}

public partial class Product
{
    public string Id { get; set; }
    public long? StatusId { get; set; }
    public object Variants { get; set; }
    public object Modifier { get; set; }
}
