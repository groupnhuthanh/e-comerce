﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.AppVersion;

public class AppVersionModel : BaseModel
{

    public string VersionNumber { get; set; }
    public long? VersionCode { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public long? Source { get; set; }
    public Uri Path { get; set; }
    public long? DeviceTypeId { get; set; }
    public long? Visible { get; set; }
    public long? AssignTo { get; set; }
    public long? UpdateType { get; set; }
    public string LocationList { get; set; }
    public long? IsDeploy { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? DateUpdate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? DeployDate { get; set; }
}
