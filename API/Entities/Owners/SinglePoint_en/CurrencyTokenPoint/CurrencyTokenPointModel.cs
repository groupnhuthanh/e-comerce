﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.CurrencyTokenPoint;

public class CurrencyTokenPointModel : BaseModel
{

    public long? Key { get; set; }
    public string Rev { get; set; }
    public string Title { get; set; }
    public string Symbol { get; set; }
    public long? TypeCurrencyId { get; set; }
    public long? Visible { get; set; }
    public long? NetworkChain { get; set; }
    public long? TokenType { get; set; }
    public string Address { get; set; }
    public long? IsHasRoyalty { get; set; }
    public long? Royalty { get; set; }
    public long? IsHasBurn { get; set; }
    public long? Burn { get; set; }
    public string BurnAddress { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string Icon { get; set; }
}
