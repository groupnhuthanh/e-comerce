﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.VoucherPayment;

public class VoucherPaymentModel : BaseModel
{
    public string PaymentTypeGuid { get; set; }
    public string Name { get; set; }
    public long? Money { get; set; }
    public long? OrderNo { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? DateCreated { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
