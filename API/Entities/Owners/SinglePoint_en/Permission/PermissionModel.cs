﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Permission;

public class PermissionModel : BaseModel
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string Acronymn { get; set; }
    public string Color { get; set; }
    public bool? IsFullAccess { get; set; }
    public List<Permission> Permission { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class Permission
{
    public long? Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public bool? IsActive { get; set; }
    public bool? IsMandatory { get; set; }
    public List<PermissionGroupList> PermissionGroupList { get; set; }
}

public partial class PermissionGroupList
{
    public long? Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public List<PermissionsList> PermissionsList { get; set; }
}

public partial class PermissionsList
{
    public long? Id { get; set; }
    public string Name { get; set; }
    public bool? IsMandatory { get; set; }
    public bool? IsActive { get; set; }
    public List<PermissionsList> SubPermissionsList { get; set; }
    public string Description { get; set; }

}
