﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Firebases.FirebaseMenus;

public class FirebaseMenusModel : BaseModel
{
    public string ListLocation { get; set; }
    public long? AsignTo { get; set; }
    public string UserGuid { get; set; }
    public long? Version { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
