﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Firebases.FirebaseCustomerDiscounts;

public class FirebaseCustomerDiscountsModel : BaseModel
{
    public string ListLocation { get; set; }
    public long? AssignTo { get; set; }
    public string UserGuid { get; set; }
    public long? Version { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
