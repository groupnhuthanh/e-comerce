﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Firebases.FirebaseSettings;

public class FirebaseSettingsModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string UserGuid { get; set; }
    public string RootDb { get; set; }
    public string FileAuthentication { get; set; }
    public string UserDb { get; set; }
    public string Environment { get; set; }
    public FireStorePath FireStorePath { get; set; }
    public Define Define { get; set; }
    public string FireStoreDb { get; set; }
    public string FilePathAdmin { get; set; }
}

public partial class Define
{
    public string The0 { get; set; }
    public string The1 { get; set; }
    public string The2 { get; set; }
    public string The3 { get; set; }
    public string The4 { get; set; }
    public string The5 { get; set; }
}

public partial class FireStorePath
{
    public string ProductList { get; set; }
    public string Transaction { get; set; }
    public string AppVersion { get; set; }
    public string DataVersion { get; set; }
    public string OrderList { get; set; }
    public string LocationList { get; set; }
    public string CustomerDiscounts { get; set; }
    public string Menus { get; set; }
    public string ReportList { get; set; }
    public string ChallengeList { get; set; }
}
