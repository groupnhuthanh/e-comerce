﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Products.ProductModifier;

public class ProductModifierModel : BaseModel
{
    public string ProductGuid { get; set; }
    public string ModifierGuid { get; set; }
    public long? RequiredModifier { get; set; }
    public long? MaximumModifier { get; set; }
    public long? HideModifier { get; set; }
    public string ModifierItem { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
