﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Products.ProductPriceOverride;

public class ProductPriceOverrideModel : BaseModel
{
    public string VariationName { get; set; }
    public string VariationSku { get; set; }
    public long? UnitTypeId { get; set; }
    public string ProductGuid { get; set; }
    public long? Price { get; set; }
    public List<ListGroupLocation> ListGroupLocation { get; set; }
    public string PriceOverride { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class ListGroupLocation
{
    public long? Price { get; set; }
    public string GroupLocation { get; set; }
}
