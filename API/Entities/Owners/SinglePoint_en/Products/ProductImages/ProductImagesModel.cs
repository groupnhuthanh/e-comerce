﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Products.ProductImages;

public class ProductImagesModel : BaseModel
{
    public string ProductGuid { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public long? Width { get; set; }
    public long? Height { get; set; }
    public string Url { get; set; }
    public string UrlRewrite { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
