﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Products.Product;

public class ProductModel : BaseModel
{
    public bool? IsRequireKyc { get; set; }
    public string CategoryGuid { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Name2 { get; set; }
    public string Description2 { get; set; }
    public string Name3 { get; set; }
    public string Name4 { get; set; }
    public string Color { get; set; }
    public string Acronymn { get; set; }
    public long? UnitType { get; set; }
    public long? Price { get; set; }
    public object ComparePrice { get; set; }
    public object CostPerItem { get; set; }
    public long? ChargeTaxes { get; set; }
    public string Sku { get; set; }
    public string Barcode { get; set; }
    public long? Quantity { get; set; }
    public long? InventoryPolicy { get; set; }
    public long? Purchase { get; set; }
    public string Variants { get; set; }
    public string Modifier { get; set; }
    public string Combo { get; set; }
    public string PrintingLocation { get; set; }
    public string PricingMethod { get; set; }
    public string ModifierPricing { get; set; }
    public long? ProductType { get; set; }
    public long? Visible { get; set; }
    public long? ComboType { get; set; }
    public string GroupSku { get; set; }
    public string BrandGuid { get; set; }
    public long? IsSchedules { get; set; }
    public long? IsDateRange { get; set; }
    public long? ProductReceiveId { get; set; }
    public long? DigitalTypeId { get; set; }
    public string DigitalType { get; set; }
    public string ListCollection { get; set; }
    public long? MaximumPurchasePerCustomer { get; set; }
    public long? MaximumQuantityPerOrder { get; set; }
    public long? MaximumQuantityPerDay { get; set; }
    public BillOfMaterial BillOfMaterial { get; set; }
    public long? PerLevelQuantity { get; set; }
    public long? MaximumQuantity { get; set; }
    public long? StockAlertLevel { get; set; }
    public long? IsBom { get; set; }
    public long? UnitId { get; set; }
    public long? RawProductTypeId { get; set; }
    public string ReferenceId1 { get; set; }
    public string ReferenceId2 { get; set; }
    public List<ListSettingsHardware> ListSettingsHardware { get; set; }
    public string Version { get; set; }
    public long? AsignTo { get; set; }
    public string Location { get; set; }
    public object VariantsGroup { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string EmpGuid { get; set; }
    public long? StatusId { get; set; }
    public long? ProductId { get; set; }
    public string Handle { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public List<ListDateRange> ListDateRange { get; set; }
    public List<ListSchedule> ListSchedules { get; set; }
    public DateTimeOffset? DateUpdate { get; set; }
}

public partial class BillOfMaterial
{
}

public partial class ListDateRange
{
    public long? DateRangeId { get; set; }
    public string TitleDateEn { get; set; }
    public string DateOn { get; set; }
    public string TitleTimeEn { get; set; }
    public string TimeOn { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
}

public partial class ListSchedule
{
    public long? ScheduleId { get; set; }
    public string Day { get; set; }
    public List<Time> Time { get; set; }
    public long? Visible { get; set; }
    public long? OrderNo { get; set; }
}

public partial class Time
{
    public long? TimeId { get; set; }
    public TimeOn? TimeOn { get; set; }
    public TimeOff? TimeOff { get; set; }
}

public partial class ListSettingsHardware
{
    public string Id { get; set; }
    public string Name { get; set; }
}

public enum TimeOff { The1400, The2200 };

public enum TimeOn { The0800, The1400 };
