﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Groups.GroupPayment;

public class GroupPaymentModel : BaseModel
{
    public List<Payment> Payments { get; set; }
    public string UserGuid { get; set; }
    public long? PaymentListTypeId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}

public partial class Payment
{
    public string PaymentTypeGuid { get; set; }
    public long? OrderNo { get; set; }
}
