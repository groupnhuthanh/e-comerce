﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Groups.GroupLocation;

public class GroupLocationModel : BaseModel
{
    public string GroupName { get; set; }
    public long? Visible { get; set; }
    public long? IsAllLocation { get; set; }
    public List<string> Location { get; set; }
    public string Handle { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? GroupTypeId { get; set; }
}
