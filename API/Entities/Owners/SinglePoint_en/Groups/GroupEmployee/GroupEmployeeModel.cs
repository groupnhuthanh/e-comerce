﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Groups.GroupEmployee;

public class GroupEmployeeModel : BaseModel
{
    public string TitleEn { get; set; }
    public string Color { get; set; }
    public string Acronymn { get; set; }
    public string Description { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public List<Navigation> Navigations { get; set; }
    public long? IsAdmin { get; set; }
}

public partial class Navigation
{
    public long? NavigationId { get; set; }
    public string Handle { get; set; }
}

