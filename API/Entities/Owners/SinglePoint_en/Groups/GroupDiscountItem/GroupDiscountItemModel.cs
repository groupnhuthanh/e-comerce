﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Groups.GroupDiscountItem;

public class GroupDiscountItemModel : BaseModel
{
    public string DiscountGuid { get; set; }
    public long? Visible { get; set; }
    public string DiscountName { get; set; }
    public string GroupDiscountGuid { get; set; }
    public long? OrderNo { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
