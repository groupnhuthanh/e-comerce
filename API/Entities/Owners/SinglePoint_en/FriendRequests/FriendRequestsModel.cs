﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.FriendRequests;

public class FriendRequestsModel : BaseModel
{
    public string ReceiverId { get; set; }
    public List<Sender> Senders { get; set; }
    public string UserGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string LocationGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdatedAt { get; set; }
}

public partial class Sender
{
    public string SenderId { get; set; }
    public long? Timestamp { get; set; }
}
