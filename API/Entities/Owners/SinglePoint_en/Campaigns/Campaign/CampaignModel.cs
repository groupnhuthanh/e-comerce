﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Campaigns.Campaign;

public class CampaignModel : BaseModel
{

    public string Name { get; set; }
    public string CampaignCode { get; set; }
    public List<ListCampaignCode> ListCampaignCode { get; set; }
    public List<string> Location { get; set; }
    public bool? IsAllLocation { get; set; }
    public long? CampaignTypeId { get; set; }
    public bool? IsAutomatic { get; set; }
    public bool? IsSchedule { get; set; }
    public List<ListSchedule> ListSchedule { get; set; }
    public bool? IsDateRange { get; set; }
    public List<object> ListDateRange { get; set; }
    public string NickName { get; set; }
    public string ShortName { get; set; }
    public string Acronymn { get; set; }
    public string Description { get; set; }
    public string TermsConditions { get; set; }
    public string Color { get; set; }
    public List<long> DiningOption { get; set; }
    public long? CampaignCodeTypeId { get; set; }
    public List<object> CampaignCodeUsageLimits { get; set; }
    public List<UsageLimit> UsageLimits { get; set; }
    public List<object> Trigger { get; set; }
    public long? CampaignValue { get; set; }
    public bool? Excluding { get; set; }
    public long? CampaignApplyToId { get; set; }
    public bool? IsMaximumDiscountPerOrder { get; set; }
    public long? MaximumDiscountPerOrder { get; set; }
    public List<MinimumRequire> MinimumRequire { get; set; }
    public long? CustomerEligibillityId { get; set; }
    public bool? IsAllPayment { get; set; }
    public List<object> ListPayment { get; set; }
    public long? CombineId { get; set; }
    public List<string> ListDiscount { get; set; }
    public long? DisplayTypeId { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public ListItemApplyTo ListItemApplyTo { get; set; }
    public ListGroupApplyTo ListGroupApplyTo { get; set; }
    public ListCateApplyTo ListCateApplyTo { get; set; }
    public List<object> ListCustomer { get; set; }
    public List<object> ListGroupCustomer { get; set; }
}
public partial class ListCampaignCode
{
    public long? CampaignCodeId { get; set; }
    public long? Code { get; set; }
    public long? OrderNo { get; set; }
    public long? Status { get; set; }
}

public partial class ListCateApplyTo
{
    public long? IsMaxAmount { get; set; }
    public long? IsMaxQuantity { get; set; }
}

public partial class ListGroupApplyTo
{
    public long? IsMaxAmount { get; set; }
    public long? IsMaxQuantity { get; set; }
    public List<GroupApplyTo> GroupApplyTo { get; set; }
}

public partial class GroupApplyTo
{
    public long? MaxQuantity { get; set; }
    public long? MaxAmount { get; set; }
    public long? ApplyToModifier { get; set; }
    public string GroupGuid { get; set; }
    public string Name { get; set; }
    public List<ListProduct> ListProduct { get; set; }
}

public partial class ListProduct
{
    public string ProductGuid { get; set; }
    public string ProductName { get; set; }
}

public partial class ListItemApplyTo
{
}

public partial class ListSchedule
{
    public long? ScheduleId { get; set; }
    public string Day { get; set; }
    public List<Time> Time { get; set; }
    public long? Visible { get; set; }
}

public partial class Time
{
    public long? TimeId { get; set; }
    public string TimeOn { get; set; }
    public string TimeOff { get; set; }
    public long? OrderNo { get; set; }
}

public partial class MinimumRequire
{
    public long? MinimumRequireId { get; set; }
    public long? Amount { get; set; }
}

public partial class UsageLimit
{
    public long? UsageLimitsId { get; set; }
    public long? Limit { get; set; }
}

