﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.FCMToken;

public class FCMTokenModel : BaseModel
{
    public string Token { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string CustomerGuestsId { get; set; }
    public string Timestamp { get; set; }
    public long? IsActive { get; set; }
}
