﻿using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Owners.SinglePoint_en.Devices.Device;

[BsonIgnoreExtraElements]
public class DeviceModel : BaseModel
{
    public string Nickname { get; set; }
    public string Location { get; set; }
    public long? DeviceType { get; set; }
    public int? DevMode { get; set; }
    public bool? IsServer { get; set; }
    public int? ResolutionId { get; set; }
    public int? ShowReceiptScreen { get; set; }
    public int? PrintStandard { get; set; }
    public int? PrintKitchen { get; set; }
    public int? DisplayStyle { get; set; }
    public int? ExtraStyle { get; set; }
    public int? ExtraItemStyle { get; set; }
    public string Acronymn { get; set; }
    public string Color { get; set; }
    public PurchaseFormat PurchaseFormat { get; set; }
    public int? SecondaryScreenSettingsId { get; set; }
    public VfdMode VfdMode { get; set; }
    public DualDisplayMode DualDisplayMode { get; set; }
    public string AppCode { get; set; }
    public string HexCode { get; set; }

    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public long? DeviceId { get; set; }
    public string DeviceKey { get; set; }

    public string SecretKey { get; set; }
    public string InDate { get; set; }
    public long? Status { get; set; }
    public long? IsDeploy { get; set; }
}

public partial class DualDisplayMode
{
    public int? DualDisplayModeId { get; set; }
    public int? SecondaryScreenModeId { get; set; }
    public string Text1 { get; set; }
    public string Text2 { get; set; }
    public string Text3 { get; set; }
    public string Text4 { get; set; }
    public string Color1 { get; set; }
    public string Color2 { get; set; }
    public string Color3 { get; set; }
    public string Color4 { get; set; }
    public string Background { get; set; }
}

public partial class PurchaseFormat
{
    public long? PurchaseSettingTypeId { get; set; }
    public string Prefix { get; set; }
    public int? Number { get; set; }
    public int? MinimumNumber { get; set; }
    public string Preview { get; set; }
    public string NumberFormat { get; set; }
    public int? MaxChar { get; set; }
    public string Acronymn { get; set; }
    public string RandomPrefix { get; set; }
}

public partial class VfdMode
{
    public string Text1 { get; set; }
    public string Text2 { get; set; }
    public string Text3 { get; set; }
    public string Text4 { get; set; }
    public long? ComPort { get; set; }
    public bool? IsOrderTotal { get; set; }
    public bool? IsChange { get; set; }
    public bool? IsLoyaltyPointsEarned { get; set; }
    public bool? IsCustomerName { get; set; }
    public bool? IsThankYouMessage { get; set; }
}
