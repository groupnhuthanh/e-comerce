﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Devices.DeviceRequestHistory;

public class DeviceRequestHistoryModel : BaseModel
{
    public string Payload { get; set; }
    public string Verb { get; set; }
    public string BaseUrl { get; set; }
    public string Path { get; set; }
    public string Timestamp { get; set; }
    public string Nonce { get; set; }
    public string CustomerGuest_id { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string DeviceCode { get; set; }
    public string Checksum { get; set; }
    public long? CreateDate { get; set; }
}
