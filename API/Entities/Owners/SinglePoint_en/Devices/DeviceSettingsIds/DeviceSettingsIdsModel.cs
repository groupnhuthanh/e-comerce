﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Devices.DeviceSettingsIds;

public class DeviceSettingsIdsModel : BaseModel
{
    public long? SettingId { get; set; }
    public long? SettingTypeId { get; set; }
    public string Prefix { get; set; }
    public long? Number { get; set; }
    public long? MinimumNumber { get; set; }
    public string Preview { get; set; }
    public long? MaxChar { get; set; }
    public string LocationGuid { get; set; }
    public string Acronymn { get; set; }
    public string NumberFormat { get; set; }
    public string RandomPrefix { get; set; }
    public string DeviceGuid { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
