﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Devices.DeviceInfo;

public class DeviceInfoModel : BaseModel
{
    public string DeviceGuid { get; set; }
    public string UserGuid { get; set; }
    public string AppCode { get; set; }
    public string Name { get; set; }
    public string Ip { get; set; }
    public long? Version { get; set; }
    public string TimeZoneId { get; set; }
    public long? CountryId { get; set; }
    public string CreateDate { get; set; }
    public long? Status { get; set; }
    public string InDate { get; set; }
    public string OutDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
