﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Devices.DevicesViewItemMode;

public class DevicesViewItemModeModel : BaseModel
{
    public string LocationGuid { get; set; }
    public long? ViewMode { get; set; }
    public long? PictureMode { get; set; }
    public string DeviceGuid { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
