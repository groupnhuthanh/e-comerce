﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Devices.DeviceVideo;

public class DeviceVideoModel : BaseModel
{
    public string FileName { get; set; }
    public string Name { get; set; }
    public long? OrderNo { get; set; }
    public string Path { get; set; }
    public string Poster { get; set; }
    public string Time { get; set; }
    public string Url { get; set; }
    public long? Size { get; set; }
    public string Type { get; set; }
    public string UserGuid { get; set; }
    public string DeviceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
