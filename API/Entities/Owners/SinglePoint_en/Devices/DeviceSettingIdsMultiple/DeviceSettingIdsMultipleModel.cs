﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Devices.DeviceSettingIdsMultiple;

public class DeviceSettingIdsMultipleModel : BaseModel
{
    public long? Index { get; set; }
    public string Uuid { get; set; }
    public long? SettingTypeId { get; set; }
    public long? MinimumNumber { get; set; }
    public long? NumberIncrement { get; set; }
    public string NumberRandom { get; set; }
    public long? Prefixuuid { get; set; }
    public string Prefix { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string Acronymn { get; set; }
    public string NumberFormat { get; set; }
    public long? MaxChar { get; set; }
    public long? Number { get; set; }
}
