﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Devices.DevicesSettings;

public class DevicesSettingsModel : BaseModel
{
    public long? ThemeMode { get; set; }
    public string Layout { get; set; }
    public string FontSize { get; set; }
    public string Color { get; set; }
    public string TextColor { get; set; }
    public string Button { get; set; }
    public string DeviceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
