﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.TransactionExport;

public class TransactionExportModel : BaseModel
{
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public List<Filter> Filter { get; set; }
    public long? Status { get; set; }
    public long? Type { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string FileCsv { get; set; }
    public string FileXlsx { get; set; }
}

public partial class Filter
{
    public string Source { get; set; }
    public string PaymentMethod { get; set; }
    public string Location { get; set; }
    public long? Status { get; set; }
    public string DateRanger { get; set; }
    public string DinningOption { get; set; }
    public string RefundType { get; set; }
    public string CashDrawer { get; set; }
    public string UserGuid { get; set; }
}
