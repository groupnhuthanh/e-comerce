﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.FAQ.FAQsGroup;

public class FAQsGroupModel : BaseModel
{
    public string Title { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Handle { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public string UserGuid { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public List<string> ListFaQs { get; set; }
}
