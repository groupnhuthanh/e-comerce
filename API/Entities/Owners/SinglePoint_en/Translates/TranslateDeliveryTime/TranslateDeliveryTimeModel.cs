﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Translates.TranslateDeliveryTime;

public class TranslateDeliveryTimeModel : BaseModel
{
    public Date Date { get; set; }
    public Times Times { get; set; }
    public long? Duration { get; set; }
    public long? LanguageId { get; set; }
    public string Code { get; set; }
    public string UserGuid { get; set; }
    public string DeliveryTimeGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}

public partial class Date
{
    public string Date1 { get; set; }
    public string Date2 { get; set; }
    public string Date3 { get; set; }
    public string Date4 { get; set; }
}

public partial class Times
{
    public string Time1 { get; set; }
    public string Time2 { get; set; }
    public string Time3 { get; set; }
    public string Time4 { get; set; }
}
