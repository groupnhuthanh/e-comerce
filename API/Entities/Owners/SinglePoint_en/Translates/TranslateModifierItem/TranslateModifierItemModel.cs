﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Translates.TranslateModifierItem;

public class TranslateModifierItemModel : BaseModel
{
    public string Modifier { get; set; }
    public string Description { get; set; }
    public string Description2 { get; set; }
    public string Name2 { get; set; }
    public string Name3 { get; set; }
    public string Name4 { get; set; }
    public long? LanguageId { get; set; }
    public string ModifierItemGuid { get; set; }
    public string Code { get; set; }
    public string UserGuid { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
