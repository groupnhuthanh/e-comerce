﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Translates.TranslateFAQsGroup;

public class TranslateFAQsGroupModel : BaseModel
{
    public string FaQsGroupGuid { get; set; }
    public string Title { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public long? LanguageId { get; set; }
    public string Code { get; set; }
    public string UserGuid { get; set; }
    public string FaQsGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
