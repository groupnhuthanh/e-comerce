﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Translates.TranslateGroupDiscount;

public class TranslateGroupDiscountModel : BaseModel
{
    public string GroupDiscountGuid { get; set; }
    public string GroupName { get; set; }
    public string ShortName { get; set; }
    public string PrinterName { get; set; }
    public string Description { get; set; }
    public long? LanguageId { get; set; }
    public string Code { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
