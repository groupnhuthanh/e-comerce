﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Translates.TranslateOrderStatusDetail;

public class TranslateOrderStatusDetailModel : BaseModel
{
    public long? DiningOptionId { get; set; }
    public long? OrderStatusId { get; set; }
    public string Title { get; set; }
    public long? LanguageId { get; set; }
    public List<Button> Button { get; set; }
    public List<Notification> Notification { get; set; }
    public string UserGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class Button
{
    public long? TypeId { get; set; }
    public string Title { get; set; }
}

public partial class Notification
{
    public long? TypeId { get; set; }
    public string Title { get; set; }
    public string Body { get; set; }
    public string NotificationTypeGuid { get; set; }
}
