﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Translates.TranslatePolicies;

public class TranslatePoliciesModel : BaseModel
{
    public long? PolicieId { get; set; }
    public string Name { get; set; }
    public string Title { get; set; }
    public string Body { get; set; }
    public long? LanguageId { get; set; }
    public string Code { get; set; }
    public string UserGuid { get; set; }
    public string Url { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
