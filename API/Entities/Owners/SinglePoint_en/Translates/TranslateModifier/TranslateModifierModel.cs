﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Translates.TranslateModifier;

public class TranslateModifierModel : BaseModel
{
    public string Name { get; set; }
    public string ReferenceId1 { get; set; }
    public string ReferenceId2 { get; set; }
    public string DisplayName { get; set; }
    public long? LanguageId { get; set; }
    public string ModifierGuid { get; set; }
    public string Code { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
