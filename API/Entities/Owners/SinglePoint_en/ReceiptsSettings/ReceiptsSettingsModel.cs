﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.ReceiptsSettings;

public class ReceiptsSettingsModel : BaseModel
{
    public string LocationGuid { get; set; }
    public string Color { get; set; }
    public long? ShowItemDescription { get; set; }
    public long? ShowLocation { get; set; }
    public string Facebook { get; set; }
    public string Instagram { get; set; }
    public string Twitter { get; set; }
    public string Website { get; set; }
    public string ReturnPolicy { get; set; }
    public string CustomText { get; set; }
    public long? ShowReferralBanner { get; set; }
    public string Phone { get; set; }
    public string AddressInReceipt { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
