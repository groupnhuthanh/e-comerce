﻿using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Owners.SinglePoint_en.PaymentType;

[BsonIgnoreExtraElements]
public class PaymentTypeModel : BaseModel
{
    public int? PaymentTypeId { get; set; }
    public string Title_en { get; set; }
    public string Title_vi { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public string Css { get; set; }
    public long? Default { get; set; }
    public string UserGuid { get; set; }

    public string ApiTypeGuid { get; set; }
    public long? ApplyToId { get; set; }
    public int? CashTypeId { get; set; }
    public string Color { get; set; }
    public string Description { get; set; }
    public long? GroupType { get; set; }
    public string PaymentGroup { get; set; }
    public string PaymentListType { get; set; }
    public string Sub { get; set; }
    public string Url { get; set; }
}
