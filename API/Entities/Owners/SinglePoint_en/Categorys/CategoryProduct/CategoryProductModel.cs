﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Categorys.CategoryProduct;

public class CategoryProductModel : BaseModel
{

    public string CategoryGuid { get; set; }
    public string ProductGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
