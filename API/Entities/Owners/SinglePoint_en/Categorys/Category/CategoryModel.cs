﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Categorys.Category;

public class CategoryModel : BaseModel
{

    public string Title { get; set; }
    public string Description { get; set; }
    public long? SubCategory { get; set; }
    public long? ParentCategoryId { get; set; }
    public long? Level { get; set; }
    public string Color { get; set; }
    public string Acronymn { get; set; }
    public string ReferenceId { get; set; }
    public string Version { get; set; }
    public string Handle { get; set; }
    public string Ansi { get; set; }
    public string SeoTitle { get; set; }
    public string SeoDescription { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public string Guid { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? CategoryId { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
