﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Payoo.PayooPrepaidDetail;

public class PayooPrepaidDetailModel : BaseModel
{
    public long? WalletMethodId { get; set; }
    public long? Receivable { get; set; }
    public long? Payable { get; set; }
    public string Note { get; set; }
    public string TransactionCode { get; set; }
    public string PrepaidId { get; set; }
    public string CustomerGuid { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public object TransactionNo { get; set; }
    public long? TransactionType { get; set; }
    public object TransactionDate { get; set; }
    public long? OrderStatusId { get; set; }
    public long? PaymentStatusId { get; set; }
}
