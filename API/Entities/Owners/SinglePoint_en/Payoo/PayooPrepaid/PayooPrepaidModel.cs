﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Payoo.PayooPrepaid;

public class PayooPrepaidModel : BaseModel
{
    public string UserGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string LocationGuid { get; set; }
    public string CustomerGuid { get; set; }
    public string AccountNo { get; set; }
    public long? Receivable { get; set; }
    public long? Payable { get; set; }
    public long? Balance { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? LastDate { get; set; }
    public long? Visible { get; set; }
    public long? Locker { get; set; }
    public DateTimeOffset? LockerDate { get; set; }
    public DateTimeOffset? ActiveDate { get; set; }
    public long? PassCode { get; set; }
    public long? Active { get; set; }
    public string PrepaidPayooCode { get; set; }
}
