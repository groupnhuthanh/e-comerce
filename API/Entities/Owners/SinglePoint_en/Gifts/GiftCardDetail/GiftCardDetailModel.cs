﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Gifts.GiftCardDetail;

public class GiftCardDetailModel : BaseModel
{
    public string TransactionCode { get; set; }
    public string OrderGuid { get; set; }
    public Able Receivable { get; set; }
    public Able Payable { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string GiftCardGuid { get; set; }
    public string WalletMethodId { get; set; }
    public long? PaymentMethodType { get; set; }
    public long? ApplyToId { get; set; }
    public long? Visible { get; set; }
    public string CustomerGuid { get; set; }
    public string UserGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public long? Source { get; set; }
}

public partial class Able
{
    public string T { get; set; }
    public long? V { get; set; }
}
