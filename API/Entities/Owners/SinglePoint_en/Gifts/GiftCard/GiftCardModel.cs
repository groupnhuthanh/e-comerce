﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Gifts.GiftCard;

public class GiftCardModel : BaseModel
{
    public string UserGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public long? GiftCardTypeId { get; set; }
    public DateTimeOffset? ExpDate { get; set; }
    public long? Receivable { get; set; }
    public long? Payable { get; set; }
    public long? Balance { get; set; }
    public long? Discount { get; set; }
    public long? LinePrice { get; set; }
    public long? Visible { get; set; }
    public string Location { get; set; }
    public string CardNumber { get; set; }
    public long? IsAllLocation { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string QrCode { get; set; }
    public DateTimeOffset? LastUpdate { get; set; }
    public DateTimeOffset? LastDate { get; set; }
}
