﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Suppliers.Supplier;

public class SupplierModel : BaseModel
{
    public string CompanyName { get; set; }
    public string Acronymn { get; set; }
    public long? CountryId { get; set; }
    public string CountryName { get; set; }
    public long? CityId { get; set; }
    public string CityName { get; set; }
    public long? DistrictId { get; set; }
    public long? DistrictName { get; set; }
    public string DistrictLevel { get; set; }
    public long? WardId { get; set; }
    public string WardName { get; set; }
    public string WardLevel { get; set; }
    public long? AddressTypeId { get; set; }
    public string AddressName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Zip { get; set; }
    public string TaxId { get; set; }
    public string PaymentTerm { get; set; }
    public string Phone { get; set; }
    public long? DialingCode { get; set; }
    public string Email { get; set; }
    public string ReferenceId { get; set; }
    public string ReferenceId2 { get; set; }
    public string Note { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Handle { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
