﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Suppliers.SupplierCatalog;

public class SupplierCatalogModel : BaseModel
{
    public string SupplierGuid { get; set; }
    public string VendorCode { get; set; }
    public long? Visible { get; set; }
    public long? ProductTypeId { get; set; }
    public long? ProductKey { get; set; }
    public string ProductId { get; set; }
    public string ProductName { get; set; }
    public long? UnitId { get; set; }
    public string InnerPackage { get; set; }
    public string MasterCase { get; set; }
    public string Pallet { get; set; }
    public long? LeadTime { get; set; }
    public MinimumOrderQuantity MinimumOrderQuantity { get; set; }
    public List<object> UnitPrice { get; set; }
    public string ProductHandle { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class MinimumOrderQuantity
{
    public long? MoqId { get; set; }
    public long? Quantity { get; set; }
}
