﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Hardware;

public class HardwareModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public NfcReader NfcReader { get; set; }
    public NfcReader PosBank { get; set; }
    public Printer Printer { get; set; }
    public NfcReader Scanner { get; set; }
    public string UserGuid { get; set; }
}

public partial class NfcReader
{
}

public partial class Printer
{
    public List<DeliveryList> DeliveryList { get; set; }
    public List<ConnectList> ConnectList { get; set; }
}

public partial class ConnectList
{
    public long? PrinterId { get; set; }
    public string PrinterName { get; set; }
    public long? PrinterType { get; set; }
    public long? PrinterPort { get; set; }
    public bool? ConnectToCashDrawer { get; set; }
    public List<PrinterReceipt> PrinterReceipt { get; set; }
    public bool? Enable { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
}

public partial class PrinterReceipt
{
    public long? ReceiptType { get; set; }
    public long? ReceiptQuantity { get; set; }
}

public partial class DeliveryList
{
    public long? PrinterId { get; set; }
    public long? ReceiptType { get; set; }
    public long? ReceiptQuantity { get; set; }
}
