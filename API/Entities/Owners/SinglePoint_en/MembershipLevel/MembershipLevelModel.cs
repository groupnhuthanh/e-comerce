﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.MembershipLevel;

public class MembershipLevelModel : BaseModel
{
    public List<Level> Level { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class Level
{
    public long? LevelId { get; set; }
    public string LevelName { get; set; }
    public long? MembershipTypeId { get; set; }
    public long? From { get; set; }
    public long? To { get; set; }
    public string Image { get; set; }
    public string Icon { get; set; }
    public long? Visible { get; set; }
    public bool? IsMemberDiscount { get; set; }
    public List<MemberDiscount> MemberDiscount { get; set; }
}

public partial class MemberDiscount
{
    public string Id { get; set; }
    public string DiscountName { get; set; }
    public long? Key { get; set; }
}
