﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Marketing.MarketingPoll;

public class MarketingPollModel : BaseModel
{
    public string Name { get; set; }
    public string Title { get; set; }
    public bool? IsAnonymous { get; set; }
    public string Description { get; set; }
    public long? RecipientId { get; set; }
    public List<object> ListCustomer { get; set; }
    public List<object> ListGroupCustomer { get; set; }
    public List<DateRange> DateRange { get; set; }
    public List<Option> Options { get; set; }
    public bool? IsMultipleOptions { get; set; }
    public long? MultipleOption { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public List<object> Media { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class DateRange
{
    public long? DateRangeId { get; set; }
    public string Date { get; set; }
    public string Time { get; set; }
    public long? Visible { get; set; }
}

public partial class Option
{
    public long? OptionId { get; set; }
    public string OptionOption { get; set; }
    public string Value { get; set; }
}
