﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Marketing.MarketingMessage;

public class MarketingMessageModel : BaseModel
{
    public string Name { get; set; }
    public long? SendingTypeId { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public long? RecipientId { get; set; }
    public long? PublishingId { get; set; }
    public DateTimeOffset? StartDate { get; set; }
    public string StartTime { get; set; }
    public List<object> ListCustomer { get; set; }
    public List<object> ListGroupCustomer { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
