﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Settings.SettingsDeliveryRole;

public class SettingsDeliveryRoleModel : BaseModel
{
    public string Name { get; set; }
    public long? DepartmentId { get; set; }
    public string EmployeeList { get; set; }
    public long? ClockInTypeId { get; set; }
    public long? DeliveryLocation { get; set; }
    public long? PosConfirmation { get; set; }
    public long? DriverAllocation { get; set; }
    public string UserGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
