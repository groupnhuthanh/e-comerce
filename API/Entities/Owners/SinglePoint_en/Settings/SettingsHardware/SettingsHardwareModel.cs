﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Settings.SettingsHardware;

public class SettingsHardwareModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string UserGuid { get; set; }
    public List<PrinterList> PrinterList { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
}

public partial class PrinterList
{
    public string Id { get; set; }
    public string Name { get; set; }
    public long? PrinterTypeId { get; set; }
    public bool? IsConnectCashDrawer { get; set; }
    public List<ConnectionList> ConnectionList { get; set; }
    public List<PaperSizeList> PaperSizeList { get; set; }
    public List<ReceiptList> ReceiptList { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public bool? Enable { get; set; }
    public bool? IsPrintVietnamese { get; set; }
}

public partial class ConnectionList
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Port { get; set; }
    public long? ConnectionTypeId { get; set; }
    public bool? IsChecked { get; set; }
}

public partial class PaperSizeList
{
    public string Id { get; set; }
    public string Name { get; set; }
    public long? Size { get; set; }
    public bool? IsChecked { get; set; }
}

public partial class ReceiptList
{
    public Id? Id { get; set; }
    public string Name { get; set; }
    public long? Quantity { get; set; }
    public bool? IsChecked { get; set; }
    public bool? IsCustom { get; set; }
}

public enum Id { Receipt1, Receipt2, Receipt3 };
