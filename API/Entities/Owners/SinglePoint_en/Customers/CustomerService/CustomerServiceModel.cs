﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerService;

public class CustomerServiceModel : BaseModel
{
    public string Name { get; set; }
    public long? ServiceValue { get; set; }
    public bool? AssignTo { get; set; }
    public bool? EnableService { get; set; }
    public long? ServiceId { get; set; }
    public bool? AvailableFutureLocations { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
