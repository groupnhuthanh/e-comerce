﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyDetail;

public class CustomerGuestLoyaltyDetailModel : BaseModel
{
    public long? Source { get; set; }
    public DateTimeOffset? LastDate { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public bool? IsNew { get; set; }
    public string CustomerGuid { get; set; }
    public string TransactionCode { get; set; }
    public string OrderGuid { get; set; }
    public long? Receivable { get; set; }
    public long? Payable { get; set; }
    public string CustomerLoyaltyGuid { get; set; }
    public long? EarnId { get; set; }
    public string LocationGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string DeviceGuid { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public long? OrderStatusId { get; set; }
    public long? PaymentStatusId { get; set; }
}
