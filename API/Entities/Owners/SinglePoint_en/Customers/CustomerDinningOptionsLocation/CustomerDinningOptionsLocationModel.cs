﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerDinningOptionsLocation;

public class CustomerDinningOptionsLocationModel : BaseModel
{

    public string LocationGuid { get; set; }
    public long? DiningOptionId { get; set; }
    public long? OrderNo { get; set; }
    public string UserGuid { get; set; }
    public string CreateDate { get; set; }
    public long? Visible { get; set; }
    public string SubDiningOption { get; set; }
    public long? GroupId { get; set; }
    public long? PriceId { get; set; }
    public string AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
