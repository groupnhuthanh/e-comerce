﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerSurcharge;

public class CustomerSurchargeModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string Name { get; set; }
    public long? SurchargeValue { get; set; }
    public bool? AssignTo { get; set; }
    public bool? EnableSurcharge { get; set; }
    public long? SurchargeId { get; set; }
    public bool? AvailableFutureLocations { get; set; }
    public string UserGuid { get; set; }
    public string CreateDate { get; set; }
}
