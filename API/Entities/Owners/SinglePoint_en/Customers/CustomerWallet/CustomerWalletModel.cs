﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerWallet;

public class CustomerWalletModel : BaseModel
{
    public string Rev { get; set; }
    public long? Key { get; set; }
    public string UserGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string LocationGuid { get; set; }
    public string CustomerGuid { get; set; }
    public long? Receivable { get; set; }
    public long? Payable { get; set; }
    public long? Balance { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? LastDate { get; set; }
    public long? Visible { get; set; }
    public long? Locker { get; set; }
    public string LockerDate { get; set; }
    public DateTimeOffset? ActiveDate { get; set; }
    public long? PassCode { get; set; }
    public long? Active { get; set; }
}
