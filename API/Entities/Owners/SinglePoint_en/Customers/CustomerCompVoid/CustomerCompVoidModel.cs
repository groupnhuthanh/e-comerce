﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerCompVoid;

public class CustomerCompVoidModel : BaseModel
{
    public string WelcomeId { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string CreateDate { get; set; }
    public long? GroupId { get; set; }
    public long? Id { get; set; }
    public string TitleEn { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
}
