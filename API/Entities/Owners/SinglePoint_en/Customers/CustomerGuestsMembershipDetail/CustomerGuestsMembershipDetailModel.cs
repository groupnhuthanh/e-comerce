﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestsMembershipDetail;

public class CustomerGuestsMembershipDetailModel : BaseModel
{
    public long? Payable { get; set; }
    public string Receivable { get; set; }
    public string CurrentBalance { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Timestamp { get; set; }
    public long? Visible { get; set; }
    public Reason Reason { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string CustomerGuestsId { get; set; }
    public string OrderId { get; set; }
    public bool? IsNew { get; set; }
    public object OrderStatusId { get; set; }
    public object PaymentStatusId { get; set; }
    public object UpdateDate { get; set; }
}
public partial class Reason
{
    public long? ReasonTypeId { get; set; }
    public string ReasonNotes { get; set; }
    public long? SourceType { get; set; }
    public string SourceId { get; set; }
}
