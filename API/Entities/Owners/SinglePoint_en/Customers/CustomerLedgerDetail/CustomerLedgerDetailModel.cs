﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerLedgerDetail;

public class CustomerLedgerDetailModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string CashDrawerGuid { get; set; }
    public long? Change { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string CustomerGuid { get; set; }
    public string CustomerLedgerGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string LocationGuid { get; set; }
    public string OrderGuid { get; set; }
    public long? OverPay { get; set; }
    public long? Payable { get; set; }
    public double? Receivable { get; set; }
    public long? TransactionSource { get; set; }
    public string TransactionCode { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public string PaymentTypeGuid { get; set; }
    public long? TransactionType { get; set; }
    public string CustomerWalletGuid { get; set; }
    public string CustomerWalletDetailGuid { get; set; }
    public string Note { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}
