﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestsPaymentsBillings;

public class CustomerGuestsPaymentsBillingsModel : BaseModel
{
    public object LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string DialingCode { get; set; }
    public string FullNameAnsi { get; set; }
    public string NameAcronymn { get; set; }
    public bool? Default { get; set; }
    public string Name { get; set; }
    public long? CountryId { get; set; }
    public object AddressTypeName { get; set; }
    public string WardName { get; set; }
    public long? DistrictName { get; set; }
    public string CityName { get; set; }
    public long? Visible { get; set; }
    public string FirstName { get; set; }
    public long? LastName { get; set; }
    public string FullName { get; set; }
    public string Phone { get; set; }
    public object E164Format { get; set; }
    public object Email { get; set; }
    public string Company { get; set; }
    public object Gender { get; set; }
    public long? AddressTypeId { get; set; }
    public object AddressName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public long? WardId { get; set; }
    public long? DistrictId { get; set; }
    public long? CityId { get; set; }
    public object IsUseShipping { get; set; }
    public string Note { get; set; }
    public string DeviceGuid { get; set; }
    public string LocationGuid { get; set; }
    public string UserGuid { get; set; }
    public string CustomerGuestGuid { get; set; }
}
