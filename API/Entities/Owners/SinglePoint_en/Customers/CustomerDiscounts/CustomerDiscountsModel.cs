﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerDiscounts;

public class CustomerDiscountsModel : BaseModel
{

    public long? SpecificPrice { get; set; }
    public long? IsDiscountLimit { get; set; }
    public long? DiscountValue { get; set; }
    public long? MaximumDiscount { get; set; }
    public long? IsAllPaymentMethod { get; set; }
    public string PaymentMethodList { get; set; }
    public string Trigger { get; set; }
    public long? ApplyToPriceProduct { get; set; }
    public string DiscountName { get; set; }
    public long? DiscountType { get; set; }
    public string Location { get; set; }
    public bool? DiscountAutomatic { get; set; }
    public long? DiscountCodeType { get; set; }
    public string DiscountCode { get; set; }
    public string Color { get; set; }
    public string Acronymn { get; set; }
    public long? OnlyApplyDiscountOncePerOrder { get; set; }
    public long? OnlyApplyDiscountApplyTo { get; set; }
    public long? NumberOnlyApplyDiscountApplyTo { get; set; }
    public long? MinimumRequiredType { get; set; }
    public string MinimumRequired { get; set; }
    public long? CustomerEligibility { get; set; }
    public string ListCustomerEligibility { get; set; }
    public string UsageLimits { get; set; }
    public long? SetSchedules { get; set; }
    public long? DateRange { get; set; }
    public string ListSetSchedules { get; set; }
    public string ListDateRange { get; set; }
    public long? Excluding { get; set; }
    public string DinningOption { get; set; }
    public long? IsVisiblePos { get; set; }
    public string Description { get; set; }
    public string TermsConditions { get; set; }
    public string ShortName { get; set; }
    public string NickName { get; set; }
    public string ReferenceId { get; set; }
    public long? IsBannerDefault { get; set; }
    public long? DisplayType { get; set; }
    public string DiscountCodeUsageLimits { get; set; }
    public bool? IsLoyaltyReward { get; set; }
    public long? Price { get; set; }
    public long? ApplyTo { get; set; }
    public long? MemberLevelId { get; set; }
    public bool? IsGift { get; set; }
    public long? AssignTo { get; set; }
    public List<ListUseA> ListUseAs { get; set; }
    public List<long> ListVisibility { get; set; }
    public List<ListDiningOption> ListDiningOption { get; set; }
    public bool? IsPromotionRange { get; set; }
    public List<object> ListPromotionRange { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public string Handle { get; set; }
    public string UserGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
public partial class ListDiningOption
{
    public long? DiningOptionId { get; set; }
    public string DiningOption { get; set; }
    public List<SubDiningOption> SubDiningOption { get; set; }
}

public partial class SubDiningOption
{
    public long? SubId { get; set; }
    public string SubTitle { get; set; }
}

public partial class ListUseA
{
    public long? UseAsId { get; set; }
    public long? Value { get; set; }
    public string Handle { get; set; }
}
