﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestsDiningOption;

public class CustomerGuestsDiningOptionModel : BaseModel
{
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public string CustomerGuid { get; set; }
    public string UserGuid { get; set; }
    public List<DiningOptionList> DiningOptionList { get; set; }
}
public partial class DiningOptionList
{
    public long? DiningOptionId { get; set; }
    public string StoreId { get; set; }
    public bool? IsSelected { get; set; }
}
