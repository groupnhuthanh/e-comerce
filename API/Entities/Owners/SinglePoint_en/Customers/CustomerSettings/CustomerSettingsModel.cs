﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerSettings;

public class CustomerSettingsModel : BaseModel
{
    public PersonalInfo PersonalInfo { get; set; }
    public BillingAddress BillingAddress { get; set; }
    public List<Marketing> Marketing { get; set; }
    public Login Login { get; set; }
    public CartSettings CartSettings { get; set; }
    public Captcha Captcha { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public PrepaidProvider PrepaidProvider { get; set; }
}

public partial class BillingAddress
{
    public bool? Enable { get; set; }
    public List<ListBillingAddress> ListBillingAddress { get; set; }
}

public partial class ListBillingAddress
{
    public long? BillingId { get; set; }
    public string Title { get; set; }
    public long? Visible { get; set; }
    public long? RadioTypeId { get; set; }
    public long? ContactId { get; set; }
    public long? CustomerId { get; set; }
}

public partial class Captcha
{
    public bool? Enable { get; set; }
    public List<Body> Body { get; set; }
}

public partial class Body
{
    public long? CaptchaId { get; set; }
    public string Title { get; set; }
    public long? Visible { get; set; }
    public string Description { get; set; }
}

public partial class CartSettings
{
    public List<RequireVerification> RequireVerification { get; set; }
    public List<CartAllow> CartAllow { get; set; }
}

public partial class CartAllow
{
    public long? CartAllowId { get; set; }
    public string Title { get; set; }
    public long? Visible { get; set; }
    public long? Default { get; set; }
    public bool? Enable { get; set; }
}

public partial class RequireVerification
{
    public long? RequireId { get; set; }
    public string Title { get; set; }
    public long? Visible { get; set; }
    public bool? Enable { get; set; }
    public long? AccountId { get; set; }
    public long? TypeId { get; set; }
    public long? SubId { get; set; }
    public long? AllowEditCustomerId { get; set; }
}

public partial class Login
{
    public List<RequireVerification> LoginAccount { get; set; }
    public LoginType LoginType { get; set; }
    public List<EnableLogin> EnableLogin { get; set; }
}

public partial class EnableLogin
{
    public string Title { get; set; }
    public bool? Enable { get; set; }
}

public partial class LoginType
{
    public bool? Enable { get; set; }
    public List<RequireVerification> ListType { get; set; }
}

public partial class Marketing
{
    public long? MarketingId { get; set; }
    public string Title { get; set; }
    public long? Visible { get; set; }
    public bool? Enable { get; set; }
    public List<RequireVerification> SubMarketing { get; set; }
}

public partial class PersonalInfo
{
    public List<ListBillingAddress> Customer { get; set; }
    public List<RequireVerification> AllowEditCustomer { get; set; }
    public List<ListBillingAddress> Contact { get; set; }
    public List<AllowContact> AllowContact { get; set; }
}

public partial class AllowContact
{
    public long? AllowContactId { get; set; }
    public string Title { get; set; }
    public long? Visible { get; set; }
    public bool? Enable { get; set; }
    public List<SubAllowContact> SubAllowContact { get; set; }
}

public partial class SubAllowContact
{
    public long? SubId { get; set; }
    public string Title { get; set; }
    public long? Visible { get; set; }
    public long? Value { get; set; }
    public string Unit { get; set; }
}

public partial class PrepaidProvider
{
    public long? PrepaidProviderId { get; set; }
    public bool? IsAllowPrepaid { get; set; }
    public bool? IsAllowTopup { get; set; }
}
