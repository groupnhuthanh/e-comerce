﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestsMembership;

public class CustomerGuestsMembershipModel : BaseModel
{
    public string CustomerGuestsId { get; set; }
    public long? CurrentLevelId { get; set; }
    public DateTimeOffset? StartDateMembershipLevel { get; set; }
    public DateTimeOffset? EndDateMembershipLevel { get; set; }
    public string Receivable { get; set; }
    public string Balance { get; set; }
    public long? NextLevel { get; set; }
    public DateTimeOffset? StartDateEarningTimer { get; set; }
    public DateTimeOffset? EndDateEarningTimer { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Timestamp { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public string UserGuid { get; set; }
    public long? MembershipTypeId { get; set; }
    public long? Payable { get; set; }
    public string MemberShipLevelId { get; set; }
    public long? TimestampUpdate { get; set; }
    public bool? IsNewUpdateBalance { get; set; }
}
