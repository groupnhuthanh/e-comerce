﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerWalletDetail;

public class CustomerWalletDetailModel : BaseModel
{
    public string TransactionCode { get; set; }
    public string OrderGuid { get; set; }
    public Able Receivable { get; set; }
    public Able Payable { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string CustomerWalletGuid { get; set; }
    public string WalletMethodId { get; set; }
    public long? Visible { get; set; }
    public string CustomerGuid { get; set; }
    public string UserGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public long? Source { get; set; }
}

public partial class Able
{
    public string T { get; set; }
    public string V { get; set; }
}
