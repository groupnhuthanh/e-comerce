﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerServiceLocation;

public class CustomerServiceLocationModel : BaseModel
{
    public string LocationGuid { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public string ServiceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
