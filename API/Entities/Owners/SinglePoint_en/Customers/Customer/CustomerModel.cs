﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.Customer;

public class CustomerModel : BaseModel
{

    public long? Key { get; set; }
    public string Rev { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public long? Country { get; set; }
    public string Websites { get; set; }
    public string CustomerCode { get; set; }
    public long? CustomerStatusId { get; set; }
    public string CreateDate { get; set; }
    public string EmpGuid { get; set; }
    public long? Visible { get; set; }
    public long? CustomerId { get; set; }
    public string TimeZoneId { get; set; }
    public string TimeZoneDisplayName { get; set; }
    public string Point { get; set; }
    public string StartedDate { get; set; }
    public string Phone { get; set; }
    public string CurrencySymbol { get; set; }
    public string PassCode { get; set; }
    public string CurrencyNativeName { get; set; }
    public string CurrencyId { get; set; }
    public long? BussinessTypeId { get; set; }
    public string LastName { get; set; }
    public string Url { get; set; }
    public string ReferenceId { get; set; }
    public string FirstName { get; set; }
    public string BusinessName { get; set; }
    public string InterestGrade { get; set; }
}
