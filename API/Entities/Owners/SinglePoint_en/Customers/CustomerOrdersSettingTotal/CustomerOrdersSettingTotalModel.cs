﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerOrdersSettingTotal;

public class CustomerOrdersSettingTotalModel : BaseModel
{
    public long? Id { get; set; }
    public string TitleVi { get; set; }
    public string TitleEn { get; set; }
    public long? AssignTo { get; set; }
    public string Location { get; set; }
    public long? AvailableFutureLocations { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? GroupId { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
