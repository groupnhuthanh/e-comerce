﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerLanguages;

public class CustomerLanguagesModel : BaseModel
{
    public string CustomerGuid { get; set; }
    public string Flag { get; set; }
    public long? NumericCode { get; set; }
    public string Code { get; set; }
    public long? LanguageId { get; set; }
    public string Language { get; set; }
    public string RegionDisplayName { get; set; }
    public bool? IsMain { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
