﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyaltyPending;

public class CustomerGuestLoyaltyPendingModel : BaseModel
{
    public string CustomerGuid { get; set; }
    public string UserGuid { get; set; }
    public string OrderGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public object EmployeeGuid { get; set; }
    public long? EarnId { get; set; }
    public long? Receivable { get; set; }
    public long? Payable { get; set; }
    public string SpentAmount { get; set; }
    public long? Point { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Status { get; set; }
    public bool? Visible { get; set; }
    public long? TotalCheck { get; set; }
}
