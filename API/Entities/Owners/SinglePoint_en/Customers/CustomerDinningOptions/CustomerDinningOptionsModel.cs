﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerDinningOptions;

public class CustomerDinningOptionsModel : BaseModel
{

    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? DiningOptionId { get; set; }
    public string TitleEn { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? DiningOptionTypeId { get; set; }
    public object Description1 { get; set; }
    public object Description2 { get; set; }
    public object Acronymn { get; set; }
    public object SubDiningOption { get; set; }
    public object UrlSelected { get; set; }
    public object UrlNormal { get; set; }
}
