﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestsGroup;

public class CustomerGuestsGroupModel : BaseModel
{

    public string GroupName { get; set; }
    public string Description { get; set; }
    public long? IsDefault { get; set; }
    public long? CustomerGroupType { get; set; }
    public long? Visible { get; set; }
    public string Handle { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
