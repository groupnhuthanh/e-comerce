﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGroupType;

public class CustomerGroupTypeModel : BaseModel
{
    public string WelcomeId { get; set; }
    public string TitleEn { get; set; }
    public string TitleVi { get; set; }
    public long? Visible { get; set; }
    public long? Default { get; set; }
    public long? Id { get; set; }
    public long? OrderNo { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
