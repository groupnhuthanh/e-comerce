﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerTax;

public class CustomerTaxModel : BaseModel
{
    public string Name { get; set; }
    public long? TaxValue { get; set; }
    public bool? AssignTo { get; set; }
    public long? TaxId { get; set; }
    public bool? EnableTax { get; set; }
    public bool? AvailableFutureLocations { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
