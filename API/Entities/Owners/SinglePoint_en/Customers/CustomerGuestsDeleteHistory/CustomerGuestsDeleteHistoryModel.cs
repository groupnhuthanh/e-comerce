﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestsDeleteHistory;

public class CustomerGuestsDeleteHistoryModel : BaseModel
{

    public string UserId { get; set; }
    public string LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public string CustomerGuestGuid { get; set; }
    public long? ReasonId { get; set; }
    public string ReasonNote { get; set; }
}
