﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestsLoginHistory;

public class CustomerGuestsLoginHistoryModel : BaseModel
{
    public object LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string FirebaseToken { get; set; }
    public string CustomerGuestGuid { get; set; }
    public string Phone { get; set; }
    public string FullName { get; set; }
    public Uri Url { get; set; }
    public string Email { get; set; }
    public bool? EmailVerified { get; set; }
    public bool? PhoneVerified { get; set; }
    public string GrantType { get; set; }
    public DateTimeOffset? LastLoginDate { get; set; }
    public string UserId { get; set; }
    public bool? IsAccountExist { get; set; }
    public object AccessToken { get; set; }
}
