﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuestLoyalty;

public class CustomerGuestLoyaltyModel : BaseModel
{
    public long? Source { get; set; }
    public DateTimeOffset? LastDate { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public bool? IsNew { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string CustomerGuid { get; set; }
    public long? Receivable { get; set; }
    public long? Payable { get; set; }
    public long? Balance { get; set; }
    public object ActiveDate { get; set; }
    public object Active { get; set; }
    public long? Locker { get; set; }
    public DateTimeOffset? LockerDate { get; set; }
    public string LocationGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string DeviceGuid { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
}
