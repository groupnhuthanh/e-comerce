﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerTaxLocation;

public class CustomerTaxLocationModel : BaseModel
{
    public string LocationGuid { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public string TaxGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
