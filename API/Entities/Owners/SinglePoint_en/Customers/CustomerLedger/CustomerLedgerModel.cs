﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerLedger;

public class CustomerLedgerModel : BaseModel
{
    public DateTimeOffset? CreateDate { get; set; }
    public long? Payable { get; set; }
    public long? Receivable { get; set; }
    public long? Balance { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}
