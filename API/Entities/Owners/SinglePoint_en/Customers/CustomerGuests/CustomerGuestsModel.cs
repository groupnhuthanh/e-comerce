﻿using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Owners.SinglePoint_en.Customers.CustomerGuests;

[BsonNoId]
[BsonIgnoreExtraElements]
public class CustomerGuestsModel : BaseModel
{

    public object LocationId { get; set; }
    public string DialingCode { get; set; }
    public bool? IsSignIn { get; set; }
    public string LastLoginDate { get; set; }
    public string Token { get; set; }
    public List<string> TokenList { get; set; }
    public string Password { get; set; }
    public object LastUpdateDate { get; set; }
    public object UnlinkDate { get; set; }
    public string UserName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string E164Format { get; set; }
    public long? Gender { get; set; }
    public string Company { get; set; }
    public string EmailVerifyDate { get; set; }
    public string Avatar { get; set; }
    public string GrantType { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string Iss { get; set; }
    public string Aud { get; set; }
    public string Firebase { get; set; }
    public bool? IsVerifyPhone { get; set; }
    public bool? IsVerifyEmail { get; set; }
    public object Sub { get; set; }
    public object SubGoogle { get; set; }
    public object IsLinkedGoogle { get; set; }
    public object SubFacebook { get; set; }
    public object IsLinkedFacebook { get; set; }
    public object SubApple { get; set; }
    public object IsLinkedApple { get; set; }
    public object SubTwitter { get; set; }
    public object IsLinkedTwitter { get; set; }
    public object SubGithub { get; set; }
    public object IsLinkedGithub { get; set; }
    public object SubMicrosoft { get; set; }
    public object IsLinkedMicrosoft { get; set; }
    public object SubEmail { get; set; }
    public object IsLinkedEmail { get; set; }
    public string SubPhone { get; set; }
    public bool? IsLinkedPhone { get; set; }
    public object UnlinkCustomerGuestId { get; set; }
    public object CustomerGuestGuid { get; set; }
    public object SecurityCode { get; set; }
    public long? ExpiredCode { get; set; }
    public DateTime? ExpiredDate { get; set; }
    public object SecurityCodeMask { get; set; }
    public bool? EmailVerified { get; set; }
    public bool? PhoneVerified { get; set; }
    public object Birthday { get; set; }
    public object Address1 { get; set; }
    public object EmployeeGuid { get; set; }
    public object Source { get; set; }
    public object CityName { get; set; }
    public string? LastDateSignOut { get; set; }
    public object CountryId { get; set; }
    public object Latitude { get; set; }
    public object Longitude { get; set; }
    public object CityId { get; set; }
    public long? AddressTypeId { get; set; }
    public object Name { get; set; }
    public object AddressTypeName { get; set; }
    public object Address2 { get; set; }
    public object DistrictId { get; set; }
    public object DistrictName { get; set; }
    public object WardId { get; set; }
    public object WardName { get; set; }
    public object Note { get; set; }
    public object WelcomeGrantType { get; set; }
    public object NickName { get; set; }
    public object Active { get; set; }
    public object Locker { get; set; }
    public object LockerDate { get; set; }
    public string HashId { get; set; }
    public bool? IsSystemCheck { get; set; }
    public string FirebaseTokenLog { get; set; }
    public string Version { get; set; }
    public object IsLinkedPrepaidPayoo { get; set; }
    public object PrepaidPayooDate { get; set; }
    public object PrepaidPayooCode { get; set; }
    public object Acronymn { get; set; }
    public string FirstName { get; set; }
    public string FullName { get; set; }
    public string FullName_Ansi { get; set; }
    public string LastName { get; set; }
    public string ListGroups { get; set; }
    public string ReferenceId { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public object Zip { get; set; }
    public object ReferenceId2 { get; set; }
    public object PassCode { get; set; }
    public object IsKyc { get; set; }
    public object KycDate { get; set; }
    public string NameAcronymn { get; set; }
    public bool? HasPassword { get; set; }
    public bool? IsNew { get; set; }
    public long? IsAssign { get; set; }
    public object HasBirthday { get; set; }
    public object HasFullName { get; set; }
    public object PolicyList { get; set; }
}