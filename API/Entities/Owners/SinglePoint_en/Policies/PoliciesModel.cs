﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Policies;

public class PoliciesModel : BaseModel
{
    public List<Datum> Data { get; set; }
    public long? IsIcon { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}

public partial class Datum
{
    public long? PolicieId { get; set; }
    public long? PolicyTypeId { get; set; }
    public string Handle { get; set; }
    public long? TypeId { get; set; }
    public string Name { get; set; }
    public string Title { get; set; }
    public string Body { get; set; }
    public string Icon { get; set; }
    public long? Visible { get; set; }
    public long? IsAccept { get; set; }
    public string Url { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
}
