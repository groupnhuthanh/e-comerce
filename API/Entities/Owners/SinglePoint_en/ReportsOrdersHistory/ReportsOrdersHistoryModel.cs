﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.ReportsOrdersHistory;

public class ReportsOrdersHistoryModel : BaseModel
{
    public string Rev { get; set; }
    public long? Key { get; set; }
    public string UserId { get; set; }
    public string LocationId { get; set; }
    public string DeviceId { get; set; }
    public string EmployeeId { get; set; }
    public string CashDrawerId { get; set; }
    public bool? IsCurrentCashDrawer { get; set; }
    public bool? IsAllDevice { get; set; }
    public long? TotalDay { get; set; }
    public DateTimeOffset? StartDay { get; set; }
    public DateTimeOffset? EndDay { get; set; }
    public string StartHour { get; set; }
    public string EndHour { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? RequestDate { get; set; }
    public long? StatusId { get; set; }
    public bool? IsRead { get; set; }
}
