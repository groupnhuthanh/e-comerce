﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Domains;

public class DomainsModel : BaseModel
{
    public string Id { get; set; }
    public List<object> ListDomain { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string Callback { get; set; }
}
