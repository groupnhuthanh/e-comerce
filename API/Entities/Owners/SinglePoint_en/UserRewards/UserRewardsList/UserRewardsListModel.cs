﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.UserRewards.UserRewardsList;

public class UserRewardsListModel : BaseModel
{
    public object LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string CustomerGuestsId { get; set; }
    public long? Quantity { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
}
