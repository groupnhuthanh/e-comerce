﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.UserRewards.UserReward;

public class UserRewardModel : BaseModel
{
    public string CustomerGuestsId { get; set; }
    public string CustomerDiscountId { get; set; }
    public string CustomerDiscountName { get; set; }
    public object OrderId { get; set; }
    public long? DiscountType { get; set; }
    public long? ApplyTo { get; set; }
    public bool? LoyaltyReward { get; set; }
    public bool? IsUsed { get; set; }
    public bool? IsLock { get; set; }
    public string ExpiryDate { get; set; }
    public string Ref { get; set; }
    public string Image { get; set; }
    public string Acronym { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public object UserRewardsListId { get; set; }
    public string DeviceGuid { get; set; }
    public string LocationGuid { get; set; }
    public string UserGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? Visible { get; set; }
}
