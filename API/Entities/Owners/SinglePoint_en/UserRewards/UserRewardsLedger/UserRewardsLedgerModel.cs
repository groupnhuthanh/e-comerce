﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.UserRewards.UserRewardsLedger;

public class UserRewardsLedgerModel : BaseModel
{
    public string UserRewardsId { get; set; }
    public string OrderId { get; set; }
    public string CustomerGuestsId { get; set; }
    public string CustomerDiscountId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? QuantityIn { get; set; }
    public long? QuantityOut { get; set; }
    public string ReasonId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string EmployeeId { get; set; }
    public long? UserRewardsTypeId { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
