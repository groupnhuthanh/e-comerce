﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.SignInValidateHistory;

public class SignInValidateHistoryModel : BaseModel
{
    public object LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string OtpToken { get; set; }
    public long? Visible { get; set; }
    public string CustomerGuestGuid { get; set; }
    public string Phone { get; set; }
    public object PhoneE164 { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string Email { get; set; }
    public long? Otp { get; set; }
    public long? ExpiredIn { get; set; }
    public string ExpiredDate { get; set; }
    public long? SignupTypeId { get; set; }
}
