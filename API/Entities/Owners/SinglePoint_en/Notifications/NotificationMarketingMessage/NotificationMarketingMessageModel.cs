﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Notifications.NotificationMarketingMessage;

public class NotificationMarketingMessageModel : BaseModel
{
    public DateTimeOffset? CreateDate { get; set; }
    public long? Success { get; set; }
    public long? Fail { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string MarketingMessageGuid { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
