﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Notifications.NotificationType;

public class NotificationTypeModel : BaseModel
{
    public string Name { get; set; }
    public string Description { get; set; }
    public long? GotoId { get; set; }
    public string Handle { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? DateCreate { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
