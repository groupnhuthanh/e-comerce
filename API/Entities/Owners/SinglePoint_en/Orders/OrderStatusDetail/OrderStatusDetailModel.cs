﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Orders.OrderStatusDetail;

public class OrderStatusDetailModel : BaseModel
{
    public long? DiningOptionId { get; set; }
    public long? OrderStatusId { get; set; }
    public long? Visible { get; set; }
    public List<Notification> Notification { get; set; }
    public List<Button> Button { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class Button
{
    public long? TypeId { get; set; }
    public long? Trigger { get; set; }
}

public partial class Notification
{
    public long? TypeId { get; set; }
    public long? IsNotification { get; set; }
    public string NotificationTypeGuid { get; set; }
}
