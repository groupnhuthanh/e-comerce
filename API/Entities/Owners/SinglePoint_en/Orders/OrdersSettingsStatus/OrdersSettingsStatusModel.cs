﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Orders.OrdersSettingsStatus;

public class OrdersSettingsStatusModel : BaseModel
{
    public long? OrderStatusGroupId { get; set; }
    public long? DinningOptionId { get; set; }
    public long? DinningOptionVisible { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string ListOrderStatus { get; set; }
    public string EmployeeGuid { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}
