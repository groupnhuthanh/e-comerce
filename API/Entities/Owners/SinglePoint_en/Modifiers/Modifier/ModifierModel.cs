﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Modifiers.Modifier;

public class ModifierModel : BaseModel
{
    public string Name { get; set; }
    public long? ModifierOptionType { get; set; }
    public string DisplayName { get; set; }
    public long? IsDisplayName { get; set; }
    public long? Select { get; set; }
    public string Location { get; set; }
    public long? LocationAll { get; set; }
    public string EmpGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? StatusId { get; set; }
    public long? Visible { get; set; }
    public long? ModifierId { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
