﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Modifiers.ModifierItemPriceOverride;

public class ModifierItemPriceOverrideModel : BaseModel
{
    public string Name { get; set; }
    public string Sku { get; set; }
    public long? UnitTypeId { get; set; }
    public string ModifierItemGuid { get; set; }
    public long? Price { get; set; }
    public List<ListGroupLocation> ListGroupLocation { get; set; }
    public List<PriceOverride> PriceOverride { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class ListGroupLocation
{
    public long? Price { get; set; }
    public string GroupLocation { get; set; }
}

public partial class PriceOverride
{
    public string LocationGuid { get; set; }
    public long? Price { get; set; }
}
