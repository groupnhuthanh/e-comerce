﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Modifiers.ModifierItem;

public class ModifierItemModel : BaseModel
{
    public string Modifier { get; set; }
    public string ReferenceId { get; set; }
    public string ReferenceId2 { get; set; }
    public long? Price { get; set; }
    public List<object> Location { get; set; }
    public long? isAllLocation { get; set; }
    public string Description { get; set; }
    public string Description2 { get; set; }
    public string Name2 { get; set; }
    public string Name3 { get; set; }
    public string Name4 { get; set; }
    public string Acronymn { get; set; }
    public string Color { get; set; }
    public long? Visible { get; set; }
    public long? UnitTypeId { get; set; }
    public long? UnitId { get; set; }
    public long? ComparePrice { get; set; }
    public long? CostPerItem { get; set; }
    public string SKU { get; set; }
    public string Barcode { get; set; }
    public long? Quantity { get; set; }
    public long? InventoryPolicyId { get; set; }
    public string Handle { get; set; }
    public string UserGuid { get; set; }
    public int ModifierItemId { get; set; }
    public string ModifierItemGuid { get; set; }
    public string AdminUpdate { get; set; }

    public long? OrderNo { get; set; }
    public string ModifierGuid { get; set; }

}
