﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.DateSettings;

public class DateSettingsModel : BaseModel
{
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string OpenTime { get; set; }
    public string Holidays { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string DaySegments { get; set; }
}
