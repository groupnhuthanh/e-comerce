﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.ScannerDevice;

public class ScannerDeviceModel : BaseModel
{
    public string NickName { get; set; }
    public string DeviceGuid { get; set; }
    public List<Challenge> Challenges { get; set; }
    public bool? IsAllowOffline { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class Challenge
{
    public string ChallengeId { get; set; }
    public string Name { get; set; }
}
