﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Locations.LocationShipping;

public class LocationShippingModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string LocationGuid { get; set; }
    public string ShippingZone { get; set; }
    public long? CountryId { get; set; }
    public string CountryName { get; set; }
    public long? CityId { get; set; }
    public string CityName { get; set; }
    public string ShippingDelivery { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
}
