﻿using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Owners.SinglePoint_en.Locations.Location;

[BsonIgnoreExtraElements]
public class LocationModel : BaseModel
{
    public string LegalName { get; set; }
    public int? OnlyToday { get; set; }
    public string NickName { get; set; }
    public int? AddressType { get; set; }
    public int? Country { get; set; }
    public int? City { get; set; }
    public string AddressTypeName { get; set; }
    public int? District { get; set; }
    public int? Ward { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Email { get; set; }
    public string CityName { get; set; }
    public string DistrictName { get; set; }
    public string WardName { get; set; }
    public string Phone { get; set; }
    public string Facebook { get; set; }
    public string Website { get; set; }
    public string LanguageId { get; set; }
    public string TimeZoneDisplayName { get; set; }
    public string FullAddress { get; set; }
    public string ReferenceId { get; set; }
    public string OpenTime { get; set; }
    public string Holidays { get; set; }
    public double? Latitude { get; set; }
    public double? Longitude { get; set; }
    public int? TypeLocationId { get; set; }
    public string RegionCode { get; set; }
    public string StoreCode { get; set; }
    public string ReferenceId2 { get; set; }
    public Loc Loc { get; set; }
    public string ListLanguage { get; set; }
    public string Handle { get; set; }
    //public DateTimeOffset? CreateDate { get; set; }
    public string EmpGuid { get; set; }
    public int? Visible { get; set; }
    public int? LocationId { get; set; }
    public string TimeZoneId { get; set; }
    //public long? Key { get; set; }
    //public string Rev { get; set; }
    public string AdminUpdate { get; set; }
    public int? Status { get; set; }
}
[BsonIgnoreExtraElements]
public partial class Loc
{
    public string Type { get; set; }
    public List<long> Coordinates { get; set; }

}
