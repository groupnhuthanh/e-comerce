﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Locations.LocationSettingsIds;

public class LocationSettingsIdsModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? SettingId { get; set; }
    public long? SettingTypeId { get; set; }
    public string Prefix { get; set; }
    public long? Number { get; set; }
    public long? MinimumNumber { get; set; }
    public string Preview { get; set; }
    public long? MaxChar { get; set; }
    public string Location { get; set; }
    public string Acronymn { get; set; }
    public string NumberFormat { get; set; }
    public string Device { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
}
