﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Discounts.Discounts_ApplyToItem;

public class Discounts_ApplyToItemModel : BaseModel
{
    public long? ApplyTo { get; set; }
    public string ListParents { get; set; }
    public string ListProducts { get; set; }
    public string DiscountGuid { get; set; }
    public string UserGuid { get; set; }
    public long? DiscountType { get; set; }
    public string GetListParents { get; set; }
    public string GetListProducts { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}
