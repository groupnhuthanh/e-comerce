﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Discounts.Discounts_FixedAmount;

public class Discounts_FixedAmountModel : BaseModel
{
    public long? AppliesTo { get; set; }
    public long? MaximumDiscount { get; set; }
    public long? IsDiscountLimit { get; set; }
    public string ListFromItem { get; set; }
    public long? IsMaxQuantity { get; set; }
    public long? IsMaxAmount { get; set; }
    public string UserGuid { get; set; }
    public long? DiscountValue { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string DiscountGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
