﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Discounts.Discounts_Combo;

public class Discounts_ComboModel : BaseModel
{
    public long? DiscountTypeForOrder { get; set; }
    public long? DiscountValueForOrder { get; set; }
    public string ListBuyApplyTo { get; set; }
    public string UserGuid { get; set; }
    public string DiscountGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public List<ListComboDiscount> ListComboDiscount { get; set; }
}

public partial class ListComboDiscount
{
    public long? Id { get; set; }
    public string ComboName { get; set; }
    public long? MinimumQuantityTypeId { get; set; }
    public long? MinimumQuantity { get; set; }
    public long? MaximumQuantity { get; set; }
    public long? Amount { get; set; }
    public long? DiscountValueTypeId { get; set; }
    public long? DiscountValue { get; set; }
    public long? ApplyTo { get; set; }
    public List<ListBuyApplyTo> ListBuyApplyTo { get; set; }
    public bool? IsNotRequire { get; set; }
}

public partial class ListBuyApplyTo
{
    public long? ComboId { get; set; }
    public string GroupGuid { get; set; }
    public string Name { get; set; }
    public List<ListFromItem> ListFromItem { get; set; }
    public long? Visible { get; set; }
    public long? OrderNo { get; set; }
}

public partial class ListFromItem
{
    public string ProductGuid { get; set; }
    public long? Price { get; set; }
    public long? Discount { get; set; }
    public long? FinalPrice { get; set; }
    public string Name { get; set; }
    public List<ListVariant> ListVariant { get; set; }
    public long? Visible { get; set; }
}

public partial class ListVariant
{
    public long? GroupId { get; set; }
    public string GroupName { get; set; }
    public long? Price { get; set; }
    public long? Discount { get; set; }
    public long? FinalPrice { get; set; }
    public long? OrderNo { get; set; }
    public long? IsUpsize { get; set; }
    public long? Visible { get; set; }
}
