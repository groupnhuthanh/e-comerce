﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Discounts.Discounts_BuyXgetY;

public class Discounts_BuyXgetYModel : BaseModel
{
    public string CustomerBuys { get; set; }
    public string CustomerGets { get; set; }
    public string ListBuyApplyTo { get; set; }
    public string ListGetApplyTo { get; set; }
    public long? MaximumNumberOfUses { get; set; }
    public long? MaximumNumberOfUsesValue { get; set; }
    public long? MaximumDiscount { get; set; }
    public long? IsDiscountLimit { get; set; }
    public string UserGuid { get; set; }
    public string DiscountGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}
