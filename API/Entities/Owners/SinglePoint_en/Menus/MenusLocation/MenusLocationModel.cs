﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Menus.MenusLocation;

public class MenusLocationModel : BaseModel
{
    public string LocationGuid { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string MenusGuid { get; set; }
    public string UserGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
