﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Menus.MenusGroupItem;

public class MenusGroupItemModel : BaseModel
{
    public string GroupGuid { get; set; }
    public string ItemGuid { get; set; }
    public string Variants { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? Price { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}
