﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Menus.MenusRearrange;

public class MenusRearrangeModel : BaseModel
{
    public string MenusGuid { get; set; }
    public List<ListTo> ListToArray { get; set; }
    public List<ListTo> ListToHierarchy { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class ListTo
{
    public string MenusGuid { get; set; }
    public string GroupGuid { get; set; }
    public long? MenusType { get; set; }
    public string Title { get; set; }
    public string Url { get; set; }
    public string Color { get; set; }
    public long? Level { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public string MenusGroupGuid { get; set; }
    public List<object> Children { get; set; }
}
