﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Menus.MenusGroup;

public class MenusGroupModel : BaseModel
{
    public string MenusGuid { get; set; }
    public string GroupGuid { get; set; }
    public long? MenusType { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
