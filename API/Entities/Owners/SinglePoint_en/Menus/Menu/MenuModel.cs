﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Menus.Menu;

public class MenuModel : BaseModel
{
    public long? AvailableFutureLocation { get; set; }
    public string Name { get; set; }
    public long? Visible { get; set; }
    public long? AsignTo { get; set; }
    public long? Location { get; set; }
    public string Acronymn { get; set; }
    public string Handle { get; set; }
    public string UserGuid { get; set; }
    public long? OrderNo { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
