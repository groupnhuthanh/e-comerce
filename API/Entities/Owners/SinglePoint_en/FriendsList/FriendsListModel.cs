﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.FriendsList;

public class FriendsListModel : BaseModel
{
    public string CustomerGuestsId { get; set; }
    public List<Friend> Friends { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdatedAt { get; set; }
}

public partial class Friend
{
    public string CustomerGuestsId { get; set; }
    public long? Timestamp { get; set; }
    public long? Type { get; set; }
}
