﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Environment;

public class EnvironmentModel : BaseModel
{
    public long? EnvironmentGroupId { get; set; }
    public string GroupName { get; set; }
    public long? Environment { get; set; }
    public string EnvironmentName { get; set; }
    public bool? IsRequireApiSignatureClientDevices { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? Update { get; set; }
}
