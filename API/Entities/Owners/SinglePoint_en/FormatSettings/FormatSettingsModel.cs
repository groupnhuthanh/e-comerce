﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.FormatSettings;

public class FormatSettingsModel : BaseModel
{
    public string Country { get; set; }
    public string Currency { get; set; }
    public string Number { get; set; }
    public string Date { get; set; }
    public string Hour { get; set; }
    public string NumberFormat { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
