﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.ShippingZone;

public class ShippingZoneModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string LocationGuid { get; set; }
    public string ShippingZone { get; set; }
    public long? PricingMode { get; set; }
    public long? PricingMethod { get; set; }
    public long? DefaultPrice { get; set; }
    public long? MaximumPrice { get; set; }
    public long? Conditions { get; set; }
    public long? MinimumValue { get; set; }
    public long? MaximumValue { get; set; }
    public string Zone { get; set; }
    public long? Visible { get; set; }
    public long? OrderNo { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
}
