﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Events.EventsOverview;

public class EventsOverviewModel : BaseModel
{
    public string Name { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public bool? IsLocation { get; set; }
    public DateRange Location { get; set; }
    public long? VisibilityId { get; set; }
    public bool? IsDateRange { get; set; }
    public DateRange DateRange { get; set; }
    public List<object> GroupCustomer { get; set; }
    public long? EventTypesId { get; set; }
    public bool? IsAllowCountdown { get; set; }
    public bool? IsAllGroupEvent { get; set; }
    public List<string> GroupEvent { get; set; }
    public string Handle { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? CheckinTotal { get; set; }
    public long? FriendShareTotal { get; set; }
    public long? InterestedTotal { get; set; }
    public long? LikeTotal { get; set; }
    public long? OngoingTotal { get; set; }
    public long? SocialShareTotal { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
}

public partial class DateRange
{
}
