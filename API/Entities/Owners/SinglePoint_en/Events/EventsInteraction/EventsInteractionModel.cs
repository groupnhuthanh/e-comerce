﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Events.EventsInteraction;

public class EventsInteractionModel : BaseModel
{
    public object LocationId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string CustomerGuestId { get; set; }
    public string EventsOverviewId { get; set; }
    public bool? IsOngoing { get; set; }
    public bool? IsInterested { get; set; }
    public bool? IsLike { get; set; }
    public bool? IsSocialShare { get; set; }
    public bool? IsFriendShare { get; set; }
    public bool? IsCheckin { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public Point PointSource { get; set; }
    public Point PointDestination { get; set; }
    public long? DistanceValue { get; set; }
    public object CheckinDate { get; set; }
}

public partial class Point
{
    public double? Latitude { get; set; }
    public double? Longitude { get; set; }
}
