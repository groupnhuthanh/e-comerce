﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Employees.EmployeePermission;

public class EmployeePermissionModel : BaseModel
{
    public long? Role { get; set; }
    public string EmployeeGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
