﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Employees.Employee;

public class EmployeeModel : BaseModel
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string NickName { get; set; }
    public string ReferenceId { get; set; }
    public long? PassCode { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public long? DepartmentId { get; set; }
    public long? PositionId { get; set; }
    public string GroupEmployeeId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
