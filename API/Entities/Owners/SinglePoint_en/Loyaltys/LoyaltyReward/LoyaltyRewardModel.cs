﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Loyaltys.LoyaltyReward;

public class LoyaltyRewardModel : BaseModel
{
    public long? RewardTypeId { get; set; }
    public long? RewardPoint { get; set; }
    public long? Visible { get; set; }
    public long? RewardDiscount { get; set; }
    public string RewardDiscountType { get; set; }
    public long? RewardDiscountTypeId { get; set; }
    public long? RewardMaxDiscount { get; set; }
    public string RewardName { get; set; }
    public string Item { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
