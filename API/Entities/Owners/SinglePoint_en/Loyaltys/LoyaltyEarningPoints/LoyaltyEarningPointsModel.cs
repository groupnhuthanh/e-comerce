﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Loyaltys.LoyaltyEarningPoints;

public class LoyaltyEarningPointsModel : BaseModel
{
    public long? EarnById { get; set; }
    public string ExpirationText { get; set; }
    public long? ExpirationValue { get; set; }
    public string Plural { get; set; }
    public string Singular { get; set; }
    public string Rules { get; set; }
    public long? MinimumSpent { get; set; }
    public string DiningOption { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
