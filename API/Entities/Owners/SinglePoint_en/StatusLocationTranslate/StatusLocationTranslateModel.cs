﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.StatusLocationTranslate;

public class StatusLocationTranslateModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? LanguageId { get; set; }
    public string LanguageTwoLetter { get; set; }
    public string Title { get; set; }
    public string Body { get; set; }
    public string LocationGuid { get; set; }
    public long? Status { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
}
