﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.OtpSms;

public class OtpSmsModel : BaseModel
{
    public object U { get; set; }
    public object Pwd { get; set; }
    public object From { get; set; }
    public string Phone { get; set; }
    public string Sms { get; set; }
    public object Bid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public bool? Status { get; set; }
    public string UserGuid { get; set; }
    public Uri Link { get; set; }
    public long? Otp { get; set; }
}
