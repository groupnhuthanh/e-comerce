﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Brands;

public class BrandsModel : BaseModel
{

    public long? Key { get; set; }
    public string Rev { get; set; }
    public string Title { get; set; }
    public string Color { get; set; }
    public string Acronymn { get; set; }
    public string Description { get; set; }
    public string ReferenceId { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
}
