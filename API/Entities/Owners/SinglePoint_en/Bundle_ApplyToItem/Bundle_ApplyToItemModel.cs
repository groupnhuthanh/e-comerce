﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Bundle_ApplyToItem;

public class Bundle_ApplyToItemModel : BaseModel
{

    public long? Key { get; set; }
    public string Rev { get; set; }
    public string ListParents { get; set; }
    public string ListProducts { get; set; }
    public string ProductGuid { get; set; }
    public long? ApplyTo { get; set; }
    public long? ProductType { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
}
