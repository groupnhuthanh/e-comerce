﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Accounts.AccountGroupImages;

public class AccountGroupImagesModel : BaseModel
{


    public string GroupGuid { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public long? StyleId { get; set; }
    public long? Style { get; set; }
    public long? Size { get; set; }
    public long? Width { get; set; }
    public long? Height { get; set; }
    public string Url { get; set; }
    public string UrlRewrite { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }



}
