﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.Accounts.AccountGroup;

public class AccountGroupModel : BaseModel
{


    public string GroupName { get; set; }
    public string ShortName { get; set; }
    public string Description { get; set; }
    public string PrinterName { get; set; }
    public string Color { get; set; }
    public string Acronymn { get; set; }
    public string Location { get; set; }
    public long? IsAllLocation { get; set; }
    public string Handle { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }


}
