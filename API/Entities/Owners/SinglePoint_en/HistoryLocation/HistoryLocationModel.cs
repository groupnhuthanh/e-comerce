﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_en.HistoryLocation;

public class HistoryLocationModel : BaseModel
{
    public string UserGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string LocationGuid { get; set; }
    public string Reason { get; set; }
    public List<string> ReasonList { get; set; }
    public bool? IsAdmin { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
