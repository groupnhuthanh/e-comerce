﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocketDetail;

public class DeliveryDocketDetailModel : BaseModel
{
    public bool? IsCompletedOrder { get; set; }
    public Order Order { get; set; }
    public List<OrderProduct> OrderProducts { get; set; }
    public string DeliveryDocketId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class Order
{
    public long? TotalQuantity { get; set; }
}

public partial class OrderProduct
{
    public string Id { get; set; }
    public string Name1 { get; set; }
    public string Acronymn { get; set; }
    public string Name2 { get; set; }
    public string Sku { get; set; }
    public string Note { get; set; }
    public string Variant { get; set; }
    public long? VariantGroupId { get; set; }
    public long? UnitId { get; set; }
    public long? UnitType { get; set; }
    public string UnitTypeName { get; set; }
    public long? Price { get; set; }
    public long? QuantityDecimal { get; set; }
    public long? TotalReceivedQuantity { get; set; }
    public bool? IsReceiveCompleted { get; set; }
    public bool? IsReward { get; set; }
    public long? Subtotal { get; set; }
    public long? LineTotal { get; set; }
    public List<ReceivedList> ReceivedList { get; set; }
    public long? OrderDetailId { get; set; }
    public long? ProductTypeId { get; set; }
    public string CategoryId { get; set; }
    public long? ParentIndex { get; set; }
    public long? ProductApplyTo { get; set; }
    public string SupplierCode { get; set; }
    public long? RawProductTypeId { get; set; }
    public string RawProductTypeName { get; set; }
}

public partial class ReceivedList
{
    public long? Quantity { get; set; }
}
