﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Delivery.DeliveryDocket;

public class DeliveryDocketModel : BaseModel
{
    public string Checksum { get; set; }
    public string Code { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string EmployeeGuid { get; set; }
    public string PoId { get; set; }
    public string PrId { get; set; }
    public string SupplierCatalogId { get; set; }
    public string SupplierCode { get; set; }
    public string SupplierGuid { get; set; }
    public string AppVersion { get; set; }
    public bool? IsNew { get; set; }
    public DateTimeOffset? SubmitDate { get; set; }
    public DateTimeOffset? SyncDate { get; set; }
    public string BaseUtcOffSet { get; set; }
    public long? IsOfflineSync { get; set; }
    public bool? IsRead { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
