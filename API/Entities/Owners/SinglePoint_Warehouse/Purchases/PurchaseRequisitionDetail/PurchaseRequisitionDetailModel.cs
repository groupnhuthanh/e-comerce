﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseRequisitionDetail;

public class PurchaseRequisitionDetailModel : BaseModel
{
    public bool? IsCompletedOrder { get; set; }
    public List<OrderProduct> OrderProducts { get; set; }
    public string PurchaseRequisitionId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}

public partial class OrderProduct
{
    public string Id { get; set; }
    public string Name1 { get; set; }
    public string Acronymn { get; set; }
    public string Name2 { get; set; }
    public string Sku { get; set; }
    public long? UnitId { get; set; }
    public long? UnitType { get; set; }
    public object UnitTypeName { get; set; }
    public long? Price { get; set; }
    public long? Quantity { get; set; }
    public long? QuantityDecimal { get; set; }
    public bool? IsReceiveCompleted { get; set; }
    public long? OrderDetailId { get; set; }
    public long? ProductTypeId { get; set; }
    public long? PricingMethodType { get; set; }
    public string CategoryId { get; set; }
    public long? ParentIndex { get; set; }
    public long? ProductApplyTo { get; set; }
    public long? RawProductTypeId { get; set; }
    public string RawProductTypeName { get; set; }
    public long? Status { get; set; }
    public object InStock { get; set; }
    public object IsProduct { get; set; }
    public object VariantList { get; set; }
    public object Variant { get; set; }
    public object Note { get; set; }
}
