﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrder;

public class PurchaseOrderModel : BaseModel
{
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public string Checksum { get; set; }
    public long? DiningOptionId { get; set; }
    public object CurrencySymbol { get; set; }
    public long? CurrencyId { get; set; }
    public long? OrderStatusId { get; set; }
    public long? PaymentStatusId { get; set; }
    public string EmployeeGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string Code { get; set; }
    public string PrId { get; set; }
    public string SupplierGuid { get; set; }
    public string SupplierCode { get; set; }
    public string SupplierCatalogId { get; set; }
    public string SupplierName { get; set; }
    public long? CodeIndex { get; set; }
    public string PurchaseOrderLedgerId { get; set; }
    public bool? IsOfflineSync { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
