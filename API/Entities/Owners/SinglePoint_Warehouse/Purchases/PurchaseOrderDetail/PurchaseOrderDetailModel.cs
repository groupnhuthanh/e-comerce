﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Purchases.PurchaseOrderDetail;

public class PurchaseOrderDetailModel : BaseModel
{
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public long? TotalQuantity { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public List<OrderProduct> OrderProducts { get; set; }
    public string PurchaseOrderLedgerId { get; set; }
    public string PurchaseOrderId { get; set; }
    public string EmployeeGuid { get; set; }
    public string PrId { get; set; }
    public Order Order { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class Order
{
    public long? Subtotal { get; set; }
    public long? Grandtotal { get; set; }
}

public partial class OrderProduct
{
    public string CategoryId { get; set; }
    public object VariantList { get; set; }
    public bool? IsProduct { get; set; }
    public string Id { get; set; }
    public string Name1 { get; set; }
    public string Name2 { get; set; }
    public string Acronymn { get; set; }
    public string Sku { get; set; }
    public string Variant { get; set; }
    public long? ProductTypeId { get; set; }
    public long? RawProductTypeId { get; set; }
    public string RawProductTypeName { get; set; }
    public string Note { get; set; }
    public long? OrderDetailId { get; set; }
    public long? QuantityDecimal { get; set; }
    public long? TotalReceivedQuantity { get; set; }
    public long? Remaining { get; set; }
    public long? Change { get; set; }
    public bool? IsReceiveCompleted { get; set; }
    public string SupplierCode { get; set; }
    public long? Price { get; set; }
    public long? Subtotal { get; set; }
    public long? ShippingFee { get; set; }
    public long? Discount { get; set; }
    public long? TaxFee { get; set; }
    public long? Linetotal { get; set; }
    public long? UnitType { get; set; }
    public long? UnitId { get; set; }
    public string UnitTypeName { get; set; }
}
