﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Works.WorkOrderDetail;

public class WorkOrderDetailModel : BaseModel
{
    public bool? IsCompletedOrder { get; set; }
    public List<OrderProduct> OrderProducts { get; set; }
    public string WorkOrderId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class OrderProduct
{
    public string Id { get; set; }
    public string Name1 { get; set; }
    public string Acronymn { get; set; }
    public string Name2 { get; set; }
    public string Sku { get; set; }
    public object Note { get; set; }
    public object Variant { get; set; }
    public object VariantList { get; set; }
    public long? UnitId { get; set; }
    public long? UnitType { get; set; }
    public string UnitTypeName { get; set; }
    public long? QuantityDecimal { get; set; }
    public object TotalReceivedQuantity { get; set; }
    public bool? IsReceiveCompleted { get; set; }
    public object InStock { get; set; }
    public List<object> ReceivedList { get; set; }
    public long? OrderDetailId { get; set; }
    public long? ProductTypeId { get; set; }
    public object CategoryId { get; set; }
    public long? ParentIndex { get; set; }
    public long? ProductApplyTo { get; set; }
    public object SupplierCode { get; set; }
    public long? RawProductTypeId { get; set; }
    public string RawProductTypeName { get; set; }
}
