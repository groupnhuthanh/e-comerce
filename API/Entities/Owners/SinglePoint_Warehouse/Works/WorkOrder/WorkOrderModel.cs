﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Works.WorkOrder;

public class WorkOrderModel : BaseModel
{
    public string CashDrawerId { get; set; }
    public string Checksum { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string DeviceGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public string LocationGuid { get; set; }
    public long? OrderStatusId { get; set; }
    public string UserGuid { get; set; }
    public string AppVersion { get; set; }
    public bool? IsNew { get; set; }
    public long? PaymentStatusId { get; set; }
    public DateTimeOffset? SubmitDate { get; set; }
    public DateTimeOffset? SyncDate { get; set; }
    public string BaseUtcOffSet { get; set; }
    public long? IsOfflineSync { get; set; }
    public bool? IsRead { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string Code { get; set; }
}
