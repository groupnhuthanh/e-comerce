﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Inventories.InventoryHistory;

public class InventoryHistoryModel : BaseModel
{
    public object LocationId { get; set; }
    public object CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? ProductIndex { get; set; }
    public string ProductId { get; set; }
    public long? VariantId { get; set; }
    public object VariantName { get; set; }
    public List<Variant> Variant { get; set; }
    public long? RawProductType { get; set; }
    public object Upc { get; set; }
    public object MerchantSku { get; set; }
    public string ProductName { get; set; }
    public long? EventType { get; set; }
    public long? QuantityIn { get; set; }
    public long? QuantityOut { get; set; }
    public long? UnitId { get; set; }
    public object UnitType { get; set; }
    public object UnitTypeName { get; set; }
    public string ReasonDescription { get; set; }
    public long? ReasonId { get; set; }
    public string EmployeeGuid { get; set; }
    public string LocationGuid { get; set; }
    public string UseGuid { get; set; }
    public string DeviceGuid { get; set; }
    public long? ProductLevel { get; set; }
}

public partial class Variant
{
    public object VariantVariant { get; set; }
    public long? Id { get; set; }
    public string Value { get; set; }
    public long? Level { get; set; }
    public bool? Visible { get; set; }
    public object GroupValue { get; set; }
    public object ComparePrice { get; set; }
    public object CostPerItem { get; set; }
    public object Price { get; set; }
    public object Sku { get; set; }
    public object Inventory { get; set; }
    public object Discount { get; set; }
    public object Barcode { get; set; }
    public object ReferenceId1 { get; set; }
    public object ReferenceId2 { get; set; }
}
