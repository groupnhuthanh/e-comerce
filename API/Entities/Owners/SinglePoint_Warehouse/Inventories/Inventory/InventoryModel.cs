﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Inventories.Inventory;

public class InventoryModel : BaseModel
{
    public string ProductId { get; set; }
    public string Variant { get; set; }
    public List<object> VariantList { get; set; }
    public long? UnitId { get; set; }
    public long? UnitType { get; set; }
    public string UnitTypeName { get; set; }
    public object MerchantSku { get; set; }
    public string ProductName { get; set; }
    public string Location { get; set; }
    public long? LocationType { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public long? CityId { get; set; }
    public string CityName { get; set; }
    public long? DistrictName { get; set; }
    public long? DistrictId { get; set; }
    public long? WardName { get; set; }
    public long? WardId { get; set; }
    public long? AddressTypeId { get; set; }
    public long? ProductTypeId { get; set; }
    public object RawProductTypeId { get; set; }
    public object RawProductTypeName { get; set; }
    public string AddressTypeName { get; set; }
    public long? Beginning { get; set; }
    public long? QuantityIn { get; set; }
    public long? QuantityOut { get; set; }
    public long? QuantityBalance { get; set; }
    public string Image { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string EmployeeId { get; set; }
    public string DeviceGuid { get; set; }
    public string CashdrawerGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public object SupplierGuid { get; set; }
    public object SupplierCode { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
}
