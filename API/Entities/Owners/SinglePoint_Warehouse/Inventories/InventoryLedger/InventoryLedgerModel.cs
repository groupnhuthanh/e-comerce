﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Warehouse.Inventories.InventoryLedger;

public class InventoryLedgerModel : BaseModel
{
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string Code { get; set; }
    public object ProductTypeId { get; set; }
    public long? RawProductTypeId { get; set; }
    public object RawProductTypeName { get; set; }
    public string ProductId { get; set; }
    public string RawProductId { get; set; }
    public string Variant { get; set; }
    public long? UnitType { get; set; }
    public long? UnitId { get; set; }
    public string UnitTypeName { get; set; }
    public List<VariantList> VariantList { get; set; }
    public string Isbn { get; set; }
    public string Upc { get; set; }
    public object Sku { get; set; }
    public string SupplierGuid { get; set; }
    public string SupplierCode { get; set; }
    public string MerchantSku { get; set; }
    public string Name1 { get; set; }
    public string Name2 { get; set; }
    public string EventType { get; set; }
    public string Disposition { get; set; }
    public long? QuantityIn { get; set; }
    public long? QuantityOut { get; set; }
    public string LocationId { get; set; }
    public string Location { get; set; }
    public string ReasonDescription { get; set; }
    public string ReasonId { get; set; }
    public long? TransactionReferenceId { get; set; }
    public string OrderReferenceId { get; set; }
    public long? OrderLinePosition { get; set; }
    public string PoReferenceId { get; set; }
    public string PoLinePosition { get; set; }
    public string DeliveryDocketReferenceId { get; set; }
    public string InventoryLedgerReferenceId { get; set; }
    public object WorkOrderReferenceId { get; set; }
    public string EmployeeId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string CashdrawerGuid { get; set; }
}

public partial class VariantList
{
    public long? Id { get; set; }
    public string Value { get; set; }
    public long? Level { get; set; }
    public bool? Visible { get; set; }
    public object ReferenceId1 { get; set; }
    public object ReferenceId2 { get; set; }
}
