﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Hr.ClockInOut;

public class ClockInOutModel : BaseModel
{
    public long? Source { get; set; }
    public string In { get; set; }
    public string Out { get; set; }
    public long? Active { get; set; }
    public string Code { get; set; }
    public long? ClockType { get; set; }
    public long? ClockMethod { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string EmployeeGuid { get; set; }
    public DateTimeOffset? ClockDate { get; set; }
    public DateTimeOffset? ClockOutDate { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
