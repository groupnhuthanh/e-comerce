﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.EnvironmentGroup;

public class EnvironmentGroupModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? EnvironmentGroupId { get; set; }
    public string Title { get; set; }
}
