﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.VariableDetail;

public class VariableDetailModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? Environment { get; set; }
    public long? VariableDetailId { get; set; }
    public string Name { get; set; }
    public Uri Domain { get; set; }
    public long? Status { get; set; }
    public long? Type { get; set; }
}
