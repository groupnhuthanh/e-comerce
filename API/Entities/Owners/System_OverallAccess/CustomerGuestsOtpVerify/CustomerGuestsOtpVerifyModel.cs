﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.CustomerGuestsOtpVerify;

public class CustomerGuestsOtpVerifyModel : BaseModel
{
    public long? Otp { get; set; }
    public string Email { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string VerifyToken { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public object CustomerGuid { get; set; }
}
