﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.Status;

public class StatusModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? StatusId { get; set; }
    public string TitleVi { get; set; }
    public string TitleEn { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? Default { get; set; }
}
