﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.TypeServer;

public class TypeServerModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? Id { get; set; }
    public string Title { get; set; }
}
