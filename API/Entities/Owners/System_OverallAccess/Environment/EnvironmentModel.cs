﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.Environment;

public class EnvironmentModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? EnvironmentId { get; set; }
    public string TitleVi { get; set; }
    public string TitleEn { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? Default { get; set; }
    public long? EnvironmentGroupId { get; set; }
    public long? TypeServer { get; set; }
}
