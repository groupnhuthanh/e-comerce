﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.AccessToken;

public class AccessTokenModel : BaseModel
{
    public object LocationId { get; set; }
    public object CreateDate { get; set; }
    public object UpdateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public object Token { get; set; }
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
    public long? ExpiresIn { get; set; }
    public object SystemExpiresIn { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public object CustomerGuid { get; set; }
    public string ExpiredDate { get; set; }
}
