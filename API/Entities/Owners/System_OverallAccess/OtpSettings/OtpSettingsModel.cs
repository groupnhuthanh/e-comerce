﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.OtpSettings;

public class OtpSettingsModel : BaseModel
{

    public long? OtpTimeOut { get; set; }
    public long? ResendOtpNumber { get; set; }
    public bool? IsSkipPhoneOtp { get; set; }
    public bool? IsSkipEmailOtp { get; set; }
}
