﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.Variable;

public class VariableModel : BaseModel
{
    public long? Id { get; set; }
    public string Company { get; set; }
    public DateTimeOffset? BeginningDate { get; set; }
    public DateTimeOffset? ExpireDate { get; set; }
    public string DeviceName { get; set; }
    public string Key { get; set; }
    public long? EnvironmentGroupId { get; set; }
    public long? ExpiredKey { get; set; }
    public long? Environment { get; set; }
    public long? Status { get; set; }
    public string LastAccessDate { get; set; }
    public string AccessTotal { get; set; }
    public string UserGuid { get; set; }
    public string DeviceGuid { get; set; }
    public long? WelcomeKey { get; set; }
    public string Rev { get; set; }
}
