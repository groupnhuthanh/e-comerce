﻿using Entities.Base;

namespace Entities.Owners.System_OverallAccess.Logs;

public class LogsModel : BaseModel
{
    public string Key { get; set; }
    public long? DeviceTypeId { get; set; }
    public object Id { get; set; }
    public string DeviceName { get; set; }
    public long? CurrencyKey { get; set; }
    public string CurrencyNativeName { get; set; }
    public string CurrencySymbol { get; set; }
    public long? ExpiresIn { get; set; }
    public string Environment { get; set; }
    public DateTimeOffset? LastestLoginTime { get; set; }
    public Uri DomainApi { get; set; }
    public string DomainSignalR { get; set; }
    public string DomainApiAsync { get; set; }
    public string DeviceCode { get; set; }
    public string RefreshToken { get; set; }
    public string AccessToken { get; set; }
    public FormatSetting FormatSetting { get; set; }
    public string LocationGuid { get; set; }
    public long? LocationKey { get; set; }
    public FireStorePath FireStorePath { get; set; }
    public PaymentApiList PaymentApiList { get; set; }
    public string LocationName { get; set; }
    public string Phone { get; set; }
    public LicensedBy LicensedBy { get; set; }
    public long? OtpTimeOut { get; set; }
    public long? ResendOtpNumber { get; set; }
    public bool? IsSkipEmailOtp { get; set; }
    public bool? IsSkipPhoneOtp { get; set; }
    public ScanDevice ScanDevice { get; set; }
    public QrPrefixSetting QrPrefixSetting { get; set; }
    public LoyaltySetting LoyaltySetting { get; set; }
    public bool? IsCaptchaRequired { get; set; }
}

public partial class FireStorePath
{
    public string T { get; set; }
    public FireStorePathV V { get; set; }
}

public partial class FireStorePathV
{
    public string ProductList { get; set; }
    public string Transaction { get; set; }
    public string AppVersion { get; set; }
    public string DataVersion { get; set; }
    public string OrderList { get; set; }
    public string LocationList { get; set; }
    public string CustomerDiscounts { get; set; }
    public string Menus { get; set; }
    public string ReportList { get; set; }
    public string ChallengeList { get; set; }
}

public partial class FormatSetting
{
    public string T { get; set; }
    public object Currency { get; set; }
    public object Number { get; set; }
    public object Date { get; set; }
    public object Hour { get; set; }
    public object Country { get; set; }
    public object NumberFormat { get; set; }
}

public partial class LicensedBy
{
    public string T { get; set; }
    public LicensedByV V { get; set; }
}

public partial class LicensedByV
{
    public string CompanyName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
}

public partial class LoyaltySetting
{
    public string T { get; set; }
    public string Plural { get; set; }
    public string Singular { get; set; }
}

public partial class PaymentApiList
{
    public string T { get; set; }
    public List<object> V { get; set; }
}

public partial class QrPrefixSetting
{
    public string T { get; set; }
    public List<QrPrefixList> QrPrefixList { get; set; }
    public string QrPrefixSeparator { get; set; }
}

public partial class QrPrefixList
{
    public long? Default { get; set; }
    public long? OrderNo { get; set; }
    public long? SystemQrPrefixId { get; set; }
    public long? TypeId { get; set; }
    public long? Prefix { get; set; }
    public string TitleEn { get; set; }
    public string TitleVi { get; set; }
    public long? Visible { get; set; }
}

public partial class ScanDevice
{
    public string T { get; set; }
    public object NickName { get; set; }
    public object DeviceGuid { get; set; }
    public object Challenges { get; set; }
    public object IsAllowOffline { get; set; }
}
