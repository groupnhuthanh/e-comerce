﻿using Entities.Base;

namespace Entities.Owners.ManagementIndex;

public class ManagementIndexModel : BaseModel
{
    public long? Value { get; set; }
}
