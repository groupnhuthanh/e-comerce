﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Pos.SystemTableStatus;

public class SystemTableStatusModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string BgColor { get; set; }
    public long? Default { get; set; }
    public long? Id { get; set; }
    public long? OrderNo { get; set; }
    public string TitleEn { get; set; }
    public string TitleVi { get; set; }
    public long? Visible { get; set; }
}
