﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Pos.Sections.Section;

public class SectionModel : BaseModel
{
    public long? SectionId { get; set; }
    public string SectionName { get; set; }
    public long? NamingType { get; set; }
    public string Label { get; set; }
    public long? NumberOfTable { get; set; }
    public long? NumberToTable { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string LocationGuid { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
