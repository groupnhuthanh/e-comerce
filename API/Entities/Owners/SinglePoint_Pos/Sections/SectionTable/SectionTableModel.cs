﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Pos.Sections.SectionTable;

public class SectionTableModel : BaseModel
{
    public string FloorGuid { get; set; }
    public string SectionGuid { get; set; }
    public string LocationGuid { get; set; }
    public long? TableTypeId { get; set; }
    public string TableName { get; set; }
    public string Label { get; set; }
    public long? Number { get; set; }
    public long? PeopleQuantity { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
