﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Pos.POS.POSSecondaryScreenImage;

public class POSSecondaryScreenImageModel : BaseModel
{
    public string PosSecondaryScreenGuid { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public string FileName { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public string Path { get; set; }
    public string Url { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Width { get; set; }
    public long? Height { get; set; }
    public string UserGuid { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
