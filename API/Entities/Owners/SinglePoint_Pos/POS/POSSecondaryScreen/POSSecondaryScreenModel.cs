﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Pos.POS.POSSecondaryScreen;

public class POSSecondaryScreenModel : BaseModel
{
    public long? SecondaryScreenSettingsId { get; set; }
    public VfdMode VfdMode { get; set; }
    public DualDisplayMode DualDisplayMode { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}

public partial class DualDisplayMode
{
    public long? DualDisplayModeId { get; set; }
    public string Text1 { get; set; }
    public string Text2 { get; set; }
    public string Text3 { get; set; }
    public string Text4 { get; set; }
    public string Color1 { get; set; }
    public string Color2 { get; set; }
    public string Color3 { get; set; }
    public string Color4 { get; set; }
    public string Background { get; set; }
}

public partial class VfdMode
{
    public string Text1 { get; set; }
    public string Text2 { get; set; }
    public string Text3 { get; set; }
    public string Text4 { get; set; }
    public long? ComPort { get; set; }
    public bool? IsOrderTotal { get; set; }
    public bool? IsChange { get; set; }
    public bool? IsLoyaltyPointsEarned { get; set; }
    public bool? IsCustomerName { get; set; }
    public bool? IsThankYouMessage { get; set; }
}
