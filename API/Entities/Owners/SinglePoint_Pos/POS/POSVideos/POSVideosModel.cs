﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Pos.POS.POSVideos;

public class POSVideosModel : BaseModel
{
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string Url { get; set; }
    public string Name { get; set; }
    public long? AssignTo { get; set; }
    public string Location { get; set; }
    public long? OrderNo { get; set; }
    public string Time { get; set; }
    public string Poster { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
