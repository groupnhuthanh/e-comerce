﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Pos.Floors.FloorTable;

public class FloorTableModel : BaseModel
{
    public string FloorGuid { get; set; }
    public long? TableTypeId { get; set; }
    public string SectionGuid { get; set; }
    public string TableName { get; set; }
    public long? Left { get; set; }
    public string Top { get; set; }
    public long? Width { get; set; }
    public long? Height { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? OrderNo { get; set; }
    public string UserGuid { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}
