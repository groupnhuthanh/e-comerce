﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Pos.Floors.Floor;

public class FloorModel : BaseModel
{
    public long? FloorId { get; set; }
    public string FloorCode { get; set; }
    public string Name { get; set; }
    public string Handle { get; set; }
    public string LocationGuid { get; set; }
    public long? DiningOptionId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string UserGuid { get; set; }
    public long? OrderNo { get; set; }
    public long? Visible { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
}
