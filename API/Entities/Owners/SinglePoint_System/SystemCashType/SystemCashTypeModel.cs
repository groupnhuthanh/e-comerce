﻿using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Owners.SinglePoint_System.SystemCashType;

[BsonNoId]
[BsonIgnoreExtraElements]
public class SystemCashTypeModel : BaseModel
{
   
    public int? Id { get; set; }
    public string Title_en { get; set; }
    public string Title_vi { get; set; }
    public int? Visible { get; set; }
    public int? Default { get; set; }
}
