﻿using Entities.Base;

namespace Entities.Owners.SingleWorker.WorkerLogs;

public class WorkerLogsModel : BaseModel
{
    public long? WorkerTypeId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public Data Data { get; set; }
    public string Exception { get; set; }
}

public partial class Data
{
    public long? TotalItem { get; set; }
    public List<string> GetList { get; set; }
    public List<object> SuccessList { get; set; }
    public List<string> FailureList { get; set; }
    public List<DataExtraList> DataExtraList { get; set; }
}

public partial class DataExtraList
{
    public string DataId { get; set; }
    public long? Receivable { get; set; }
    public long? Payable { get; set; }
    public long? Quantity { get; set; }
    public long? OrderStatusId { get; set; }
    public long? PaymentStatusId { get; set; }
}
