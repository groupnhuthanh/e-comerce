﻿using Entities.Base;

namespace Entities.Owners.SingleWorker.Settings;

public class SettingsModel : BaseModel
{
    public BirthdayGift OrderToOnRamp { get; set; }
    public BirthdayGift Payoo { get; set; }
    public BirthdayGift Loyalty { get; set; }
    public BirthdayGift BirthdayGift { get; set; }
    public BirthdayGift SumMembership { get; set; }
    public BirthdayGift UpdateLevelMembership { get; set; }
    public BirthdayGift PayooPrepaid { get; set; }
}

public partial class BirthdayGift
{
    public long? Started { get; set; }
    public long? Ended { get; set; }
    public long? Delay { get; set; }
    public bool? IsRunning { get; set; }
    public long? TypeId { get; set; }
    public DateTimeOffset? FixTime { get; set; }
    public long? Duration { get; set; }
    public long? DelayToDb { get; set; }
    public long? StandBy { get; set; }
}
