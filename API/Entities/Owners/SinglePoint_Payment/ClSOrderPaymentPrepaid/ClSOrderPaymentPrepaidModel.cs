﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Payment.ClSOrderPaymentPrepaid;

public class ClSOrderPaymentPrepaidModel : BaseModel
{
    public Data Data { get; set; }
    public string PartnerTransactionNo { get; set; }
    public string ResponseNo { get; set; }
    public string PayooTransactionNo { get; set; }
    public DateTimeOffset? PartnerTransactionDate { get; set; }
    public string AccountNo { get; set; }
    public object Listener { get; set; }
    public string PaymentOption { get; set; }
    public string Checksum { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public DateTimeOffset? UpdateDate { get; set; }
    public long? PaymentStatusId { get; set; }
    public long? OrderStatusId { get; set; }
    public long? AfterSupportType { get; set; }
    public long? TransactionStatus { get; set; }
    public object TransactionDate { get; set; }
    public object TransactionNo { get; set; }
    public object TransactionType { get; set; }
    public object Receivable { get; set; }
    public object Payable { get; set; }
    public long? Visible { get; set; }
    public object Note { get; set; }
}

public partial class Data
{
    public string AccountNo { get; set; }
    public string PaymentAmount { get; set; }
    public string PartnerTransactionNo { get; set; }
    public DateTimeOffset? PartnerTransactionDate { get; set; }
    public long? PaymentChannelPerform { get; set; }
    public object QrCode { get; set; }
    public string Description { get; set; }
    public long? PartnerStoreCode { get; set; }
    public string AppName { get; set; }
}
