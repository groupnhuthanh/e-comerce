﻿namespace Entities.Owners.SinglePoint_Payment.OrderGetInfoPayooHistory;

public class OrderGetInfoPayooHistoryModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public string OrderId { get; set; }
    public long? SdkStatus { get; set; }
    public ResponseData ResponseData { get; set; }
    public ApiRequest ApiRequest { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public Result Result { get; set; }
}

public partial class ApiRequest
{
    public string RequestData { get; set; }
    public string Signature { get; set; }
}

public partial class ResponseData
{
    public long? OrderStatus { get; set; }
    public long? OrderCash { get; set; }
    public string PaymentDate { get; set; }
    public string ShippingDate { get; set; }
    public string DeliveryDate { get; set; }
    public long? PaymentMethod { get; set; }
    public string BankName { get; set; }
    public string CardNumber { get; set; }
    public string CardHolderName { get; set; }
    public object InstallmentPeriod { get; set; }
    public long? InstallmentMoneyTotal { get; set; }
    public string BillingCode { get; set; }
    public string BuyerIpAddress { get; set; }
    public object RequestValueAmount { get; set; }
    public object PaymentSource { get; set; }
    public object QrPaymentSystemTrace { get; set; }
    public long? ResponseCode { get; set; }
}

public partial class Result
{
    public string OrderId { get; set; }
    public long? ResponseCode { get; set; }
    public long? OrderStatus { get; set; }
    public long? Status { get; set; }
    public object Message { get; set; }
}
