﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Payment.OrderPaymentHistory;

public class OrderPaymentHistoryModel : BaseModel
{
    public long? Key { get; set; }
    public string Rev { get; set; }
    public long? ShopId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string RequestId { get; set; }
    public string OrderId { get; set; }
    public long? OrderKey { get; set; }
    public string Code { get; set; }
    public string Checksum { get; set; }
    public long? TypeId { get; set; }
    public long? Grandtotal { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string Deeplink { get; set; }
    public long? Status { get; set; }
    public string PaymentTypeId { get; set; }
    public long? PaymentSubId { get; set; }
    public string ApiType { get; set; }
}
