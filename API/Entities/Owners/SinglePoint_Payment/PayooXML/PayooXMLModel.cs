﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Payment.PayooXML;

public class PayooXMLModel : BaseModel
{
    public string Xml { get; set; }
    public string Checksum { get; set; }
    public Uri Refer { get; set; }
    public string Method { get; set; }
    public string Bank { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string OrderId { get; set; }
    public string UserGuid { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string ApiType { get; set; }
    public string RegionCode { get; set; }
    public long? StoreCode { get; set; }
}
