﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Payment.OrderPaymentPayooNotify;

public class OrderPaymentPayooNotifyModel : BaseModel
{
    public long? ShopId { get; set; }
    public string LocationGuid { get; set; }
    public string DeviceGuid { get; set; }
    public string UserGuid { get; set; }
    public string Grandtotal { get; set; }
    public OrderInformation OrderInformation { get; set; }
    public long? OrderId { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Status { get; set; }
    public string ResponseChecksum { get; set; }
}

public partial class OrderInformation
{
    public long? PaymentMethod { get; set; }
    public string PaymentSource { get; set; }
    public object QrPaymentSystemTrace { get; set; }
    public string PurchaseDate { get; set; }
    public string MerchantUsername { get; set; }
    public long? ShopId { get; set; }
    public long? MasterShopId { get; set; }
    public long? OrderNo { get; set; }
    public string OrderCash { get; set; }
    public string BankName { get; set; }
    public string CardNumber { get; set; }
    public object BillingCode { get; set; }
    public long? CardIssuanceType { get; set; }
    public long? PaymentStatus { get; set; }
    public object TransactionFee { get; set; }

}
