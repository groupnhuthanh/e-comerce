﻿using Entities.Base;

namespace Entities.Owners.SinglePoint_Payment.OrderPaymentHistoryLog;

public class OrderPaymentHistoryLogModel : BaseModel
{
    public long? ShopId { get; set; }
    public long? OrderNo { get; set; }
    public string NotifyData { get; set; }
    public NotifyDataPackage NotifyDataPackage { get; set; }
    public OrderInformation OrderInformation { get; set; }
    public string PayooSignature { get; set; }
    public string ResponseData { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public string Encryption { get; set; }
}

public partial class NotifyDataPackage
{
    public string T { get; set; }
    public string ResponseData { get; set; }
    public string Signature { get; set; }
}

public partial class OrderInformation
{
    public string T { get; set; }
    public long? PaymentMethod { get; set; }
    public string PaymentSource { get; set; }
    public object QrPaymentSystemTrace { get; set; }
    public string PurchaseDate { get; set; }
    public string MerchantUsername { get; set; }
    public long? ShopId { get; set; }
    public long? MasterShopId { get; set; }
    public long? OrderNo { get; set; }
    public string OrderCash { get; set; }
    public string BankName { get; set; }
    public string CardNumber { get; set; }
    public object BillingCode { get; set; }
    public long? CardIssuanceType { get; set; }
    public long? PaymentStatus { get; set; }
    public object TransactionFee { get; set; }
}
