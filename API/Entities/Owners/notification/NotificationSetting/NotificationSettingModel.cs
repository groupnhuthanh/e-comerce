﻿using Entities.Base;

namespace Entities.Owners.notification.NotificationSetting;

public class NotificationSettingModel : BaseModel
{
    public Define Define { get; set; }
    public List<MessageList> MessageList { get; set; }
    public NotificationPath NotificationPath { get; set; }
    public string UserGuid { get; set; }
}

public partial class Define
{
    public string Code { get; set; }
}

public partial class MessageList
{
    public long? Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public long? TypeId { get; set; }
}

public partial class NotificationPath
{
    public string Token { get; set; }
    public string Send { get; set; }
}
