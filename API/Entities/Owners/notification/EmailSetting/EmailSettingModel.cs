﻿using Entities.Base;

namespace Entities.Owners.notification.EmailSetting;

public class EmailSettingModel : BaseModel
{
    public List<SenderList> SenderList { get; set; }
    public long? ProviderId { get; set; }
    public string AccountName { get; set; }
    public string Passcode { get; set; }
    public string SendId { get; set; }
    public string BodySms { get; set; }
    public EmailOtp EmailOtp { get; set; }
    public string CustomerGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }
    public Uri BaseUrlSms { get; set; }
    public string SendUrlSms { get; set; }
    public string ApiEndpoint { get; set; }
    public Uri BaseUrl { get; set; }
    public long? OtpExpiry { get; set; }
    public DateTimeOffset? AdminUpdate { get; set; }
}

public partial class EmailOtp
{
    public string OtpSender { get; set; }
    public string SenderName { get; set; }
    public string Title { get; set; }
    public string Body { get; set; }
}

public partial class SenderList
{
    public long? Id { get; set; }
    public string Email { get; set; }
    public string DisplayName { get; set; }
    public string SenderName { get; set; }
}
