﻿using Entities.Base;

namespace Entities.Owners.APIType;

public class APITypeModel : BaseModel
{
    public string Name { get; set; }
    public long? Type { get; set; }
    public long? Provider { get; set; }
    public string ConfigFile { get; set; }
    public long? Visible { get; set; }
    public string UserGuid { get; set; }
    public DateTimeOffset? CreateDate { get; set; }
    public long? Key { get; set; }
    public string Rev { get; set; }



}
