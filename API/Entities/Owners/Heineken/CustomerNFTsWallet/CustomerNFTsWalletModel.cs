﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Entities.Owners.Heineken.CustomerNFTsWallet;

[BsonIgnoreExtraElements]
public partial class CustomerNFTsWalletModel
{
    [BsonElement("_id")]
    [BsonIgnoreIfNull]
    [JsonProperty("_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public ObjectId _id { get; set; }
    [JsonProperty("NFTsCollectionId", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string NFTsCollectionId { get; set; }

    [JsonProperty("AssetsId", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string AssetsId { get; set; }

    [JsonProperty("TokenId", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public long TokenId { get; set; }

    [JsonProperty("AssetUrl", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string AssetUrl { get; set; }
    [JsonProperty("AssetMediaType", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public int? AssetMediaType { get; set; }

    [JsonProperty("Image", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Image { get; set; }

    [JsonProperty("Icon", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Icon { get; set; }

    [JsonProperty("Name", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Name { get; set; }

    [JsonProperty("MetaData", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public MetaData MetaData { get; set; }

    [JsonProperty("Owner", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Owner { get; set; }
    [JsonProperty("Product_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Product_id { get; set; }
    [JsonProperty("Order_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Order_id { get; set; }
    [JsonProperty("NFTsToken_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public object NFTsToken_id { get; set; }

    [JsonProperty("Visible", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public bool? Visible { get; set; }

    [JsonProperty("UserGuid", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string UserGuid { get; set; }
    [JsonProperty("NFTsWalletTypeId", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public long? NFTsWalletTypeId { get; set; } = 1;
    public bool? IsUsed { get; set; } = false;
    public string CreateDate { get; set; }

    // Update check 
    public string UsedDate { get; set; }

    [JsonProperty("Supplier_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string SupplierId { get; set; }

    [JsonProperty("VoucherCode", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string VoucherCode { get; set; }

    [JsonProperty("Customer_id", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Customer_id { get; set; }

    [JsonProperty("CustomerPhone", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string CustomerPhone { get; set; }

    [JsonProperty("Timestamp", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public long? Timestamp { get; set; }

    [JsonProperty("Date", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Date { get; set; }

    [JsonProperty("StoreID", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string StoreId { get; set; }

    [JsonProperty("PromotionID", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string PromotionId { get; set; }

    [JsonProperty("PromotionName", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string PromotionName { get; set; }
    public string Challenge_id { get; set; }
    public bool IsVerify { get; set; }

}
[BsonIgnoreExtraElements]
public partial class MetaData
{
    [JsonProperty("Attributes", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public List<Attributes> Attributes { get; set; }
}
[BsonIgnoreExtraElements]
public partial class Attributes
{
    [JsonProperty("PropertyType", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public int? PropertyType { get; set; }
    [JsonProperty("PropertyName", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string PropertyName { get; set; }

    [JsonProperty("Value", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Value { get; set; }

    [JsonProperty("Rarity", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public decimal? Rarity { get; set; }
    [JsonProperty("RarityFormat", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string RarityFormat { get { return String.Format("{0:0.####}", (Rarity * 100)) + "%"; } }
    [JsonProperty("IsUsed", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public bool? IsUsed { get; set; } = false;
}
