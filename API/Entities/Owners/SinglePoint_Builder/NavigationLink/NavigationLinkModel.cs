using Entities.Base;

namespace Entities.Owners.SinglePoint_Builder.NavigationLink;

public class NavigationLinkModel : BaseModel
{
    public string Data { get; set; }
    public string Handle { get; set; }
    public string Language { get; set; }
    public int? PageType { get; set; }
    public string Store { get; set; }
    public int? SystemNavigationLinkId { get; set; }
    public string UserGuid { get; set; }
}
public class ShopPageModel
{
    public string Shop { get; set; }
    public string Handle { get; set; }
    public string GroupGuid { get; set; }
    public string Name { get; set; }
}