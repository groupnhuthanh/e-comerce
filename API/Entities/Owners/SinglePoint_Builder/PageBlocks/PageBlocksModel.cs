using Entities.Base;
using MongoDB.Bson.Serialization.Attributes;

namespace Entities.Owners.SinglePoint_Builder.PageBlocks;

[BsonIgnoreExtraElements]
public class PageBlocksModel : BaseModel
{
    public int PageId { get; set; }
    public string Handle { get; set; }
    public string UserGuid { get; set; }
    public List<BlockList> BlockList { get; set; }
}
[BsonIgnoreExtraElements]
public class BlockList
{
    public int BlockId { get; set; }
    public string Name { get; set; }
    public int BlockTypeId { get; set; }
    public bool UseMore { get; set; }
    public List<object> Data { get; set; }
    public bool Visible { get; set; }
    public int ViewMode { get; set; }
    public ModeSetting ModeSetting { get; set; }
    public string DisplayName { get; set; }
}

[BsonIgnoreExtraElements]
public class ModeSetting
{
    public bool UseCustomerName { get; set; }
    public bool UseMembershipRank { get; set; }
    public bool UseWallet { get; set; }
    public bool UseLoyaltyPoints { get; set; }
    public bool UseBrandLogo { get; set; }
    public int? SpacingBetweenSlides { get; set; }
    public int? SlidesPerView { get; set; }
    public int? Effect { get; set; }
    public int? Speed { get; set; }
    public bool? Loop { get; set; }
    public int? TimeLoop { get; set; }
    public bool? Auto { get; set; }
    public int? Delay { get; set; }
    public bool? IsListFriend { get; set; }
    public bool? IsInvite { get; set; }
    public bool? IsBackground { get; set; }
}