﻿using BackgroundQueue.Generic;
using Cores.Services.Antiforgery;
using Cores.Services.ApiVersion;
using Cores.Services.AppSetting;
using Cores.Services.Captcha;
using Cores.Services.CORS;
using Cores.Services.DependenceInjection;
using Cores.Services.Firebase;
using Cores.Services.GRPC;
using Cores.Services.Gzip;
using Cores.Services.HealthCheck;
using Cores.Services.Hsts;
using Cores.Services.IISIntegration;
using Cores.Services.JWT;
using Cores.Services.Language;
using Cores.Services.LazyCache;
using Cores.Services.LazyResolution;
using Cores.Services.Logger;
using Cores.Services.MongoDb;
using Cores.Services.Newtonsoft;
using Cores.Services.RateLimitResponse;
using Cores.Services.ResponseCaching;
using Cores.Services.Scoped;
using Cores.Services.Session;
using Cores.Services.Swagger;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Logging;

namespace Cores.Register;

public static class Configuration
{
    public static void RegisterConfiguration(this IServiceCollection services, IConfiguration Configuration, IWebHostEnvironment env)
    {
        #region config 3.0
        //3.0
        //fix KindValue//Microsoft.AspNetCore.Mvc.NewtonsoftJson
        //services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        IdentityModelEventSource.ShowPII = true;//Hiển thị lỗi JWT            
        services.ConfigFirebase();
        services.ConfigApiVersion();
        services.ConfigSession();
        services.AddHsts();
        services.NewtonsoftJson();
        services.ConfigCORS();
        services.ConfigIISIntegration();
        services.ConfigLogger();

        services.ConfigureMongoDbContext(Configuration);
        services.AuthenticationJwtBearer(Configuration);
        services.AddSwagger(Configuration, env);
        services.AddSwaggerAuthorization(option =>
        {
            option.RedirectUrl = "/Login";
            option.UrlSegment = "/swagger";
        });
        services.AddConfigScoped();
        services.AddDependenceInjection();
        services.ConfigLanguage();
        services.AbpAntiForgeryOptionService();
        services.ConfigGzip();
        services.ConfigGRPC();
        services.ConfigResponseCaching();

        //services.SetupNServiceBus().GetAwaiter().GetResult();
        services.AddConfigHealthCheck();
        services.AddLazyResolution();//Lazily resolving services to fix circular dependencies in .NET Core
        services.ConfigRateLimitResponse(Configuration);//tam tat: 2022.01.21
                                                        //services.AddOcelot();//https://github.com/ThreeMammals/Ocelot            
        #endregion
        #region Connect to appsettings.json
        services.ConfigAppSetting(Configuration);
        services.AddLazyCache();
        services.AddBackgroundResultQueue();
        services.AddHttpContextAccessor();
        services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();
        services.ConfigCaptcha();
        //var serviceProvider = services.BuildServiceProvider();
        //var logger = serviceProvider.GetService<ILogger<Resource>>();
        //services.AddSingleton(typeof(ILogger), logger);
        #endregion
    }
}