﻿using AspNetCoreRateLimit;
using Cores.Services.Antiforgery;
using Cores.Services.AntiXss;
using Cores.Services.CSP;
using Cores.Services.Exception;
using Cores.Services.HealthCheck;
using Cores.Services.HttpOverrides;
using Cores.Services.JWT;
using Cores.Services.Language;
using Cores.Services.Logger;
using Cores.Services.ResponseCaching;
using Cores.Services.Swagger;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Cores.Register;

public static class Application
{
    public static void RegisterApplication(this IApplicationBuilder app, IWebHostEnvironment env, IAntiforgery antiforgery, IApiVersionDescriptionProvider apiVersion, IConfiguration Configuration)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        if (env.IsProduction() || env.IsStaging())
        {
            app.UseExceptionHandler("/Error");
            //app.UseHsts();
            // Becarful, It should be after UseAuthorization & UseAuthentication middlewares.
            app.UseSwaggerAuthorization();
        }

        app.HttpOverride();
        app.AppLanguage();
        app.AppCsp();
        app.ConfigCustomExceptionMiddleware();
        app.UseCors("CorsPolicy");
        app.UseRouting();
        app.AppConfigurationResponseCaching();
        app.ConfigAuthJwtBearer(Configuration);
        //3.0
        app.AddAntiforgery(antiforgery);
        app.UseMiddleware<AntiXssMiddleware>();
        // HERE            
        app.UseCustomSwagger(apiVersion);
        app.UseResponseCompression();
        //3.0 - Required for routes
        app.ConfigExceptionHandle();
        app.UseIpRateLimiting();//tam tat: 2022.01.21
        app.AppLoggerService();
        app.AppHealCheck();
        //app.UseMiddleware<DosAttackMiddleware>();
        app.UseSession();
        app.UseAuthorization();

        //app.UseMiddleware<IpBlockMiddelware>();
        //app.UseMiddleware<IpBlockActionFilter>();
        //app.UseOcelot().Wait();
        //app.UseEndpoints(endpoints =>
        //{
        //    endpoints.MapControllers();
        //});
    }
}