﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using System.IO.Compression;

namespace Cores.Services.Gzip;

public static class GzipService
{
    #region Gzip
    /// <summary>
    /// Configures the gzip.
    /// </summary>
    /// <param name="services">The services.</param>
    public static void ConfigGzip(this IServiceCollection services)
    {
        services.Configure<GzipCompressionProviderOptions>(options =>
        {
            options.Level = CompressionLevel.Fastest;
        });
        services.AddResponseCompression(options =>
        {
            options.EnableForHttps = true;
            options.Providers.Add<GzipCompressionProvider>();
            options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[]
                        {
               "image/svg+xml",
               "application/atom+xml"
                        });
        });
    }
    #endregion
}