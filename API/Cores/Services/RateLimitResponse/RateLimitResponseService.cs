﻿using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.RateLimitResponse;

public static class RateLimitResponseService
{
    #region Rate Limit Request
    public static void ConfigRateLimitResponse(this IServiceCollection services, IConfiguration configuration)
    {
        // needed to load configuration from appsettings.json
        services.AddOptions();

        // needed to store rate limit counters and ip rules
        services.AddMemoryCache();
        //load general configuration from appsettings.json
        services.Configure<IpRateLimitOptions>(configuration.GetSection("IpRateLimiting"));
        services.AddDistributedRateLimiting<AsyncKeyLockProcessingStrategy>();
        // inject counter and rules stores
        services.AddInMemoryRateLimiting();
        services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
        services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
        // https://github.com/aspnet/Hosting/issues/793
        // the IHttpContextAccessor service is not registered by default.
        // the clientId/clientIp resolvers use it.
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

        // configuration (resolvers, counter key builders)
        services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
    }
    #endregion
}