﻿using Cores.Services.Hsts;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.Net;

namespace Cores.Services.Hsts;

public static class HstsService
{
    #region Hsts
    /// <summary>
    ///The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    ///Enforce HTTPS in ASP.NET Core.https://docs.microsoft.com/en-us/aspnet/core/security/enforcing-ssl
    /// </summary>
    /// <param name="services"></param>
    public static void AddHsts(this IServiceCollection services)
    {
        #region Hsts
        services.AddHsts(options =>
        {
            options.Preload = true;
            options.IncludeSubDomains = true;
            options.MaxAge = TimeSpan.FromDays(60);
            //options.ExcludedHosts.Add("example.com");
            //options.ExcludedHosts.Add("www.example.com");
        });

        services.AddHttpsRedirection(options =>
        {
            options.RedirectStatusCode = (int)HttpStatusCode.TemporaryRedirect;
            options.HttpsPort = 5001;
        });
        #endregion
    }
    #endregion
}