﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Cores.Services.HealthCheck;

public static class HealthCheckService
{
    /// <summary>
    /// Adds the configure health check.
    /// </summary>
    /// <param name="services">The services.</param>
    public static void AddConfigHealthCheck(this IServiceCollection services)
    {
        services.AddHealthChecks().AddCheck("Diadiem",
            () =>
            {
                return HealthCheckResult.Degraded("The check of the service did not work well");
            }
        ).AddCheck("Database",
        () => HealthCheckResult.Healthy("The check of the database worked."));
        //services.AddHealthChecksUI().AddInMemoryStorage();
    }
}