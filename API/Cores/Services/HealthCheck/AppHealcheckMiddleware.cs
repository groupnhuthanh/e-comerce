﻿using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;

namespace Cores.Services.HealthCheck;

internal static class AppHealcheckMiddleware
{
    public static void AppHealCheck(this IApplicationBuilder app)
    {
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapHealthChecks("/quickhealth", new HealthCheckOptions()
            {
                Predicate = _ => false
            });
            endpoints.MapHealthChecks("/health/service", new HealthCheckOptions()
            {
                Predicate = reg => reg.Tags.Contains("service"),
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });
            endpoints.MapHealthChecks("/health", new HealthCheckOptions()
            {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });
            //endpoints.MapHealthChecksUI();
            endpoints.MapControllers();
        });
    }
}