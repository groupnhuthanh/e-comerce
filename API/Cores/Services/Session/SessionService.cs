﻿using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.Session;

public static class SessionService
{
    #region Session
    /// <summary>
    /// Configures the session.
    /// </summary>
    /// <param name="services">The services.</param>
    public static void ConfigSession(this IServiceCollection services)
    {
        services.AddSession(options =>
        {
            options.IdleTimeout = TimeSpan.FromMinutes(60);
        });
    }
    #endregion
}