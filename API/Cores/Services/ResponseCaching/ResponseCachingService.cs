﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.ResponseCaching;

public static class ResponseCachingService
{
    #region Response Caching
    //Phải khai báo sau Cors
    /// <summary>
    /// Configurations the response caching.
    /// </summary>
    /// <param name="services">The services.</param>
    public static void ConfigResponseCaching(this IServiceCollection services)
    {
        services.AddResponseCaching(options =>
        {
            options.MaximumBodySize = 1024;
            options.UseCaseSensitivePaths = true;
        });
    }
    /// <summary>
    /// Apps the configuration response caching.
    /// </summary>
    /// <param name="app">The app.</param>
    public static void AppConfigurationResponseCaching(this IApplicationBuilder app)
    {
        app.UseResponseCaching();
        app.Use(async (context, next) =>
        {
            // Microsoft.AspNetCore.Http;
            context.Response.GetTypedHeaders().CacheControl =
                new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
                {
                    Public = true,
                    MaxAge = TimeSpan.FromSeconds(10),
                    NoCache = true,
                    NoStore = true
                };
            context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
                new string[] { "Accept-Encoding" };

            await next.Invoke();
        });
    }
    #endregion
}