﻿using LazyCache;
using LazyCache.Providers;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Cores.Services.LazyCache;


public static class LazyCacheService
{
    public static IServiceCollection AddLazyCache(this IServiceCollection services)
    {
        if (services == null)
        {
            throw new ArgumentNullException("services");
        }

        services.AddOptions();
        services.TryAdd(ServiceDescriptor.Singleton<IMemoryCache, MemoryCache>());
        services.TryAdd(ServiceDescriptor.Singleton<ICacheProvider, MemoryCacheProvider>());
        //services.TryAdd(ServiceDescriptor.Singleton<IAppCache, CachingService>((IServiceProvider serviceProvider) => new CachingService(new Lazy<ICacheProvider>(new Func<ICacheProvider>(serviceProvider.GetRequiredService)))));
        return services;
    }

    public static IServiceCollection AddLazyCache(this IServiceCollection services, Func<IServiceProvider, CachingService> implementationFactory)
    {
        if (services == null)
        {
            throw new ArgumentNullException("services");
        }

        if (implementationFactory == null)
        {
            throw new ArgumentNullException("implementationFactory");
        }

        services.AddOptions();
        services.TryAdd(ServiceDescriptor.Singleton<IMemoryCache, MemoryCache>());
        services.TryAdd(ServiceDescriptor.Singleton<ICacheProvider, MemoryCacheProvider>());
        services.TryAdd(ServiceDescriptor.Singleton((Func<IServiceProvider, IAppCache>)implementationFactory));
        return services;
    }
}