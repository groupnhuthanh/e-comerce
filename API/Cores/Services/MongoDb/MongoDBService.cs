﻿using Entities.ExtendModels.Globals.Path;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Cores.Services.MongoDb;

internal static class MongoDBService
{
    /// <summary>
    /// Gets the environment variable.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns>A string.</returns>
    static string environmentName = GOEnvironment.TWO_Mongo;
    public static string GetEnvironmentVariable(string name, string defaultValue)
                => System.Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process) ?? defaultValue;
    #region ArangoDB | MongoDB
    /// <summary>
    /// Configure ArangoDB Context
    /// </summary>
    /// <param name="services"></param>
    /// <param name="config"></param>
    public static void ConfigureMongoDbContext(this IServiceCollection services, IConfiguration config)
    {
        #region Database connection
        // var environmentName = GetEnvironmentVariable("ARANGO_SERVER", config.GetConnectionString("Arango"));
        // services.AddArango(environmentName);
        //services.Configure<DiaDiemDatabaseSettings>(config.GetSection("ConnectionStrings"));

        //services.Configure<MongoDbSettings>(opt =>
        //{
        //    opt.DiaDiem = config.GetSection("ConnectionStrings:DiaDiem").Value;
        //    //opt.DatabaseName = config.GetSection("ConnectionStrings:DatabaseName").Value;
        //});
        //services.AddSingleton<IMongoContext, MongoDbContext>();            
        //environmentName = string.IsNullOrEmpty(environmentName) ? environmentName : "MONGO_SERVER";
        environmentName = GetEnvironmentVariable(environmentName, config.GetConnectionString("Server_MongoDb"));
        services.AddSingleton<IMongoClient>(sp =>
        {
            return new MongoClient(environmentName);
        });
        #endregion
    }
    #endregion
}