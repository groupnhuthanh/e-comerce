﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.IISIntegration;

public static class IISIntegrationService
{
    #region ConfigureIIS
    //IIS integration which will help us with the IIS deployment
    /// <summary>
    /// Configure IIS Intergration
    /// </summary>
    /// <param name="services"></param>
    // ReSharper disable once InconsistentNaming
    public static void ConfigIISIntegration(this IServiceCollection services)
    {
        services.Configure<IISOptions>(_ =>
        {
        });
    }
    #endregion
}