﻿using BusinessLogicLayer.BusinessLogic.Owners.Request;
using BusinessLogicLayer.BusinessLogic.Wrapper;
using BusinessLogicLayer.ILogicCommon.IManagerCommon.Wrapper;
using BusinessLogicLayer.ILogicCommon.IPipeLogic.Wrapper;
using BusinessLogicLayer.IRepository.IMongoQueryLanguage;
using BusinessLogicLayer.IRepository.Wrapper;
using DataAccessLayer.DataAccess.Owners.Request;
using DataAccessLayer.DataAccess.Wrapper;
using DataAccessLayer.LogicCommon.MangerCommon.Wrapper;
using DataAccessLayer.LogicCommon.PipeLogic.Wrapper;
using DataAccessLayer.Repository.MongoQueryLanguage;
using DataAccessLayer.Repository.Wrapper;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.DependenceInjection;

internal static class DIService
{
    #region Dependence Injection Service
    /// <summary>
    /// Configure Loggers Service
    /// </summary>
    /// <param name="services"></param>
    public static void AddDependenceInjection(this IServiceCollection services)
    {
        //1: Singleton: Chỉ chạy cho những phương thức bên trong class
        //Không chạy biến global trong class, không constructor

        //2.Transient: Cho phép khởi tạo class mỗi khi gọi (global, constructor)
        //3.Scoped: Chỉ giới hạn trong 1 phương thức

        services.AddTransient<BusinessLogicWrapper, DataAccessWrapper>();
        services.AddTransient<IPipeLogicWrapper, PipeLogicWrapper>();
        services.AddTransient<IManagerCommonWrapper, ManagerCommonWrapper>();
        services.AddSingleton<IRepositoryWrapper, RepositoryWrapper>();
        services.AddSingleton<IQuery, Query>();
        services.AddTransient<IRestfullRequest, RestfullRequest>();
        //services.AddTransient<CaptchaCoreSetting>();


    }
    #endregion
}