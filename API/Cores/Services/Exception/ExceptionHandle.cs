﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace Cores.Services.Exception;

internal static class ExceptionHandle
{
    #region Custom Exception
    /// <summary>
    /// Configure Custom Exception Middleware
    /// </summary>
    /// <param name="app"></param>
    public static void ConfigCustomExceptionMiddleware(this IApplicationBuilder app)
    {
        app.UseMiddleware<ExceptionMiddleware>();
    }
    /// <summary>
    /// Configures the exception handle.
    /// </summary>
    /// <param name="app">The app.</param>
    public static void ConfigExceptionHandle(this IApplicationBuilder app)
    {
        app.UseExceptionHandler(c => c.Run(async context =>
        {
            var exception = context.Features
                .Get<IExceptionHandlerPathFeature>()
                .Error;
            var response = new { error = exception.Message };
            await context.Response.WriteAsJsonAsync(response);
        }));
    }
    #endregion
}