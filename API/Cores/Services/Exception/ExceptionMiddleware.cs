﻿using BusinessLogicLayer.ILogicCommon.IManagerCommon.Owners.LoggerManager;
using Entities.ExtendModels.Globals.Error;
using Microsoft.AspNetCore.Http;
using System.Net;

namespace Cores.Services.Exception;

/// <summary>
/// Exception Middle
/// </summary>
public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILoggerManager _logger;

    /// <summary>
    /// Exception construction
    /// </summary>
    /// <param name="next"></param>
    /// <param name="logger"></param>
    public ExceptionMiddleware(RequestDelegate next, ILoggerManager logger)
    {
        _logger = logger;
        _next = next;
    }
    /// <summary>
    /// Invoke Async
    /// </summary>
    /// <param name="httpContext"></param>
    /// <returns></returns>
    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            //1                
            await _next(httpContext)
                    //.ConfigureAwait(false)
                    ;
        }
        catch (System.Exception ex)
        {
            _logger.LogError($"Something went wrong: {ex}");
            await HandleExceptionAsync(httpContext, ex.Message + " - " + ex.StackTrace).ConfigureAwait(false);
        }
    }
    /// <summary>
    /// Handles the exception async.
    /// </summary>
    /// <param name="context">The context.</param>
    /// <param name="message">The message.</param>
    /// <returns>A Task.</returns>
    private static Task HandleExceptionAsync(HttpContext context, string message)
    {
        try
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                //Message = "Internal Server Error from the custom middleware."
                Message = "<p>" + message + "</p>"
            }.ToString());
        }
        catch (System.Exception)
        {
            return Task.CompletedTask;
        }

    }
}