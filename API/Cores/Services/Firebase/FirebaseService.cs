﻿using Entities.ExtendModels.Globals.Path;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.Firebase;

public static class FirebaseService
{
    readonly static string environmentName = GOEnvironment.TWO_Firebase;
    //private static string folderPath = System.Environment.CurrentDirectory+ "/data/firebase/highlands/two-pos-hc-79bf94e44c24.json";
    private static string folderPath = Environment.CurrentDirectory + "/";
    public static string GetEnvironmentVariable(string name, string defaultValue)
               => Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process) ?? defaultValue;

    public static void ConfigFirebase(this IServiceCollection services)
    {
        var pathFile = GetEnvironmentVariable(environmentName, folderPath);
        if (File.Exists(pathFile))
        {
            var googleAccount = Path.Combine(AppContext.BaseDirectory, pathFile);
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", googleAccount);
            Environment.SetEnvironmentVariable("GRPC_DNS_RESOLVER", "native");

            FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromFile(googleAccount),
            });
        }
        else
        {
            Console.WriteLine("PathFile FB fail");
        }
    }
}