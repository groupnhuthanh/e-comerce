﻿using Cores.SupportCustom.ModelValidationAttribute;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.Scoped;

public static class ScopedService
{
    #region Scoped
    /// <summary>
    /// Adds the scoped.
    /// </summary>
    /// <param name="services">The services.</param>
    public static void AddConfigScoped(this IServiceCollection services)
    {
        services.AddScoped<ModelValidationAttribute>();

    }
    #endregion
}