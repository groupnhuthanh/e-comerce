﻿using BusinessLogicLayer.ILogicCommon.IManagerCommon.Owners.LoggerManager;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using System.Globalization;

namespace Cores.Services.ResponseTime;

/// <summary>
/// tries to measure request processing time
/// </summary>
public class ResponseTimeMiddleware
{
    // Name of the Response Header, Custom Headers starts with "X-"  
    /// <summary>
    /// The response header response time.
    /// </summary>
    private const string ResponseHeaderResponseTime = "X-Response-Time-ms";

    // Handle to the next Middleware in the pipeline  
    private readonly RequestDelegate _next;

    ///<inheritdoc/>
    public ResponseTimeMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    ///<inheritdoc/>
    public Task InvokeAsync(HttpContext context)
    {
        // skipping measurement of non-actual work like OPTIONS
        if (context.Request.Method == "OPTIONS")
            return _next(context);
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB", false);
        // Start the Timer using Stopwatch  
        var watch = new Stopwatch();
        watch.Start();
        try
        {
            if (!context.Response.HasStarted)
            {
                context.Response.OnStarting(() =>
                {
                    try
                    {
                        // Stop the timer information and calculate the time   
                        watch.Stop();
                        var responseTimeForCompleteRequest = watch.ElapsedMilliseconds;
                        // Add the Response time information in the Response headers.   
                        context.Response.Headers[ResponseHeaderResponseTime] = responseTimeForCompleteRequest.ToString();

                        var logger = context.RequestServices.GetService<ILoggerManager>();
                        string fullUrl = $"{context.Request.Scheme}://{context.Request.Host}{context.Request.Path}{context.Request.QueryString}";
                        var log = $"[Performance] Request to {fullUrl} took {responseTimeForCompleteRequest} ms";
                        logger?.LogDebug(log);
                        Console.WriteLine(log);
                        return Task.CompletedTask;
                    }
                    catch (System.Exception e)
                    {
                        context.Response.WriteAsync(e.StackTrace);
                        return Task.CompletedTask;
                    }
                });
            }
        }
        catch (System.Exception e)
        {
            return context.Response.WriteAsync(e.StackTrace);
        }
        // Call the next delegate/middleware in the pipeline   
        return _next(context);
    }
}