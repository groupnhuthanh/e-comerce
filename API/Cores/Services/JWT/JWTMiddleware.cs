﻿using Entities.ExtendModels.System.Config;
using Helpers.Convert;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;

namespace Cores.Services.JWT;

public class JWTMiddleware
{
    /// <summary>
    /// The jwt middleware.
    /// </summary>
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        readonly IOptions<TokenManagement> _tokenManager;
        private string errorCode = "\"0\"";
        /// <summary>
        /// Initializes a new instance of the <see cref="JwtMiddleware"/> class.
        /// </summary>
        /// <param name="next">The next.</param>
        /// <param name="tokenManagement">The token management.</param>
        /// <param name="repository"></param>
        public JwtMiddleware(RequestDelegate next, IOptions<TokenManagement> tokenManagement)
        {
            _tokenManager = tokenManagement;
            _next = next;
        }

        /// <summary>
        /// Invokes the.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>A Task.</returns>
        public async Task Invoke(HttpContext context)
        {
            //Get the upload token, which can be customized and extended

            var segment = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").First();
            if (!string.IsNullOrEmpty(segment) && !segment.ToLower().Equals("basic"))
            {
                var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last()
                            ?? context.Request.Headers["X-Token"].FirstOrDefault()
                            ?? context.Request.Query["Token"].FirstOrDefault()
                            ?? context.Request.Cookies["Token"];

                if (token != null)
                    AttachUserToContext(context, token);//Khong su dung dc async, bao loi           
            }
            await _next(context);
        }

        /// <summary>
        /// Attaches the user to context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="token">The token.</param>
        private void AttachUserToContext(HttpContext context, string token)
        {
            try
            {
                var tokenValue = _tokenManager?.Value;
                if (tokenValue != null)
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    // var key = Convert.FromBase64String(tokenValue.Secret);
                    //var key =Encoding.ASCII.GetBytes(tokenValue.Secret);
                    var key = Convert.FromBase64String(tokenValue.Secret);
                    // var key =Encoding.ASCII.GetBytes(tokenValue.Secret);                    
                    tokenHandler.ValidateToken(token, new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = tokenValue.Issuer,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        // IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(token.Secret)),
                        ValidAudience = tokenValue.Audience,
                        ValidateAudience = true,
                        ValidateLifetime = false,//here we are saying that we don't care about the token's expiration date
                        RequireExpirationTime = false,
                        LifetimeValidator = CustomLifetimeValidator,
                        ClockSkew = TimeSpan.Zero
                    }, out SecurityToken validatedToken);

                    var jwtToken = (JwtSecurityToken)validatedToken;
                    var claims = jwtToken.Claims;
                    var user = jwtToken.Payload;

                    //Write authentication information to facilitate the use of business classes


                    ////Authentication                    
                    var header = context.Request.Headers;
                    var xAuthCustomerGuest_id = header["X-AUTH-CustomerGuest_id"].FirstOrDefault() ?? "";
                    var xAuthDeviceCode = header["X-AUTH-DeviceCode"].FirstOrDefault() ?? "";
                    var xAuthTimestamp = header["X-AUTH-Timestamp"].FirstOrDefault() ?? "";
                    var xAuthNonce = header["X-AUTH-Nonce"].FirstOrDefault() ?? "";
                    var xAuthChecksum = header["X-AUTH-Checksum"].FirstOrDefault() ?? "";
                    var xAuthSignature = header["X-AUTH-Signature"].FirstOrDefault() ?? "";
                    var xAuthSource = header["Postman-Token"].FirstOrDefault() ?? "";
                    user["X-AUTH-CustomerGuest_id"] = xAuthCustomerGuest_id;
                    user["X-AUTH-DeviceCode"] = xAuthDeviceCode;
                    user["X-AUTH-Timestamp"] = xAuthTimestamp;
                    user["X-AUTH-Nonce"] = xAuthNonce;
                    user["X-AUTH-Checksum"] = xAuthChecksum;
                    user["X-AUTH-Signature"] = xAuthSignature;

                    var claimsIdentity = new ClaimsIdentity(claims);
                    claimsIdentity.AddClaim(new Claim("X_AUTH_CustomerGuest_id", xAuthCustomerGuest_id));
                    claimsIdentity.AddClaim(new Claim("X_AUTH_DeviceCode", xAuthDeviceCode));
                    claimsIdentity.AddClaim(new Claim("X_AUTH_Timestamp", xAuthTimestamp));
                    claimsIdentity.AddClaim(new Claim("X_AUTH_Nonce", xAuthNonce));
                    claimsIdentity.AddClaim(new Claim("X_AUTH_Checksum", xAuthChecksum));
                    claimsIdentity.AddClaim(new Claim("X_AUTH_Signature", xAuthSignature));
                    claimsIdentity.AddClaim(new Claim("X_AUTH_Postman", xAuthSource));
                    Thread.CurrentPrincipal = new ClaimsPrincipal(claimsIdentity);
                    // attach user to context on successful jwt validation                   
                    context.Items["User"] = user;
                }
            }
            catch (System.Exception e)
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
                var dicToken = libs_Tokens.ValidateToken(token);
                var customerGuid = "";
                var token_id = "";
                if (dicToken != null && dicToken.Any())
                {
                    customerGuid = dicToken.ContainsKey("CustomerGuid") ? !string.IsNullOrEmpty(dicToken["CustomerGuid"]?.ToString()) ? dicToken["CustomerGuid"]?.ToString() ?? "" : "" : "";
                    token_id = dicToken.ContainsKey("Token_id") ? !string.IsNullOrEmpty(dicToken["Token_id"]?.ToString()) ? dicToken["Token_id"]?.ToString() ?? "" : "" : "";
                }
                errorCode = "\"1110\"";
                var message = e.Message;
                if (message.Contains("IDX10230"))
                {
                    if (!string.IsNullOrEmpty(customerGuid) || !string.IsNullOrEmpty(token_id))
                    {
                        errorCode = "\"1111\"";
                    }
                    else
                    {
                        errorCode = "\"1119\"";
                    }
                    //errorCode = "\"1111\"";
                }
                else if (message.Contains("IDX10503"))
                {
                    errorCode = "\"1110\"";
                }
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.Response.WriteAsync("{\"errorCode\":" + errorCode + ",\"message\":\"Unauthorized\"}");
            }
        }
        private bool CustomLifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken tokenToValidate, TokenValidationParameters @param)
        {
            var flag = false;
            if (expires != null)
            {
                var dateNow = DateTime.UtcNow;
                flag = expires > dateNow;
                errorCode = flag ? "\"0\"" : "\"1111\"";
                return flag;
            }
            return flag;
        }
    }
}