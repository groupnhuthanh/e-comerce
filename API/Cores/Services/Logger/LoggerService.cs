﻿using BusinessLogicLayer.ILogicCommon.IManagerCommon.Owners.LoggerManager;
using Cores.Services.ResponseTime;
using DataAccessLayer.LogicCommon.MangerCommon.Owners.LoggerManager;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.Logger;

public static class LoggerService
{
    #region Logger Service
    /// <summary>
    /// Configure Loggers Service
    /// </summary>
    /// <param name="services"></param>
    public static void ConfigLogger(this IServiceCollection services)
    {
        services.AddSingleton<ILoggerManager, LoggerManager>();
        //services.AddSingleton<IImagesManager, ImagesManager>();
    }
    /// <summary>
    /// Apps the logger service.
    /// </summary>
    /// <param name = "app" > The app.</param>
    public static void AppLoggerService(this IApplicationBuilder app)
    {
        app.UseMiddleware<ResponseTimeMiddleware>();
    }
    #endregion
}