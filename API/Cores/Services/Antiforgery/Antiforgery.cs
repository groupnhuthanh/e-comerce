﻿using Cores.Services.Antiforgery;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.Antiforgery;

public static class Antiforgery
{
    #region Forgery Option Service
    /// <summary>
    /// Abps the anti forgery option service.
    /// </summary>
    /// <param name="services">The services.</param>
    public static void AbpAntiForgeryOptionService(this IServiceCollection services)
    {
        //https://docs.microsoft.com/en-us/aspnet/core/security/anti-request-forgery?view=aspnetcore-6.0
        services.AddAntiforgery(options =>
        {
            // Set Cookie properties using CookieBuilder properties†.
            options.FormFieldName = "Antiforgery";
            options.HeaderName = "XSRF-TOKEN";
            options.SuppressXFrameOptionsHeader = false;
        });
        services.AddControllersWithViews(options =>
            options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute()));
    }

    /// <summary>
    /// Adds the antiforgery.
    /// </summary>
    /// <param name="app">The app.</param>
    /// <param name="antiforgery">The antiforgery.</param>
    public static void AddAntiforgery(this IApplicationBuilder app, IAntiforgery antiforgery)
    {
        app.Use(next => context =>
        {
            string path = context.Request.Path.Value;

            if (
                string.Equals(path, "/", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(path, "/index.html", StringComparison.OrdinalIgnoreCase))
            {
                // The request token can be sent as a JavaScript-readable cookie, 
                // and Angular uses it by default.
                var tokens = antiforgery.GetAndStoreTokens(context);
                context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
                    new CookieOptions() { HttpOnly = false });
            }
            return next(context);
        });
    }
    #endregion
}