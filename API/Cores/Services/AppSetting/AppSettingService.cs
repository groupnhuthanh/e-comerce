﻿using Entities.ExtendModels.System.Config;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.AppSetting;

public static class AppSettingService
{
    #region App Setting
    /// <summary>
    /// Configures the app setting.
    /// </summary>
    /// <param name="services">The services.</param>
    /// <param name="config">The config.</param>
    public static void ConfigAppSetting(this IServiceCollection services, IConfiguration config)
    {
        #region Database connection
        var appIdentitySettings = config.GetSection("AppIdentitySettings");
        var appDBSettings = config.GetSection("AppDBSettings");
        var tokenManagement = config.GetSection("TokenManagement");
        var appsAuthenticate = config.GetSection("AppsAuthenticate");
        var domainSettings = config.GetSection("DomainSettings");

        services.Configure<AppIdentitySettings>(appIdentitySettings);
        services.Configure<AppDBSettings>(appDBSettings);
        services.Configure<TokenManagement>(tokenManagement);
        services.Configure<AppsAuthenticate>(appsAuthenticate);
        services.Configure<AppDomainSettings>(domainSettings);
        #endregion
    }
    #endregion
}