﻿using GST.Library.Middleware.HttpOverrides;
using GST.Library.Middleware.HttpOverrides.Builder;
using Microsoft.AspNetCore.Builder;

namespace Cores.Services.HttpOverrides;

internal static class HttpOverridesService
{
    public static void HttpOverride(this IApplicationBuilder app)
    {
        app.UseGSTForwardedHeaders(new GST.Library.Middleware.HttpOverrides.Builder.ForwardedHeadersOptions
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XRealIp,
            ForceXForxardedOrRealIp = true,
        });
    }
}