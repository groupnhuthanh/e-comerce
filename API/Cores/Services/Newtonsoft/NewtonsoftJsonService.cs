﻿

using Cores.Services.BadRequest;
using Cores.SupportCustom.RawRequestBodyFormatter;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System.Text.Json.Serialization;

namespace Cores.Services.Newtonsoft;


internal static class NewtonsoftService
{
    #region Newtonsoft Json
    /// <summary>
    /// Newtonsofts the json.
    /// </summary>
    /// <param name="services">The services.</param>
    public static void NewtonsoftJson(this IServiceCollection services)
    {
        #region Cors Policy
#pragma warning disable ASP5001 // Type or member is obsolete
        _ = services.AddMvc(options =>
        {
            options.AllowEmptyInputInBodyModelBinding = true;
            //options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            //foreach (var formatter in options.InputFormatters)
            //{
            //    if (formatter.GetType() == typeof(SystemTextJsonInputFormatter))
            //        ((SystemTextJsonInputFormatter)formatter).SupportedMediaTypes.Add(
            //        Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse("text/plain"));
            //}
            options.InputFormatters.Insert(0, new RawRequestBodyFormatterService());
        })
            .SetCompatibilityVersion(version: CompatibilityVersion.Latest)
            //Microsoft.AspNetCore.Mvc.NewtonsoftJson
            .AddNewtonsoftJson(opt =>
            {
                opt.SerializerSettings.ContractResolver =
                    new DefaultContractResolver();//Fixed Swagger hiển thị kiểu CamelCase (convert CamelCase-->PascalCase)                
            }).ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problems = new CustomBadRequest(context);

                    return new BadRequestObjectResult(problems);
                };
            });
#pragma warning restore ASP5001 // Type or member is obsolete
        _ = services.AddControllers().AddJsonOptions(options =>
        {
            options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;//Bỏ qua giá trị null trả về
                                                                                                       //options.JsonSerializerOptions.IgnoreNullValues = true;
            options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            //options.JsonSerializerOptions.IgnoreNullValues = true;
            options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
            // Use the default property (Pascal) casing
            options.JsonSerializerOptions.PropertyNamingPolicy = null;
        });
        #endregion
    }
    #endregion
}