﻿using CaptchaCore;
using CaptchaCore.Settings;
using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.Captcha;

public static class CapchaService
{
    public static void ConfigCaptcha(this IServiceCollection services)
    {
        //https://github.com/EdiWang/Edi.Captcha.AspNetCore
        //services.AddSession(options =>
        //{
        //    options.IdleTimeout = TimeSpan.FromMinutes(20);
        //    options.Cookie.HttpOnly = true;
        //});

        //services.AddSessionBasedCaptcha();
        //services.AddSession(options =>
        //{
        //    //options.IdleTimeout = TimeSpan.FromMinutes(20);
        //    //options.Cookie.HttpOnly = true;
        //});

        //services.AddSessionBasedCaptcha(option =>
        //{
        //    option.Letters = "2346789ABCDEFGHJKLMNPRTUVWXYZ";
        //    option.SessionName = "CaptchaCode";
        //    option.CodeLength = 4;
        //});
        //https://github.com/ilhesam/CaptchaCore
        services.AddCaptchaCore(new CaptchaSettings
        {
            Issuer = "CaptchaCode",
            CryptoKey = "J1f66U54BWikTPIIx3NOsqcHfP31QEkgFgybVQFgfH0QN+JlXX72eVN5ngb+MmNWHErCEi3yRQTvq9n5sca33Q==",
            ExpirationInSeconds = 30,
            PermittedLetters = "2346789ABCDEFGHJKLMNPRTUVWXYZabcdefghjklmnprtuvwxyz",
            CodeLength = 6,
        });
        //services.AddCaptchaCore();
        //services.AddControllers();
    }
}