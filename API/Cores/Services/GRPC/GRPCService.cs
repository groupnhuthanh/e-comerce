﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;
using ZNetCS.AspNetCore.Compression;
using ZNetCS.AspNetCore.Compression.Compressors;

namespace Cores.Services.GRPC;

public static class GRPCService
{
    #region GRPC
    /// <summary>
    /// Configures the g r p c.
    /// </summary>
    /// <param name="services">The services.</param>
    public static void ConfigGRPC(this IServiceCollection services)
    {
        services.AddCompression(
                                 options =>
                                 {
                                     options.AllowedMediaTypes = new List<MediaTypeHeaderValue>
                                     {
                            MediaTypeHeaderValue.Parse("text/*"),
                            MediaTypeHeaderValue.Parse("message/*"),
                            MediaTypeHeaderValue.Parse("application/x-javascript"),
                            MediaTypeHeaderValue.Parse("application/javascript"),
                            MediaTypeHeaderValue.Parse("application/json"),
                            MediaTypeHeaderValue.Parse("application/xml"),
                            MediaTypeHeaderValue.Parse("application/atom+xml"),
                            MediaTypeHeaderValue.Parse("application/xaml+xml")
                                     };
                                     options.IgnoredPaths = new List<string>
                                     {
                            "/css/",
                            "/images/",
                            "/js/",
                            "/lib/"
                                     };
                                     options.MinimumCompressionThreshold = 860;
                                     options.Compressors = new List<ICompressor> { new BrotliCompressor(), new GZipCompressor(), new DeflateCompressor() };
                                     options.Decompressors = new List<IDecompressor> { new BrotliDecompressor(), new GZipDecompressor(), new DeflateDecompressor() };
                                 });
    }
    #endregion
}