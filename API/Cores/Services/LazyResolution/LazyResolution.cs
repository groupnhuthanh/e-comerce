﻿using Microsoft.Extensions.DependencyInjection;

namespace Cores.Services.LazyResolution;

public static class LazyResolutionService
{
    #region Lazy
    public static IServiceCollection AddLazyResolution(this IServiceCollection services)
    {
        return services.AddTransient(
            typeof(Lazy<>),
            typeof(LazilyResolved<>));
    }
    private class LazilyResolved<T> : Lazy<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LazilyResolved"/> class.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        public LazilyResolved(IServiceProvider serviceProvider)
            : base(serviceProvider.GetRequiredService<T>)
        {
        }
    }
    #endregion
}